import unittest

from learning_to_style_check.codestyle import get_random_codestyle
from learning_to_style_check.codestyle.codestyle import mutate_codestyle


class TestMutateCodestyle(unittest.TestCase):
    def test_mutate_no_rule(self):
        codestyle = get_random_codestyle()
        mutated_codestyle = mutate_codestyle(codestyle, 0)
        self.assertEqual(codestyle, mutated_codestyle)

    def test_mutate_single_rule(self):
        codestyle = get_random_codestyle()
        mutated_codestyle = mutate_codestyle(codestyle, 1)
        self.assertNotEqual(codestyle, mutated_codestyle)


if __name__ == "__main__":
    unittest.main()
