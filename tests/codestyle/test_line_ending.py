import unittest

from learning_to_style_check.codestyle import CodestyleConfig, apply_codestyle
from learning_to_style_check.libcst import parse_module


class TestLineEnding(unittest.TestCase):
    def test_replace_unix_with_windows_newlines(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1, blank_lines_bottom=1, line_ending="\r\n"
        )
        code = "\ndef test():\n    pass\n"
        styled_code = "\r\ndef test():\r\n    pass\r\n"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_replace_unix_with_mac_newlines(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1, blank_lines_bottom=1, line_ending="\r"
        )
        code = "\ndef test():\n    pass\n"
        styled_code = "\rdef test():\r    pass\r"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_replace_windows_with_unix_newlines(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1, blank_lines_bottom=1, line_ending="\n"
        )
        code = "\r\ndef test():\r\n    pass\r\n"
        styled_code = "\ndef test():\n    pass\n"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_replace_windows_with_mac_newlines(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1, blank_lines_bottom=1, line_ending="\r"
        )
        code = "\r\ndef test():\r\n    pass\r\n"
        styled_code = "\rdef test():\r    pass\r"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_replace_mac_with_unix_newlines(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1, blank_lines_bottom=1, line_ending="\n"
        )
        code = "\rdef test():\r    pass\r"
        styled_code = "\ndef test():\n    pass\n"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_replace_mac_with_windows_newlines(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1, blank_lines_bottom=1, line_ending="\r\n"
        )
        code = "\rdef test():\r    pass\r"
        styled_code = "\r\ndef test():\r\n    pass\r\n"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_line_ending_replacement_in_string(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1, blank_lines_bottom=1, line_ending="\n"
        )
        code = '\ndef test():\n    print("""test\nstring""")\n'
        styled_code = '\ndef test():\n    print("""test\nstring""")\n'
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1, blank_lines_bottom=1, line_ending="\r\n"
        )
        code = '\ndef test():\n    print("""test\nstring""")\n'
        styled_code = '\r\ndef test():\r\n    print("""test\nstring""")\r\n'
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1, blank_lines_bottom=1, line_ending="\r"
        )
        code = '\ndef test():\n    print("""test\nstring""")\n'
        styled_code = '\rdef test():\r    print("""test\nstring""")\r'
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)


if __name__ == "__main__":
    unittest.main()
