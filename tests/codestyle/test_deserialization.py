import json
import unittest

from learning_to_style_check.codestyle import CodestyleConfig, FunctionStyleConfig, \
    NameStyleConfig, DocStringStyleConfig, StringStyleConfig, NumberStyle, NumberStyleConfig, TypingStyleConfig
from learning_to_style_check.codestyle.codestyle import StringPrefixStyle, VarStyleConfig, ArgsStyleConfig, \
    ClassStyleConfig, CodestyleConfigJSONDecoder, CodestyleConfigJSONEncoder
from learning_to_style_check.libcst import NameStyle, NameStyleSuffix, NameStylePrefix


class TestDeserializationCodestyleConfig(unittest.TestCase):

    def test_deserialization(self):
        codestyle = CodestyleConfig(function_style_config=FunctionStyleConfig(name_style_config=NameStyleConfig(
            name_style=NameStyle.MagicName, whitespace_before='   ', whitespace_after='\t\t',
            keep_names=('self', 'cls')), args_style_config=ArgsStyleConfig(name_style_config=NameStyleConfig(
            name_style=NameStyle.MagicName, whitespace_before='     ', whitespace_after='', keep_names=('self', 'cls')),
            typing_style_config=None, whitespace_before_args='\t',
            whitespace_after_args='\t\t\t\t\t\t\t'), var_style_config=VarStyleConfig(
            name_style_config=NameStyleConfig(name_style=(
                NameStyle.Lowercase, NameStyleSuffix.SingleTrailingUnderscore),
                whitespace_before='\t\t\t\t', whitespace_after='\t\t\t', keep_names=('self', 'cls')),
            typing_style_config=TypingStyleConfig(whitespace_before_indicator='\t', whitespace_after_indicator=' '),
            string_style_config=StringStyleConfig(quotes='"', f_string_quotes=('"""', '"'),
                                                  f_prefix=StringPrefixStyle.UP, b_prefix=StringPrefixStyle.LOW,
                                                  r_prefix=StringPrefixStyle.LOW, u_prefix=StringPrefixStyle.UP),
            number_style_config=NumberStyleConfig(
                int_underscore_every=2, float_underscore_every=4,
                exp_style=NumberStyle.UP, exp_underscore_every=0, hex_style=NumberStyle.LOW, hex_underscore_every=2,
                hex_letter_case=NumberStyle.LOW, oct_style=NumberStyle.LOW, oct_underscore_every=0,
                bin_style=NumberStyle.LOW, bin_underscore_every=0, imag_style=NumberStyle.UP, imag_underscore_every=1),
            whitespace_before_equal='\t\t', whitespace_after_equal='\t\t\t\t\t\t'),
            typing_style_config=TypingStyleConfig(
                whitespace_before_indicator='\t\t\t\t',
                whitespace_after_indicator='   '), docstring_style_config=None, indent_size=1, blank_lines_before=0,
            blank_lines_after=6), class_style_config=ClassStyleConfig(
            name_style_config=NameStyleConfig(
                name_style=NameStyle.LowerCamelCase2, whitespace_before='  ', whitespace_after='\t',
                keep_names=('self', 'cls')),
            args_style_config=ArgsStyleConfig(name_style_config=NameStyleConfig(name_style=(
                NameStyle.LetterUp, NameStyleSuffix.SingleTrailingUnderscore),
                whitespace_before='    ',
                whitespace_after='     ',
                keep_names=('self', 'cls')),
                typing_style_config=TypingStyleConfig(
                    whitespace_before_indicator='   ',
                    whitespace_after_indicator='  '),
                whitespace_before_args='\t\t\t\t\t\t\t',
                whitespace_after_args='\t\t\t'), method_style_config=FunctionStyleConfig(
                name_style_config=NameStyleConfig(name_style=(
                    NameStylePrefix.SingleLeadingUnderscore, NameStyle.LetterLow),
                    whitespace_before='      ', whitespace_after='\t\t\t\t', keep_names=('self', 'cls')),
                args_style_config=ArgsStyleConfig(name_style_config=NameStyleConfig(name_style=(
                    NameStyle.UpperCamelCase2, NameStyleSuffix.SingleTrailingUnderscore),
                    whitespace_before='\t',
                    whitespace_after=' ', keep_names=('self', 'cls')),
                    typing_style_config=None, whitespace_before_args='   ',
                    whitespace_after_args='\t\t\t\t\t\t\t'), var_style_config=VarStyleConfig(
                    name_style_config=NameStyleConfig(name_style=(
                        NameStyle.UpperCamelCase, NameStyleSuffix.DoubleTrailingUnderscore),
                        whitespace_before='      ', whitespace_after='\t\t\t\t', keep_names=('self', 'cls')),
                    typing_style_config=TypingStyleConfig(whitespace_before_indicator='\t\t\t',
                                                          whitespace_after_indicator=' '),
                    string_style_config=StringStyleConfig(quotes="'", f_string_quotes=('"""', "'"),
                                                          f_prefix=StringPrefixStyle.UP, b_prefix=StringPrefixStyle.UP,
                                                          r_prefix=StringPrefixStyle.LOW,
                                                          u_prefix=StringPrefixStyle.UP),
                    number_style_config=NumberStyleConfig(
                        int_underscore_every=2, float_underscore_every=1,
                        exp_style=NumberStyle.UP, exp_underscore_every=1, hex_style=NumberStyle.UP,
                        hex_underscore_every=4, hex_letter_case=NumberStyle.UP, oct_style=NumberStyle.LOW,
                        oct_underscore_every=1, bin_style=NumberStyle.LOW, bin_underscore_every=3,
                        imag_style=NumberStyle.UP, imag_underscore_every=0), whitespace_before_equal='\t\t',
                    whitespace_after_equal=' '), typing_style_config=None, docstring_style_config=None, indent_size=4,
                blank_lines_before=4, blank_lines_after=7), var_style_config=VarStyleConfig(
                name_style_config=NameStyleConfig(name_style=(
                    NameStyle.LowerCamelCase2, NameStyleSuffix.DoubleTrailingUnderscore),
                    whitespace_before='\t\t\t\t\t', whitespace_after='\t\t\t\t', keep_names=('self', 'cls')),
                typing_style_config=None,
                string_style_config=StringStyleConfig(quotes='"', f_string_quotes=("'''", "'"),
                                                      f_prefix=StringPrefixStyle.LOW, b_prefix=StringPrefixStyle.UP,
                                                      r_prefix=StringPrefixStyle.UP, u_prefix=StringPrefixStyle.UP),
                number_style_config=NumberStyleConfig(
                    int_underscore_every=4, float_underscore_every=4,
                    exp_style=NumberStyle.UP, exp_underscore_every=4, hex_style=NumberStyle.LOW, hex_underscore_every=1,
                    hex_letter_case=NumberStyle.UP, oct_style=NumberStyle.UP, oct_underscore_every=3,
                    bin_style=NumberStyle.LOW, bin_underscore_every=1, imag_style=NumberStyle.UP,
                    imag_underscore_every=1), whitespace_before_equal='', whitespace_after_equal='    '),
            docstring_style_config=None, indent_size=7, blank_lines_before=1, blank_lines_after=7),
            var_style_config=VarStyleConfig(
                name_style_config=NameStyleConfig(name_style=(
                    NameStyle.LowerCamelCase2, NameStyleSuffix.SingleTrailingUnderscore),
                    whitespace_before='\t\t\t\t', whitespace_after='\t\t', keep_names=('self', 'cls')),
                typing_style_config=TypingStyleConfig(whitespace_before_indicator='\t\t\t\t\t',
                                                      whitespace_after_indicator='       '),
                string_style_config=StringStyleConfig(quotes='"""', f_string_quotes=('"', "'"),
                                                      f_prefix=StringPrefixStyle.LOW,
                                                      b_prefix=StringPrefixStyle.LOW,
                                                      r_prefix=StringPrefixStyle.UP,
                                                      u_prefix=StringPrefixStyle.LOW),
                number_style_config=NumberStyleConfig(
                    int_underscore_every=2, float_underscore_every=2,
                    exp_style=NumberStyle.UP, exp_underscore_every=4, hex_style=NumberStyle.UP,
                    hex_underscore_every=3,
                    hex_letter_case=NumberStyle.LOW, oct_style=NumberStyle.LOW, oct_underscore_every=3,
                    bin_style=NumberStyle.LOW, bin_underscore_every=3, imag_style=NumberStyle.LOW,
                    imag_underscore_every=0), whitespace_before_equal='     ', whitespace_after_equal='      '),
            docstring_style_config=DocStringStyleConfig(
                string_style_config=StringStyleConfig(quotes='"""', f_string_quotes=('"', "'"),
                                                      f_prefix=StringPrefixStyle.LOW,
                                                      b_prefix=StringPrefixStyle.LOW,
                                                      r_prefix=StringPrefixStyle.LOW,
                                                      u_prefix=StringPrefixStyle.LOW),
                blank_lines_before=4, blank_lines_after=4), indent_char='\t', indent_size=5, blank_lines_top=4,
            blank_lines_bottom=6, line_ending='\r')
        codestyle_json = json.dumps(codestyle, cls=CodestyleConfigJSONEncoder)
        decoded_codestyle = json.loads(codestyle_json, cls=CodestyleConfigJSONDecoder)
        self.assertEqual(codestyle, decoded_codestyle)


if __name__ == "__main__":
    unittest.main()
