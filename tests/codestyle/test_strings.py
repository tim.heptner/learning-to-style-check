import textwrap
import unittest

from learning_to_style_check.codestyle import (
    CodestyleConfig,
    apply_codestyle,
    VarStyleConfig,
    DocStringStyleConfig,
    StringStyleConfig,
    FunctionStyleConfig,
    ClassStyleConfig,
)
from learning_to_style_check.codestyle.codestyle import StringPrefixStyle
from learning_to_style_check.libcst import parse_module


class TestModuleDocStringStyle(unittest.TestCase):
    def test_triple_double_quotes_to_triple_single_quotes(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            docstring_style_config=DocStringStyleConfig(
                string_style_config=StringStyleConfig(quotes="'''")
            ),
        )
        code = textwrap.dedent(
            '''
        """
        test docstring
        """
        test = 42
        '''
        )
        styled_code = textwrap.dedent(
            """
        '''simple docstring'''
        test = 42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_triple_double_quotes_to_triple_double_quotes(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            docstring_style_config=DocStringStyleConfig(
                string_style_config=StringStyleConfig(quotes='"""')
            ),
        )
        code = textwrap.dedent(
            '''
        """
        test docstring
        """
        test = 42
        '''
        )
        styled_code = textwrap.dedent(
            '''
        """simple docstring"""
        test = 42
        '''
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_triple_single_quotes_to_triple_double_quotes(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            docstring_style_config=DocStringStyleConfig(
                string_style_config=StringStyleConfig(quotes='"""')
            ),
        )
        code = textwrap.dedent(
            """
        '''
        test docstring
        '''
        test = 42
        """
        )
        styled_code = textwrap.dedent(
            '''
        """simple docstring"""
        test = 42
        '''
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_triple_single_quotes_to_triple_single_quotes(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            docstring_style_config=DocStringStyleConfig(
                string_style_config=StringStyleConfig(quotes="'''")
            ),
        )
        code = textwrap.dedent(
            """
        '''
        test docstring
        '''
        test = 42
        """
        )
        styled_code = textwrap.dedent(
            """
        '''simple docstring'''
        test = 42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_no_docstring(self):
        codestyle = CodestyleConfig(blank_lines_top=1, blank_lines_bottom=1)
        code = textwrap.dedent(
            '''
        """
        test docstring
        """
        test = 42
        '''
        )
        styled_code = textwrap.dedent(
            """
        test = 42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_no_docstring_to_docstring(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            docstring_style_config=DocStringStyleConfig(
                string_style_config=StringStyleConfig(quotes='"""')
            ),
        )

        code = textwrap.dedent(
            """
        test = 42
        """
        )
        styled_code = textwrap.dedent(
            '''
        """simple docstring"""
        test = 42
        '''
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            docstring_style_config=DocStringStyleConfig(
                string_style_config=StringStyleConfig(quotes='"""'),
                blank_lines_before=2,
                blank_lines_after=2,
            ),
        )

        code = textwrap.dedent(
            """
        test = 42
        """
        )
        styled_code = textwrap.dedent(
            '''
        """simple docstring"""
        
        
        test = 42
        '''
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            docstring_style_config=DocStringStyleConfig(
                string_style_config=StringStyleConfig(quotes='"""'),
                blank_lines_before=2,
                blank_lines_after=1,
            ),
        )

        code = textwrap.dedent(
            """
        
        
        
        
        test = 42
        """
        )
        styled_code = textwrap.dedent(
            '''
        """simple docstring"""

        test = 42
        '''
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_docstring_blanklines_before(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            docstring_style_config=DocStringStyleConfig(
                string_style_config=StringStyleConfig(quotes="'''"),
                blank_lines_before=1,
            ),
        )
        code = textwrap.dedent(
            '''
        """
        test docstring
        """
        '''
        )
        styled_code = textwrap.dedent(
            """
        '''simple docstring'''
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            docstring_style_config=DocStringStyleConfig(
                string_style_config=StringStyleConfig(quotes="'''"),
                blank_lines_before=2,
            ),
        )
        code = textwrap.dedent(
            '''
        """
        test docstring
        """
        '''
        )
        styled_code = textwrap.dedent(
            """
        '''simple docstring'''
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            docstring_style_config=DocStringStyleConfig(
                string_style_config=StringStyleConfig(quotes="'''"),
                blank_lines_before=1,
            ),
        )
        code = textwrap.dedent(
            '''
        m = 42
        """
        test docstring
        """
        '''
        )
        styled_code = textwrap.dedent(
            """
        '''simple docstring'''
        m = 42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            docstring_style_config=DocStringStyleConfig(
                string_style_config=StringStyleConfig(quotes="'''"),
                blank_lines_before=2,
            ),
        )
        code = textwrap.dedent(
            '''
        m = 42
        """
        test docstring
        """
        '''
        )
        styled_code = textwrap.dedent(
            """
        '''simple docstring'''
        m = 42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            docstring_style_config=DocStringStyleConfig(
                string_style_config=StringStyleConfig(quotes="'''"),
                blank_lines_before=2,
            ),
        )
        code = textwrap.dedent(
            '''
        m = 42
        
        
        
        
        """
        test docstring
        """
        '''
        )
        styled_code = textwrap.dedent(
            """
        '''simple docstring'''
        m = 42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_docstring_blanklines_after(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            docstring_style_config=DocStringStyleConfig(
                string_style_config=StringStyleConfig(quotes="'''"),
                blank_lines_after=1
            )
        )
        code = textwrap.dedent('''
        """
        test docstring
        """
        ''')
        styled_code = textwrap.dedent("""
        '''simple docstring'''
        """)
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            docstring_style_config=DocStringStyleConfig(
                string_style_config=StringStyleConfig(quotes="'''"),
                blank_lines_after=2
            )
        )
        code = textwrap.dedent('''
        """
        test docstring
        """
        ''')
        styled_code = textwrap.dedent("""
        '''simple docstring'''
        """)
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            docstring_style_config=DocStringStyleConfig(
                string_style_config=StringStyleConfig(quotes="'''"), blank_lines_after=1
            ),
        )
        code = textwrap.dedent(
            '''
        """
        test docstring
        """
        m = 42
        '''
        )
        styled_code = textwrap.dedent(
            """
        '''simple docstring'''

        m = 42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            docstring_style_config=DocStringStyleConfig(
                string_style_config=StringStyleConfig(quotes="'''"), blank_lines_after=2
            ),
        )
        code = textwrap.dedent(
            '''
        """
        test docstring
        """
        m = 42
        '''
        )
        styled_code = textwrap.dedent(
            """
        '''simple docstring'''


        m = 42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            docstring_style_config=DocStringStyleConfig(
                string_style_config=StringStyleConfig(quotes="'''"), blank_lines_after=2
            ),
        )
        code = textwrap.dedent(
            '''
        """
        test docstring
        """
        
        
        
        
        m = 42
        '''
        )
        styled_code = textwrap.dedent(
            """
        '''simple docstring'''


        m = 42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_docstring_blanklines_before_and_after(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            docstring_style_config=DocStringStyleConfig(
                string_style_config=StringStyleConfig(quotes="'''"),
                blank_lines_before=1,
                blank_lines_after=1,
            ),
        )
        code = textwrap.dedent(
            '''
        """
        test docstring
        """
        '''
        )
        styled_code = textwrap.dedent(
            """
        '''simple docstring'''
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            docstring_style_config=DocStringStyleConfig(
                string_style_config=StringStyleConfig(quotes="'''"),
                blank_lines_before=1,
                blank_lines_after=1,
            ),
        )
        code = textwrap.dedent(
            '''

        """
        test docstring
        """

        '''
        )
        styled_code = textwrap.dedent(
            """
        '''simple docstring'''
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            docstring_style_config=DocStringStyleConfig(
                string_style_config=StringStyleConfig(quotes="'''"),
                blank_lines_before=2,
                blank_lines_after=2,
            ),
        )
        code = textwrap.dedent(
            '''
        """
        test docstring
        """
        '''
        )
        styled_code = textwrap.dedent(
            """
        '''simple docstring'''
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            docstring_style_config=DocStringStyleConfig(
                string_style_config=StringStyleConfig(quotes="'''"),
                blank_lines_before=2,
                blank_lines_after=2,
            ),
        )
        code = textwrap.dedent(
            '''
        m = 42
        """
        test docstring
        """
        n = 42
        '''
        )
        styled_code = textwrap.dedent(
            """
        '''simple docstring'''
        
        
        m = 42
        n = 42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            docstring_style_config=DocStringStyleConfig(
                string_style_config=StringStyleConfig(quotes="'''"),
                blank_lines_before=2,
                blank_lines_after=3,
            ),
        )
        code = textwrap.dedent(
            '''
        """
        test docstring 1
        """
        """
        test docstring 2
        """
        '''
        )
        styled_code = textwrap.dedent(
            """
        '''simple docstring'''
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            docstring_style_config=DocStringStyleConfig(
                string_style_config=StringStyleConfig(quotes="'''"),
                blank_lines_before=3,
                blank_lines_after=2,
            ),
        )
        code = textwrap.dedent(
            '''
        """
        test docstring 1
        """
        """
        test docstring 2
        """
        '''
        )
        styled_code = textwrap.dedent(
            """
        '''simple docstring'''
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_docstring_with_comment_on_top(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            docstring_style_config=DocStringStyleConfig(
                string_style_config=StringStyleConfig(quotes="'''"),
                blank_lines_after=1
            )
        )
        code = '''# comment\n"""\ntest docstring\n"""\n'''
        styled_code = '''\n\'\'\'simple docstring\'\'\'\n\n# comment\n'''
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_docstring_with_comment_on_bottom(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            docstring_style_config=DocStringStyleConfig(
                string_style_config=StringStyleConfig(quotes="'''"),
                blank_lines_after=1
            )
        )
        code = '''"""\ntest docstring\n"""\n# comment'''
        styled_code = '''\n\'\'\'simple docstring\'\'\'\n\n# comment\n'''
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)


class TestFunctionDocStringStyle(unittest.TestCase):
    def test_triple_double_quotes_to_triple_single_quotes(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                docstring_style_config=DocStringStyleConfig(
                    string_style_config=StringStyleConfig(quotes="'''")
                )
            ),
        )
        code = textwrap.dedent(
            '''
        def test():
            """
            test docstring
            """
            test = 42
        '''
        )
        styled_code = textwrap.dedent(
            """
        def test():
            '''simple docstring'''
            test = 42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_triple_double_quotes_to_triple_double_quotes(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                docstring_style_config=DocStringStyleConfig(
                    string_style_config=StringStyleConfig(quotes='"""')
                )
            ),
        )
        code = textwrap.dedent(
            '''
        def test():
            """
            test docstring
            """
            test = 42
        '''
        )
        styled_code = textwrap.dedent(
            '''
        def test():
            """simple docstring"""
            test = 42
        '''
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_triple_single_quotes_to_triple_double_quotes(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                docstring_style_config=DocStringStyleConfig(
                    string_style_config=StringStyleConfig(quotes='"""')
                )
            ),
        )
        code = textwrap.dedent(
            """
        def test():
            '''
            test docstring
            '''
            test = 42
        """
        )
        styled_code = textwrap.dedent(
            '''
        def test():
            """simple docstring"""
            test = 42
        '''
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_triple_single_quotes_to_triple_single_quotes(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                docstring_style_config=DocStringStyleConfig(
                    string_style_config=StringStyleConfig(quotes="'''")
                )
            ),
        )
        code = textwrap.dedent(
            """
        def test():
            '''
            test docstring
            '''
            test = 42
        """
        )
        styled_code = textwrap.dedent(
            """
        def test():
            '''simple docstring'''
            test = 42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_no_docstring(self):
        codestyle = CodestyleConfig(blank_lines_top=1, blank_lines_bottom=1)
        code = textwrap.dedent(
            '''
        def test():
            """
            test docstring
            """
            test = 42
        '''
        )
        styled_code = textwrap.dedent(
            """
        def test():
            test = 42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_no_docstring_to_docstring(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                docstring_style_config=DocStringStyleConfig(
                    string_style_config=StringStyleConfig(quotes='"""')
                )
            ),
        )

        code = textwrap.dedent(
            """
        def test():
            test = 42
        """
        )
        styled_code = textwrap.dedent(
            '''
        def test():
            """simple docstring"""
            test = 42
        '''
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                docstring_style_config=DocStringStyleConfig(
                    string_style_config=StringStyleConfig(quotes='"""'),
                    blank_lines_before=2,
                    blank_lines_after=2,
                )
            ),
        )

        code = textwrap.dedent(
            """
        def test():
            test = 42
        """
        )
        styled_code = textwrap.dedent(
            '''
        def test():
        
        
            """simple docstring"""


            test = 42
        '''
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                docstring_style_config=DocStringStyleConfig(
                    string_style_config=StringStyleConfig(quotes='"""'),
                    blank_lines_before=2,
                    blank_lines_after=1,
                )
            ),
        )

        code = textwrap.dedent(
            """
        def test():
            



            test = 42
        """
        )
        styled_code = textwrap.dedent(
            '''
        def test():
        
        
            """simple docstring"""

            test = 42
        '''
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_docstring_blanklines_before(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                docstring_style_config=DocStringStyleConfig(
                    string_style_config=StringStyleConfig(quotes="'''"),
                    blank_lines_before=1,
                )
            ),
        )
        code = textwrap.dedent(
            '''
        def test():
            """
            test docstring
            """
        '''
        )
        styled_code = textwrap.dedent(
            """
        def test():
        
            '''simple docstring'''
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                docstring_style_config=DocStringStyleConfig(
                    string_style_config=StringStyleConfig(quotes="'''"),
                    blank_lines_before=2,
                )
            ),
        )
        code = textwrap.dedent(
            '''
        def test():
            """
            test docstring
            """
        '''
        )
        styled_code = textwrap.dedent(
            """
        def test():
        
        
            '''simple docstring'''
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                docstring_style_config=DocStringStyleConfig(
                    string_style_config=StringStyleConfig(quotes="'''"),
                    blank_lines_before=1,
                )
            ),
        )
        code = textwrap.dedent(
            '''
        def test():
            m = 42
            """
            test docstring
            """
        '''
        )
        styled_code = textwrap.dedent(
            """
        def test():
        
            '''simple docstring'''
            m = 42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                docstring_style_config=DocStringStyleConfig(
                    string_style_config=StringStyleConfig(quotes="'''"),
                    blank_lines_before=2,
                )
            ),
        )
        code = textwrap.dedent(
            '''
        def test():
            m = 42
            """
            test docstring
            """
        '''
        )
        styled_code = textwrap.dedent(
            """
        def test():
        
        
            '''simple docstring'''
            m = 42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                docstring_style_config=DocStringStyleConfig(
                    string_style_config=StringStyleConfig(quotes="'''"),
                    blank_lines_before=2,
                )
            ),
        )
        code = textwrap.dedent(
            '''
        def test():
            m = 42




            """
            test docstring
            """
        '''
        )
        styled_code = textwrap.dedent(
            """
        def test():
        
        
            '''simple docstring'''
            m = 42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_docstring_blanklines_after(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                docstring_style_config=DocStringStyleConfig(
                    string_style_config=StringStyleConfig(quotes="'''"),
                    blank_lines_after=1,
                )
            ),
        )
        code = textwrap.dedent(
            '''
        def test():
            """
            test docstring
            """
        '''
        )
        styled_code = textwrap.dedent(
            """
        def test():
            '''simple docstring'''
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                docstring_style_config=DocStringStyleConfig(
                    string_style_config=StringStyleConfig(quotes="'''"),
                    blank_lines_after=2,
                )
            ),
        )
        code = textwrap.dedent(
            '''
        def test():
            """
            test docstring
            """
        '''
        )
        styled_code = textwrap.dedent(
            """
        def test():
            '''simple docstring'''
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                docstring_style_config=DocStringStyleConfig(
                    string_style_config=StringStyleConfig(quotes="'''"),
                    blank_lines_after=1,
                )
            ),
        )
        code = textwrap.dedent(
            '''
        def test():
            """
            test docstring
            """
            m = 42
        '''
        )
        styled_code = textwrap.dedent(
            """
        def test():
            '''simple docstring'''
    
            m = 42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                docstring_style_config=DocStringStyleConfig(
                    string_style_config=StringStyleConfig(quotes="'''"),
                    blank_lines_after=2,
                )
            ),
        )
        code = textwrap.dedent(
            '''
        def test():
            """
            test docstring
            """
            m = 42
        '''
        )
        styled_code = textwrap.dedent(
            """
        def test():
            '''simple docstring'''
    
    
            m = 42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                docstring_style_config=DocStringStyleConfig(
                    string_style_config=StringStyleConfig(quotes="'''"),
                    blank_lines_after=2,
                )
            ),
        )
        code = textwrap.dedent(
            '''
        def test():
            """
            test docstring
            """
    
    
    
    
            m = 42
        '''
        )
        styled_code = textwrap.dedent(
            """
        def test():
            '''simple docstring'''
    
    
            m = 42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_docstring_blanklines_before_and_after(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                docstring_style_config=DocStringStyleConfig(
                    string_style_config=StringStyleConfig(quotes="'''"),
                    blank_lines_before=1,
                    blank_lines_after=1,
                )
            ),
        )
        code = textwrap.dedent(
            '''
        def test():
            """
            test docstring
            """
        '''
        )
        styled_code = textwrap.dedent(
            """
        def test():
        
            '''simple docstring'''
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                docstring_style_config=DocStringStyleConfig(
                    string_style_config=StringStyleConfig(quotes="'''"),
                    blank_lines_before=1,
                    blank_lines_after=1,
                )
            ),
        )
        code = textwrap.dedent(
            '''
        def test():
            
            """
            test docstring
            """

        '''
        )
        styled_code = textwrap.dedent(
            """
        def test():
        
            '''simple docstring'''
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                docstring_style_config=DocStringStyleConfig(
                    string_style_config=StringStyleConfig(quotes="'''"),
                    blank_lines_before=2,
                    blank_lines_after=2,
                )
            ),
        )
        code = textwrap.dedent(
            '''
        def test():
            """
            test docstring
            """
        '''
        )
        styled_code = textwrap.dedent(
            """
        def test():
        
        
            '''simple docstring'''
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                docstring_style_config=DocStringStyleConfig(
                    string_style_config=StringStyleConfig(quotes="'''"),
                    blank_lines_before=2,
                    blank_lines_after=2,
                )
            ),
        )
        code = textwrap.dedent(
            '''
        def test():
            m = 42
            """
            test docstring
            """
            n = 42
        '''
        )
        styled_code = textwrap.dedent(
            """
        def test():
        
        
            '''simple docstring'''
    
    
            m = 42
            n = 42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                docstring_style_config=DocStringStyleConfig(
                    string_style_config=StringStyleConfig(quotes="'''"),
                    blank_lines_before=2,
                    blank_lines_after=3,
                )
            ),
        )
        code = textwrap.dedent(
            '''
        def test():
            """
            test docstring 1
            """
            """
            test docstring 2
            """
        '''
        )
        styled_code = textwrap.dedent(
            """
        def test():
        
        
            '''simple docstring'''
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                docstring_style_config=DocStringStyleConfig(
                    string_style_config=StringStyleConfig(quotes="'''"),
                    blank_lines_before=3,
                    blank_lines_after=2,
                )
            ),
        )
        code = textwrap.dedent(
            '''
        def test():
            """
            test docstring 1
            """
            """
            test docstring 2
            """
        '''
        )
        styled_code = textwrap.dedent(
            """
        def test():
        
        
        
            '''simple docstring'''
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)


class TestClassDocStringStyle(unittest.TestCase):
    def test_triple_double_quotes_to_triple_single_quotes(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                docstring_style_config=DocStringStyleConfig(
                    string_style_config=StringStyleConfig(quotes="'''")
                )
            ),
        )
        code = textwrap.dedent(
            '''
        class Test:
            """
            test docstring
            """
            test = 42
        '''
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            '''simple docstring'''
            test = 42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_triple_double_quotes_to_triple_double_quotes(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                docstring_style_config=DocStringStyleConfig(
                    string_style_config=StringStyleConfig(quotes='"""')
                )
            ),
        )
        code = textwrap.dedent(
            '''
        class Test:
            """
            test docstring
            """
            test = 42
        '''
        )
        styled_code = textwrap.dedent(
            '''
        class Test:
            """simple docstring"""
            test = 42
        '''
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_triple_single_quotes_to_triple_double_quotes(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                docstring_style_config=DocStringStyleConfig(
                    string_style_config=StringStyleConfig(quotes='"""')
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            '''
            test docstring
            '''
            test = 42
        """
        )
        styled_code = textwrap.dedent(
            '''
        class Test:
            """simple docstring"""
            test = 42
        '''
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_triple_single_quotes_to_triple_single_quotes(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                docstring_style_config=DocStringStyleConfig(
                    string_style_config=StringStyleConfig(quotes="'''")
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            '''
            test docstring
            '''
            test = 42
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            '''simple docstring'''
            test = 42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_no_docstring(self):
        codestyle = CodestyleConfig(blank_lines_top=1, blank_lines_bottom=1)
        code = textwrap.dedent(
            '''
        class Test:
            """
            test docstring
            """
            test = 42
        '''
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            test = 42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_no_docstring_to_docstring(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                docstring_style_config=DocStringStyleConfig(
                    string_style_config=StringStyleConfig(quotes='"""')
                )
            ),
        )

        code = textwrap.dedent(
            """
        class Test:
            test = 42
        """
        )
        styled_code = textwrap.dedent(
            '''
        class Test:
            """simple docstring"""
            test = 42
        '''
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                docstring_style_config=DocStringStyleConfig(
                    string_style_config=StringStyleConfig(quotes='"""'),
                    blank_lines_before=2,
                    blank_lines_after=2,
                )
            ),
        )

        code = textwrap.dedent(
            """
        class Test:
            test = 42
        """
        )
        styled_code = textwrap.dedent(
            '''
        class Test:
        
        
            """simple docstring"""


            test = 42
        '''
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                docstring_style_config=DocStringStyleConfig(
                    string_style_config=StringStyleConfig(quotes='"""'),
                    blank_lines_before=2,
                    blank_lines_after=1,
                )
            ),
        )

        code = textwrap.dedent(
            """
        class Test:
            



            test = 42
        """
        )
        styled_code = textwrap.dedent(
            '''
        class Test:
        
        
            """simple docstring"""

            test = 42
        '''
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_docstring_blanklines_before(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                docstring_style_config=DocStringStyleConfig(
                    string_style_config=StringStyleConfig(quotes="'''"),
                    blank_lines_before=1,
                )
            ),
        )
        code = textwrap.dedent(
            '''
        class Test:
            """
            test docstring
            """
        '''
        )
        styled_code = textwrap.dedent(
            """
        class Test:
        
            '''simple docstring'''
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                docstring_style_config=DocStringStyleConfig(
                    string_style_config=StringStyleConfig(quotes="'''"),
                    blank_lines_before=2,
                )
            ),
        )
        code = textwrap.dedent(
            '''
        class Test:
            """
            test docstring
            """
        '''
        )
        styled_code = textwrap.dedent(
            """
        class Test:
        
        
            '''simple docstring'''
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                docstring_style_config=DocStringStyleConfig(
                    string_style_config=StringStyleConfig(quotes="'''"),
                    blank_lines_before=1,
                )
            ),
        )
        code = textwrap.dedent(
            '''
        class Test:
            m = 42
            """
            test docstring
            """
        '''
        )
        styled_code = textwrap.dedent(
            """
        class Test:
        
            '''simple docstring'''
            m = 42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                docstring_style_config=DocStringStyleConfig(
                    string_style_config=StringStyleConfig(quotes="'''"),
                    blank_lines_before=2,
                )
            ),
        )
        code = textwrap.dedent(
            '''
        class Test:
            m = 42
            """
            test docstring
            """
        '''
        )
        styled_code = textwrap.dedent(
            """
        class Test:
        
        
            '''simple docstring'''
            m = 42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                docstring_style_config=DocStringStyleConfig(
                    string_style_config=StringStyleConfig(quotes="'''"),
                    blank_lines_before=2,
                )
            ),
        )
        code = textwrap.dedent(
            '''
        class Test:
            m = 42




            """
            test docstring
            """
        '''
        )
        styled_code = textwrap.dedent(
            """
        class Test:
        
        
            '''simple docstring'''
            m = 42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_docstring_blanklines_after(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                docstring_style_config=DocStringStyleConfig(
                    string_style_config=StringStyleConfig(quotes="'''"),
                    blank_lines_after=1,
                )
            ),
        )
        code = textwrap.dedent(
            '''
        class Test:
            """
            test docstring
            """
        '''
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            '''simple docstring'''
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                docstring_style_config=DocStringStyleConfig(
                    string_style_config=StringStyleConfig(quotes="'''"),
                    blank_lines_after=2,
                )
            ),
        )
        code = textwrap.dedent(
            '''
        class Test:
            """
            test docstring
            """
        '''
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            '''simple docstring'''
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                docstring_style_config=DocStringStyleConfig(
                    string_style_config=StringStyleConfig(quotes="'''"),
                    blank_lines_after=1,
                )
            ),
        )
        code = textwrap.dedent(
            '''
        class Test:
            """
            test docstring
            """
            m = 42
        '''
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            '''simple docstring'''

            m = 42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                docstring_style_config=DocStringStyleConfig(
                    string_style_config=StringStyleConfig(quotes="'''"),
                    blank_lines_after=2,
                )
            ),
        )
        code = textwrap.dedent(
            '''
        class Test:
            """
            test docstring
            """
            m = 42
        '''
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            '''simple docstring'''


            m = 42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                docstring_style_config=DocStringStyleConfig(
                    string_style_config=StringStyleConfig(quotes="'''"),
                    blank_lines_after=2,
                )
            ),
        )
        code = textwrap.dedent(
            '''
        class Test:
            """
            test docstring
            """
    
    
    
    
            m = 42
        '''
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            '''simple docstring'''


            m = 42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_docstring_blanklines_before_and_after(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                docstring_style_config=DocStringStyleConfig(
                    string_style_config=StringStyleConfig(quotes="'''"),
                    blank_lines_before=1,
                    blank_lines_after=1,
                )
            ),
        )
        code = textwrap.dedent(
            '''
        class Test:
            """
            test docstring
            """
        '''
        )
        styled_code = textwrap.dedent(
            """
        class Test:
        
            '''simple docstring'''
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                docstring_style_config=DocStringStyleConfig(
                    string_style_config=StringStyleConfig(quotes="'''"),
                    blank_lines_before=1,
                    blank_lines_after=1,
                )
            ),
        )
        code = textwrap.dedent(
            '''
        class Test:
            
            """
            test docstring
            """

        '''
        )
        styled_code = textwrap.dedent(
            """
        class Test:
        
            '''simple docstring'''
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                docstring_style_config=DocStringStyleConfig(
                    string_style_config=StringStyleConfig(quotes="'''"),
                    blank_lines_before=2,
                    blank_lines_after=2,
                )
            ),
        )
        code = textwrap.dedent(
            '''
        class Test:
            """
            test docstring
            """
        '''
        )
        styled_code = textwrap.dedent(
            """
        class Test:
        
        
            '''simple docstring'''
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                docstring_style_config=DocStringStyleConfig(
                    string_style_config=StringStyleConfig(quotes="'''"),
                    blank_lines_before=2,
                    blank_lines_after=2,
                )
            ),
        )
        code = textwrap.dedent(
            '''
        class Test:
            m = 42
            """
            test docstring
            """
            n = 42
        '''
        )
        styled_code = textwrap.dedent(
            """
        class Test:
        
        
            '''simple docstring'''


            m = 42
            n = 42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                docstring_style_config=DocStringStyleConfig(
                    string_style_config=StringStyleConfig(quotes="'''"),
                    blank_lines_before=2,
                    blank_lines_after=3,
                )
            ),
        )
        code = textwrap.dedent(
            '''
        class Test:
            """
            test docstring 1
            """
            """
            test docstring 2
            """
        '''
        )
        styled_code = textwrap.dedent(
            """
        class Test:
        
        
            '''simple docstring'''
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                docstring_style_config=DocStringStyleConfig(
                    string_style_config=StringStyleConfig(quotes="'''"),
                    blank_lines_before=3,
                    blank_lines_after=2,
                )
            ),
        )
        code = textwrap.dedent(
            '''
        class Test:
            """
            test docstring 1
            """
            """
            test docstring 2
            """
        '''
        )
        styled_code = textwrap.dedent(
            """
        class Test:
        
        
        
            '''simple docstring'''
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)


class TestMethodDocStringStyle(unittest.TestCase):
    def test_triple_double_quotes_to_triple_single_quotes(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    blank_lines_after=0,
                    docstring_style_config=DocStringStyleConfig(
                        string_style_config=StringStyleConfig(quotes="'''")
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            '''
        class Test:
            def test(self):
                """
                test docstring
                """
                test = 42
        '''
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def test(self):
                '''simple docstring'''
                test = 42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_triple_double_quotes_to_triple_double_quotes(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    blank_lines_after=0,
                    docstring_style_config=DocStringStyleConfig(
                        string_style_config=StringStyleConfig(quotes='"""')
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            '''
        class Test:
            def test(self):
                """
                test docstring
                """
                test = 42
        '''
        )
        styled_code = textwrap.dedent(
            '''
        class Test:
            def test(self):
                """simple docstring"""
                test = 42
        '''
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_triple_single_quotes_to_triple_double_quotes(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    blank_lines_after=0,
                    docstring_style_config=DocStringStyleConfig(
                        string_style_config=StringStyleConfig(quotes='"""')
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test(self):
                '''
                test docstring
                '''
                test = 42
        """
        )
        styled_code = textwrap.dedent(
            '''
        class Test:
            def test(self):
                """simple docstring"""
                test = 42
        '''
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_triple_single_quotes_to_triple_single_quotes(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    blank_lines_after=0,
                    docstring_style_config=DocStringStyleConfig(
                        string_style_config=StringStyleConfig(quotes="'''")
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test(self):
                '''
                test docstring
                '''
                test = 42
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def test(self):
                '''simple docstring'''
                test = 42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_no_docstring(self):
        codestyle = CodestyleConfig(blank_lines_top=1, blank_lines_bottom=1)
        code = textwrap.dedent(
            '''
        class Test:
            def test(self):
                """
                test docstring
                """
                test = 42
        '''
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def test(self):
                test = 42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_no_docstring_to_docstring(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    blank_lines_after=0,
                    docstring_style_config=DocStringStyleConfig(
                        string_style_config=StringStyleConfig(quotes='"""')
                    ),
                )
            ),
        )

        code = textwrap.dedent(
            """
        class Test:
            def test(self):
                test = 42
        """
        )
        styled_code = textwrap.dedent(
            '''
        class Test:
            def test(self):
                """simple docstring"""
                test = 42
        '''
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    blank_lines_after=0,
                    docstring_style_config=DocStringStyleConfig(
                        string_style_config=StringStyleConfig(quotes='"""'),
                        blank_lines_before=2,
                        blank_lines_after=2,
                    ),
                )
            ),
        )

        code = textwrap.dedent(
            """
        class Test:
            def test(self):
                test = 42
        """
        )
        styled_code = textwrap.dedent(
            '''
        class Test:
            def test(self):
            
            
                """simple docstring"""


                test = 42
        '''
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    blank_lines_after=0,
                    docstring_style_config=DocStringStyleConfig(
                        string_style_config=StringStyleConfig(quotes='"""'),
                        blank_lines_before=2,
                        blank_lines_after=1,
                    ),
                )
            ),
        )

        code = textwrap.dedent(
            """
        class Test:
            def test(self):
                



                test = 42
        """
        )
        styled_code = textwrap.dedent(
            '''
        class Test:
            def test(self):
            
            
                """simple docstring"""

                test = 42
        '''
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_docstring_blanklines_before(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    blank_lines_after=0,
                    docstring_style_config=DocStringStyleConfig(
                        string_style_config=StringStyleConfig(quotes="'''"),
                        blank_lines_before=1,
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            '''
        class Test:
            def test(self):
                """
                test docstring
                """
        '''
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def test(self):
            
                '''simple docstring'''
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    blank_lines_after=0,
                    docstring_style_config=DocStringStyleConfig(
                        string_style_config=StringStyleConfig(quotes="'''"),
                        blank_lines_before=2,
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            '''
        class Test:
            def test(self):
                """
                test docstring
                """
        '''
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def test(self):
            
            
                '''simple docstring'''
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    blank_lines_after=0,
                    docstring_style_config=DocStringStyleConfig(
                        string_style_config=StringStyleConfig(quotes="'''"),
                        blank_lines_before=1,
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            '''
        class Test:
            def test(self):
                m = 42
                """
                test docstring
                """
        '''
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def test(self):
            
                '''simple docstring'''
                m = 42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    blank_lines_after=0,
                    docstring_style_config=DocStringStyleConfig(
                        string_style_config=StringStyleConfig(quotes="'''"),
                        blank_lines_before=2,
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            '''
        class Test:
            def test(self):
                m = 42
                """
                test docstring
                """
        '''
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def test(self):
            
            
                '''simple docstring'''
                m = 42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    blank_lines_after=0,
                    docstring_style_config=DocStringStyleConfig(
                        string_style_config=StringStyleConfig(quotes="'''"),
                        blank_lines_before=2,
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            '''
        class Test:
            def test(self):
                m = 42




                """
                test docstring
                """
        '''
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def test(self):
            
            
                '''simple docstring'''
                m = 42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_docstring_blanklines_after(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    blank_lines_after=0,
                    docstring_style_config=DocStringStyleConfig(
                        string_style_config=StringStyleConfig(quotes="'''"),
                        blank_lines_after=1,
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            '''
        class Test:
            def test(self):
                """
                test docstring
                """
        '''
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def test(self):
                '''simple docstring'''
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    blank_lines_after=0,
                    docstring_style_config=DocStringStyleConfig(
                        string_style_config=StringStyleConfig(quotes="'''"),
                        blank_lines_after=2,
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            '''
        class Test:
            def test(self):
                """
                test docstring
                """
        '''
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def test(self):
                '''simple docstring'''
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    blank_lines_after=0,
                    docstring_style_config=DocStringStyleConfig(
                        string_style_config=StringStyleConfig(quotes="'''"),
                        blank_lines_after=1,
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            '''
        class Test:
            def test(self):
                """
                test docstring
                """
                m = 42
        '''
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def test(self):
                '''simple docstring'''

                m = 42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    blank_lines_after=0,
                    docstring_style_config=DocStringStyleConfig(
                        string_style_config=StringStyleConfig(quotes="'''"),
                        blank_lines_after=2,
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            '''
        class Test:
            def test(self):
                """
                test docstring
                """
                m = 42
        '''
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def test(self):
                '''simple docstring'''


                m = 42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    blank_lines_after=0,
                    docstring_style_config=DocStringStyleConfig(
                        string_style_config=StringStyleConfig(quotes="'''"),
                        blank_lines_after=2,
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            '''
        class Test:
            def test(self):
                """
                test docstring
                """




                m = 42
        '''
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def test(self):
                '''simple docstring'''


                m = 42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_docstring_blanklines_before_and_after(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    blank_lines_after=0,
                    docstring_style_config=DocStringStyleConfig(
                        string_style_config=StringStyleConfig(quotes="'''"),
                        blank_lines_before=1,
                        blank_lines_after=1,
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            '''
        class Test:
            def test(self):
                """
                test docstring
                """
        '''
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def test(self):
            
                '''simple docstring'''
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    blank_lines_after=0,
                    docstring_style_config=DocStringStyleConfig(
                        string_style_config=StringStyleConfig(quotes="'''"),
                        blank_lines_before=1,
                        blank_lines_after=1,
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            '''
        class Test:
            def test(self):
                
                """
                test docstring
                """
        '''
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def test(self):
            
                '''simple docstring'''
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    blank_lines_after=0,
                    docstring_style_config=DocStringStyleConfig(
                        string_style_config=StringStyleConfig(quotes="'''"),
                        blank_lines_before=2,
                        blank_lines_after=2,
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            '''
        class Test:
            def test(self):
                """
                test docstring
                """
        '''
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def test(self):
            
            
                '''simple docstring'''
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    blank_lines_after=0,
                    docstring_style_config=DocStringStyleConfig(
                        string_style_config=StringStyleConfig(quotes="'''"),
                        blank_lines_before=2,
                        blank_lines_after=2,
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            '''
        class Test:
            def test(self):
                m = 42
                """
                test docstring
                """
                n = 42
        '''
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def test(self):
            
            
                '''simple docstring'''


                m = 42
                n = 42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    blank_lines_after=0,
                    docstring_style_config=DocStringStyleConfig(
                        string_style_config=StringStyleConfig(quotes="'''"),
                        blank_lines_before=2,
                        blank_lines_after=3,
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            '''
        class Test:
            def test(self):
                """
                test docstring 1
                """
                """
                test docstring 2
                """
        '''
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def test(self):
            
            
                '''simple docstring'''
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    blank_lines_after=0,
                    docstring_style_config=DocStringStyleConfig(
                        string_style_config=StringStyleConfig(quotes="'''"),
                        blank_lines_before=3,
                        blank_lines_after=2,
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            '''
        class Test:
            def test(self):
                """
                test docstring 1
                """
                """
                test docstring 2
                """
        '''
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def test(self):
            
            
            
                '''simple docstring'''
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)


class TestModuleStringStyle(unittest.TestCase):
    def test_double_quotes_to_single_quotes_string(self):
        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                string_style_config=StringStyleConfig(
                    quotes="'", f_string_quotes=("'", '"')
                ),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = 'test = "test"'
        styled_code = "test = 'test'"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_double_quotes_to_double_quotes_string(self):
        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                string_style_config=StringStyleConfig(
                    quotes='"', f_string_quotes=("'", '"')
                ),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = 'test = "test"'
        styled_code = 'test = "test"'
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_single_quotes_to_double_quotes_string(self):
        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                string_style_config=StringStyleConfig(
                    quotes='"', f_string_quotes=("'", '"')
                ),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "test = 'test'"
        styled_code = 'test = "test"'
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_single_quotes_to_single_quotes_string(self):
        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                string_style_config=StringStyleConfig(
                    quotes="'", f_string_quotes=("'", '"')
                ),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "test = 'test'"
        styled_code = "test = 'test'"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_raw_string(self):
        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                string_style_config=StringStyleConfig(
                    quotes="'", f_string_quotes=("'", '"')
                ),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "test = r'test'"
        styled_code = "test = r'test'"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_raw_string_double_quotes(self):
        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                string_style_config=StringStyleConfig(
                    quotes='"', f_string_quotes=("'", '"')
                ),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = 'test = r"test"'
        styled_code = 'test = r"test"'
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_raw_string_single_quotes_to_double_quotes(self):
        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                string_style_config=StringStyleConfig(
                    quotes='"', f_string_quotes=("'", '"')
                ),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "test = r'test'"
        styled_code = 'test = r"test"'
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_raw_string_double_quotes_to_single_quotes(self):
        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                string_style_config=StringStyleConfig(
                    quotes="'", f_string_quotes=("'", '"')
                ),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = 'test = r"test"'
        styled_code = "test = r'test'"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_formatted_string(self):
        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                string_style_config=StringStyleConfig(
                    quotes="'", f_string_quotes=("'", '"')
                ),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "test = f'test'"
        styled_code = "test = f'test'"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                string_style_config=StringStyleConfig(
                    quotes="'", f_string_quotes=('"', "'")
                ),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "test = f'test'"
        styled_code = 'test = f"test"'
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_formatted_string_double_quotes(self):
        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                string_style_config=StringStyleConfig(
                    quotes='"', f_string_quotes=('"', "'")
                ),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = 'test = f"test"'
        styled_code = 'test = f"test"'
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_formatted_string_single_quotes_to_double_quotes(self):
        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                string_style_config=StringStyleConfig(
                    quotes='"', f_string_quotes=('"', "'")
                ),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "test = f'test'"
        styled_code = 'test = f"test"'
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_formatted_string_double_quotes_to_single_quotes(self):
        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                string_style_config=StringStyleConfig(
                    quotes="'", f_string_quotes=("'", '"')
                ),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = 'test = f"test"'
        styled_code = "test = f'test'"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_multiline_string_to_single_line(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            var_style_config=VarStyleConfig(
                string_style_config=StringStyleConfig(
                    quotes="'", f_string_quotes=("'", '"')
                ),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            ),
        )
        code = textwrap.dedent(
            """
        test = '''
        test string
        '''
        """
        )
        styled_code = textwrap.dedent(
            r"""
        test = '\ntest string\n'
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        parse_module(transformed_code)
        self.assertEqual(styled_code, transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            var_style_config=VarStyleConfig(
                string_style_config=StringStyleConfig(
                    quotes="'", f_string_quotes=("'", '"')
                ),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            ),
        )
        code = textwrap.dedent(
            r"""
        test = '''
        test \\n string
        '''
        """
        )
        styled_code = textwrap.dedent(
            r"""
        test = '\ntest \\n string\n'
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            var_style_config=VarStyleConfig(
                string_style_config=StringStyleConfig(
                    quotes='"', f_string_quotes=("'", '"')
                ),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            ),
        )
        code = textwrap.dedent(
            """
        test = '''
        test string
        '''
        """
        )
        styled_code = textwrap.dedent(
            r"""
        test = "\ntest string\n"
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            var_style_config=VarStyleConfig(
                string_style_config=StringStyleConfig(
                    quotes='"', f_string_quotes=("'", '"')
                ),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            ),
        )
        code = textwrap.dedent(
            '''
        test = """
        test string
        """
        '''
        )
        styled_code = textwrap.dedent(
            r"""
        test = "\ntest string\n"
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            var_style_config=VarStyleConfig(
                string_style_config=StringStyleConfig(
                    quotes="'", f_string_quotes=("'", '"')
                ),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            ),
        )
        code = textwrap.dedent(
            '''
        test = """
        test string
        """
        '''
        )
        styled_code = textwrap.dedent(
            r"""
        test = '\ntest string\n'
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_multiline_f_string_to_single_line(self):
        codestyle = CodestyleConfig(
            blank_lines_top=0,
            blank_lines_bottom=0,
            var_style_config=VarStyleConfig(
                string_style_config=StringStyleConfig(
                    quotes="'", f_string_quotes=("'", '"')
                ),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            ),
        )
        code = textwrap.dedent(
            """
        test = f'''
        test string
        '''
        """
        )
        styled_code = r"test = f'\ntest string\n'"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=0,
            blank_lines_bottom=0,
            var_style_config=VarStyleConfig(
                string_style_config=StringStyleConfig(
                    quotes="'", f_string_quotes=("'", '"')
                ),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            ),
        )
        code = textwrap.dedent(
            r"""
        test = f'''
        test \\n string
        '''
        """
        )
        styled_code = r"test = f'\ntest \\n string\n'"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=0,
            blank_lines_bottom=0,
            var_style_config=VarStyleConfig(
                string_style_config=StringStyleConfig(
                    quotes='"', f_string_quotes=('"', "'")
                ),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            ),
        )
        code = textwrap.dedent(
            """
        test = f'''
        test string
        '''
        """
        )
        styled_code = r'test = f"\ntest string\n"'
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=0,
            blank_lines_bottom=0,
            var_style_config=VarStyleConfig(
                string_style_config=StringStyleConfig(
                    quotes='"', f_string_quotes=('"', "'")
                ),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            ),
        )
        code = textwrap.dedent(
            '''
        test = f"""
        test string
        """
        '''
        )
        styled_code = r'test = f"\ntest string\n"'
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=0,
            blank_lines_bottom=0,
            var_style_config=VarStyleConfig(
                string_style_config=StringStyleConfig(
                    quotes="'", f_string_quotes=("'", '"')
                ),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            ),
        )
        code = textwrap.dedent(
            '''
        test = f"""
        test string
        """
        '''
        )
        styled_code = r"test = f'\ntest string\n'"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_single_line_string_to_multiline(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            var_style_config=VarStyleConfig(
                string_style_config=StringStyleConfig(quotes="'''"),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            ),
        )
        code = textwrap.dedent(
            """
        test = "test string"
        """
        )
        styled_code = textwrap.dedent(
            """
        test = '''test string'''
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            var_style_config=VarStyleConfig(
                string_style_config=StringStyleConfig(quotes="'''"),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            ),
        )
        code = textwrap.dedent(
            """
        test = 'test string'
        """
        )
        styled_code = textwrap.dedent(
            """
        test = '''test string'''
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            var_style_config=VarStyleConfig(
                string_style_config=StringStyleConfig(quotes='"""'),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            ),
        )
        code = textwrap.dedent(
            """
        test = "test string"
        """
        )
        styled_code = textwrap.dedent(
            '''
        test = """test string"""
        '''
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            var_style_config=VarStyleConfig(
                string_style_config=StringStyleConfig(quotes='"""'),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            ),
        )
        code = textwrap.dedent(
            """
        test = 'test string'
        """
        )
        styled_code = textwrap.dedent(
            '''
        test = """test string"""
        '''
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_inner_f_string_double_quotes(self):
        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                string_style_config=StringStyleConfig(
                    quotes="'", f_string_quotes=("'", '"')
                ),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "test = f\"{'' + '42'} {call('string')}test'test\""
        styled_code = 'test = f\'{"" + "42"} {call("string")}test\\\'test\''
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                string_style_config=StringStyleConfig(
                    quotes="'", f_string_quotes=("'", '"')
                ),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "test(f\"{'' + '42'} {call('string')}test'test\")"
        styled_code = 'test(f\'{"" + "42"} {call("string")}test\\\'test\')'
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_inner_f_string_single_quotes(self):
        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                string_style_config=StringStyleConfig(
                    quotes='"', f_string_quotes=('"', "'")
                ),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = """test = f'{"" + "42"} {call("string")}test"test'"""
        styled_code = '''test = f"{'' + '42'} {call('string')}test\\"test"'''
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                string_style_config=StringStyleConfig(
                    quotes='"', f_string_quotes=('"', "'")
                ),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = """test(f'{"" + "42"} {call("string")}test"test')"""
        styled_code = """test(f"{'' + '42'} {call('string')}test\\"test")"""
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)


class TestFunctionStringStyle(unittest.TestCase):
    def test_double_quotes_to_single_quotes_string(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                var_style_config=VarStyleConfig(
                    string_style_config=StringStyleConfig(
                        quotes="'", f_string_quotes=("'", '"')
                    ),
                    whitespace_before_equal=" ",
                    whitespace_after_equal=" ",
                )
            ),
        )
        code = textwrap.dedent(
            """
        def test():
            test = "test"
        """
        )
        styled_code = textwrap.dedent(
            """
        def test():
            test = 'test'
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_double_quotes_to_double_quotes_string(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                var_style_config=VarStyleConfig(
                    string_style_config=StringStyleConfig(
                        quotes='"', f_string_quotes=("'", '"')
                    ),
                    whitespace_before_equal=" ",
                    whitespace_after_equal=" ",
                )
            ),
        )
        code = textwrap.dedent(
            """
        def test():
            test = "test"
        """
        )
        styled_code = textwrap.dedent(
            """
        def test():
            test = "test"
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_single_quotes_to_double_quotes_string(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                var_style_config=VarStyleConfig(
                    string_style_config=StringStyleConfig(
                        quotes='"', f_string_quotes=("'", '"')
                    ),
                    whitespace_before_equal=" ",
                    whitespace_after_equal=" ",
                )
            ),
        )
        code = textwrap.dedent(
            """
        def test():
            test = 'test'
        """
        )
        styled_code = textwrap.dedent(
            """
        def test():
            test = "test"
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_single_quotes_to_single_quotes_string(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                var_style_config=VarStyleConfig(
                    string_style_config=StringStyleConfig(
                        quotes="'", f_string_quotes=("'", '"')
                    ),
                    whitespace_before_equal=" ",
                    whitespace_after_equal=" ",
                )
            ),
        )
        code = textwrap.dedent(
            """
        def test():
            test = 'test'
        """
        )
        styled_code = textwrap.dedent(
            """
        def test():
            test = 'test'
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_raw_string(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                var_style_config=VarStyleConfig(
                    string_style_config=StringStyleConfig(
                        quotes="'", f_string_quotes=("'", '"')
                    ),
                    whitespace_before_equal=" ",
                    whitespace_after_equal=" ",
                )
            ),
        )
        code = textwrap.dedent(
            """
        def test():
            test = r'test'
        """
        )
        styled_code = textwrap.dedent(
            """
        def test():
            test = r'test'
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_raw_string_double_quotes(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                var_style_config=VarStyleConfig(
                    string_style_config=StringStyleConfig(
                        quotes='"', f_string_quotes=("'", '"')
                    ),
                    whitespace_before_equal=" ",
                    whitespace_after_equal=" ",
                )
            ),
        )
        code = textwrap.dedent(
            """
        def test():
            test = r"test"
        """
        )
        styled_code = textwrap.dedent(
            """
        def test():
            test = r"test"
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_raw_string_single_quotes_to_double_quotes(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                var_style_config=VarStyleConfig(
                    string_style_config=StringStyleConfig(
                        quotes='"', f_string_quotes=("'", '"')
                    ),
                    whitespace_before_equal=" ",
                    whitespace_after_equal=" ",
                )
            ),
        )
        code = textwrap.dedent(
            """
        def test():
            test = r'test'
        """
        )
        styled_code = textwrap.dedent(
            """
        def test():
            test = r"test"
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_raw_string_double_quotes_to_single_quotes(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                var_style_config=VarStyleConfig(
                    string_style_config=StringStyleConfig(
                        quotes="'", f_string_quotes=("'", '"')
                    ),
                    whitespace_before_equal=" ",
                    whitespace_after_equal=" ",
                )
            ),
        )
        code = textwrap.dedent(
            """
        def test():
            test = r"test"
        """
        )
        styled_code = textwrap.dedent(
            """
        def test():
            test = r'test'
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_formatted_string(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                var_style_config=VarStyleConfig(
                    string_style_config=StringStyleConfig(
                        quotes="'", f_string_quotes=("'", '"')
                    ),
                    whitespace_before_equal=" ",
                    whitespace_after_equal=" ",
                )
            ),
        )
        code = textwrap.dedent(
            """
        def test():
            test = f'test'
        """
        )
        styled_code = textwrap.dedent(
            """
        def test():
            test = f'test'
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                var_style_config=VarStyleConfig(
                    string_style_config=StringStyleConfig(
                        quotes="'", f_string_quotes=('"', '"')
                    ),
                    whitespace_before_equal=" ",
                    whitespace_after_equal=" ",
                )
            ),
        )
        code = textwrap.dedent(
            """
        def test():
            test = f'test'
        """
        )
        styled_code = textwrap.dedent(
            """
        def test():
            test = f"test"
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_formatted_string_double_quotes(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                var_style_config=VarStyleConfig(
                    string_style_config=StringStyleConfig(
                        quotes='"', f_string_quotes=('"', '"')
                    ),
                    whitespace_before_equal=" ",
                    whitespace_after_equal=" ",
                )
            ),
        )
        code = textwrap.dedent(
            """
        def test():
            test = f"test"
        """
        )
        styled_code = textwrap.dedent(
            """
        def test():
            test = f"test"
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_formatted_string_single_quotes_to_double_quotes(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                var_style_config=VarStyleConfig(
                    string_style_config=StringStyleConfig(
                        quotes='"', f_string_quotes=('"', '"')
                    ),
                    whitespace_before_equal=" ",
                    whitespace_after_equal=" ",
                )
            ),
        )
        code = textwrap.dedent(
            """
        def test():
            test = f'test'
        """
        )
        styled_code = textwrap.dedent(
            """
        def test():
            test = f"test"
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_formatted_string_double_quotes_to_single_quotes(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                var_style_config=VarStyleConfig(
                    string_style_config=StringStyleConfig(
                        quotes="'", f_string_quotes=("'", '"')
                    ),
                    whitespace_before_equal=" ",
                    whitespace_after_equal=" ",
                )
            ),
        )
        code = textwrap.dedent(
            """
        def test():
            test = f"test"
        """
        )
        styled_code = textwrap.dedent(
            """
        def test():
            test = f'test'
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_multiline_string_to_single_line(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                var_style_config=VarStyleConfig(
                    string_style_config=StringStyleConfig(
                        quotes="'", f_string_quotes=("'", '"')
                    ),
                    whitespace_before_equal=" ",
                    whitespace_after_equal=" ",
                )
            ),
        )
        code = textwrap.dedent(
            """
        def test():
            test = '''
            test string
            '''
        """
        )
        styled_code = textwrap.dedent(
            r"""
        def test():
            test = '\n    test string\n    '
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                var_style_config=VarStyleConfig(
                    string_style_config=StringStyleConfig(
                        quotes="'", f_string_quotes=("'", '"')
                    ),
                    whitespace_before_equal=" ",
                    whitespace_after_equal=" ",
                )
            ),
        )
        code = textwrap.dedent(
            r"""
        def test():
            test = '''
            test \\n string
            '''
        """
        )
        styled_code = textwrap.dedent(
            r"""
        def test():
            test = '\n    test \\n string\n    '
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                var_style_config=VarStyleConfig(
                    string_style_config=StringStyleConfig(
                        quotes='"', f_string_quotes=("'", '"')
                    ),
                    whitespace_before_equal=" ",
                    whitespace_after_equal=" ",
                )
            ),
        )
        code = textwrap.dedent(
            """
        def test():
            test = '''
            test string
            '''
        """
        )
        styled_code = textwrap.dedent(
            r"""
        def test():
            test = "\n    test string\n    "
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                var_style_config=VarStyleConfig(
                    string_style_config=StringStyleConfig(
                        quotes='"', f_string_quotes=("'", '"')
                    ),
                    whitespace_before_equal=" ",
                    whitespace_after_equal=" ",
                )
            ),
        )
        code = textwrap.dedent(
            '''
        def test():
            test = """
            test string
            """
        '''
        )
        styled_code = textwrap.dedent(
            r"""
        def test():
            test = "\n    test string\n    "
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                var_style_config=VarStyleConfig(
                    string_style_config=StringStyleConfig(
                        quotes="'", f_string_quotes=("'", '"')
                    ),
                    whitespace_before_equal=" ",
                    whitespace_after_equal=" ",
                )
            ),
        )
        code = textwrap.dedent(
            '''
        def test():
            test = """
            test string
            """
        '''
        )
        styled_code = textwrap.dedent(
            r"""
        def test():
            test = '\n    test string\n    '
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_multiline_f_string_to_single_line(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                var_style_config=VarStyleConfig(
                    string_style_config=StringStyleConfig(
                        quotes="'", f_string_quotes=("'", '"')
                    ),
                    whitespace_before_equal=" ",
                    whitespace_after_equal=" ",
                )
            ),
        )
        code = textwrap.dedent(
            """
        def test():
            test = f'''
            test string
            '''
          """
        )
        styled_code = textwrap.dedent(
            r"""
        def test():
            test = f'\n    test string\n    '
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                var_style_config=VarStyleConfig(
                    string_style_config=StringStyleConfig(
                        quotes="'", f_string_quotes=("'", '"')
                    ),
                    whitespace_before_equal=" ",
                    whitespace_after_equal=" ",
                )
            ),
        )
        code = textwrap.dedent(
            r"""
        def test():
            test = f'''
            test \\n string
            '''
        """
        )
        styled_code = textwrap.dedent(
            r"""
        def test():
            test = f'\n    test \\n string\n    '
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                var_style_config=VarStyleConfig(
                    string_style_config=StringStyleConfig(
                        quotes='"', f_string_quotes=('"', '"')
                    ),
                    whitespace_before_equal=" ",
                    whitespace_after_equal=" ",
                )
            ),
        )
        code = textwrap.dedent(
            """
        def test():
            test = f'''
            test string
            '''
        """
        )
        styled_code = textwrap.dedent(
            r"""
        def test():
            test = f"\n    test string\n    "
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                var_style_config=VarStyleConfig(
                    string_style_config=StringStyleConfig(
                        quotes='"', f_string_quotes=('"', '"')
                    ),
                    whitespace_before_equal=" ",
                    whitespace_after_equal=" ",
                )
            ),
        )
        code = textwrap.dedent(
            '''
        def test():
            test = f"""
            test string
            """
        '''
        )
        styled_code = textwrap.dedent(
            r"""
        def test():
            test = f"\n    test string\n    "
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                var_style_config=VarStyleConfig(
                    string_style_config=StringStyleConfig(
                        quotes="'", f_string_quotes=("'", '"')
                    ),
                    whitespace_before_equal=" ",
                    whitespace_after_equal=" ",
                )
            ),
        )
        code = textwrap.dedent(
            '''
        def test():
            test = f"""
            test string
            """
        '''
        )
        styled_code = textwrap.dedent(
            r"""
        def test():
            test = f'\n    test string\n    '
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_single_line_string_to_multiline(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                var_style_config=VarStyleConfig(
                    string_style_config=StringStyleConfig(quotes="'''"),
                    whitespace_before_equal=" ",
                    whitespace_after_equal=" ",
                )
            ),
        )
        code = textwrap.dedent(
            """
        def test():
            test = "test string"
        """
        )
        styled_code = textwrap.dedent(
            """
        def test():
            test = '''test string'''
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                var_style_config=VarStyleConfig(
                    string_style_config=StringStyleConfig(quotes="'''"),
                    whitespace_before_equal=" ",
                    whitespace_after_equal=" ",
                )
            ),
        )
        code = textwrap.dedent(
            """
        def test():
            test = 'test string'
        """
        )
        styled_code = textwrap.dedent(
            """
        def test():
            test = '''test string'''
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                var_style_config=VarStyleConfig(
                    string_style_config=StringStyleConfig(quotes='"""'),
                    whitespace_before_equal=" ",
                    whitespace_after_equal=" ",
                )
            ),
        )
        code = textwrap.dedent(
            """
        def test():
            test = "test string"
        """
        )
        styled_code = textwrap.dedent(
            '''
        def test():
            test = """test string"""
        '''
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                var_style_config=VarStyleConfig(
                    string_style_config=StringStyleConfig(quotes='"""'),
                    whitespace_before_equal=" ",
                    whitespace_after_equal=" ",
                )
            ),
        )
        code = textwrap.dedent(
            """
        def test():
            test = 'test string'
        """
        )
        styled_code = textwrap.dedent(
            '''
        def test():
            test = """test string"""
        '''
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_inner_f_string_double_quotes(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                var_style_config=VarStyleConfig(
                    string_style_config=StringStyleConfig(
                        quotes="'", f_string_quotes=("'", '"')
                    ),
                    whitespace_before_equal=" ",
                    whitespace_after_equal=" ",
                )
            ),
        )
        code = textwrap.dedent(
            """
        def test():
            test = f\"{'' + '42'} {call('string')}test'test\"
        """
        )
        styled_code = textwrap.dedent(
            """
        def test():
            test = f'{\"\" + \"42\"} {call(\"string\")}test\\'test'
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                var_style_config=VarStyleConfig(
                    string_style_config=StringStyleConfig(
                        quotes="'", f_string_quotes=("'", '"')
                    ),
                    whitespace_before_equal=" ",
                    whitespace_after_equal=" ",
                )
            ),
        )
        code = textwrap.dedent(
            """
        def test():
            test(f\"{'' + '42'} {call('string')}test'test\")
        """
        )
        styled_code = textwrap.dedent(
            """
        def test():
            test(f'{\"\" + \"42\"} {call(\"string\")}test\\'test')
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_inner_f_string_single_quotes(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                var_style_config=VarStyleConfig(
                    string_style_config=StringStyleConfig(
                        quotes='"', f_string_quotes=('"', "'")
                    ),
                    whitespace_before_equal=" ",
                    whitespace_after_equal=" ",
                )
            ),
        )
        code = textwrap.dedent(
            """
        def test():
            test = f'{"" + "42"} {call("string")}test"test'
        """
        )
        styled_code = textwrap.dedent(
            """
        def test():
            test = f"{'' + '42'} {call('string')}test\\"test"
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                var_style_config=VarStyleConfig(
                    string_style_config=StringStyleConfig(
                        quotes='"', f_string_quotes=('"', "'")
                    ),
                    whitespace_before_equal=" ",
                    whitespace_after_equal=" ",
                )
            ),
        )
        code = textwrap.dedent(
            """
        def test():
            test(f'{"" + "42"} {call("string")}test"test')
        """
        )
        styled_code = textwrap.dedent(
            """
        def test():
            test(f"{'' + '42'} {call('string')}test\\"test")
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)


class TestClassMethodStringStyle(unittest.TestCase):
    def test_double_quotes_to_single_quotes_string(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    var_style_config=VarStyleConfig(
                        string_style_config=StringStyleConfig(
                            quotes="'", f_string_quotes=("'", '"')
                        ),
                        whitespace_before_equal=" ",
                        whitespace_after_equal=" ",
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test():
                test = "test"
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def test():
                test = 'test'
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_double_quotes_to_double_quotes_string(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    var_style_config=VarStyleConfig(
                        string_style_config=StringStyleConfig(
                            quotes='"', f_string_quotes=("'", '"')
                        ),
                        whitespace_before_equal=" ",
                        whitespace_after_equal=" ",
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test():
                test = "test"
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def test():
                test = "test"
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_single_quotes_to_double_quotes_string(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    var_style_config=VarStyleConfig(
                        string_style_config=StringStyleConfig(
                            quotes='"', f_string_quotes=("'", '"')
                        ),
                        whitespace_before_equal=" ",
                        whitespace_after_equal=" ",
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test():
                test = 'test'
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def test():
                test = "test"
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_single_quotes_to_single_quotes_string(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    var_style_config=VarStyleConfig(
                        string_style_config=StringStyleConfig(
                            quotes="'", f_string_quotes=("'", '"')
                        ),
                        whitespace_before_equal=" ",
                        whitespace_after_equal=" ",
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test():
                test = 'test'
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def test():
                test = 'test'
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_raw_string(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    var_style_config=VarStyleConfig(
                        string_style_config=StringStyleConfig(
                            quotes="'", f_string_quotes=("'", '"')
                        ),
                        whitespace_before_equal=" ",
                        whitespace_after_equal=" ",
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test():
                test = r'test'
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def test():
                test = r'test'
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_raw_string_double_quotes(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    var_style_config=VarStyleConfig(
                        string_style_config=StringStyleConfig(
                            quotes='"', f_string_quotes=("'", '"')
                        ),
                        whitespace_before_equal=" ",
                        whitespace_after_equal=" ",
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test():
                test = r"test"
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def test():
                test = r"test"
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_raw_string_single_quotes_to_double_quotes(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    var_style_config=VarStyleConfig(
                        string_style_config=StringStyleConfig(
                            quotes='"', f_string_quotes=("'", '"')
                        ),
                        whitespace_before_equal=" ",
                        whitespace_after_equal=" ",
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test():
                test = r'test'
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def test():
                test = r"test"
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_raw_string_double_quotes_to_single_quotes(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    var_style_config=VarStyleConfig(
                        string_style_config=StringStyleConfig(
                            quotes="'", f_string_quotes=("'", '"')
                        ),
                        whitespace_before_equal=" ",
                        whitespace_after_equal=" ",
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test():
                test = r"test"
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def test():
                test = r'test'
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_formatted_string(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    var_style_config=VarStyleConfig(
                        string_style_config=StringStyleConfig(
                            quotes="'", f_string_quotes=("'", '"')
                        ),
                        whitespace_before_equal=" ",
                        whitespace_after_equal=" ",
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test():
                test = f'test'
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def test():
                test = f'test'
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    var_style_config=VarStyleConfig(
                        string_style_config=StringStyleConfig(
                            quotes="'", f_string_quotes=('"', '"')
                        ),
                        whitespace_before_equal=" ",
                        whitespace_after_equal=" ",
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test():
                test = f'test'
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def test():
                test = f"test"
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_formatted_string_double_quotes(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    var_style_config=VarStyleConfig(
                        string_style_config=StringStyleConfig(
                            quotes='"', f_string_quotes=('"', '"')
                        ),
                        whitespace_before_equal=" ",
                        whitespace_after_equal=" ",
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test():
                test = f"test"
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def test():
                test = f"test"
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_formatted_string_single_quotes_to_double_quotes(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    var_style_config=VarStyleConfig(
                        string_style_config=StringStyleConfig(
                            quotes='"', f_string_quotes=('"', '"')
                        ),
                        whitespace_before_equal=" ",
                        whitespace_after_equal=" ",
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test():
                test = f'test'
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def test():
                test = f"test"
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_formatted_string_double_quotes_to_single_quotes(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    var_style_config=VarStyleConfig(
                        string_style_config=StringStyleConfig(
                            quotes="'", f_string_quotes=("'", '"')
                        ),
                        whitespace_before_equal=" ",
                        whitespace_after_equal=" ",
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test():
                test = f"test"
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def test():
                test = f'test'
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_raw_bytes_string(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    var_style_config=VarStyleConfig(
                        string_style_config=StringStyleConfig(
                            quotes="'", f_string_quotes=("'", '"')
                        ),
                        whitespace_before_equal=" ",
                        whitespace_after_equal=" ",
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test():
                test = rb'test'
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def test():
                test = rb'test'
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    var_style_config=VarStyleConfig(
                        string_style_config=StringStyleConfig(
                            quotes="'", f_string_quotes=("'", '"')
                        ),
                        whitespace_before_equal=" ",
                        whitespace_after_equal=" ",
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test():
                test = rB'test'
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def test():
                test = rB'test'
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    var_style_config=VarStyleConfig(
                        string_style_config=StringStyleConfig(
                            quotes="'", f_string_quotes=("'", '"')
                        ),
                        whitespace_before_equal=" ",
                        whitespace_after_equal=" ",
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test():
                test = RB'test'
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def test():
                test = RB'test'
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    var_style_config=VarStyleConfig(
                        string_style_config=StringStyleConfig(
                            quotes="'", f_string_quotes=("'", '"')
                        ),
                        whitespace_before_equal=" ",
                        whitespace_after_equal=" ",
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test():
                test = Rb'test'
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def test():
                test = Rb'test'
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_raw_bytes_string_double_quotes(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    var_style_config=VarStyleConfig(
                        string_style_config=StringStyleConfig(
                            quotes='"', f_string_quotes=("'", '"')
                        ),
                        whitespace_before_equal=" ",
                        whitespace_after_equal=" ",
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test():
                test = rb"test"
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def test():
                test = rb"test"
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_raw_bytes_string_single_quotes_to_double_quotes(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    var_style_config=VarStyleConfig(
                        string_style_config=StringStyleConfig(
                            quotes='"', f_string_quotes=("'", '"')
                        ),
                        whitespace_before_equal=" ",
                        whitespace_after_equal=" ",
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test():
                test = rb'test'
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def test():
                test = rb"test"
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_raw_bytes_string_double_quotes_to_single_quotes(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    var_style_config=VarStyleConfig(
                        string_style_config=StringStyleConfig(
                            quotes="'", f_string_quotes=("'", '"')
                        ),
                        whitespace_before_equal=" ",
                        whitespace_after_equal=" ",
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test():
                test = rb"test"
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def test():
                test = rb'test'
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_multiline_string_to_single_line(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    var_style_config=VarStyleConfig(
                        string_style_config=StringStyleConfig(
                            quotes="'", f_string_quotes=("'", '"')
                        ),
                        whitespace_before_equal=" ",
                        whitespace_after_equal=" ",
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test():
                test = '''
                test string
                '''
        """
        )
        styled_code = textwrap.dedent(
            r"""
        class Test:
            def test():
                test = '\n        test string\n        '
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    var_style_config=VarStyleConfig(
                        string_style_config=StringStyleConfig(
                            quotes="'", f_string_quotes=("'", '"')
                        ),
                        whitespace_before_equal=" ",
                        whitespace_after_equal=" ",
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            r"""
        class Test:
            def test():
                test = '''
                test \\n string
                '''
        """
        )
        styled_code = textwrap.dedent(
            r"""
        class Test:
            def test():
                test = '\n        test \\n string\n        '
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    var_style_config=VarStyleConfig(
                        string_style_config=StringStyleConfig(
                            quotes='"', f_string_quotes=("'", '"')
                        ),
                        whitespace_before_equal=" ",
                        whitespace_after_equal=" ",
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test():
                test = '''
                test string
                '''
        """
        )
        styled_code = textwrap.dedent(
            r"""
        class Test:
            def test():
                test = "\n        test string\n        "
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    var_style_config=VarStyleConfig(
                        string_style_config=StringStyleConfig(
                            quotes='"', f_string_quotes=("'", '"')
                        ),
                        whitespace_before_equal=" ",
                        whitespace_after_equal=" ",
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            '''
        class Test:
            def test():
                test = """
                test string
                """
        '''
        )
        styled_code = textwrap.dedent(
            r"""
        class Test:
            def test():
                test = "\n        test string\n        "
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    var_style_config=VarStyleConfig(
                        string_style_config=StringStyleConfig(
                            quotes="'", f_string_quotes=("'", '"')
                        ),
                        whitespace_before_equal=" ",
                        whitespace_after_equal=" ",
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            '''
        class Test:
            def test():
                test = """
                test string
                """
        '''
        )
        styled_code = textwrap.dedent(
            r"""
        class Test:
            def test():
                test = '\n        test string\n        '
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_multiline_f_string_to_single_line(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    var_style_config=VarStyleConfig(
                        string_style_config=StringStyleConfig(
                            quotes="'", f_string_quotes=("'", '"')
                        ),
                        whitespace_before_equal=" ",
                        whitespace_after_equal=" ",
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test():
                test = f'''
                test string
                '''
          """
        )
        styled_code = textwrap.dedent(
            r"""
        class Test:
            def test():
                test = f'\n        test string\n        '
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    var_style_config=VarStyleConfig(
                        string_style_config=StringStyleConfig(
                            quotes="'", f_string_quotes=("'", '"')
                        ),
                        whitespace_before_equal=" ",
                        whitespace_after_equal=" ",
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            r"""
        class Test:
            def test():
                test = f'''
                test \n string
                '''
        """
        )
        styled_code = textwrap.dedent(
            r"""
        class Test:
            def test():
                test = f'\n        test \n string\n        '
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    var_style_config=VarStyleConfig(
                        string_style_config=StringStyleConfig(
                            quotes='"', f_string_quotes=('"', '"')
                        ),
                        whitespace_before_equal=" ",
                        whitespace_after_equal=" ",
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test():
                test = f'''
                test string
                '''
        """
        )
        styled_code = textwrap.dedent(
            r"""
        class Test:
            def test():
                test = f"\n        test string\n        "
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    var_style_config=VarStyleConfig(
                        string_style_config=StringStyleConfig(
                            quotes='"', f_string_quotes=('"', '"')
                        ),
                        whitespace_before_equal=" ",
                        whitespace_after_equal=" ",
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            '''
        class Test:
            def test():
                test = f"""
                test string
                """
        '''
        )
        styled_code = textwrap.dedent(
            r"""
        class Test:
            def test():
                test = f"\n        test string\n        "
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    var_style_config=VarStyleConfig(
                        string_style_config=StringStyleConfig(
                            quotes="'", f_string_quotes=("'", '"')
                        ),
                        whitespace_before_equal=" ",
                        whitespace_after_equal=" ",
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            '''
        class Test:
            def test():
                test = f"""
                test string
                """
        '''
        )
        styled_code = textwrap.dedent(
            r"""
        class Test:
            def test():
                test = f'\n        test string\n        '
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_single_line_string_to_multiline(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    var_style_config=VarStyleConfig(
                        string_style_config=StringStyleConfig(quotes="'''"),
                        whitespace_before_equal=" ",
                        whitespace_after_equal=" ",
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test():
                test = "test string"
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def test():
                test = '''test string'''
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    var_style_config=VarStyleConfig(
                        string_style_config=StringStyleConfig(quotes="'''"),
                        whitespace_before_equal=" ",
                        whitespace_after_equal=" ",
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test():
                test = 'test string'
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def test():
                test = '''test string'''
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    var_style_config=VarStyleConfig(
                        string_style_config=StringStyleConfig(quotes='"""'),
                        whitespace_before_equal=" ",
                        whitespace_after_equal=" ",
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test():
                test = "test string"
        """
        )
        styled_code = textwrap.dedent(
            '''
        class Test:
            def test():
                test = """test string"""
        '''
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    var_style_config=VarStyleConfig(
                        string_style_config=StringStyleConfig(quotes='"""'),
                        whitespace_before_equal=" ",
                        whitespace_after_equal=" ",
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test():
                test = 'test string'
        """
        )
        styled_code = textwrap.dedent(
            '''
        class Test:
            def test():
                test = """test string"""
        '''
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_inner_f_string_double_quotes(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    var_style_config=VarStyleConfig(
                        string_style_config=StringStyleConfig(
                            quotes="'", f_string_quotes=("'", '"')
                        ),
                        whitespace_before_equal=" ",
                        whitespace_after_equal=" ",
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test():
                test = f\"{'' + '42'} {call('string')}test'test\"
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def test():
                test = f'{\"\" + \"42\"} {call(\"string\")}test\\'test'
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    var_style_config=VarStyleConfig(
                        string_style_config=StringStyleConfig(
                            quotes="'", f_string_quotes=("'", '"')
                        ),
                        whitespace_before_equal=" ",
                        whitespace_after_equal=" ",
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test():
                test(f\"{'' + '42'} {call('string')}test'test\")
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def test():
                test(f'{\"\" + \"42\"} {call(\"string\")}test\\'test')
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_inner_f_string_single_quotes(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    var_style_config=VarStyleConfig(
                        string_style_config=StringStyleConfig(
                            quotes='"', f_string_quotes=('"', "'")
                        ),
                        whitespace_before_equal=" ",
                        whitespace_after_equal=" ",
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test():
                test = f'{"" + "42"} {call("string")}test"test'
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def test():
                test = f"{'' + '42'} {call('string')}test\\"test"
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    var_style_config=VarStyleConfig(
                        string_style_config=StringStyleConfig(
                            quotes='"', f_string_quotes=('"', "'")
                        ),
                        whitespace_before_equal=" ",
                        whitespace_after_equal=" ",
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test():
                test(f'{"" + "42"} {call("string")}test"test')
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def test():
                test(f"{'' + '42'} {call('string')}test\\"test")
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)


class TestEscaping(unittest.TestCase):
    def test_single_quote_escaping(self):
        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                string_style_config=StringStyleConfig(
                    quotes="'", f_string_quotes=("'", '"')
                ),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = 'test = "test\'test"'
        styled_code = "test = 'test\\'test'"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                string_style_config=StringStyleConfig(
                    quotes="'''", f_string_quotes=("'", '"')
                ),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = 'test = "test\'test"'
        styled_code = "test = '''test\\'test'''"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_single_quote_escaping_f_string(self):
        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                string_style_config=StringStyleConfig(
                    quotes="'", f_string_quotes=("'", '"')
                ),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = 'test = f"{t} {m}test\'test"'
        styled_code = "test = f'{t} {m}test\\'test'"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                string_style_config=StringStyleConfig(
                    quotes="'", f_string_quotes=("'''", '"')
                ),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = 'test = f"{t} {m}test\'test"'
        styled_code = "test = f'''{t} {m}test\\'test'''"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_double_quote_escaping(self):
        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                string_style_config=StringStyleConfig(
                    quotes='"', f_string_quotes=("'", '"')
                ),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "test = 'test\"test'"
        styled_code = 'test = "test\\"test"'
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                string_style_config=StringStyleConfig(
                    quotes='"""', f_string_quotes=("'", '"')
                ),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "test = 'test\"test'"
        styled_code = 'test = """test\\"test"""'
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_double_quote_escaping_f_string(self):
        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                string_style_config=StringStyleConfig(
                    quotes="'", f_string_quotes=('"', "'")
                ),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "test = f'{t} {m}test\"test'"
        styled_code = 'test = f"{t} {m}test\\"test"'
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                string_style_config=StringStyleConfig(
                    quotes="'", f_string_quotes=('"""', "'")
                ),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "test = f'{t} {m}test\"test'"
        styled_code = 'test = f"""{t} {m}test\\"test"""'
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_escaping_already_escaped_newline_string(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            var_style_config=VarStyleConfig(
                string_style_config=StringStyleConfig(
                    quotes="'", f_string_quotes=("'", '"')
                ),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            ),
        )
        code = textwrap.dedent(
            r"""
        test = '''
        test \\nstring
        '''
        """
        )
        styled_code = textwrap.dedent(
            r"""
        test = '\ntest \\nstring\n'
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_escaping_already_escaped_quotes_string(self):
        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                string_style_config=StringStyleConfig(
                    quotes='"', f_string_quotes=('"', "'")
                ),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "test = 'test\\'test'"
        styled_code = 'test = "test\\\'test"'
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                string_style_config=StringStyleConfig(
                    quotes='"', f_string_quotes=('"', "'")
                ),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = 'test = "test\\"test"'
        styled_code = 'test = "test\\"test"'
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                string_style_config=StringStyleConfig(
                    quotes="'", f_string_quotes=('"', "'")
                ),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "test = 'test\\'test'"
        styled_code = "test = 'test\\'test'"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                string_style_config=StringStyleConfig(
                    quotes="'", f_string_quotes=('"', "'")
                ),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = 'test = "test\\\'test"'
        styled_code = "test = 'test\\'test'"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_escaping_nested_f_strings(self):
        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                string_style_config=StringStyleConfig(
                    quotes="'", f_string_quotes=('"', "'")
                ),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = """test = f'{test} {f\"{test}\"}'"""
        styled_code = """test = f\"{test} {'nested fstring'}\""""
        transformed_code = apply_codestyle(code, codestyle)
        parse_module(transformed_code)
        self.assertEqual(styled_code, transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                string_style_config=StringStyleConfig(
                    quotes="'", f_string_quotes=('"', "'")
                ),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = """test = f\"\"\"{test} {f"{f'{test}'}"}\"\"\""""
        styled_code = """test = f\"{test} {'nested fstring'}\""""
        transformed_code = apply_codestyle(code, codestyle)
        parse_module(transformed_code)
        self.assertEqual(styled_code, transformed_code)

    def test_escaping_escaped_newline(self):
        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                string_style_config=StringStyleConfig(
                    quotes='"', f_string_quotes=('"', "'")
                ),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = textwrap.dedent(
            """
        test = '''\
        '''
        """
        )
        styled_code = r'test = "        "'
        transformed_code = apply_codestyle(code, codestyle)
        parse_module(transformed_code)
        self.assertEqual(styled_code, transformed_code)


class TestStringPrefix(unittest.TestCase):
    def test_f_prefix(self):
        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                string_style_config=StringStyleConfig(
                    quotes="'",
                    f_string_quotes=("'", '"'),
                    f_prefix=StringPrefixStyle.LOW,
                ),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "test = f'test test'"
        styled_code = "test = f'test test'"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                string_style_config=StringStyleConfig(
                    quotes="'",
                    f_string_quotes=("'", '"'),
                    f_prefix=StringPrefixStyle.UP,
                ),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "test = f'test test'"
        styled_code = "test = F'test test'"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                string_style_config=StringStyleConfig(
                    quotes="'",
                    f_string_quotes=("'", '"'),
                    f_prefix=StringPrefixStyle.UP,
                ),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "test = F'test test'"
        styled_code = "test = F'test test'"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                string_style_config=StringStyleConfig(
                    quotes="'",
                    f_string_quotes=("'", '"'),
                    f_prefix=StringPrefixStyle.LOW,
                ),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "test = F'test test'"
        styled_code = "test = f'test test'"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_r_prefix(self):
        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                string_style_config=StringStyleConfig(
                    quotes="'",
                    f_string_quotes=("'", '"'),
                    r_prefix=StringPrefixStyle.LOW,
                ),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "test = r'test test'"
        styled_code = "test = r'test test'"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                string_style_config=StringStyleConfig(
                    quotes="'",
                    f_string_quotes=("'", '"'),
                    r_prefix=StringPrefixStyle.UP,
                ),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "test = r'test test'"
        styled_code = "test = R'test test'"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                string_style_config=StringStyleConfig(
                    quotes="'",
                    f_string_quotes=("'", '"'),
                    r_prefix=StringPrefixStyle.UP,
                ),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "test = R'test test'"
        styled_code = "test = R'test test'"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                string_style_config=StringStyleConfig(
                    quotes="'",
                    f_string_quotes=("'", '"'),
                    r_prefix=StringPrefixStyle.LOW,
                ),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "test = R'test test'"
        styled_code = "test = r'test test'"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_b_prefix(self):
        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                string_style_config=StringStyleConfig(
                    quotes="'",
                    f_string_quotes=("'", '"'),
                    b_prefix=StringPrefixStyle.LOW,
                ),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "test = b'test test'"
        styled_code = "test = b'test test'"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                string_style_config=StringStyleConfig(
                    quotes="'",
                    f_string_quotes=("'", '"'),
                    b_prefix=StringPrefixStyle.UP,
                ),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "test = b'test test'"
        styled_code = "test = B'test test'"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                string_style_config=StringStyleConfig(
                    quotes="'",
                    f_string_quotes=("'", '"'),
                    b_prefix=StringPrefixStyle.UP,
                ),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "test = B'test test'"
        styled_code = "test = B'test test'"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                string_style_config=StringStyleConfig(
                    quotes="'",
                    f_string_quotes=("'", '"'),
                    b_prefix=StringPrefixStyle.LOW,
                ),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "test = B'test test'"
        styled_code = "test = b'test test'"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_u_prefix(self):
        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                string_style_config=StringStyleConfig(
                    quotes="'",
                    f_string_quotes=("'", '"'),
                    u_prefix=StringPrefixStyle.LOW,
                ),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "test = u'test test'"
        styled_code = "test = u'test test'"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                string_style_config=StringStyleConfig(
                    quotes="'",
                    f_string_quotes=("'", '"'),
                    u_prefix=StringPrefixStyle.UP,
                ),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "test = u'test test'"
        styled_code = "test = U'test test'"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                string_style_config=StringStyleConfig(
                    quotes="'",
                    f_string_quotes=("'", '"'),
                    u_prefix=StringPrefixStyle.UP,
                ),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "test = U'test test'"
        styled_code = "test = U'test test'"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                string_style_config=StringStyleConfig(
                    quotes="'",
                    f_string_quotes=("'", '"'),
                    u_prefix=StringPrefixStyle.LOW,
                ),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "test = U'test test'"
        styled_code = "test = u'test test'"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_fr_prefix(self):
        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                string_style_config=StringStyleConfig(
                    quotes="'",
                    f_string_quotes=("'", '"'),
                    f_prefix=StringPrefixStyle.LOW,
                    r_prefix=StringPrefixStyle.LOW,
                ),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "test = fr'test test'"
        styled_code = "test = fr'test test'"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                string_style_config=StringStyleConfig(
                    quotes="'",
                    f_string_quotes=("'", '"'),
                    f_prefix=StringPrefixStyle.UP,
                    r_prefix=StringPrefixStyle.LOW,
                ),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "test = fr'test test'"
        styled_code = "test = Fr'test test'"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                string_style_config=StringStyleConfig(
                    quotes="'",
                    f_string_quotes=("'", '"'),
                    f_prefix=StringPrefixStyle.LOW,
                    r_prefix=StringPrefixStyle.UP,
                ),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "test = fr'test test'"
        styled_code = "test = fR'test test'"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                string_style_config=StringStyleConfig(
                    quotes="'",
                    f_string_quotes=("'", '"'),
                    f_prefix=StringPrefixStyle.UP,
                    r_prefix=StringPrefixStyle.UP,
                ),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "test = fr'test test'"
        styled_code = "test = FR'test test'"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                string_style_config=StringStyleConfig(
                    quotes="'",
                    f_string_quotes=("'", '"'),
                    f_prefix=StringPrefixStyle.UP,
                    r_prefix=StringPrefixStyle.UP,
                ),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "test = FR'test test'"
        styled_code = "test = FR'test test'"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                string_style_config=StringStyleConfig(
                    quotes="'",
                    f_string_quotes=("'", '"'),
                    f_prefix=StringPrefixStyle.UP,
                    r_prefix=StringPrefixStyle.LOW,
                ),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "test = FR'test test'"
        styled_code = "test = Fr'test test'"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                string_style_config=StringStyleConfig(
                    quotes="'",
                    f_string_quotes=("'", '"'),
                    f_prefix=StringPrefixStyle.LOW,
                    r_prefix=StringPrefixStyle.UP,
                ),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "test = FR'test test'"
        styled_code = "test = fR'test test'"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                string_style_config=StringStyleConfig(
                    quotes="'",
                    f_string_quotes=("'", '"'),
                    f_prefix=StringPrefixStyle.LOW,
                    r_prefix=StringPrefixStyle.LOW,
                ),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "test = FR'test test'"
        styled_code = "test = fr'test test'"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                string_style_config=StringStyleConfig(
                    quotes="'",
                    f_string_quotes=("'", '"'),
                    f_prefix=StringPrefixStyle.LOW,
                    r_prefix=StringPrefixStyle.UP,
                ),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "test = Fr'test test'"
        styled_code = "test = fR'test test'"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                string_style_config=StringStyleConfig(
                    quotes="'",
                    f_string_quotes=("'", '"'),
                    f_prefix=StringPrefixStyle.LOW,
                    r_prefix=StringPrefixStyle.LOW,
                ),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "test = rf'test test'"
        styled_code = "test = rf'test test'"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                string_style_config=StringStyleConfig(
                    quotes="'",
                    f_string_quotes=("'", '"'),
                    f_prefix=StringPrefixStyle.UP,
                    r_prefix=StringPrefixStyle.UP,
                ),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "test = rf'test test'"
        styled_code = "test = RF'test test'"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_br_prefix(self):
        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                string_style_config=StringStyleConfig(
                    quotes="'",
                    f_string_quotes=("'", '"'),
                    b_prefix=StringPrefixStyle.LOW,
                    r_prefix=StringPrefixStyle.LOW,
                ),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "test = br'test test'"
        styled_code = "test = br'test test'"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                string_style_config=StringStyleConfig(
                    quotes="'",
                    f_string_quotes=("'", '"'),
                    b_prefix=StringPrefixStyle.UP,
                    r_prefix=StringPrefixStyle.LOW,
                ),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "test = br'test test'"
        styled_code = "test = Br'test test'"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                string_style_config=StringStyleConfig(
                    quotes="'",
                    f_string_quotes=("'", '"'),
                    b_prefix=StringPrefixStyle.LOW,
                    r_prefix=StringPrefixStyle.UP,
                ),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "test = br'test test'"
        styled_code = "test = bR'test test'"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                string_style_config=StringStyleConfig(
                    quotes="'",
                    f_string_quotes=("'", '"'),
                    b_prefix=StringPrefixStyle.UP,
                    r_prefix=StringPrefixStyle.UP,
                ),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "test = br'test test'"
        styled_code = "test = BR'test test'"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                string_style_config=StringStyleConfig(
                    quotes="'",
                    f_string_quotes=("'", '"'),
                    b_prefix=StringPrefixStyle.UP,
                    r_prefix=StringPrefixStyle.UP,
                ),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "test = BR'test test'"
        styled_code = "test = BR'test test'"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                string_style_config=StringStyleConfig(
                    quotes="'",
                    f_string_quotes=("'", '"'),
                    b_prefix=StringPrefixStyle.UP,
                    r_prefix=StringPrefixStyle.LOW,
                ),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "test = BR'test test'"
        styled_code = "test = Br'test test'"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                string_style_config=StringStyleConfig(
                    quotes="'",
                    f_string_quotes=("'", '"'),
                    b_prefix=StringPrefixStyle.LOW,
                    r_prefix=StringPrefixStyle.UP,
                ),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "test = BR'test test'"
        styled_code = "test = bR'test test'"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                string_style_config=StringStyleConfig(
                    quotes="'",
                    f_string_quotes=("'", '"'),
                    b_prefix=StringPrefixStyle.LOW,
                    r_prefix=StringPrefixStyle.LOW,
                ),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "test = BR'test test'"
        styled_code = "test = br'test test'"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                string_style_config=StringStyleConfig(
                    quotes="'",
                    f_string_quotes=("'", '"'),
                    b_prefix=StringPrefixStyle.LOW,
                    r_prefix=StringPrefixStyle.UP,
                ),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "test = Br'test test'"
        styled_code = "test = bR'test test'"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                string_style_config=StringStyleConfig(
                    quotes="'",
                    f_string_quotes=("'", '"'),
                    b_prefix=StringPrefixStyle.LOW,
                    r_prefix=StringPrefixStyle.LOW,
                ),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "test = rb'test test'"
        styled_code = "test = rb'test test'"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                string_style_config=StringStyleConfig(
                    quotes="'",
                    f_string_quotes=("'", '"'),
                    b_prefix=StringPrefixStyle.UP,
                    r_prefix=StringPrefixStyle.UP,
                ),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "test = rb'test test'"
        styled_code = "test = RB'test test'"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)


if __name__ == "__main__":
    unittest.main()
