import textwrap
import unittest

from learning_to_style_check.codestyle import (
    CodestyleConfig,
    ClassStyleConfig,
    NameStyleConfig,
    apply_codestyle,
    ArgsStyleConfig,
)
from learning_to_style_check.libcst import NameStyle, NameStylePrefix, parse_module, NameStyleSuffix


class TestClassNameStyle(unittest.TestCase):
    def test_class_name_to_letter_low(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                name_style_config=NameStyleConfig(
                    name_style=NameStyle.LetterLow,
                    whitespace_before="",
                    whitespace_after="",
                )
            ),
        )
        code = textwrap.dedent(
            """
        class test:
            pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class a:
            pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_class_name_to_letter_up(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                name_style_config=NameStyleConfig(
                    name_style=NameStyle.LetterUp,
                    whitespace_before="",
                    whitespace_after="",
                )
            ),
        )
        code = textwrap.dedent(
            """
        class test:
            pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class A:
            pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_class_name_to_upper_camel_case(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                name_style_config=NameStyleConfig(
                    name_style=NameStyle.UpperCamelCase,
                    whitespace_before="",
                    whitespace_after="",
                )
            ),
        )
        code = textwrap.dedent(
            """
        class test:
            pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class UpperCamelCase:
            pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_class_name_to_lower_camel_case(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                name_style_config=NameStyleConfig(
                    name_style=NameStyle.LowerCamelCase,
                    whitespace_before="",
                    whitespace_after="",
                )
            ),
        )
        code = textwrap.dedent(
            """
        class test:
            pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class lowerCamelCase:
            pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_class_name_to_screaming_snake_case(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                name_style_config=NameStyleConfig(
                    name_style=NameStyle.ScreamingSnakeCase,
                    whitespace_before="",
                    whitespace_after="",
                )
            ),
        )
        code = textwrap.dedent(
            """
        class test:
            pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class SCREAMING_SNAKE_CASE:
            pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_class_name_to_snake_case(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                name_style_config=NameStyleConfig(
                    name_style=NameStyle.SnakeCase,
                    whitespace_before="",
                    whitespace_after="",
                )
            ),
        )
        code = textwrap.dedent(
            """
        class test:
            pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class snake_case:
            pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_class_name_to_magic_name(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                name_style_config=NameStyleConfig(
                    name_style=NameStyle.MagicName,
                    whitespace_before="",
                    whitespace_after="",
                )
            ),
        )
        code = textwrap.dedent(
            """
        class test:
            pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class __magic_name__:
            pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_class_name_to_letter_low_with_single_leading_underscore(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                name_style_config=NameStyleConfig(
                    name_style=(
                        NameStylePrefix.SingleLeadingUnderscore,
                        NameStyle.LetterLow,
                    ),
                    whitespace_before="",
                    whitespace_after="",
                )
            ),
        )
        code = textwrap.dedent(
            """
        class test:
            pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class _a:
            pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_class_name_to_letter_up_with_single_leading_underscore(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                name_style_config=NameStyleConfig(
                    name_style=(
                        NameStylePrefix.SingleLeadingUnderscore,
                        NameStyle.LetterUp,
                    ),
                    whitespace_before="",
                    whitespace_after="",
                )
            ),
        )
        code = textwrap.dedent(
            """
        class test:
            pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class _A:
            pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_class_name_to_lowercase_with_single_leading_underscore(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                name_style_config=NameStyleConfig(
                    name_style=(
                        NameStylePrefix.SingleLeadingUnderscore,
                        NameStyle.Lowercase,
                    ),
                    whitespace_before="",
                    whitespace_after="",
                )
            ),
        )
        code = textwrap.dedent(
            """
        class test:
            pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class _lowercase:
            pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_class_name_to_lower_camel_case_with_single_leading_underscore(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                name_style_config=NameStyleConfig(
                    name_style=(
                        NameStylePrefix.SingleLeadingUnderscore,
                        NameStyle.LowerCamelCase,
                    ),
                    whitespace_before="",
                    whitespace_after="",
                )
            ),
        )
        code = textwrap.dedent(
            """
        class test:
            pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class _lowerCamelCase:
            pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_class_name_to_lower_camel_case_2_with_single_leading_underscore(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                name_style_config=NameStyleConfig(
                    name_style=(
                        NameStylePrefix.SingleLeadingUnderscore,
                        NameStyle.LowerCamelCase2,
                    ),
                    whitespace_before="",
                    whitespace_after="",
                )
            ),
        )
        code = textwrap.dedent(
            """
        class test:
            pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class _lowerCAmelCase:
            pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_class_name_to_upper_camel_case_with_single_leading_underscore(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                name_style_config=NameStyleConfig(
                    name_style=(
                        NameStylePrefix.SingleLeadingUnderscore,
                        NameStyle.UpperCamelCase,
                    ),
                    whitespace_before="",
                    whitespace_after="",
                )
            ),
        )
        code = textwrap.dedent(
            """
        class test:
            pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class _UpperCamelCase:
            pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_class_name_to_upper_camel_case_2_with_single_leading_underscore(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                name_style_config=NameStyleConfig(
                    name_style=(
                        NameStylePrefix.SingleLeadingUnderscore,
                        NameStyle.UpperCamelCase2,
                    ),
                    whitespace_before="",
                    whitespace_after="",
                )
            ),
        )
        code = textwrap.dedent(
            """
        class test:
            pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class _UpperCAmelCase:
            pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_class_name_to_screaming_snake_case_with_single_leading_underscore(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                name_style_config=NameStyleConfig(
                    name_style=(
                        NameStylePrefix.SingleLeadingUnderscore,
                        NameStyle.ScreamingSnakeCase,
                    ),
                    whitespace_before="",
                    whitespace_after="",
                )
            ),
        )
        code = textwrap.dedent(
            """
        class test:
            pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class _SCREAMING_SNAKE_CASE:
            pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_class_name_to_snake_case_with_single_leading_underscore(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                name_style_config=NameStyleConfig(
                    name_style=(
                        NameStylePrefix.SingleLeadingUnderscore,
                        NameStyle.SnakeCase,
                    ),
                    whitespace_before="",
                    whitespace_after="",
                )
            ),
        )
        code = textwrap.dedent(
            """
        class test:
            pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class _snake_case:
            pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_class_name_to_letter_low_with_double_leading_underscore(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                name_style_config=NameStyleConfig(
                    name_style=(
                        NameStylePrefix.DoubleLeadingUnderscore,
                        NameStyle.LetterLow,
                    ),
                    whitespace_before="",
                    whitespace_after="",
                )
            ),
        )
        code = textwrap.dedent(
            """
        class test:
            pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class __a:
            pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_class_name_to_letter_up_with_double_leading_underscore(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                name_style_config=NameStyleConfig(
                    name_style=(
                        NameStylePrefix.DoubleLeadingUnderscore,
                        NameStyle.LetterUp,
                    ),
                    whitespace_before="",
                    whitespace_after="",
                )
            ),
        )
        code = textwrap.dedent(
            """
        class test:
            pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class __A:
            pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_class_name_to_lowercase_with_double_leading_underscore(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                name_style_config=NameStyleConfig(
                    name_style=(
                        NameStylePrefix.DoubleLeadingUnderscore,
                        NameStyle.Lowercase,
                    ),
                    whitespace_before="",
                    whitespace_after="",
                )
            ),
        )
        code = textwrap.dedent(
            """
        class test:
            pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class __lowercase:
            pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_class_name_to_lower_camel_case_with_double_leading_underscore(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                name_style_config=NameStyleConfig(
                    name_style=(
                        NameStylePrefix.DoubleLeadingUnderscore,
                        NameStyle.LowerCamelCase,
                    ),
                    whitespace_before="",
                    whitespace_after="",
                )
            ),
        )
        code = textwrap.dedent(
            """
        class test:
            pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class __lowerCamelCase:
            pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_class_name_to_lower_camel_case_2_with_double_leading_underscore(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                name_style_config=NameStyleConfig(
                    name_style=(
                        NameStylePrefix.DoubleLeadingUnderscore,
                        NameStyle.LowerCamelCase2,
                    ),
                    whitespace_before="",
                    whitespace_after="",
                )
            ),
        )
        code = textwrap.dedent(
            """
        class test:
            pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class __lowerCAmelCase:
            pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_class_name_to_upper_camel_case_with_double_leading_underscore(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                name_style_config=NameStyleConfig(
                    name_style=(
                        NameStylePrefix.DoubleLeadingUnderscore,
                        NameStyle.UpperCamelCase,
                    ),
                    whitespace_before="",
                    whitespace_after="",
                )
            ),
        )
        code = textwrap.dedent(
            """
        class test:
            pass
        """
        )

        styled_code = textwrap.dedent(
            """
        class __UpperCamelCase:
            pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_class_name_to_upper_camel_case_2_with_double_leading_underscore(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                name_style_config=NameStyleConfig(
                    name_style=(
                        NameStylePrefix.DoubleLeadingUnderscore,
                        NameStyle.UpperCamelCase2,
                    ),
                    whitespace_before="",
                    whitespace_after="",
                )
            ),
        )
        code = textwrap.dedent(
            """
        class test:
            pass
        """
        )

        styled_code = textwrap.dedent(
            """
        class __UpperCAmelCase:
            pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_class_name_to_screaming_snake_case_with_double_leading_underscore(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                name_style_config=NameStyleConfig(
                    name_style=(
                        NameStylePrefix.DoubleLeadingUnderscore,
                        NameStyle.ScreamingSnakeCase,
                    ),
                    whitespace_before="",
                    whitespace_after="",
                )
            ),
        )
        code = textwrap.dedent(
            """
        class test:
            pass
        """
        )

        styled_code = textwrap.dedent(
            """
        class __SCREAMING_SNAKE_CASE:
            pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_class_name_to_snake_case_with_double_leading_underscore(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                name_style_config=NameStyleConfig(
                    name_style=(
                        NameStylePrefix.DoubleLeadingUnderscore,
                        NameStyle.SnakeCase,
                    ),
                    whitespace_before="",
                    whitespace_after="",
                )
            ),
        )
        code = textwrap.dedent(
            """
        class test:
            pass
        """
        )

        styled_code = textwrap.dedent(
            """
        class __snake_case:
            pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_class_name_to_lowercase_with_single_trailing_underscore(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                name_style_config=NameStyleConfig(
                    name_style=(
                        NameStyle.Lowercase,
                        NameStyleSuffix.SingleTrailingUnderscore
                    ),
                    whitespace_before="",
                    whitespace_after="",
                )
            ),
        )
        code = textwrap.dedent(
            """
        class test:
            pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class lowercase_:
            pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_class_name_to_lowercase_with_double_trailing_underscore(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                name_style_config=NameStyleConfig(
                    name_style=(
                        NameStyle.Lowercase,
                        NameStyleSuffix.DoubleTrailingUnderscore
                    ),
                    whitespace_before="",
                    whitespace_after="",
                )
            ),
        )
        code = textwrap.dedent(
            """
        class test:
            pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class lowercase__:
            pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)


class TestClassNameStylePadding(unittest.TestCase):
    def test_class_name_with_left_padding1(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                name_style_config=NameStyleConfig(
                    name_style=NameStyle.Lowercase,
                    whitespace_before=" ",
                    whitespace_after="",
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class  lowercase:
            pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_class_name_with_left_padding2(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                name_style_config=NameStyleConfig(
                    name_style=NameStyle.Lowercase,
                    whitespace_before="  ",
                    whitespace_after="",
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class   lowercase:
            pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_class_name_with_right_padding1(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                name_style_config=NameStyleConfig(
                    name_style=NameStyle.Lowercase,
                    whitespace_before="",
                    whitespace_after=" ",
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class lowercase :
            pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_class_name_with_right_padding2(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                name_style_config=NameStyleConfig(
                    name_style=NameStyle.Lowercase,
                    whitespace_before="",
                    whitespace_after="  ",
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class lowercase  :
            pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_class_name_with_left1_and_right1_padding(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                name_style_config=NameStyleConfig(
                    name_style=NameStyle.Lowercase,
                    whitespace_before=" ",
                    whitespace_after=" ",
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class  lowercase :
            pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_class_name_with_left2_and_right1_padding(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                name_style_config=NameStyleConfig(
                    name_style=NameStyle.Lowercase,
                    whitespace_before="  ",
                    whitespace_after=" ",
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class   lowercase :
            pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_class_name_with_left2_and_right3_padding(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                name_style_config=NameStyleConfig(
                    name_style=NameStyle.Lowercase,
                    whitespace_before="  ",
                    whitespace_after="   ",
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class   lowercase   :
            pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)


class TestClassArgsStyle(unittest.TestCase):
    def test_class_single_arg(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                args_style_config=ArgsStyleConfig(
                    whitespace_before_args="",
                    whitespace_after_args="",
                    name_style_config=NameStyleConfig(
                        name_style=NameStyle.LetterLow,
                        whitespace_before="",
                        whitespace_after="",
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class test(MyClass):
            pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class test(a):
            pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_class_args_to_letter_low(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                args_style_config=ArgsStyleConfig(
                    whitespace_before_args="",
                    whitespace_after_args="",
                    name_style_config=NameStyleConfig(
                        name_style=NameStyle.LetterLow,
                        whitespace_before="",
                        whitespace_after="",
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class test(MyClass, OtherClass):
            pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class test(a,a):
            pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_class_args_to_letter_up(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                args_style_config=ArgsStyleConfig(
                    whitespace_before_args="",
                    whitespace_after_args="",
                    name_style_config=NameStyleConfig(
                        name_style=NameStyle.LetterUp,
                        whitespace_before="",
                        whitespace_after="",
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class test(MyClass, OtherClass):
            pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class test(A,A):
            pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_class_args_to_lower_camel_case(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                args_style_config=ArgsStyleConfig(
                    whitespace_before_args="",
                    whitespace_after_args="",
                    name_style_config=NameStyleConfig(
                        name_style=NameStyle.LowerCamelCase,
                        whitespace_before="",
                        whitespace_after="",
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class test(MyClass, OtherClass):
            pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class test(lowerCamelCase,lowerCamelCase):
            pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_class_args_to_lower_camel_case_ignore_padding(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                args_style_config=ArgsStyleConfig(
                    whitespace_before_args="",
                    whitespace_after_args="",
                    name_style_config=NameStyleConfig(
                        name_style=NameStyle.LowerCamelCase,
                        whitespace_before="",
                        whitespace_after="",
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class test( MyClass , OtherClass ):
            pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class test(lowerCamelCase,lowerCamelCase):
            pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_class_args_to_lower_camel_case_with_left_right_padding(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                args_style_config=ArgsStyleConfig(
                    whitespace_before_args="",
                    whitespace_after_args="",
                    name_style_config=NameStyleConfig(
                        name_style=NameStyle.LowerCamelCase,
                        whitespace_before=" ",
                        whitespace_after=" ",
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class test(MyClass):
            pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class test(lowerCamelCase):
            pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                args_style_config=ArgsStyleConfig(
                    whitespace_before_args="",
                    whitespace_after_args="",
                    name_style_config=NameStyleConfig(
                        name_style=NameStyle.LowerCamelCase,
                        whitespace_before=" ",
                        whitespace_after="",
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class test(MyClass, OtherClass):
            pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class test(lowerCamelCase, lowerCamelCase):
            pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                args_style_config=ArgsStyleConfig(
                    whitespace_before_args="",
                    whitespace_after_args="",
                    name_style_config=NameStyleConfig(
                        name_style=NameStyle.LowerCamelCase,
                        whitespace_before=" ",
                        whitespace_after=" ",
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class test(MyClass, OtherClass):
            pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class test(lowerCamelCase , lowerCamelCase):
            pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                args_style_config=ArgsStyleConfig(
                    whitespace_before_args=" ",
                    whitespace_after_args=" ",
                    name_style_config=NameStyleConfig(
                        name_style=NameStyle.LowerCamelCase,
                        whitespace_before=" ",
                        whitespace_after=" ",
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class test(MyClass, OtherClass):
            pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class test( lowerCamelCase , lowerCamelCase ):
            pass
        """
        )

        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                args_style_config=ArgsStyleConfig(
                    whitespace_before_args="  ",
                    whitespace_after_args="  ",
                    name_style_config=NameStyleConfig(
                        name_style=NameStyle.LowerCamelCase,
                        whitespace_before=" ",
                        whitespace_after=" ",
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class test(MyClass, OtherClass):
            pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class test(  lowerCamelCase , lowerCamelCase  ):
            pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                args_style_config=ArgsStyleConfig(
                    whitespace_before_args=" ",
                    whitespace_after_args="  ",
                    name_style_config=NameStyleConfig(
                        name_style=NameStyle.LowerCamelCase,
                        whitespace_before="   ",
                        whitespace_after="     ",
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class test(MyClass, OtherClass):
            pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class test( lowerCamelCase     ,   lowerCamelCase  ):
            pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                args_style_config=ArgsStyleConfig(
                    whitespace_before_args=" ",
                    whitespace_after_args="  ",
                    name_style_config=NameStyleConfig(
                        name_style=NameStyle.LowerCamelCase,
                        whitespace_before="   ",
                        whitespace_after="     ",
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class test(  MyClass ,   OtherClass    ):
            pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class test( lowerCamelCase     ,   lowerCamelCase  ):
            pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_class_args_is_function(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                args_style_config=ArgsStyleConfig(
                    whitespace_before_args="",
                    whitespace_after_args="",
                    name_style_config=NameStyleConfig(
                        name_style=NameStyle.LetterLow,
                        whitespace_before="",
                        whitespace_after="",
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test(test(ab)):
            pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test(test(a)):
            pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                args_style_config=ArgsStyleConfig(
                    whitespace_before_args="",
                    whitespace_after_args="",
                    name_style_config=NameStyleConfig(
                        name_style=NameStyle.LetterLow,
                        whitespace_before="",
                        whitespace_after="",
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test(test(ab), TEST):
            pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test(test(a),a):
            pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)

    def test_class_args_to_lower_camel_case_with_single_leading_underscore(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                args_style_config=ArgsStyleConfig(
                    whitespace_before_args="",
                    whitespace_after_args="",
                    name_style_config=NameStyleConfig(
                        name_style=(
                            NameStylePrefix.SingleLeadingUnderscore,
                            NameStyle.LowerCamelCase,
                        ),
                        whitespace_before="",
                        whitespace_after="",
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class test(MyClass, OtherClass):
            pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class test(_lowerCamelCase,_lowerCamelCase):
            pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_class_args_to_lower_camel_case_with_double_leading_underscore(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                args_style_config=ArgsStyleConfig(
                    whitespace_before_args="",
                    whitespace_after_args="",
                    name_style_config=NameStyleConfig(
                        name_style=(
                            NameStylePrefix.DoubleLeadingUnderscore,
                            NameStyle.LowerCamelCase,
                        ),
                        whitespace_before="",
                        whitespace_after="",
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class test(MyClass, OtherClass):
            pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class test(__lowerCamelCase,__lowerCamelCase):
            pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_class_args_to_lower_camel_case_with_single_trailing_underscore(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                args_style_config=ArgsStyleConfig(
                    whitespace_before_args="",
                    whitespace_after_args="",
                    name_style_config=NameStyleConfig(
                        name_style=(
                            NameStyle.LowerCamelCase,
                            NameStyleSuffix.SingleTrailingUnderscore,
                        ),
                        whitespace_before="",
                        whitespace_after="",
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class test(MyClass, OtherClass):
            pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class test(lowerCamelCase_,lowerCamelCase_):
            pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_class_args_to_lower_camel_case_with_double_trailing_underscore(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                args_style_config=ArgsStyleConfig(
                    whitespace_before_args="",
                    whitespace_after_args="",
                    name_style_config=NameStyleConfig(
                        name_style=(
                            NameStyle.LowerCamelCase,
                            NameStyleSuffix.DoubleTrailingUnderscore,
                        ),
                        whitespace_before="",
                        whitespace_after="",
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class test(MyClass, OtherClass):
            pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class test(lowerCamelCase__,lowerCamelCase__):
            pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)


if __name__ == "__main__":
    unittest.main()
