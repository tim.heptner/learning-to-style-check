import textwrap
import unittest

from learning_to_style_check.codestyle import (
    CodestyleConfig,
    NameStyleConfig,
    apply_codestyle,
    FunctionStyleConfig,
    ArgsStyleConfig,
)
from learning_to_style_check.libcst import NameStyle, parse_module, NameStylePrefix, NameStyleSuffix


class TestFunctionNameStyle(unittest.TestCase):
    def test_function_name_to_letter_low(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                name_style_config=NameStyleConfig(
                    name_style=NameStyle.LetterLow,
                    whitespace_before="",
                    whitespace_after="",
                )
            ),
        )
        code = textwrap.dedent(
            """
        def test():
            pass
        """
        )
        styled_code = textwrap.dedent(
            """
        def a():
            pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_function_name_to_letter_up(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                name_style_config=NameStyleConfig(
                    name_style=NameStyle.LetterUp,
                    whitespace_before="",
                    whitespace_after="",
                )
            ),
        )
        code = textwrap.dedent(
            """
        def test():
            pass
        """
        )
        styled_code = textwrap.dedent(
            """
        def A():
            pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_function_name_to_upper_camel_case(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                name_style_config=NameStyleConfig(
                    name_style=NameStyle.UpperCamelCase,
                    whitespace_before="",
                    whitespace_after="",
                )
            ),
        )
        code = textwrap.dedent(
            """
        def test():
            pass
        """
        )
        styled_code = textwrap.dedent(
            """
        def UpperCamelCase():
            pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_function_name_to_lower_camel_case(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                name_style_config=NameStyleConfig(
                    name_style=NameStyle.LowerCamelCase,
                    whitespace_before="",
                    whitespace_after="",
                )
            ),
        )
        code = textwrap.dedent(
            """
        def test():
            pass
        """
        )
        styled_code = textwrap.dedent(
            """
        def lowerCamelCase():
            pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_function_name_to_screaming_snake_case(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                name_style_config=NameStyleConfig(
                    name_style=NameStyle.ScreamingSnakeCase,
                    whitespace_before="",
                    whitespace_after="",
                )
            ),
        )
        code = textwrap.dedent(
            """
        def test():
            pass
        """
        )
        styled_code = textwrap.dedent(
            """
        def SCREAMING_SNAKE_CASE():
            pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_function_name_to_snake_case(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                name_style_config=NameStyleConfig(
                    name_style=NameStyle.SnakeCase,
                    whitespace_before="",
                    whitespace_after="",
                )
            ),
        )
        code = textwrap.dedent(
            """
        def test():
            pass
        """
        )
        styled_code = textwrap.dedent(
            """
        def snake_case():
            pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_function_name_to_magic_name(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                name_style_config=NameStyleConfig(
                    name_style=NameStyle.MagicName,
                    whitespace_before="",
                    whitespace_after="",
                )
            ),
        )
        code = textwrap.dedent(
            """
        def test():
            pass
        """
        )
        styled_code = textwrap.dedent(
            """
        def __magic_name__():
            pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_function_name_to_lowercase(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                name_style_config=NameStyleConfig(
                    name_style=NameStyle.Lowercase,
                    whitespace_before="",
                    whitespace_after="",
                )
            ),
        )
        code = textwrap.dedent(
            """
        def TestName():
            pass
        """
        )
        styled_code = textwrap.dedent(
            """
        def lowercase():
            pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_function_name_to_lowercase_with_single_underscore_prefix(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                name_style_config=NameStyleConfig(
                    name_style=(NameStylePrefix.SingleLeadingUnderscore, NameStyle.Lowercase),
                    whitespace_before="",
                    whitespace_after="",
                )
            ),
        )
        code = textwrap.dedent(
            """
        def TestName():
            pass
        """
        )
        styled_code = textwrap.dedent(
            """
        def _lowercase():
            pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_function_name_to_lowercase_with_double_underscore_prefix(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                name_style_config=NameStyleConfig(
                    name_style=(NameStylePrefix.DoubleLeadingUnderscore, NameStyle.Lowercase),
                    whitespace_before="",
                    whitespace_after="",
                )
            ),
        )
        code = textwrap.dedent(
            """
        def TestName():
            pass
        """
        )
        styled_code = textwrap.dedent(
            """
        def __lowercase():
            pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_function_name_to_lowercase_with_single_underscore_suffix(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                name_style_config=NameStyleConfig(
                    name_style=(NameStyle.Lowercase, NameStyleSuffix.SingleTrailingUnderscore),
                    whitespace_before="",
                    whitespace_after="",
                )
            ),
        )
        code = textwrap.dedent(
            """
        def TestName():
            pass
        """
        )
        styled_code = textwrap.dedent(
            """
        def lowercase_():
            pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_function_name_to_lowercase_with_double_underscore_suffix(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                name_style_config=NameStyleConfig(
                    name_style=(NameStyle.Lowercase, NameStyleSuffix.DoubleTrailingUnderscore),
                    whitespace_before="",
                    whitespace_after="",
                )
            ),
        )
        code = textwrap.dedent(
            """
        def TestName():
            pass
        """
        )
        styled_code = textwrap.dedent(
            """
        def lowercase__():
            pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)


class TestFunctionArgsStyle(unittest.TestCase):
    def test_method_single_arg(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                args_style_config=ArgsStyleConfig(
                    name_style_config=NameStyleConfig(
                        name_style=NameStyle.LetterLow,
                        whitespace_before="",
                        whitespace_after="",
                    )
                )
            ),
        )
        code = textwrap.dedent(
            """
        def test(MyClass):
            pass
        """
        )
        styled_code = textwrap.dedent(
            """
        def test(a):
            pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_method_args_to_letter_low(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                args_style_config=ArgsStyleConfig(
                    name_style_config=NameStyleConfig(
                        name_style=NameStyle.LetterLow,
                        whitespace_before="",
                        whitespace_after="",
                    )
                )
            ),
        )
        code = textwrap.dedent(
            """
        def test(MyClass, OtherClass):
            pass
        """
        )
        styled_code = textwrap.dedent(
            """
        def test(a,a):
            pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_method_args_to_letter_up(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                args_style_config=ArgsStyleConfig(
                    name_style_config=NameStyleConfig(
                        name_style=NameStyle.LetterUp,
                        whitespace_before="",
                        whitespace_after="",
                    )
                )
            ),
        )
        code = textwrap.dedent(
            """
        def test(MyClass, OtherClass):
            pass
        """
        )
        styled_code = textwrap.dedent(
            """
        def test(A,A):
            pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_method_args_lowercase_with_single_underscore_prefix(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                args_style_config=ArgsStyleConfig(
                    name_style_config=NameStyleConfig(
                        name_style=(NameStylePrefix.SingleLeadingUnderscore, NameStyle.Lowercase),
                        whitespace_before="",
                        whitespace_after="",
                    )
                )
            ),
        )
        code = textwrap.dedent(
            """
        def test(MyClass, OtherClass):
            pass
        """
        )
        styled_code = textwrap.dedent(
            """
        def test(_lowercase,_lowercase):
            pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_method_args_lowercase_with_double_underscore_prefix(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                args_style_config=ArgsStyleConfig(
                    name_style_config=NameStyleConfig(
                        name_style=(NameStylePrefix.DoubleLeadingUnderscore, NameStyle.Lowercase),
                        whitespace_before="",
                        whitespace_after="",
                    )
                )
            ),
        )
        code = textwrap.dedent(
            """
        def test(MyClass, OtherClass):
            pass
        """
        )
        styled_code = textwrap.dedent(
            """
        def test(__lowercase,__lowercase):
            pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_method_args_lowercase_with_single_underscore_suffix(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                args_style_config=ArgsStyleConfig(
                    name_style_config=NameStyleConfig(
                        name_style=(NameStyle.Lowercase, NameStyleSuffix.SingleTrailingUnderscore),
                        whitespace_before="",
                        whitespace_after="",
                    )
                )
            ),
        )
        code = textwrap.dedent(
            """
        def test(MyClass, OtherClass):
            pass
        """
        )
        styled_code = textwrap.dedent(
            """
        def test(lowercase_,lowercase_):
            pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_method_args_lowercase_with_double_underscore_suffix(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                args_style_config=ArgsStyleConfig(
                    name_style_config=NameStyleConfig(
                        name_style=(NameStyle.Lowercase, NameStyleSuffix.DoubleTrailingUnderscore),
                        whitespace_before="",
                        whitespace_after="",
                    )
                )
            ),
        )
        code = textwrap.dedent(
            """
        def test(MyClass, OtherClass):
            pass
        """
        )
        styled_code = textwrap.dedent(
            """
        def test(lowercase__,lowercase__):
            pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_method_args_to_lower_camel_case(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                args_style_config=ArgsStyleConfig(
                    name_style_config=NameStyleConfig(
                        name_style=NameStyle.LowerCamelCase,
                        whitespace_before="",
                        whitespace_after="",
                    )
                )
            ),
        )
        code = textwrap.dedent(
            """
        def test(MyClass, OtherClass):
            pass
        """
        )
        styled_code = textwrap.dedent(
            """
        def test(lowerCamelCase,lowerCamelCase):
            pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_method_args_to_lower_camel_case_ignore_padding(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                args_style_config=ArgsStyleConfig(
                    name_style_config=NameStyleConfig(
                        name_style=NameStyle.LowerCamelCase,
                        whitespace_before="",
                        whitespace_after="",
                    )
                )
            ),
        )
        code = textwrap.dedent(
            """
        def test( MyClass , OtherClass ):
            pass
        """
        )
        styled_code = textwrap.dedent(
            """
        def test(lowerCamelCase,lowerCamelCase):
            pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_method_args_to_lower_camel_case_with_left1_right0_padding(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                args_style_config=ArgsStyleConfig(
                    whitespace_before_args="",
                    whitespace_after_args="",
                    name_style_config=NameStyleConfig(
                        name_style=NameStyle.LowerCamelCase,
                        whitespace_before=" ",
                        whitespace_after=" ",
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        def test(MyClass):
            pass
        """
        )
        styled_code = textwrap.dedent(
            """
        def test(lowerCamelCase):
            pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                args_style_config=ArgsStyleConfig(
                    whitespace_before_args="",
                    whitespace_after_args="",
                    name_style_config=NameStyleConfig(
                        name_style=NameStyle.LowerCamelCase,
                        whitespace_before=" ",
                        whitespace_after="",
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        def test(MyClass, OtherClass):
            pass
        """
        )
        styled_code = textwrap.dedent(
            """
        def test(lowerCamelCase, lowerCamelCase):
            pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                args_style_config=ArgsStyleConfig(
                    whitespace_before_args="",
                    whitespace_after_args="",
                    name_style_config=NameStyleConfig(
                        name_style=NameStyle.LowerCamelCase,
                        whitespace_before=" ",
                        whitespace_after=" ",
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        def test(MyClass, OtherClass):
            pass
        """
        )
        styled_code = textwrap.dedent(
            """
        def test(lowerCamelCase , lowerCamelCase):
            pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                args_style_config=ArgsStyleConfig(
                    whitespace_before_args=" ",
                    whitespace_after_args=" ",
                    name_style_config=NameStyleConfig(
                        name_style=NameStyle.LowerCamelCase,
                        whitespace_before=" ",
                        whitespace_after=" ",
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        def test(MyClass, OtherClass):
            pass
        """
        )
        styled_code = textwrap.dedent(
            """
        def test( lowerCamelCase , lowerCamelCase ):
            pass
        """
        )

        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                args_style_config=ArgsStyleConfig(
                    whitespace_before_args="  ",
                    whitespace_after_args="  ",
                    name_style_config=NameStyleConfig(
                        name_style=NameStyle.LowerCamelCase,
                        whitespace_before=" ",
                        whitespace_after=" ",
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        def test(MyClass, OtherClass):
            pass
        """
        )
        styled_code = textwrap.dedent(
            """
        def test(  lowerCamelCase , lowerCamelCase  ):
            pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                args_style_config=ArgsStyleConfig(
                    whitespace_before_args=" ",
                    whitespace_after_args="  ",
                    name_style_config=NameStyleConfig(
                        name_style=NameStyle.LowerCamelCase,
                        whitespace_before="   ",
                        whitespace_after="     ",
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        def test(MyClass, OtherClass):
            pass
        """
        )
        styled_code = textwrap.dedent(
            """
        def test( lowerCamelCase     ,   lowerCamelCase  ):
            pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                args_style_config=ArgsStyleConfig(
                    whitespace_before_args=" ",
                    whitespace_after_args="  ",
                    name_style_config=NameStyleConfig(
                        name_style=NameStyle.LowerCamelCase,
                        whitespace_before="   ",
                        whitespace_after="     ",
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        def test(  MyClass ,   OtherClass    ):
            pass
        """
        )
        styled_code = textwrap.dedent(
            """
        def test( lowerCamelCase     ,   lowerCamelCase  ):
            pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)


class TestKeepSpecialNames(unittest.TestCase):
    def test_keep_init(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                name_style_config=NameStyleConfig(
                    name_style=NameStyle.LowerCamelCase,
                )
            ),
        )
        code = textwrap.dedent(
            """
        def __init__():
            pass
        """
        )
        styled_code = textwrap.dedent(
            """
        def __init__():
            pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_keep_self(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                name_style_config=NameStyleConfig(
                    name_style=NameStyle.LowerCamelCase,
                ),
                args_style_config=ArgsStyleConfig(
                    name_style_config=NameStyleConfig(
                        name_style=NameStyle.LowerCamelCase,
                    )
                ),
            ),
        )
        code = textwrap.dedent(
            """
        def test(self):
            pass
        """
        )
        styled_code = textwrap.dedent(
            """
        def lowerCamelCase(self):
            pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                name_style_config=NameStyleConfig(
                    name_style=NameStyle.LowerCamelCase,
                ),
                args_style_config=ArgsStyleConfig(
                    name_style_config=NameStyleConfig(
                        name_style=NameStyle.LowerCamelCase,
                    )
                ),
            ),
        )
        code = textwrap.dedent(
            """
        def test(self2):
            pass
        """
        )
        styled_code = textwrap.dedent(
            """
        def lowerCamelCase(lowerCamelCase):
            pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)


if __name__ == "__main__":
    unittest.main()
