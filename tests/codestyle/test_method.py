import textwrap
import unittest

from learning_to_style_check.codestyle import (
    CodestyleConfig,
    ClassStyleConfig,
    NameStyleConfig,
    apply_codestyle,
    ArgsStyleConfig,
    FunctionStyleConfig,
)
from learning_to_style_check.libcst import NameStyle, NameStylePrefix, parse_module, NameStyleSuffix


class TestMethodNameStyle(unittest.TestCase):
    def test_method_name_to_letter_low(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    blank_lines_after=0,
                    name_style_config=NameStyleConfig(
                        name_style=NameStyle.LetterLow,
                        whitespace_before="",
                        whitespace_after="",
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test(self):
                pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def a(self):
                pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_method_name_to_letter_up(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    blank_lines_after=0,
                    name_style_config=NameStyleConfig(
                        name_style=NameStyle.LetterUp,
                        whitespace_before="",
                        whitespace_after="",
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test(self):
                pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def A(self):
                pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_method_name_to_upper_camel_case(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    blank_lines_after=0,
                    name_style_config=NameStyleConfig(
                        name_style=NameStyle.UpperCamelCase,
                        whitespace_before="",
                        whitespace_after="",
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test(self):
                pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def UpperCamelCase(self):
                pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_method_name_to_lower_camel_case(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    blank_lines_after=0,
                    name_style_config=NameStyleConfig(
                        name_style=NameStyle.LowerCamelCase,
                        whitespace_before="",
                        whitespace_after="",
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test(self):
                pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def lowerCamelCase(self):
                pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_method_name_to_screaming_snake_case(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    blank_lines_after=0,
                    name_style_config=NameStyleConfig(
                        name_style=NameStyle.ScreamingSnakeCase,
                        whitespace_before="",
                        whitespace_after="",
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test(self):
                pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def SCREAMING_SNAKE_CASE(self):
                pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_method_name_to_snake_case(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    blank_lines_after=0,
                    name_style_config=NameStyleConfig(
                        name_style=NameStyle.SnakeCase,
                        whitespace_before="",
                        whitespace_after="",
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test(self):
                pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def snake_case(self):
                pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_method_name_to_magic_name(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    blank_lines_after=0,
                    name_style_config=NameStyleConfig(
                        name_style=NameStyle.MagicName,
                        whitespace_before="",
                        whitespace_after="",
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test(self):
                pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def __magic_name__(self):
                pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_method_name_to_letter_low_with_single_leading_underscore(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    blank_lines_after=0,
                    name_style_config=NameStyleConfig(
                        name_style=(
                            NameStylePrefix.SingleLeadingUnderscore,
                            NameStyle.LetterLow,
                        ),
                        whitespace_before="",
                        whitespace_after="",
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test(self):
                pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def _a(self):
                pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_method_name_to_letter_up_with_single_leading_underscore(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    blank_lines_after=0,
                    name_style_config=NameStyleConfig(
                        name_style=(
                            NameStylePrefix.SingleLeadingUnderscore,
                            NameStyle.LetterUp,
                        ),
                        whitespace_before="",
                        whitespace_after="",
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test(self):
                pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def _A(self):
                pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_method_name_to_lowercase_with_single_leading_underscore(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    blank_lines_after=0,
                    name_style_config=NameStyleConfig(
                        name_style=(
                            NameStylePrefix.SingleLeadingUnderscore,
                            NameStyle.Lowercase,
                        ),
                        whitespace_before="",
                        whitespace_after="",
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test(self):
                pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def _lowercase(self):
                pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_method_name_to_lower_camel_case_with_single_leading_underscore(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    blank_lines_after=0,
                    name_style_config=NameStyleConfig(
                        name_style=(
                            NameStylePrefix.SingleLeadingUnderscore,
                            NameStyle.LowerCamelCase,
                        ),
                        whitespace_before="",
                        whitespace_after="",
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test(self):
                pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def _lowerCamelCase(self):
                pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_method_name_to_lower_camel_case_2_with_single_leading_underscore(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    blank_lines_after=0,
                    name_style_config=NameStyleConfig(
                        name_style=(
                            NameStylePrefix.SingleLeadingUnderscore,
                            NameStyle.LowerCamelCase2,
                        ),
                        whitespace_before="",
                        whitespace_after="",
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test(self):
                pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def _lowerCAmelCase(self):
                pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_method_name_to_upper_camel_case_with_single_leading_underscore(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    blank_lines_after=0,
                    name_style_config=NameStyleConfig(
                        name_style=(
                            NameStylePrefix.SingleLeadingUnderscore,
                            NameStyle.UpperCamelCase,
                        ),
                        whitespace_before="",
                        whitespace_after="",
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test(self):
                pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def _UpperCamelCase(self):
                pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_method_name_to_upper_camel_case_2_with_single_leading_underscore(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    blank_lines_after=0,
                    name_style_config=NameStyleConfig(
                        name_style=(
                            NameStylePrefix.SingleLeadingUnderscore,
                            NameStyle.UpperCamelCase2,
                        ),
                        whitespace_before="",
                        whitespace_after="",
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test(self):
                pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def _UpperCAmelCase(self):
                pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_method_name_to_screaming_snake_case_with_single_leading_underscore(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    blank_lines_after=0,
                    name_style_config=NameStyleConfig(
                        name_style=(
                            NameStylePrefix.SingleLeadingUnderscore,
                            NameStyle.ScreamingSnakeCase,
                        ),
                        whitespace_before="",
                        whitespace_after="",
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test(self):
                pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def _SCREAMING_SNAKE_CASE(self):
                pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_method_name_to_snake_case_with_single_leading_underscore(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    blank_lines_after=0,
                    name_style_config=NameStyleConfig(
                        name_style=(
                            NameStylePrefix.SingleLeadingUnderscore,
                            NameStyle.SnakeCase,
                        ),
                        whitespace_before="",
                        whitespace_after="",
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test(self):
                pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def _snake_case(self):
                pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_method_name_to_letter_low_with_double_leading_underscore(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    blank_lines_after=0,
                    name_style_config=NameStyleConfig(
                        name_style=(
                            NameStylePrefix.DoubleLeadingUnderscore,
                            NameStyle.LetterLow,
                        ),
                        whitespace_before="",
                        whitespace_after="",
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test(self):
                pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def __a(self):
                pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_method_name_to_letter_up_with_double_leading_underscore(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    blank_lines_after=0,
                    name_style_config=NameStyleConfig(
                        name_style=(
                            NameStylePrefix.DoubleLeadingUnderscore,
                            NameStyle.LetterUp,
                        ),
                        whitespace_before="",
                        whitespace_after="",
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test(self):
                pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def __A(self):
                pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_method_name_to_lowercase_with_double_leading_underscore(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    blank_lines_after=0,
                    name_style_config=NameStyleConfig(
                        name_style=(
                            NameStylePrefix.DoubleLeadingUnderscore,
                            NameStyle.Lowercase,
                        ),
                        whitespace_before="",
                        whitespace_after="",
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test(self):
                pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def __lowercase(self):
                pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_method_name_to_lowercase_with_double_trailing_underscore(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    blank_lines_after=0,
                    name_style_config=NameStyleConfig(
                        name_style=(
                            NameStyle.Lowercase,
                            NameStyleSuffix.DoubleTrailingUnderscore
                        ),
                        whitespace_before="",
                        whitespace_after="",
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test(self):
                pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def lowercase__(self):
                pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_method_name_to_lower_camel_case_with_double_leading_underscore(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    blank_lines_after=0,
                    name_style_config=NameStyleConfig(
                        name_style=(
                            NameStylePrefix.DoubleLeadingUnderscore,
                            NameStyle.LowerCamelCase,
                        ),
                        whitespace_before="",
                        whitespace_after="",
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test(self):
                pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def __lowerCamelCase(self):
                pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_method_name_to_lower_camel_case_2_with_double_leading_underscore(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    blank_lines_after=0,
                    name_style_config=NameStyleConfig(
                        name_style=(
                            NameStylePrefix.DoubleLeadingUnderscore,
                            NameStyle.LowerCamelCase2,
                        ),
                        whitespace_before="",
                        whitespace_after="",
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test(self):
                pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def __lowerCAmelCase(self):
                pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_method_name_to_upper_camel_case_with_double_leading_underscore(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    blank_lines_after=0,
                    name_style_config=NameStyleConfig(
                        name_style=(
                            NameStylePrefix.DoubleLeadingUnderscore,
                            NameStyle.UpperCamelCase,
                        ),
                        whitespace_before="",
                        whitespace_after="",
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test(self):
                pass
        """
        )

        styled_code = textwrap.dedent(
            """
        class Test:
            def __UpperCamelCase(self):
                pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_method_name_to_upper_camel_case_2_with_double_leading_underscore(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    blank_lines_after=0,
                    name_style_config=NameStyleConfig(
                        name_style=(
                            NameStylePrefix.DoubleLeadingUnderscore,
                            NameStyle.UpperCamelCase2,
                        ),
                        whitespace_before="",
                        whitespace_after="",
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test(self):
                pass
        """
        )

        styled_code = textwrap.dedent(
            """
        class Test:
            def __UpperCAmelCase(self):
                pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_method_name_to_screaming_snake_case_with_double_leading_underscore(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    blank_lines_after=0,
                    name_style_config=NameStyleConfig(
                        name_style=(
                            NameStylePrefix.DoubleLeadingUnderscore,
                            NameStyle.ScreamingSnakeCase,
                        ),
                        whitespace_before="",
                        whitespace_after="",
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test(self):
                pass
        """
        )

        styled_code = textwrap.dedent(
            """
        class Test:
            def __SCREAMING_SNAKE_CASE(self):
                pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_method_name_to_snake_case_with_double_leading_underscore(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    blank_lines_after=0,
                    name_style_config=NameStyleConfig(
                        name_style=(
                            NameStylePrefix.DoubleLeadingUnderscore,
                            NameStyle.SnakeCase,
                        ),
                        whitespace_before="",
                        whitespace_after="",
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test(self):
                pass
        """
        )

        styled_code = textwrap.dedent(
            """
        class Test:
            def __snake_case(self):
                pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)


class TestMethodNameStylePadding(unittest.TestCase):
    def test_method_name_with_left_padding1(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    blank_lines_after=0,
                    name_style_config=NameStyleConfig(
                        name_style=NameStyle.Lowercase,
                        whitespace_before=" ",
                        whitespace_after="",
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test(self):
                pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def  lowercase(self):
                pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_method_name_with_left_padding2(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    blank_lines_after=0,
                    name_style_config=NameStyleConfig(
                        name_style=NameStyle.Lowercase,
                        whitespace_before="  ",
                        whitespace_after="",
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test(self):
                pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def   lowercase(self):
                pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_method_name_with_right_padding1(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    blank_lines_after=0,
                    name_style_config=NameStyleConfig(
                        name_style=NameStyle.Lowercase,
                        whitespace_before="",
                        whitespace_after=" ",
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test(self):
                pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def lowercase (self):
                pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_method_name_with_right_padding2(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    blank_lines_after=0,
                    name_style_config=NameStyleConfig(
                        name_style=NameStyle.Lowercase,
                        whitespace_before="",
                        whitespace_after="  ",
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test(self):
                pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def lowercase  (self):
                pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_method_name_with_left1_and_right1_padding(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    blank_lines_after=0,
                    name_style_config=NameStyleConfig(
                        name_style=NameStyle.Lowercase,
                        whitespace_before=" ",
                        whitespace_after=" ",
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test(self):
                pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def  lowercase (self):
                pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_method_name_with_left2_and_right1_padding(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    blank_lines_after=0,
                    name_style_config=NameStyleConfig(
                        name_style=NameStyle.Lowercase,
                        whitespace_before="  ",
                        whitespace_after=" ",
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test(self):
                pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def   lowercase (self):
                pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_method_name_with_left2_and_right3_padding(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    blank_lines_after=0,
                    name_style_config=NameStyleConfig(
                        name_style=NameStyle.Lowercase,
                        whitespace_before="  ",
                        whitespace_after="   ",
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test(self):
                pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def   lowercase   (self):
                pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)


class TestMethodArgsStyle(unittest.TestCase):
    def test_method_single_arg(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    blank_lines_after=0,
                    args_style_config=ArgsStyleConfig(
                        name_style_config=NameStyleConfig(
                            name_style=NameStyle.LetterLow,
                            whitespace_before="",
                            whitespace_after="",
                        )
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test(self, var1):
                pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def test(self,a):
                pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_method_args_to_letter_low(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    blank_lines_after=0,
                    args_style_config=ArgsStyleConfig(
                        name_style_config=NameStyleConfig(
                            name_style=NameStyle.LetterLow,
                            whitespace_before="",
                            whitespace_after="",
                        )
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test(self, var1, var2):
                pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def test(self,a,a):
                pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_method_args_to_letter_up(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    blank_lines_after=0,
                    args_style_config=ArgsStyleConfig(
                        name_style_config=NameStyleConfig(
                            name_style=NameStyle.LetterUp,
                            whitespace_before="",
                            whitespace_after="",
                        )
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test(self, var1, var2):
                pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def test(self,A,A):
                pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_method_args_to_lower_camel_case(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    blank_lines_after=0,
                    args_style_config=ArgsStyleConfig(
                        name_style_config=NameStyleConfig(
                            name_style=NameStyle.LowerCamelCase,
                            whitespace_before="",
                            whitespace_after="",
                        )
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test(self, var1, var2):
                pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def test(self,lowerCamelCase,lowerCamelCase):
                pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_method_args_to_lower_camel_case_ignore_padding(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    blank_lines_after=0,
                    args_style_config=ArgsStyleConfig(
                        name_style_config=NameStyleConfig(
                            name_style=NameStyle.LowerCamelCase,
                            whitespace_before="",
                            whitespace_after="",
                        )
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test( self, var1, va2 ):
                pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def test(self,lowerCamelCase,lowerCamelCase):
                pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_method_args_to_lower_camel_case_with_left1_right0_padding(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    blank_lines_after=0,
                    args_style_config=ArgsStyleConfig(
                        whitespace_before_args="",
                        whitespace_after_args="",
                        name_style_config=NameStyleConfig(
                            name_style=NameStyle.LowerCamelCase,
                            whitespace_before=" ",
                            whitespace_after=" ",
                        ),
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test(MyClass):
                pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def test(lowerCamelCase):
                pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    blank_lines_after=0,
                    args_style_config=ArgsStyleConfig(
                        whitespace_before_args="",
                        whitespace_after_args="",
                        name_style_config=NameStyleConfig(
                            name_style=NameStyle.LowerCamelCase,
                            whitespace_before=" ",
                            whitespace_after="",
                        ),
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test(MyClass, OtherClass):
                pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def test(lowerCamelCase, lowerCamelCase):
                pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    blank_lines_after=0,
                    args_style_config=ArgsStyleConfig(
                        whitespace_before_args="",
                        whitespace_after_args="",
                        name_style_config=NameStyleConfig(
                            name_style=NameStyle.LowerCamelCase,
                            whitespace_before=" ",
                            whitespace_after=" ",
                        ),
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test(MyClass, OtherClass):
                pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def test(lowerCamelCase , lowerCamelCase):
                pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    blank_lines_after=0,
                    args_style_config=ArgsStyleConfig(
                        whitespace_before_args=" ",
                        whitespace_after_args=" ",
                        name_style_config=NameStyleConfig(
                            name_style=NameStyle.LowerCamelCase,
                            whitespace_before=" ",
                            whitespace_after=" ",
                        ),
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test(MyClass, OtherClass):
                pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def test( lowerCamelCase , lowerCamelCase ):
                pass
        """
        )

        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    blank_lines_after=0,
                    args_style_config=ArgsStyleConfig(
                        whitespace_before_args="  ",
                        whitespace_after_args="  ",
                        name_style_config=NameStyleConfig(
                            name_style=NameStyle.LowerCamelCase,
                            whitespace_before=" ",
                            whitespace_after=" ",
                        ),
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test(MyClass, OtherClass):
                pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def test(  lowerCamelCase , lowerCamelCase  ):
                pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    blank_lines_after=0,
                    args_style_config=ArgsStyleConfig(
                        whitespace_before_args=" ",
                        whitespace_after_args="  ",
                        name_style_config=NameStyleConfig(
                            name_style=NameStyle.LowerCamelCase,
                            whitespace_before="   ",
                            whitespace_after="     ",
                        ),
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test(MyClass, OtherClass):
                pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def test( lowerCamelCase     ,   lowerCamelCase  ):
                pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    blank_lines_after=0,
                    args_style_config=ArgsStyleConfig(
                        whitespace_before_args=" ",
                        whitespace_after_args="  ",
                        name_style_config=NameStyleConfig(
                            name_style=NameStyle.LowerCamelCase,
                            whitespace_before="   ",
                            whitespace_after="     ",
                        ),
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test(  MyClass ,   OtherClass    ):
                pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def test( lowerCamelCase     ,   lowerCamelCase  ):
                pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_method_args_to_lower_camel_case_with_single_leading_underscore(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    blank_lines_after=0,
                    args_style_config=ArgsStyleConfig(
                        name_style_config=NameStyleConfig(
                            name_style=(NameStylePrefix.SingleLeadingUnderscore, NameStyle.LowerCamelCase),
                            whitespace_before="",
                            whitespace_after="",
                        )
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test(self, var1, var2):
                pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def test(self,_lowerCamelCase,_lowerCamelCase):
                pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_method_args_to_lower_camel_case_with_double_leading_underscore(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    blank_lines_after=0,
                    args_style_config=ArgsStyleConfig(
                        name_style_config=NameStyleConfig(
                            name_style=(NameStylePrefix.DoubleLeadingUnderscore, NameStyle.LowerCamelCase),
                            whitespace_before="",
                            whitespace_after="",
                        )
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test(self, var1, var2):
                pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def test(self,__lowerCamelCase,__lowerCamelCase):
                pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_method_args_to_lower_camel_case_with_single_trailing_underscore(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    blank_lines_after=0,
                    args_style_config=ArgsStyleConfig(
                        name_style_config=NameStyleConfig(
                            name_style=(NameStyle.LowerCamelCase, NameStyleSuffix.SingleTrailingUnderscore),
                            whitespace_before="",
                            whitespace_after="",
                        )
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test(self, var1, var2):
                pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def test(self,lowerCamelCase_,lowerCamelCase_):
                pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_method_args_to_lower_camel_case_with_double_trailing_underscore(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    blank_lines_after=0,
                    args_style_config=ArgsStyleConfig(
                        name_style_config=NameStyleConfig(
                            name_style=(NameStyle.LowerCamelCase, NameStyleSuffix.DoubleTrailingUnderscore),
                            whitespace_before="",
                            whitespace_after="",
                        )
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test(self, var1, var2):
                pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def test(self,lowerCamelCase__,lowerCamelCase__):
                pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)


if __name__ == "__main__":
    unittest.main()
