import textwrap
import unittest

from learning_to_style_check.codestyle import (
    CodestyleConfig,
    NameStyleConfig,
    apply_codestyle,
    VarStyleConfig,
    FunctionStyleConfig,
    ClassStyleConfig,
)
from learning_to_style_check.libcst import (
    NameStyle,
    NameStylePrefix,
    parse_module,
    NameStyleSuffix,
)


class TestModuleVarNameStyle(unittest.TestCase):
    def test_var_name_to_letter_low(self):
        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                name_style_config=NameStyleConfig(
                    name_style=NameStyle.LetterLow,
                    whitespace_before="",
                    whitespace_after="",
                )
            )
        )
        code = "test = 42"
        styled_code = "a=42"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_to_letter_up(self):
        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                name_style_config=NameStyleConfig(
                    name_style=NameStyle.LetterUp,
                    whitespace_before="",
                    whitespace_after="",
                )
            )
        )
        code = "test = 42"
        styled_code = "A=42"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_to_upper_camel_case(self):
        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                name_style_config=NameStyleConfig(
                    name_style=NameStyle.UpperCamelCase,
                    whitespace_before="",
                    whitespace_after="",
                )
            )
        )
        code = "test = 42"
        styled_code = "UpperCamelCase=42"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_to_lower_camel_case(self):
        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                name_style_config=NameStyleConfig(
                    name_style=NameStyle.LowerCamelCase,
                    whitespace_before="",
                    whitespace_after="",
                )
            )
        )
        code = "test = 42"
        styled_code = "lowerCamelCase=42"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_to_screaming_snake_case(self):
        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                name_style_config=NameStyleConfig(
                    name_style=NameStyle.ScreamingSnakeCase,
                    whitespace_before="",
                    whitespace_after="",
                )
            )
        )
        code = "test = 42"
        styled_code = "SCREAMING_SNAKE_CASE=42"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_to_snake_case(self):
        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                name_style_config=NameStyleConfig(
                    name_style=NameStyle.SnakeCase,
                    whitespace_before="",
                    whitespace_after="",
                )
            )
        )
        code = "test = 42"
        styled_code = "snake_case=42"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_to_magic_name(self):
        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                name_style_config=NameStyleConfig(
                    name_style=NameStyle.MagicName,
                    whitespace_before="",
                    whitespace_after="",
                )
            )
        )
        code = "test = 42"
        styled_code = "__magic_name__=42"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_to_letter_low_with_single_leading_underscore(self):
        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                name_style_config=NameStyleConfig(
                    name_style=(
                        NameStylePrefix.SingleLeadingUnderscore,
                        NameStyle.LetterLow,
                    ),
                    whitespace_before="",
                    whitespace_after="",
                )
            )
        )
        code = "test = 42"
        styled_code = "_a=42"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_to_letter_up_with_single_leading_underscore(self):
        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                name_style_config=NameStyleConfig(
                    name_style=(
                        NameStylePrefix.SingleLeadingUnderscore,
                        NameStyle.LetterUp,
                    ),
                    whitespace_before="",
                    whitespace_after="",
                )
            )
        )
        code = "test = 42"
        styled_code = "_A=42"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_to_lowercase_with_single_leading_underscore(self):
        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                name_style_config=NameStyleConfig(
                    name_style=(
                        NameStylePrefix.SingleLeadingUnderscore,
                        NameStyle.Lowercase,
                    ),
                    whitespace_before="",
                    whitespace_after="",
                )
            )
        )
        code = "test = 42"
        styled_code = "_lowercase=42"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_to_lower_camel_case_with_single_leading_underscore(self):
        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                name_style_config=NameStyleConfig(
                    name_style=(
                        NameStylePrefix.SingleLeadingUnderscore,
                        NameStyle.LowerCamelCase,
                    ),
                    whitespace_before="",
                    whitespace_after="",
                )
            )
        )
        code = "test = 42"
        styled_code = "_lowerCamelCase=42"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_to_lower_camel_case_2_with_single_leading_underscore(self):
        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                name_style_config=NameStyleConfig(
                    name_style=(
                        NameStylePrefix.SingleLeadingUnderscore,
                        NameStyle.LowerCamelCase2,
                    ),
                    whitespace_before="",
                    whitespace_after="",
                )
            )
        )
        code = "test = 42"
        styled_code = "_lowerCAmelCase=42"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_to_upper_camel_case_with_single_leading_underscore(self):
        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                name_style_config=NameStyleConfig(
                    name_style=(
                        NameStylePrefix.SingleLeadingUnderscore,
                        NameStyle.UpperCamelCase,
                    ),
                    whitespace_before="",
                    whitespace_after="",
                )
            )
        )
        code = "test = 42"
        styled_code = "_UpperCamelCase=42"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_to_upper_camel_case_2_with_single_leading_underscore(self):
        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                name_style_config=NameStyleConfig(
                    name_style=(
                        NameStylePrefix.SingleLeadingUnderscore,
                        NameStyle.UpperCamelCase2,
                    ),
                    whitespace_before="",
                    whitespace_after="",
                )
            )
        )
        code = "test = 42"
        styled_code = "_UpperCAmelCase=42"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_to_screaming_snake_case_with_single_leading_underscore(self):
        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                name_style_config=NameStyleConfig(
                    name_style=(
                        NameStylePrefix.SingleLeadingUnderscore,
                        NameStyle.ScreamingSnakeCase,
                    ),
                    whitespace_before="",
                    whitespace_after="",
                )
            )
        )
        code = "test = 42"
        styled_code = "_SCREAMING_SNAKE_CASE=42"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_to_snake_case_with_single_leading_underscore(self):
        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                name_style_config=NameStyleConfig(
                    name_style=(
                        NameStylePrefix.SingleLeadingUnderscore,
                        NameStyle.SnakeCase,
                    ),
                    whitespace_before="",
                    whitespace_after="",
                )
            )
        )
        code = "test = 42"
        styled_code = "_snake_case=42"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_to_letter_low_with_double_leading_underscore(self):
        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                name_style_config=NameStyleConfig(
                    name_style=(
                        NameStylePrefix.DoubleLeadingUnderscore,
                        NameStyle.LetterLow,
                    ),
                    whitespace_before="",
                    whitespace_after="",
                )
            )
        )
        code = "test = 42"
        styled_code = "__a=42"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_to_letter_up_with_double_leading_underscore(self):
        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                name_style_config=NameStyleConfig(
                    name_style=(
                        NameStylePrefix.DoubleLeadingUnderscore,
                        NameStyle.LetterUp,
                    ),
                    whitespace_before="",
                    whitespace_after="",
                )
            )
        )
        code = "test = 42"
        styled_code = "__A=42"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_to_lowercase_with_double_leading_underscore(self):
        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                name_style_config=NameStyleConfig(
                    name_style=(
                        NameStylePrefix.DoubleLeadingUnderscore,
                        NameStyle.Lowercase,
                    ),
                    whitespace_before="",
                    whitespace_after="",
                )
            )
        )
        code = "test = 42"
        styled_code = "__lowercase=42"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_to_lower_camel_case_with_double_leading_underscore(self):
        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                name_style_config=NameStyleConfig(
                    name_style=(
                        NameStylePrefix.DoubleLeadingUnderscore,
                        NameStyle.LowerCamelCase,
                    ),
                    whitespace_before="",
                    whitespace_after="",
                )
            )
        )
        code = "test = 42"
        styled_code = "__lowerCamelCase=42"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_to_lower_camel_case_2_with_double_leading_underscore(self):
        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                name_style_config=NameStyleConfig(
                    name_style=(
                        NameStylePrefix.DoubleLeadingUnderscore,
                        NameStyle.LowerCamelCase2,
                    ),
                    whitespace_before="",
                    whitespace_after="",
                )
            )
        )
        code = "test = 42"
        styled_code = "__lowerCAmelCase=42"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_to_upper_camel_case_with_double_leading_underscore(self):
        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                name_style_config=NameStyleConfig(
                    name_style=(
                        NameStylePrefix.DoubleLeadingUnderscore,
                        NameStyle.UpperCamelCase,
                    ),
                    whitespace_before="",
                    whitespace_after="",
                )
            )
        )
        code = "test = 42"

        styled_code = "__UpperCamelCase=42"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_to_upper_camel_case_2_with_double_leading_underscore(self):
        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                name_style_config=NameStyleConfig(
                    name_style=(
                        NameStylePrefix.DoubleLeadingUnderscore,
                        NameStyle.UpperCamelCase2,
                    ),
                    whitespace_before="",
                    whitespace_after="",
                )
            )
        )
        code = "test = 42"

        styled_code = "__UpperCAmelCase=42"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_to_screaming_snake_case_with_double_leading_underscore(self):
        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                name_style_config=NameStyleConfig(
                    name_style=(
                        NameStylePrefix.DoubleLeadingUnderscore,
                        NameStyle.ScreamingSnakeCase,
                    ),
                    whitespace_before="",
                    whitespace_after="",
                )
            )
        )
        code = "test = 42"

        styled_code = "__SCREAMING_SNAKE_CASE=42"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_to_snake_case_with_double_leading_underscore(self):
        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                name_style_config=NameStyleConfig(
                    name_style=(
                        NameStylePrefix.DoubleLeadingUnderscore,
                        NameStyle.SnakeCase,
                    ),
                    whitespace_before="",
                    whitespace_after="",
                )
            )
        )
        code = "test = 42"

        styled_code = "__snake_case=42"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_to_snake_case_with_single_trailing_underscore(self):
        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                name_style_config=NameStyleConfig(
                    name_style=(
                        NameStyle.SnakeCase,
                        NameStyleSuffix.SingleTrailingUnderscore,
                    ),
                    whitespace_before="",
                    whitespace_after="",
                )
            )
        )
        code = "test = 42"

        styled_code = "snake_case_=42"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_to_snake_case_with_double_trailing_underscore(self):
        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                name_style_config=NameStyleConfig(
                    name_style=(
                        NameStyle.SnakeCase,
                        NameStyleSuffix.DoubleTrailingUnderscore,
                    ),
                    whitespace_before="",
                    whitespace_after="",
                )
            )
        )
        code = "test = 42"

        styled_code = "snake_case__=42"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_to_snake_case_with_parenthesis(self):
        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                name_style_config=NameStyleConfig(
                    name_style=NameStyle.SnakeCase,
                    whitespace_before="",
                    whitespace_after="",
                )
            )
        )
        code = "(test) = 42"
        styled_code = "(snake_case)=42"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                name_style_config=NameStyleConfig(
                    name_style=(
                        NameStylePrefix.SingleLeadingUnderscore,
                        NameStyle.SnakeCase,
                    ),
                    whitespace_before="",
                    whitespace_after="",
                )
            )
        )
        code = "(test) = 42"
        styled_code = "(_snake_case)=42"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                name_style_config=NameStyleConfig(
                    name_style=(
                        NameStylePrefix.DoubleLeadingUnderscore,
                        NameStyle.SnakeCase,
                    ),
                    whitespace_before="",
                    whitespace_after="",
                )
            )
        )
        code = "(test) = 42"
        styled_code = "(__snake_case)=42"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                name_style_config=NameStyleConfig(
                    name_style=(
                        NameStyle.SnakeCase,
                        NameStyleSuffix.SingleTrailingUnderscore,
                    ),
                    whitespace_before="",
                    whitespace_after="",
                )
            )
        )
        code = "(test) = 42"
        styled_code = "(snake_case_)=42"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                name_style_config=NameStyleConfig(
                    name_style=(
                        NameStyle.SnakeCase,
                        NameStyleSuffix.DoubleTrailingUnderscore,
                    ),
                    whitespace_before="",
                    whitespace_after="",
                )
            )
        )
        code = "(test) = 42"
        styled_code = "(snake_case__)=42"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)


class TestFunctionVarNameStyle(unittest.TestCase):
    def test_var_name_to_letter_low(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                var_style_config=VarStyleConfig(
                    name_style_config=NameStyleConfig(
                        name_style=NameStyle.LetterLow,
                        whitespace_before="",
                        whitespace_after="",
                    )
                )
            ),
        )
        code = textwrap.dedent(
            """
        def test():
            test = 42
        """
        )
        styled_code = textwrap.dedent(
            """
        def test():
            a=42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_to_letter_up(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                var_style_config=VarStyleConfig(
                    name_style_config=NameStyleConfig(
                        name_style=NameStyle.LetterUp,
                        whitespace_before="",
                        whitespace_after="",
                    )
                )
            ),
        )
        code = textwrap.dedent(
            """
        def test():
            test = 42
        """
        )
        styled_code = textwrap.dedent(
            """
        def test():
            A=42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_to_upper_camel_case(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                var_style_config=VarStyleConfig(
                    name_style_config=NameStyleConfig(
                        name_style=NameStyle.UpperCamelCase,
                        whitespace_before="",
                        whitespace_after="",
                    )
                )
            ),
        )
        code = textwrap.dedent(
            """
        def test():
            test = 42
        """
        )
        styled_code = textwrap.dedent(
            """
        def test():
            UpperCamelCase=42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_to_lower_camel_case(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                var_style_config=VarStyleConfig(
                    name_style_config=NameStyleConfig(
                        name_style=NameStyle.LowerCamelCase,
                        whitespace_before="",
                        whitespace_after="",
                    )
                )
            ),
        )
        code = textwrap.dedent(
            """
        def test():
            test = 42
        """
        )
        styled_code = textwrap.dedent(
            """
        def test():
            lowerCamelCase=42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_to_screaming_snake_case(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                var_style_config=VarStyleConfig(
                    name_style_config=NameStyleConfig(
                        name_style=NameStyle.ScreamingSnakeCase,
                        whitespace_before="",
                        whitespace_after="",
                    )
                )
            ),
        )
        code = textwrap.dedent(
            """
        def test():
            test = 42
        """
        )
        styled_code = textwrap.dedent(
            """
        def test():
            SCREAMING_SNAKE_CASE=42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_to_snake_case(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                var_style_config=VarStyleConfig(
                    name_style_config=NameStyleConfig(
                        name_style=NameStyle.SnakeCase,
                        whitespace_before="",
                        whitespace_after="",
                    )
                )
            ),
        )
        code = textwrap.dedent(
            """
        def test():
            test = 42
        """
        )
        styled_code = textwrap.dedent(
            """
        def test():
            snake_case=42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_to_magic_name(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                var_style_config=VarStyleConfig(
                    name_style_config=NameStyleConfig(
                        name_style=NameStyle.MagicName,
                        whitespace_before="",
                        whitespace_after="",
                    )
                )
            ),
        )
        code = textwrap.dedent(
            """
        def test():
            test = 42
        """
        )
        styled_code = textwrap.dedent(
            """
        def test():
            __magic_name__=42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_to_letter_low_with_single_leading_underscore(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                var_style_config=VarStyleConfig(
                    name_style_config=NameStyleConfig(
                        name_style=(
                            NameStylePrefix.SingleLeadingUnderscore,
                            NameStyle.LetterLow,
                        ),
                        whitespace_before="",
                        whitespace_after="",
                    )
                )
            ),
        )
        code = textwrap.dedent(
            """
        def test():
            test = 42
        """
        )
        styled_code = textwrap.dedent(
            """
        def test():
            _a=42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_to_letter_up_with_single_leading_underscore(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                var_style_config=VarStyleConfig(
                    name_style_config=NameStyleConfig(
                        name_style=(
                            NameStylePrefix.SingleLeadingUnderscore,
                            NameStyle.LetterUp,
                        ),
                        whitespace_before="",
                        whitespace_after="",
                    )
                )
            ),
        )
        code = textwrap.dedent(
            """
        def test():
            test = 42
        """
        )
        styled_code = textwrap.dedent(
            """
        def test():
            _A=42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_to_lowercase_with_single_leading_underscore(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                var_style_config=VarStyleConfig(
                    name_style_config=NameStyleConfig(
                        name_style=(
                            NameStylePrefix.SingleLeadingUnderscore,
                            NameStyle.Lowercase,
                        ),
                        whitespace_before="",
                        whitespace_after="",
                    )
                )
            ),
        )
        code = textwrap.dedent(
            """
        def test():
            test = 42
        """
        )
        styled_code = textwrap.dedent(
            """
        def test():
            _lowercase=42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_to_lower_camel_case_with_single_leading_underscore(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                var_style_config=VarStyleConfig(
                    name_style_config=NameStyleConfig(
                        name_style=(
                            NameStylePrefix.SingleLeadingUnderscore,
                            NameStyle.LowerCamelCase,
                        ),
                        whitespace_before="",
                        whitespace_after="",
                    )
                )
            ),
        )
        code = textwrap.dedent(
            """
        def test():
            test = 42
        """
        )
        styled_code = textwrap.dedent(
            """
        def test():
            _lowerCamelCase=42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_to_lower_camel_case_2_with_single_leading_underscore(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                var_style_config=VarStyleConfig(
                    name_style_config=NameStyleConfig(
                        name_style=(
                            NameStylePrefix.SingleLeadingUnderscore,
                            NameStyle.LowerCamelCase2,
                        ),
                        whitespace_before="",
                        whitespace_after="",
                    )
                )
            ),
        )
        code = textwrap.dedent(
            """
        def test():
            test = 42
        """
        )
        styled_code = textwrap.dedent(
            """
        def test():
            _lowerCAmelCase=42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_to_upper_camel_case_with_single_leading_underscore(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                var_style_config=VarStyleConfig(
                    name_style_config=NameStyleConfig(
                        name_style=(
                            NameStylePrefix.SingleLeadingUnderscore,
                            NameStyle.UpperCamelCase,
                        ),
                        whitespace_before="",
                        whitespace_after="",
                    )
                )
            ),
        )
        code = textwrap.dedent(
            """
        def test():
            test = 42
        """
        )
        styled_code = textwrap.dedent(
            """
        def test():
            _UpperCamelCase=42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_to_upper_camel_case_2_with_single_leading_underscore(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                var_style_config=VarStyleConfig(
                    name_style_config=NameStyleConfig(
                        name_style=(
                            NameStylePrefix.SingleLeadingUnderscore,
                            NameStyle.UpperCamelCase2,
                        ),
                        whitespace_before="",
                        whitespace_after="",
                    )
                )
            ),
        )
        code = textwrap.dedent(
            """
        def test():
            test = 42
        """
        )
        styled_code = textwrap.dedent(
            """
        def test():
            _UpperCAmelCase=42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_to_screaming_snake_case_with_single_leading_underscore(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                var_style_config=VarStyleConfig(
                    name_style_config=NameStyleConfig(
                        name_style=(
                            NameStylePrefix.SingleLeadingUnderscore,
                            NameStyle.ScreamingSnakeCase,
                        ),
                        whitespace_before="",
                        whitespace_after="",
                    )
                )
            ),
        )
        code = textwrap.dedent(
            """
        def test():
            test = 42
        """
        )
        styled_code = textwrap.dedent(
            """
        def test():
            _SCREAMING_SNAKE_CASE=42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_to_snake_case_with_single_leading_underscore(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                var_style_config=VarStyleConfig(
                    name_style_config=NameStyleConfig(
                        name_style=(
                            NameStylePrefix.SingleLeadingUnderscore,
                            NameStyle.SnakeCase,
                        ),
                        whitespace_before="",
                        whitespace_after="",
                    )
                )
            ),
        )
        code = textwrap.dedent(
            """
        def test():
            test = 42
        """
        )
        styled_code = textwrap.dedent(
            """
        def test():
            _snake_case=42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_to_letter_low_with_double_leading_underscore(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                var_style_config=VarStyleConfig(
                    name_style_config=NameStyleConfig(
                        name_style=(
                            NameStylePrefix.DoubleLeadingUnderscore,
                            NameStyle.LetterLow,
                        ),
                        whitespace_before="",
                        whitespace_after="",
                    )
                )
            ),
        )
        code = textwrap.dedent(
            """
        def test():
            test = 42
        """
        )
        styled_code = textwrap.dedent(
            """
        def test():
            __a=42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_to_letter_up_with_double_leading_underscore(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                var_style_config=VarStyleConfig(
                    name_style_config=NameStyleConfig(
                        name_style=(
                            NameStylePrefix.DoubleLeadingUnderscore,
                            NameStyle.LetterUp,
                        ),
                        whitespace_before="",
                        whitespace_after="",
                    )
                )
            ),
        )
        code = textwrap.dedent(
            """
        def test():
            test = 42
        """
        )
        styled_code = textwrap.dedent(
            """
        def test():
            __A=42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_to_lowercase_with_double_leading_underscore(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                var_style_config=VarStyleConfig(
                    name_style_config=NameStyleConfig(
                        name_style=(
                            NameStylePrefix.DoubleLeadingUnderscore,
                            NameStyle.Lowercase,
                        ),
                        whitespace_before="",
                        whitespace_after="",
                    )
                )
            ),
        )
        code = textwrap.dedent(
            """
        def test():
            test = 42
        """
        )
        styled_code = textwrap.dedent(
            """
        def test():
            __lowercase=42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_to_lower_camel_case_with_double_leading_underscore(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                var_style_config=VarStyleConfig(
                    name_style_config=NameStyleConfig(
                        name_style=(
                            NameStylePrefix.DoubleLeadingUnderscore,
                            NameStyle.LowerCamelCase,
                        ),
                        whitespace_before="",
                        whitespace_after="",
                    )
                )
            ),
        )
        code = textwrap.dedent(
            """
        def test():
            test = 42
        """
        )
        styled_code = textwrap.dedent(
            """
        def test():
            __lowerCamelCase=42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_to_lower_camel_case_2_with_double_leading_underscore(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                var_style_config=VarStyleConfig(
                    name_style_config=NameStyleConfig(
                        name_style=(
                            NameStylePrefix.DoubleLeadingUnderscore,
                            NameStyle.LowerCamelCase2,
                        ),
                        whitespace_before="",
                        whitespace_after="",
                    )
                )
            ),
        )
        code = textwrap.dedent(
            """
        def test():
            test = 42
        """
        )
        styled_code = textwrap.dedent(
            """
        def test():
            __lowerCAmelCase=42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_to_upper_camel_case_with_double_leading_underscore(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                var_style_config=VarStyleConfig(
                    name_style_config=NameStyleConfig(
                        name_style=(
                            NameStylePrefix.DoubleLeadingUnderscore,
                            NameStyle.UpperCamelCase,
                        ),
                        whitespace_before="",
                        whitespace_after="",
                    )
                )
            ),
        )
        code = textwrap.dedent(
            """
        def test():
            test = 42
        """
        )
        styled_code = textwrap.dedent(
            """
        def test():
            __UpperCamelCase=42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_to_upper_camel_case_2_with_double_leading_underscore(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                var_style_config=VarStyleConfig(
                    name_style_config=NameStyleConfig(
                        name_style=(
                            NameStylePrefix.DoubleLeadingUnderscore,
                            NameStyle.UpperCamelCase2,
                        ),
                        whitespace_before="",
                        whitespace_after="",
                    )
                )
            ),
        )
        code = textwrap.dedent(
            """
        def test():
            test = 42
        """
        )
        styled_code = textwrap.dedent(
            """
        def test():
            __UpperCAmelCase=42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_to_screaming_snake_case_with_double_leading_underscore(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                var_style_config=VarStyleConfig(
                    name_style_config=NameStyleConfig(
                        name_style=(
                            NameStylePrefix.DoubleLeadingUnderscore,
                            NameStyle.ScreamingSnakeCase,
                        ),
                        whitespace_before="",
                        whitespace_after="",
                    )
                )
            ),
        )
        code = textwrap.dedent(
            """
        def test():
            test = 42
        """
        )
        styled_code = textwrap.dedent(
            """
        def test():
            __SCREAMING_SNAKE_CASE=42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_to_snake_case_with_double_leading_underscore(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                var_style_config=VarStyleConfig(
                    name_style_config=NameStyleConfig(
                        name_style=(
                            NameStylePrefix.DoubleLeadingUnderscore,
                            NameStyle.SnakeCase,
                        ),
                        whitespace_before="",
                        whitespace_after="",
                    )
                )
            ),
        )
        code = textwrap.dedent(
            """
        def test():
            test = 42
        """
        )
        styled_code = textwrap.dedent(
            """
        def test():
            __snake_case=42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)


class TestClassVarNameStyle(unittest.TestCase):
    def test_var_name_to_letter_low(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                var_style_config=VarStyleConfig(
                    name_style_config=NameStyleConfig(
                        name_style=NameStyle.LetterLow,
                        whitespace_before="",
                        whitespace_after="",
                    )
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            test = 42
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            a=42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_to_letter_up(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                var_style_config=VarStyleConfig(
                    name_style_config=NameStyleConfig(
                        name_style=NameStyle.LetterUp,
                        whitespace_before="",
                        whitespace_after="",
                    )
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            test = 42
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            A=42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_to_upper_camel_case(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                var_style_config=VarStyleConfig(
                    name_style_config=NameStyleConfig(
                        name_style=NameStyle.UpperCamelCase,
                        whitespace_before="",
                        whitespace_after="",
                    )
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            test = 42
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            UpperCamelCase=42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_to_lower_camel_case(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                var_style_config=VarStyleConfig(
                    name_style_config=NameStyleConfig(
                        name_style=NameStyle.LowerCamelCase,
                        whitespace_before="",
                        whitespace_after="",
                    )
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            test = 42
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            lowerCamelCase=42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_to_screaming_snake_case(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                var_style_config=VarStyleConfig(
                    name_style_config=NameStyleConfig(
                        name_style=NameStyle.ScreamingSnakeCase,
                        whitespace_before="",
                        whitespace_after="",
                    )
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            test = 42
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            SCREAMING_SNAKE_CASE=42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_to_snake_case(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                var_style_config=VarStyleConfig(
                    name_style_config=NameStyleConfig(
                        name_style=NameStyle.SnakeCase,
                        whitespace_before="",
                        whitespace_after="",
                    )
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            test = 42
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            snake_case=42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_to_magic_name(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                var_style_config=VarStyleConfig(
                    name_style_config=NameStyleConfig(
                        name_style=NameStyle.MagicName,
                        whitespace_before="",
                        whitespace_after="",
                    )
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            test = 42
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            __magic_name__=42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_to_letter_low_with_single_leading_underscore(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                var_style_config=VarStyleConfig(
                    name_style_config=NameStyleConfig(
                        name_style=(
                            NameStylePrefix.SingleLeadingUnderscore,
                            NameStyle.LetterLow,
                        ),
                        whitespace_before="",
                        whitespace_after="",
                    )
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            test = 42
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            _a=42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_to_letter_up_with_single_leading_underscore(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                var_style_config=VarStyleConfig(
                    name_style_config=NameStyleConfig(
                        name_style=(
                            NameStylePrefix.SingleLeadingUnderscore,
                            NameStyle.LetterUp,
                        ),
                        whitespace_before="",
                        whitespace_after="",
                    )
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            test = 42
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            _A=42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_to_lowercase_with_single_leading_underscore(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                var_style_config=VarStyleConfig(
                    name_style_config=NameStyleConfig(
                        name_style=(
                            NameStylePrefix.SingleLeadingUnderscore,
                            NameStyle.Lowercase,
                        ),
                        whitespace_before="",
                        whitespace_after="",
                    )
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            test = 42
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            _lowercase=42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_to_lower_camel_case_with_single_leading_underscore(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                var_style_config=VarStyleConfig(
                    name_style_config=NameStyleConfig(
                        name_style=(
                            NameStylePrefix.SingleLeadingUnderscore,
                            NameStyle.LowerCamelCase,
                        ),
                        whitespace_before="",
                        whitespace_after="",
                    )
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            test = 42
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            _lowerCamelCase=42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_to_lower_camel_case_2_with_single_leading_underscore(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                var_style_config=VarStyleConfig(
                    name_style_config=NameStyleConfig(
                        name_style=(
                            NameStylePrefix.SingleLeadingUnderscore,
                            NameStyle.LowerCamelCase2,
                        ),
                        whitespace_before="",
                        whitespace_after="",
                    )
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            test = 42
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            _lowerCAmelCase=42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_to_upper_camel_case_with_single_leading_underscore(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                var_style_config=VarStyleConfig(
                    name_style_config=NameStyleConfig(
                        name_style=(
                            NameStylePrefix.SingleLeadingUnderscore,
                            NameStyle.UpperCamelCase,
                        ),
                        whitespace_before="",
                        whitespace_after="",
                    )
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            test = 42
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            _UpperCamelCase=42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_to_upper_camel_case_2_with_single_leading_underscore(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                var_style_config=VarStyleConfig(
                    name_style_config=NameStyleConfig(
                        name_style=(
                            NameStylePrefix.SingleLeadingUnderscore,
                            NameStyle.UpperCamelCase2,
                        ),
                        whitespace_before="",
                        whitespace_after="",
                    )
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            test = 42
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            _UpperCAmelCase=42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_to_screaming_snake_case_with_single_leading_underscore(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                var_style_config=VarStyleConfig(
                    name_style_config=NameStyleConfig(
                        name_style=(
                            NameStylePrefix.SingleLeadingUnderscore,
                            NameStyle.ScreamingSnakeCase,
                        ),
                        whitespace_before="",
                        whitespace_after="",
                    )
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            test = 42
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            _SCREAMING_SNAKE_CASE=42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_to_snake_case_with_single_leading_underscore(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                var_style_config=VarStyleConfig(
                    name_style_config=NameStyleConfig(
                        name_style=(
                            NameStylePrefix.SingleLeadingUnderscore,
                            NameStyle.SnakeCase,
                        ),
                        whitespace_before="",
                        whitespace_after="",
                    )
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            test = 42
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            _snake_case=42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_to_letter_low_with_double_leading_underscore(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                var_style_config=VarStyleConfig(
                    name_style_config=NameStyleConfig(
                        name_style=(
                            NameStylePrefix.DoubleLeadingUnderscore,
                            NameStyle.LetterLow,
                        ),
                        whitespace_before="",
                        whitespace_after="",
                    )
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            test = 42
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            __a=42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_to_letter_up_with_double_leading_underscore(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                var_style_config=VarStyleConfig(
                    name_style_config=NameStyleConfig(
                        name_style=(
                            NameStylePrefix.DoubleLeadingUnderscore,
                            NameStyle.LetterUp,
                        ),
                        whitespace_before="",
                        whitespace_after="",
                    )
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            test = 42
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            __A=42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_to_lowercase_with_double_leading_underscore(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                var_style_config=VarStyleConfig(
                    name_style_config=NameStyleConfig(
                        name_style=(
                            NameStylePrefix.DoubleLeadingUnderscore,
                            NameStyle.Lowercase,
                        ),
                        whitespace_before="",
                        whitespace_after="",
                    )
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            test = 42
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            __lowercase=42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_to_lower_camel_case_with_double_leading_underscore(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                var_style_config=VarStyleConfig(
                    name_style_config=NameStyleConfig(
                        name_style=(
                            NameStylePrefix.DoubleLeadingUnderscore,
                            NameStyle.LowerCamelCase,
                        ),
                        whitespace_before="",
                        whitespace_after="",
                    )
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            test = 42
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            __lowerCamelCase=42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_to_lower_camel_case_2_with_double_leading_underscore(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                var_style_config=VarStyleConfig(
                    name_style_config=NameStyleConfig(
                        name_style=(
                            NameStylePrefix.DoubleLeadingUnderscore,
                            NameStyle.LowerCamelCase2,
                        ),
                        whitespace_before="",
                        whitespace_after="",
                    )
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            test = 42
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            __lowerCAmelCase=42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_to_upper_camel_case_with_double_leading_underscore(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                var_style_config=VarStyleConfig(
                    name_style_config=NameStyleConfig(
                        name_style=(
                            NameStylePrefix.DoubleLeadingUnderscore,
                            NameStyle.UpperCamelCase,
                        ),
                        whitespace_before="",
                        whitespace_after="",
                    )
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            test = 42
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            __UpperCamelCase=42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_to_upper_camel_case_2_with_double_leading_underscore(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                var_style_config=VarStyleConfig(
                    name_style_config=NameStyleConfig(
                        name_style=(
                            NameStylePrefix.DoubleLeadingUnderscore,
                            NameStyle.UpperCamelCase2,
                        ),
                        whitespace_before="",
                        whitespace_after="",
                    )
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            test = 42
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            __UpperCAmelCase=42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_to_screaming_snake_case_with_double_leading_underscore(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                var_style_config=VarStyleConfig(
                    name_style_config=NameStyleConfig(
                        name_style=(
                            NameStylePrefix.DoubleLeadingUnderscore,
                            NameStyle.ScreamingSnakeCase,
                        ),
                        whitespace_before="",
                        whitespace_after="",
                    )
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            test = 42
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            __SCREAMING_SNAKE_CASE=42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_to_snake_case_with_double_leading_underscore(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                var_style_config=VarStyleConfig(
                    name_style_config=NameStyleConfig(
                        name_style=(
                            NameStylePrefix.DoubleLeadingUnderscore,
                            NameStyle.SnakeCase,
                        ),
                        whitespace_before="",
                        whitespace_after="",
                    )
                )
            ),
        )
        code = textwrap.dedent(
            """
    class Test:
            test = 42
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            __snake_case=42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_to_snake_case_with_single_trailing_underscore(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                var_style_config=VarStyleConfig(
                    name_style_config=NameStyleConfig(
                        name_style=(
                            NameStyle.SnakeCase,
                            NameStyleSuffix.SingleTrailingUnderscore,
                        ),
                        whitespace_before="",
                        whitespace_after="",
                    )
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            test = 42
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            snake_case_=42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_to_snake_case_with_double_trailing_underscore(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                var_style_config=VarStyleConfig(
                    name_style_config=NameStyleConfig(
                        name_style=(
                            NameStyle.SnakeCase,
                            NameStyleSuffix.DoubleTrailingUnderscore,
                        ),
                        whitespace_before="",
                        whitespace_after="",
                    )
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            test = 42
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            snake_case__=42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)


class TestMethodVarNameStyle(unittest.TestCase):
    def test_var_name_to_letter_low(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    var_style_config=VarStyleConfig(
                        name_style_config=NameStyleConfig(
                            name_style=NameStyle.LetterLow,
                            whitespace_before="",
                            whitespace_after="",
                        )
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test():
                test = 42
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def test():
                a=42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_to_letter_up(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    var_style_config=VarStyleConfig(
                        name_style_config=NameStyleConfig(
                            name_style=NameStyle.LetterUp,
                            whitespace_before="",
                            whitespace_after="",
                        )
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test():
                test = 42
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def test():
                A=42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_to_upper_camel_case(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    var_style_config=VarStyleConfig(
                        name_style_config=NameStyleConfig(
                            name_style=NameStyle.UpperCamelCase,
                            whitespace_before="",
                            whitespace_after="",
                        )
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test():
                test = 42
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def test():
                UpperCamelCase=42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_to_lower_camel_case(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    var_style_config=VarStyleConfig(
                        name_style_config=NameStyleConfig(
                            name_style=NameStyle.LowerCamelCase,
                            whitespace_before="",
                            whitespace_after="",
                        )
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test():
                test = 42
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def test():
                lowerCamelCase=42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_to_screaming_snake_case(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    var_style_config=VarStyleConfig(
                        name_style_config=NameStyleConfig(
                            name_style=NameStyle.ScreamingSnakeCase,
                            whitespace_before="",
                            whitespace_after="",
                        )
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test():
                test = 42
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def test():
                SCREAMING_SNAKE_CASE=42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_to_snake_case(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    var_style_config=VarStyleConfig(
                        name_style_config=NameStyleConfig(
                            name_style=NameStyle.SnakeCase,
                            whitespace_before="",
                            whitespace_after="",
                        )
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test():
                test = 42
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def test():
                snake_case=42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_to_magic_name(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    var_style_config=VarStyleConfig(
                        name_style_config=NameStyleConfig(
                            name_style=NameStyle.MagicName,
                            whitespace_before="",
                            whitespace_after="",
                        )
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test():
                test = 42
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def test():
                __magic_name__=42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_to_letter_low_with_single_leading_underscore(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    var_style_config=VarStyleConfig(
                        name_style_config=NameStyleConfig(
                            name_style=(
                                NameStylePrefix.SingleLeadingUnderscore,
                                NameStyle.LetterLow,
                            ),
                            whitespace_before="",
                            whitespace_after="",
                        )
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test():
                test = 42
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def test():
                _a=42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_to_letter_up_with_single_leading_underscore(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    var_style_config=VarStyleConfig(
                        name_style_config=NameStyleConfig(
                            name_style=(
                                NameStylePrefix.SingleLeadingUnderscore,
                                NameStyle.LetterUp,
                            ),
                            whitespace_before="",
                            whitespace_after="",
                        )
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test():
                test = 42
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def test():
                _A=42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_to_lowercase_with_single_leading_underscore(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    var_style_config=VarStyleConfig(
                        name_style_config=NameStyleConfig(
                            name_style=(
                                NameStylePrefix.SingleLeadingUnderscore,
                                NameStyle.Lowercase,
                            ),
                            whitespace_before="",
                            whitespace_after="",
                        )
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test():
                test = 42
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def test():
                _lowercase=42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_to_lower_camel_case_with_single_leading_underscore(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    var_style_config=VarStyleConfig(
                        name_style_config=NameStyleConfig(
                            name_style=(
                                NameStylePrefix.SingleLeadingUnderscore,
                                NameStyle.LowerCamelCase,
                            ),
                            whitespace_before="",
                            whitespace_after="",
                        )
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test():
                test = 42
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def test():
                _lowerCamelCase=42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_to_lower_camel_case_2_with_single_leading_underscore(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    var_style_config=VarStyleConfig(
                        name_style_config=NameStyleConfig(
                            name_style=(
                                NameStylePrefix.SingleLeadingUnderscore,
                                NameStyle.LowerCamelCase2,
                            ),
                            whitespace_before="",
                            whitespace_after="",
                        )
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test():
                test = 42
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def test():
                _lowerCAmelCase=42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_to_upper_camel_case_with_single_leading_underscore(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    var_style_config=VarStyleConfig(
                        name_style_config=NameStyleConfig(
                            name_style=(
                                NameStylePrefix.SingleLeadingUnderscore,
                                NameStyle.UpperCamelCase,
                            ),
                            whitespace_before="",
                            whitespace_after="",
                        )
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test():
                test = 42
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def test():
                _UpperCamelCase=42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_to_upper_camel_case_2_with_single_leading_underscore(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    var_style_config=VarStyleConfig(
                        name_style_config=NameStyleConfig(
                            name_style=(
                                NameStylePrefix.SingleLeadingUnderscore,
                                NameStyle.UpperCamelCase2,
                            ),
                            whitespace_before="",
                            whitespace_after="",
                        )
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test():
                test = 42
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def test():
                _UpperCAmelCase=42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_to_screaming_snake_case_with_single_leading_underscore(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    var_style_config=VarStyleConfig(
                        name_style_config=NameStyleConfig(
                            name_style=(
                                NameStylePrefix.SingleLeadingUnderscore,
                                NameStyle.ScreamingSnakeCase,
                            ),
                            whitespace_before="",
                            whitespace_after="",
                        )
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test():
                test = 42
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def test():
                _SCREAMING_SNAKE_CASE=42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_to_snake_case_with_single_leading_underscore(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    var_style_config=VarStyleConfig(
                        name_style_config=NameStyleConfig(
                            name_style=(
                                NameStylePrefix.SingleLeadingUnderscore,
                                NameStyle.SnakeCase,
                            ),
                            whitespace_before="",
                            whitespace_after="",
                        )
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test():
                test = 42
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def test():
                _snake_case=42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_to_letter_low_with_double_leading_underscore(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    var_style_config=VarStyleConfig(
                        name_style_config=NameStyleConfig(
                            name_style=(
                                NameStylePrefix.DoubleLeadingUnderscore,
                                NameStyle.LetterLow,
                            ),
                            whitespace_before="",
                            whitespace_after="",
                        )
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test():
                test = 42
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def test():
                __a=42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_to_letter_up_with_double_leading_underscore(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    var_style_config=VarStyleConfig(
                        name_style_config=NameStyleConfig(
                            name_style=(
                                NameStylePrefix.DoubleLeadingUnderscore,
                                NameStyle.LetterUp,
                            ),
                            whitespace_before="",
                            whitespace_after="",
                        )
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test():
                test = 42
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def test():
                __A=42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_to_lowercase_with_double_leading_underscore(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    var_style_config=VarStyleConfig(
                        name_style_config=NameStyleConfig(
                            name_style=(
                                NameStylePrefix.DoubleLeadingUnderscore,
                                NameStyle.Lowercase,
                            ),
                            whitespace_before="",
                            whitespace_after="",
                        )
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test():
                test = 42
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def test():
                __lowercase=42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_to_lower_camel_case_with_double_leading_underscore(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    var_style_config=VarStyleConfig(
                        name_style_config=NameStyleConfig(
                            name_style=(
                                NameStylePrefix.DoubleLeadingUnderscore,
                                NameStyle.LowerCamelCase,
                            ),
                            whitespace_before="",
                            whitespace_after="",
                        )
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test():
                test = 42
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def test():
                __lowerCamelCase=42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_to_lower_camel_case_2_with_double_leading_underscore(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    var_style_config=VarStyleConfig(
                        name_style_config=NameStyleConfig(
                            name_style=(
                                NameStylePrefix.DoubleLeadingUnderscore,
                                NameStyle.LowerCamelCase2,
                            ),
                            whitespace_before="",
                            whitespace_after="",
                        )
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test():
                test = 42
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def test():
                __lowerCAmelCase=42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_to_upper_camel_case_with_double_leading_underscore(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    var_style_config=VarStyleConfig(
                        name_style_config=NameStyleConfig(
                            name_style=(
                                NameStylePrefix.DoubleLeadingUnderscore,
                                NameStyle.UpperCamelCase,
                            ),
                            whitespace_before="",
                            whitespace_after="",
                        )
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test():
                test = 42
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def test():
                __UpperCamelCase=42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_to_upper_camel_case_2_with_double_leading_underscore(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    var_style_config=VarStyleConfig(
                        name_style_config=NameStyleConfig(
                            name_style=(
                                NameStylePrefix.DoubleLeadingUnderscore,
                                NameStyle.UpperCamelCase2,
                            ),
                            whitespace_before="",
                            whitespace_after="",
                        )
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test():
                test = 42
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def test():
                __UpperCAmelCase=42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_to_screaming_snake_case_with_double_leading_underscore(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    var_style_config=VarStyleConfig(
                        name_style_config=NameStyleConfig(
                            name_style=(
                                NameStylePrefix.DoubleLeadingUnderscore,
                                NameStyle.ScreamingSnakeCase,
                            ),
                            whitespace_before="",
                            whitespace_after="",
                        )
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test():
                test = 42
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def test():
                __SCREAMING_SNAKE_CASE=42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_to_snake_case_with_double_leading_underscore(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    var_style_config=VarStyleConfig(
                        name_style_config=NameStyleConfig(
                            name_style=(
                                NameStylePrefix.DoubleLeadingUnderscore,
                                NameStyle.SnakeCase,
                            ),
                            whitespace_before="",
                            whitespace_after="",
                        )
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test():
                test = 42
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def test():
                __snake_case=42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_to_snake_case_with_single_trailing_underscore(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    var_style_config=VarStyleConfig(
                        name_style_config=NameStyleConfig(
                            name_style=(
                                NameStyle.SnakeCase,
                                NameStyleSuffix.SingleTrailingUnderscore,
                            ),
                            whitespace_before="",
                            whitespace_after="",
                        )
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test():
                test = 42
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def test():
                snake_case_=42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_to_snake_case_with_double_single_underscore(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    var_style_config=VarStyleConfig(
                        name_style_config=NameStyleConfig(
                            name_style=(
                                NameStyle.SnakeCase,
                                NameStyleSuffix.DoubleTrailingUnderscore,
                            ),
                            whitespace_before="",
                            whitespace_after="",
                        )
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test():
                test = 42
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def test():
                snake_case__=42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)


class TestVarNameStylePadding(unittest.TestCase):
    def test_var_name_with_left_padding1(self):
        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                name_style_config=NameStyleConfig(
                    name_style=NameStyle.Lowercase,
                    whitespace_before=" ",
                    whitespace_after="",
                )
            )
        )
        code = "test = 42"
        styled_code = "lowercase=42"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_with_right_padding1(self):
        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                name_style_config=NameStyleConfig(
                    name_style=NameStyle.Lowercase,
                    whitespace_before="",
                    whitespace_after=" ",
                )
            )
        )
        code = "test = 42"
        styled_code = "lowercase =42"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_with_right_padding2(self):
        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                name_style_config=NameStyleConfig(
                    name_style=NameStyle.Lowercase,
                    whitespace_before="",
                    whitespace_after="  ",
                )
            )
        )
        code = "test = 42"
        styled_code = "lowercase  =42"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_with_left1_and_right1_padding(self):
        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                name_style_config=NameStyleConfig(
                    name_style=NameStyle.Lowercase,
                    whitespace_before=" ",
                    whitespace_after=" ",
                )
            )
        )
        code = "test = 42"
        styled_code = "lowercase =42"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_name_with_left2_and_right3_padding(self):
        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                name_style_config=NameStyleConfig(
                    name_style=NameStyle.Lowercase,
                    whitespace_before="  ",
                    whitespace_after="   ",
                )
            )
        )
        code = "test = 42"
        styled_code = "lowercase   =42"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_two_var_names_with_left2_and_right3_padding(self):
        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                name_style_config=NameStyleConfig(
                    name_style=NameStyle.Lowercase,
                    whitespace_before=" " * 2,
                    whitespace_after=" " * 3,
                )
            )
        )
        code = "test, test2 = 42"
        styled_code = "lowercase   ,  lowercase   =42"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_two_var_names_with_equal_left2_padding(self):
        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                name_style_config=NameStyleConfig(
                    name_style=NameStyle.Lowercase,
                ),
                whitespace_before_equal=" " * 2,
            )
        )
        code = "test, test2 = 42"
        styled_code = "lowercase,lowercase  =42"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_two_var_names_with_equal_left2_and_right3_padding(self):
        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                name_style_config=NameStyleConfig(
                    name_style=NameStyle.Lowercase,
                ),
                whitespace_before_equal=" " * 2,
                whitespace_after_equal=" " * 3,
            )
        )
        code = "test, test2 = 42"
        styled_code = "lowercase,lowercase  =   42"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)


if __name__ == "__main__":
    unittest.main()
