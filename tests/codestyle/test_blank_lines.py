import textwrap
import unittest

from learning_to_style_check.codestyle import (
    CodestyleConfig,
    apply_codestyle,
    FunctionStyleConfig,
    ClassStyleConfig,
)
from learning_to_style_check.libcst import parse_module


class TestFunctionBlankLinesStyle(unittest.TestCase):
    def test_blank_lines_before_function(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                blank_lines_before=1, blank_lines_after=0
            ),
        )
        code = textwrap.dedent(
            """
        def test():
            pass
        """
        )
        styled_code = textwrap.dedent(
            """
        def test():
            pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                blank_lines_before=1, blank_lines_after=0
            ),
        )
        code = textwrap.dedent(
            """
        a = 42
        def test():
            pass
        """
        )
        styled_code = textwrap.dedent(
            """
        a = 42

        def test():
            pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                blank_lines_before=2, blank_lines_after=0
            ),
        )
        code = textwrap.dedent(
            """
        a = 42
        def test():
            pass
        """
        )
        styled_code = textwrap.dedent(
            """
        a = 42
        

        def test():
            pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                blank_lines_before=1, blank_lines_after=0
            ),
        )
        code = textwrap.dedent(
            """
        a = 42
        
        
        def test():
            pass
        
        
        def test():
            pass
        
        
        """
        )
        styled_code = textwrap.dedent(
            """
        a = 42

        def test():
            pass
        
        def test():
            pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_blank_lines_after_function(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                blank_lines_before=0, blank_lines_after=1
            ),
        )
        code = textwrap.dedent(
            """
        def test():
            pass
        """
        )
        styled_code = textwrap.dedent(
            """        
        def test():
            pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                blank_lines_before=0, blank_lines_after=2
            ),
        )
        code = textwrap.dedent(
            """
        def test():
            pass
        """
        )
        styled_code = textwrap.dedent(
            """        
        def test():
            pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                blank_lines_before=0, blank_lines_after=1
            ),
        )
        code = textwrap.dedent(
            """
        a = 42
        def test():
            pass
        """
        )
        styled_code = textwrap.dedent(
            """
        a = 42
        def test():
            pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                blank_lines_before=0, blank_lines_after=2
            ),
        )
        code = textwrap.dedent(
            """
        a = 42
        def test():
            pass
        """
        )
        styled_code = textwrap.dedent(
            """
        a = 42
        def test():
            pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                blank_lines_before=0, blank_lines_after=1
            ),
        )
        code = textwrap.dedent(
            """
        a = 42


        def test():
            pass


        def test():
            pass


        """
        )
        styled_code = textwrap.dedent(
            """
        a = 42
        def test():
            pass

        def test():
            pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_blank_lines_before_and_after_function(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                blank_lines_before=1, blank_lines_after=1
            ),
        )
        code = textwrap.dedent(
            """
        def test():
            pass
        def test():
            pass
        """
        )
        styled_code = textwrap.dedent(
            """
        def test():
            pass
        
        def test():
            pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                blank_lines_before=3, blank_lines_after=2
            ),
        )
        code = textwrap.dedent(
            """
        def test():
            pass
        def test():
            pass
        """
        )
        styled_code = textwrap.dedent(
            """
        def test():
            pass



        def test():
            pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)


class TestMethodBlankLinesStyle(unittest.TestCase):
    def test_blank_lines_before_method(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=1, blank_lines_after=0
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test():
                pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:

            def test():
                pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=2, blank_lines_after=0
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test():
                pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:


            def test():
                pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=2, blank_lines_after=0
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            m: int
            def test():
                pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            m: int


            def test():
                pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=2, blank_lines_after=0
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test():
                pass
            def test():
                pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
        
        
            def test():
                pass


            def test():
                pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=2, blank_lines_after=0
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
        
            def test():
                pass
        
            def test():
                pass
        
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:


            def test():
                pass


            def test():
                pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_blank_lines_after_method(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0, blank_lines_after=1
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test():
                pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def test():
                pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0, blank_lines_after=2
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test():
                pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def test():
                pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0, blank_lines_after=2
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test():
                pass
            m: int
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def test():
                pass
            
            
            m: int
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0, blank_lines_after=2
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test():
                pass
            def test():
                pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def test():
                pass


            def test():
                pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0, blank_lines_after=2
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:

            def test():
                pass

            def test():
                pass

        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def test():
                pass


            def test():
                pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_blank_lines_before_and_after_method(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=1, blank_lines_after=1
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test():
                pass
            def test():
                pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
    
            def test():
                pass
    
            def test():
                pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=2, blank_lines_after=3
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            m: int
            def test():
                pass
            def test():
                pass
            
            
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            m: int
    
    
            def test():
                pass
    
    
    
            def test():
                pass
        """
        )

        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=2, blank_lines_after=1
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            m: int
            
            
            def test():
                pass
            
            
            def test():
                pass
            
            
            n: int
            
            

        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            m: int
            
            
            def test():
                pass
            
            
            def test():
                pass
            
            n: int
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)


class TestClassBlankLinesStyle(unittest.TestCase):
    def test_blank_lines_before_class(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                blank_lines_before=1, blank_lines_after=0
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def f():
                pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def f():
                pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                blank_lines_before=2, blank_lines_after=0
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def f():
                pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def f():
                pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                blank_lines_before=2, blank_lines_after=0
            ),
        )
        code = textwrap.dedent(
            """
        a = 42
        class Test:
            def f():
                pass
        """
        )
        styled_code = textwrap.dedent(
            """
        a = 42


        class Test:
            def f():
                pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                blank_lines_before=2, blank_lines_after=0
            ),
        )
        code = textwrap.dedent(
            """
        a = 42
        
        class Test:
            def f():
                pass
        
        
        """
        )
        styled_code = textwrap.dedent(
            """
        a = 42


        class Test:
            def f():
                pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_blank_lines_after_class(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                blank_lines_before=0, blank_lines_after=1
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def f():
                pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def f():
                pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                blank_lines_before=0, blank_lines_after=2
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def f():
                pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def f():
                pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                blank_lines_before=0, blank_lines_after=1
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def f():
                pass
        m = 42
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def f():
                pass

        m = 42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                blank_lines_before=0, blank_lines_after=2
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def f():
                pass
        m = 42
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def f():
                pass


        m = 42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                blank_lines_before=0, blank_lines_after=2
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def f():
                pass
        
        
        
        
        m = 42
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def f():
                pass


        m = 42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_blank_lines_before_and_after_class(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                blank_lines_before=1, blank_lines_after=1
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def f():
                pass
        class Test:
            def f():
                pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def f():
                pass

        class Test:
            def f():
                pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                blank_lines_before=2, blank_lines_after=3
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def f():
                pass
        class Test:
            def f():
                pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def f():
                pass



        class Test:
            def f():
                pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                blank_lines_before=2, blank_lines_after=3
            ),
        )
        code = textwrap.dedent(
            """

        class Test:
            def f():
                pass

        class Test:
            def f():
                pass

        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            def f():
                pass



        class Test:
            def f():
                pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                blank_lines_before=1, blank_lines_after=1, indent_size=4
            ),
            class_style_config=ClassStyleConfig(
                blank_lines_before=2,
                blank_lines_after=2,
                indent_size=4,
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=1, blank_lines_after=1, indent_size=4
                ),
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def f():
                pass
            def l():
                pass
        def f():
            pass
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
        
            def f():
                pass
            
            def l():
                pass
        
        
        def f():
            pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)


class TestModuleBlankLinesStyle(unittest.TestCase):
    def test_blank_lines_top(self):
        codestyle = CodestyleConfig(blank_lines_top=1, blank_lines_bottom=0)
        code = "a = 42"
        styled_code = "\na = 42"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(blank_lines_top=2, blank_lines_bottom=0)
        code = "a = 42"
        styled_code = "\n\na = 42"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(blank_lines_top=2, blank_lines_bottom=0)
        code = "\na = 42\n"
        styled_code = "\n\na = 42"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(blank_lines_top=2, blank_lines_bottom=0)
        code = "\n\n\na = 42\n"
        styled_code = "\n\na = 42"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(blank_lines_top=2, blank_lines_bottom=0)
        code = "# test comment\n\na = 42\n"
        styled_code = "\n\n# test comment\n\na = 42"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(blank_lines_top=2, blank_lines_bottom=0)
        code = "\n# test comment\n\na = 42\n"
        styled_code = "\n\n# test comment\n\na = 42"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(blank_lines_top=2, blank_lines_bottom=0)
        code = "\n\n\n# test comment\n\na = 42\n"
        styled_code = "\n\n# test comment\n\na = 42"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_blank_lines_bottom(self):
        codestyle = CodestyleConfig(blank_lines_top=0, blank_lines_bottom=1)
        code = "a = 42"
        styled_code = "a = 42\n"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(blank_lines_top=0, blank_lines_bottom=2)
        code = "a = 42"
        styled_code = "a = 42\n\n"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(blank_lines_top=0, blank_lines_bottom=2)
        code = "\na = 42\n"
        styled_code = "a = 42\n\n"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(blank_lines_top=0, blank_lines_bottom=2)
        code = "\na = 42\n\n\n"
        styled_code = "a = 42\n\n"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(blank_lines_top=0, blank_lines_bottom=2)
        code = "\na = 42\n\n\n# test comment"
        styled_code = "a = 42\n\n\n# test comment\n\n"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(blank_lines_top=0, blank_lines_bottom=2)
        code = "\na = 42\n\n\n# test comment\n"
        styled_code = "a = 42\n\n\n# test comment\n\n"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(blank_lines_top=0, blank_lines_bottom=2)
        code = "\na = 42\n\n\n# test comment\n\n\n\n"
        styled_code = "a = 42\n\n\n# test comment\n\n"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)


if __name__ == "__main__":
    unittest.main()
