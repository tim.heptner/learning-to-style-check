import textwrap
import unittest

from learning_to_style_check.codestyle import (
    CodestyleConfig,
    FunctionStyleConfig,
    ClassStyleConfig,
    apply_codestyle,
    get_random_codestyle,
    SAMPLE_CODE,
    DocStringStyleConfig,
    StringStyleConfig,
)
from learning_to_style_check.libcst import parse_module


class TestSimpleSample(unittest.TestCase):
    def test_simple_sample(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            docstring_style_config=DocStringStyleConfig(
                blank_lines_before=1,
                blank_lines_after=2,
                string_style_config=StringStyleConfig(quotes='"""'),
            ),
            function_style_config=FunctionStyleConfig(
                blank_lines_before=1, blank_lines_after=1, indent_size=4
            ),
            class_style_config=ClassStyleConfig(
                blank_lines_before=2,
                blank_lines_after=2,
                indent_size=4,
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=1, blank_lines_after=1, indent_size=4
                ),
            ),
        )
        code = textwrap.dedent(
            '''
        """
        This is a module docstring
        """

        import os
        import sys

        TEST_VAR = 42
        TEST_VAR2 = f"test {TEST_VAR} {' ' * 2}"


        class Foo:
            def __init__(self, a, b):
                self.a = a
                self.b = b
            def bar(self, c, d):
                """
                This is a method docstring
                """
                return self.a + self.b + c + d

        def test(e, f):
            return e + f

        def test2(g):
            """
            This is a function docstring
            """
            m = 42
            n = "test string"
            return g + "test string"
        '''
        )
        styled_code = textwrap.dedent(
            '''
        """simple docstring"""
        
        
        import os
        import sys

        TEST_VAR = 42
        TEST_VAR2 = f"test {TEST_VAR} {' ' * 2}"


        class Foo:
        
            def __init__(self, a, b):
                self.a = a
                self.b = b

            def bar(self, c, d):
                return self.a + self.b + c + d


        def test(e, f):
            return e + f

        def testa(g):
            m = 42
            n = "test string"
            return g + "test string"
        '''
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_random_codestyles(self):
        parse_module(SAMPLE_CODE)

        for _ in range(100):
            codestyle = get_random_codestyle()
            transformed_code = apply_codestyle(SAMPLE_CODE, codestyle)
            parse_module(transformed_code)


if __name__ == "__main__":
    unittest.main()
