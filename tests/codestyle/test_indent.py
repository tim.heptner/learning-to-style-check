import textwrap
import unittest

from learning_to_style_check.codestyle import (
    CodestyleConfig,
    apply_codestyle,
    ClassStyleConfig,
    FunctionStyleConfig,
)
from learning_to_style_check.libcst import parse_module


class TestIndentStyle(unittest.TestCase):
    def test_default_spaces_indent(self):
        codestyle = CodestyleConfig(
            indent_char=" ", indent_size=3, blank_lines_top=1, blank_lines_bottom=1
        )
        code = textwrap.dedent(
            """
        def test():
         pass

        class Test:
         def test(self):
          pass
        """
        )
        styled_code = textwrap.dedent(
            """
        def test():
           pass

        class Test:
           def test(self):
              pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_default_tabs_indent(self):
        codestyle = CodestyleConfig(
            indent_char="\t", indent_size=3, blank_lines_top=1, blank_lines_bottom=1
        )
        code = textwrap.dedent(
            """
        def test():
        \tpass

        class Test:
        \tdef test(self):
        \t\tpass
        """
        )
        styled_code = textwrap.dedent(
            """
        def test():
        \t\t\tpass

        class Test:
        \t\t\tdef test(self):
        \t\t\t\t\t\tpass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_tabs_to_spaces_indent(self):
        codestyle = CodestyleConfig(
            indent_char=" ",
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(indent_size=1),
            function_style_config=FunctionStyleConfig(
                blank_lines_before=1, blank_lines_after=0, indent_size=1
            ),
        )
        code = textwrap.dedent(
            """
        def test():
        \tpass


        class Test:

        \tdef test(self):
        \t\tpass
        """
        )
        styled_code = textwrap.dedent(
            """
        def test():
         pass


        class Test:

         def test(self):
          pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_spaces_to_tabs_indent(self):
        codestyle = CodestyleConfig(
            indent_char="\t",
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(indent_size=1),
            function_style_config=FunctionStyleConfig(
                blank_lines_before=1, blank_lines_after=0, indent_size=1
            ),
        )
        code = textwrap.dedent(
            """
        def test():
         pass


        class Test:

         def test(self):
          pass
        """
        )
        styled_code = textwrap.dedent(
            """
        def test():
        \tpass


        class Test:

        \tdef test(self):
        \t\tpass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_spaces_different_indent_indent(self):
        codestyle = CodestyleConfig(
            indent_char=" ",
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(indent_size=2),
            function_style_config=FunctionStyleConfig(
                blank_lines_before=1, blank_lines_after=0, indent_size=1
            ),
        )
        code = textwrap.dedent(
            """
        def test():
         pass


        class Test:
        
         def test(self):
          pass
        """
        )
        styled_code = textwrap.dedent(
            """
        def test():
         pass


        class Test:
        
          def test(self):
           pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)


if __name__ == "__main__":
    unittest.main()
