import itertools
import textwrap
import unittest

from learning_to_style_check.codestyle import (
    CodestyleConfig,
    NameStyleConfig,
    apply_codestyle,
    ArgsStyleConfig,
    FunctionStyleConfig,
    TypingStyleConfig,
    TYPINGS,
    VarStyleConfig,
    ClassStyleConfig,
)
from learning_to_style_check.libcst import NameStyle, parse_module


class TestTypingStyle(unittest.TestCase):
    def test_var_typing(self):
        codestyle = CodestyleConfig(
            blank_lines_top=0,
            blank_lines_bottom=0,
            var_style_config=VarStyleConfig(
                name_style_config=NameStyleConfig(
                    name_style=NameStyle.LetterLow,
                ),
                typing_style_config=TypingStyleConfig(
                    whitespace_before_indicator="", whitespace_after_indicator=""
                ),
                whitespace_before_equal="",
                whitespace_after_equal="",
            ),
        )
        code = "a = 1"
        styled_codes = [f"a:{typing}=1" for typing in TYPINGS]
        transformed_code = apply_codestyle(code, codestyle)
        self.assertIn(transformed_code, styled_codes)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=0,
            blank_lines_bottom=0,
            var_style_config=VarStyleConfig(
                name_style_config=NameStyleConfig(
                    name_style=NameStyle.LetterLow,
                ),
                typing_style_config=TypingStyleConfig(
                    whitespace_before_indicator=" ", whitespace_after_indicator=" "
                ),
                whitespace_before_equal="",
                whitespace_after_equal="",
            ),
        )
        code = "a = 1"
        styled_codes = [f"a : {typing}=1" for typing in TYPINGS]
        transformed_code = apply_codestyle(code, codestyle)
        self.assertIn(transformed_code, styled_codes)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=0,
            blank_lines_bottom=0,
            var_style_config=VarStyleConfig(
                name_style_config=NameStyleConfig(
                    name_style=NameStyle.LetterLow,
                ),
                typing_style_config=TypingStyleConfig(
                    whitespace_before_indicator=" ", whitespace_after_indicator=" "
                ),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            ),
        )
        code = "a = 1"
        styled_codes = [f"a : {typing} = 1" for typing in TYPINGS]
        transformed_code = apply_codestyle(code, codestyle)
        self.assertIn(transformed_code, styled_codes)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=0,
            blank_lines_bottom=0,
            var_style_config=VarStyleConfig(
                name_style_config=NameStyleConfig(
                    name_style=NameStyle.LetterLow,
                ),
                typing_style_config=TypingStyleConfig(
                    whitespace_before_indicator="  ", whitespace_after_indicator="  "
                ),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            ),
        )
        code = "a = 1"
        styled_codes = [f"a  :  {typing} = 1" for typing in TYPINGS]
        transformed_code = apply_codestyle(code, codestyle)
        self.assertIn(transformed_code, styled_codes)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=0,
            blank_lines_bottom=0,
            var_style_config=VarStyleConfig(
                name_style_config=NameStyleConfig(
                    name_style=NameStyle.LetterLow,
                ),
                typing_style_config=TypingStyleConfig(
                    whitespace_before_indicator="\t", whitespace_after_indicator="\t"
                ),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            ),
        )
        code = "a = 1"
        styled_codes = [f"a\t:\t{typing} = 1" for typing in TYPINGS]
        transformed_code = apply_codestyle(code, codestyle)
        self.assertIn(transformed_code, styled_codes)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                var_style_config=VarStyleConfig(
                    name_style_config=NameStyleConfig(
                        name_style=NameStyle.LetterLow,
                    ),
                    typing_style_config=TypingStyleConfig(
                        whitespace_before_indicator="", whitespace_after_indicator=""
                    ),
                    whitespace_before_equal="",
                    whitespace_after_equal="",
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            a = 1
        """
        )
        styled_codes = [
            textwrap.dedent(
                f"""
        class Test:
            a:{typing}=1
        """
            )
            for typing in TYPINGS
        ]
        transformed_code = apply_codestyle(code, codestyle)
        self.assertIn(transformed_code, styled_codes)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                var_style_config=VarStyleConfig(
                    name_style_config=NameStyleConfig(
                        name_style=NameStyle.LetterLow,
                    ),
                    typing_style_config=TypingStyleConfig(
                        whitespace_before_indicator=" ", whitespace_after_indicator=" "
                    ),
                    whitespace_before_equal="",
                    whitespace_after_equal="",
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            a = 1
        """
        )
        styled_codes = [
            textwrap.dedent(
                f"""
        class Test:
            a : {typing}=1
        """
            )
            for typing in TYPINGS
        ]
        transformed_code = apply_codestyle(code, codestyle)
        self.assertIn(transformed_code, styled_codes)
        parse_module(transformed_code)

    def test_keep_existing_var_typing(self):
        codestyle = CodestyleConfig(
            blank_lines_top=0,
            blank_lines_bottom=0,
            var_style_config=VarStyleConfig(
                name_style_config=NameStyleConfig(
                    name_style=NameStyle.LetterLow,
                ),
                typing_style_config=TypingStyleConfig(
                    whitespace_before_indicator="", whitespace_after_indicator=""
                ),
                whitespace_before_equal="",
                whitespace_after_equal="",
            ),
        )
        code = "a: ABC = 1"
        styled_codes = "a:ABC=1"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_codes, transformed_code)
        parse_module(transformed_code)

    def test_remove_var_typing(self):
        codestyle = CodestyleConfig(
            blank_lines_top=0,
            blank_lines_bottom=0,
            var_style_config=VarStyleConfig(
                name_style_config=NameStyleConfig(
                    name_style=NameStyle.LetterLow,
                ),
                typing_style_config=None,
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            ),
        )
        code = "a: int = 1"
        styled_code = "a = 1"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=0,
            blank_lines_bottom=0,
            var_style_config=VarStyleConfig(
                name_style_config=NameStyleConfig(
                    name_style=NameStyle.LetterLow,
                ),
                typing_style_config=None,
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            ),
        )
        code = "a: int"
        styled_code = "a = 42"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=0,
            blank_lines_bottom=0,
            var_style_config=VarStyleConfig(
                name_style_config=NameStyleConfig(
                    name_style=NameStyle.LetterLow,
                ),
                typing_style_config=None,
                whitespace_before_equal="",
                whitespace_after_equal="",
            ),
        )
        code = "a: int = 1"
        styled_code = "a=1"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                var_style_config=VarStyleConfig(
                    name_style_config=NameStyleConfig(
                        name_style=NameStyle.LetterLow,
                    ),
                    typing_style_config=None,
                    whitespace_before_equal="",
                    whitespace_after_equal="",
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            a: int = 1
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            a=1
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                var_style_config=VarStyleConfig(
                    name_style_config=NameStyleConfig(
                        name_style=NameStyle.LetterLow,
                    ),
                    typing_style_config=None,
                    whitespace_before_equal="",
                    whitespace_after_equal="",
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            a: int
        """
        )
        styled_code = textwrap.dedent(
            """
        class Test:
            a=42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            var_style_config=VarStyleConfig(
                name_style_config=NameStyleConfig(
                    name_style=NameStyle.LetterLow,
                ),
                typing_style_config=None,
                whitespace_before_equal="",
                whitespace_after_equal="",
            ),
        )
        code = textwrap.dedent(
            """
        a: int
        """
        )
        styled_code = textwrap.dedent(
            """
        a=42
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_method_args_typing(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    blank_lines_after=0,
                    args_style_config=ArgsStyleConfig(
                        name_style_config=NameStyleConfig(
                            name_style=NameStyle.LetterLow,
                        ),
                        typing_style_config=TypingStyleConfig(
                            whitespace_before_indicator="",
                            whitespace_after_indicator="",
                        ),
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test(a):
                pass
        """
        )
        styled_codes = [
            textwrap.dedent(
                f"""
        class Test:
            def test(a:{typing}):
                pass
        """
            )
            for typing in TYPINGS
        ]
        transformed_code = apply_codestyle(code, codestyle)
        self.assertIn(transformed_code, styled_codes)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    blank_lines_after=0,
                    args_style_config=ArgsStyleConfig(
                        name_style_config=NameStyleConfig(
                            name_style=NameStyle.LetterLow,
                        ),
                        typing_style_config=TypingStyleConfig(
                            whitespace_before_indicator=" ",
                            whitespace_after_indicator=" ",
                        ),
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test(a):
                pass
        """
        )
        styled_codes = [
            textwrap.dedent(
                f"""
        class Test:
            def test(a : {typing}):
                pass
        """
            )
            for typing in TYPINGS
        ]
        transformed_code = apply_codestyle(code, codestyle)
        self.assertIn(transformed_code, styled_codes)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    blank_lines_after=0,
                    args_style_config=ArgsStyleConfig(
                        name_style_config=NameStyleConfig(
                            name_style=NameStyle.LetterLow,
                            whitespace_before=" ",
                        ),
                        typing_style_config=TypingStyleConfig(
                            whitespace_before_indicator=" ",
                            whitespace_after_indicator=" ",
                        ),
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test(a,b):
                pass
        """
        )
        styled_codes = [
            textwrap.dedent(
                f"""
        class Test:
            def test(a : {typing1}, a : {typing2}):
                pass
        """
            )
            for (typing1, typing2) in itertools.product(TYPINGS, TYPINGS)
        ]
        transformed_code = apply_codestyle(code, codestyle)
        self.assertIn(transformed_code, styled_codes)
        parse_module(transformed_code)

    def test_keep_existing_method_args_typing(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    blank_lines_after=0,
                    args_style_config=ArgsStyleConfig(
                        name_style_config=NameStyleConfig(
                            name_style=NameStyle.LetterLow,
                        ),
                        typing_style_config=TypingStyleConfig(
                            whitespace_before_indicator="",
                            whitespace_after_indicator="",
                        ),
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test(a:ABC):
                pass
        """
        )
        styled_codes = textwrap.dedent(
            f"""
        class Test:
            def test(a:ABC):
                pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_codes, transformed_code)
        parse_module(transformed_code)

    def test_remove_method_args_typing(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    blank_lines_after=0,
                    args_style_config=ArgsStyleConfig(
                        name_style_config=NameStyleConfig(
                            name_style=NameStyle.LetterLow,
                        ),
                        typing_style_config=None,
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test(a: List):
                pass
        """
        )
        styled_code = textwrap.dedent(
            f"""
        class Test:
            def test(a):
                pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    blank_lines_after=0,
                    args_style_config=ArgsStyleConfig(
                        name_style_config=NameStyleConfig(
                            name_style=NameStyle.LetterLow,
                        ),
                        typing_style_config=None,
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test(a: str):
                pass
        """
        )
        styled_code = textwrap.dedent(
            f"""
        class Test:
            def test(a):
                pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    blank_lines_after=0,
                    args_style_config=ArgsStyleConfig(
                        name_style_config=NameStyleConfig(
                            name_style=NameStyle.LetterLow,
                            whitespace_before=" ",
                        ),
                        typing_style_config=None,
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test(a: List, b: str):
                pass
        """
        )
        styled_code = textwrap.dedent(
            f"""
        class Test:
            def test(a, a):
                pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_method_typing(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    blank_lines_after=0,
                    args_style_config=ArgsStyleConfig(
                        name_style_config=NameStyleConfig(
                            name_style=NameStyle.LetterLow,
                        )
                    ),
                    typing_style_config=TypingStyleConfig(
                        whitespace_before_indicator="", whitespace_after_indicator=""
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test(a):
                pass
        """
        )
        styled_codes = [
            textwrap.dedent(
                f"""
        class Test:
            def test(a)->{typing}:
                pass
        """
            )
            for typing in TYPINGS
        ]
        transformed_code = apply_codestyle(code, codestyle)
        self.assertIn(transformed_code, styled_codes)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    blank_lines_after=0,
                    args_style_config=ArgsStyleConfig(
                        name_style_config=NameStyleConfig(
                            name_style=NameStyle.LetterLow,
                        )
                    ),
                    typing_style_config=TypingStyleConfig(
                        whitespace_before_indicator=" ", whitespace_after_indicator=" "
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test(a):
                pass
        """
        )
        styled_codes = [
            textwrap.dedent(
                f"""
        class Test:
            def test(a) -> {typing}:
                pass
        """
            )
            for typing in TYPINGS
        ]
        transformed_code = apply_codestyle(code, codestyle)
        self.assertIn(transformed_code, styled_codes)
        parse_module(transformed_code)

    def test_keep_existing_method_typing(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    blank_lines_after=0,
                    args_style_config=ArgsStyleConfig(
                        name_style_config=NameStyleConfig(
                            name_style=NameStyle.LetterLow,
                        )
                    ),
                    typing_style_config=TypingStyleConfig(
                        whitespace_before_indicator="", whitespace_after_indicator=""
                    ),
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test(a) -> ABC:
                pass
        """
        )
        styled_codes = textwrap.dedent(
            f"""
        class Test:
            def test(a)->ABC:
                pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_codes, transformed_code)
        parse_module(transformed_code)

    def test_remove_method_typing(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    blank_lines_after=0,
                    args_style_config=ArgsStyleConfig(
                        name_style_config=NameStyleConfig(
                            name_style=NameStyle.LetterLow,
                        )
                    ),
                    typing_style_config=None,
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test(a) -> List:
                pass
        """
        )
        styled_code = textwrap.dedent(
            f"""
        class Test:
            def test(a):
                pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            class_style_config=ClassStyleConfig(
                method_style_config=FunctionStyleConfig(
                    blank_lines_before=0,
                    blank_lines_after=0,
                    args_style_config=ArgsStyleConfig(
                        name_style_config=NameStyleConfig(
                            name_style=NameStyle.LetterLow,
                        )
                    ),
                    typing_style_config=None,
                )
            ),
        )
        code = textwrap.dedent(
            """
        class Test:
            def test(a) -> str:
                pass
        """
        )
        styled_code = textwrap.dedent(
            f"""
        class Test:
            def test(a):
                pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_function_args_typing(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                blank_lines_before=0,
                blank_lines_after=0,
                args_style_config=ArgsStyleConfig(
                    name_style_config=NameStyleConfig(
                        name_style=NameStyle.LetterLow,
                    ),
                    typing_style_config=TypingStyleConfig(
                        whitespace_before_indicator="", whitespace_after_indicator=""
                    ),
                ),
            ),
        )
        code = textwrap.dedent(
            """
        def test(a):
            pass
        """
        )
        styled_codes = [
            textwrap.dedent(
                f"""
        def test(a:{typing}):
            pass
        """
            )
            for typing in TYPINGS
        ]
        transformed_code = apply_codestyle(code, codestyle)
        self.assertIn(transformed_code, styled_codes)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                blank_lines_before=0,
                blank_lines_after=0,
                args_style_config=ArgsStyleConfig(
                    name_style_config=NameStyleConfig(
                        name_style=NameStyle.LetterLow,
                    ),
                    typing_style_config=TypingStyleConfig(
                        whitespace_before_indicator=" ", whitespace_after_indicator=" "
                    ),
                ),
            ),
        )
        code = textwrap.dedent(
            """
        def test(a):
            pass
        """
        )
        styled_codes = [
            textwrap.dedent(
                f"""
        def test(a : {typing}):
            pass
        """
            )
            for typing in TYPINGS
        ]
        transformed_code = apply_codestyle(code, codestyle)
        self.assertIn(transformed_code, styled_codes)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                blank_lines_before=0,
                blank_lines_after=0,
                args_style_config=ArgsStyleConfig(
                    name_style_config=NameStyleConfig(
                        name_style=NameStyle.LetterLow,
                        whitespace_before=" ",
                    ),
                    typing_style_config=TypingStyleConfig(
                        whitespace_before_indicator=" ", whitespace_after_indicator=" "
                    ),
                ),
            ),
        )
        code = textwrap.dedent(
            """
        def test(a,b):
            pass
        """
        )
        styled_codes = [
            textwrap.dedent(
                f"""
        def test(a : {typing1}, a : {typing2}):
            pass
        """
            )
            for (typing1, typing2) in itertools.product(TYPINGS, TYPINGS)
        ]
        transformed_code = apply_codestyle(code, codestyle)
        self.assertIn(transformed_code, styled_codes)
        parse_module(transformed_code)

    def test_keep_existing_function_args_typing(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                blank_lines_before=0,
                blank_lines_after=0,
                args_style_config=ArgsStyleConfig(
                    name_style_config=NameStyleConfig(
                        name_style=NameStyle.LetterLow,
                    ),
                    typing_style_config=TypingStyleConfig(
                        whitespace_before_indicator="", whitespace_after_indicator=""
                    ),
                ),
            ),
        )
        code = textwrap.dedent(
            """
        def test(a: ABC):
            pass
        """
        )
        styled_codes = textwrap.dedent(
            f"""
        def test(a:ABC):
            pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_codes, transformed_code)
        parse_module(transformed_code)

    def test_remove_function_args_typing(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                blank_lines_before=0,
                blank_lines_after=0,
                args_style_config=ArgsStyleConfig(
                    name_style_config=NameStyleConfig(
                        name_style=NameStyle.LetterLow,
                    ),
                    typing_style_config=None,
                ),
            ),
        )
        code = textwrap.dedent(
            """
        def test(a: List):
            pass
        """
        )
        styled_code = textwrap.dedent(
            f"""
        def test(a):
            pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                blank_lines_before=0,
                blank_lines_after=0,
                args_style_config=ArgsStyleConfig(
                    name_style_config=NameStyleConfig(
                        name_style=NameStyle.LetterLow,
                    ),
                    typing_style_config=None,
                ),
            ),
        )
        code = textwrap.dedent(
            """
        def test(a: str):
            pass
        """
        )
        styled_code = textwrap.dedent(
            f"""
        def test(a):
            pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                blank_lines_before=0,
                blank_lines_after=0,
                args_style_config=ArgsStyleConfig(
                    name_style_config=NameStyleConfig(
                        name_style=NameStyle.LetterLow,
                        whitespace_before=" ",
                    ),
                    typing_style_config=None,
                ),
            ),
        )
        code = textwrap.dedent(
            """
        def test(a: List, b: str):
            pass
        """
        )
        styled_code = textwrap.dedent(
            f"""
        def test(a, a):
            pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_function_typing(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                blank_lines_before=0,
                blank_lines_after=0,
                args_style_config=ArgsStyleConfig(
                    name_style_config=NameStyleConfig(
                        name_style=NameStyle.LetterLow,
                    )
                ),
                typing_style_config=TypingStyleConfig(
                    whitespace_before_indicator="", whitespace_after_indicator=""
                ),
            ),
        )
        code = textwrap.dedent(
            """
        def test(a):
            pass
        """
        )
        styled_codes = [
            textwrap.dedent(
                f"""
        def test(a)->{typing}:
            pass
        """
            )
            for typing in TYPINGS
        ]
        transformed_code = apply_codestyle(code, codestyle)
        self.assertIn(transformed_code, styled_codes)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                blank_lines_before=0,
                blank_lines_after=0,
                args_style_config=ArgsStyleConfig(
                    name_style_config=NameStyleConfig(
                        name_style=NameStyle.LetterLow,
                    )
                ),
                typing_style_config=TypingStyleConfig(
                    whitespace_before_indicator=" ", whitespace_after_indicator=" "
                ),
            ),
        )
        code = textwrap.dedent(
            """
        def test(a):
            pass
        """
        )
        styled_codes = [
            textwrap.dedent(
                f"""
        def test(a) -> {typing}:
            pass
        """
            )
            for typing in TYPINGS
        ]
        transformed_code = apply_codestyle(code, codestyle)
        self.assertIn(transformed_code, styled_codes)
        parse_module(transformed_code)

    def test_keep_existing_function_typing(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                blank_lines_before=0,
                blank_lines_after=0,
                args_style_config=ArgsStyleConfig(
                    name_style_config=NameStyleConfig(
                        name_style=NameStyle.LetterLow,
                    )
                ),
                typing_style_config=TypingStyleConfig(
                    whitespace_before_indicator="", whitespace_after_indicator=""
                ),
            ),
        )
        code = textwrap.dedent(
            """
        def test(a) -> ABC:
            pass
        """
        )
        styled_codes = textwrap.dedent(
            f"""
        def test(a)->ABC:
            pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_codes, transformed_code)
        parse_module(transformed_code)

    def test_remove_function_typing(self):
        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                blank_lines_before=0,
                blank_lines_after=0,
                args_style_config=ArgsStyleConfig(
                    name_style_config=NameStyleConfig(
                        name_style=NameStyle.LetterLow,
                    )
                ),
                typing_style_config=None,
            ),
        )
        code = textwrap.dedent(
            """
        def test(a) -> List:
            pass
        """
        )
        styled_code = textwrap.dedent(
            f"""
        def test(a):
            pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            blank_lines_top=1,
            blank_lines_bottom=1,
            function_style_config=FunctionStyleConfig(
                blank_lines_before=0,
                blank_lines_after=0,
                args_style_config=ArgsStyleConfig(
                    name_style_config=NameStyleConfig(
                        name_style=NameStyle.LetterLow,
                    )
                ),
                typing_style_config=None,
            ),
        )
        code = textwrap.dedent(
            """
        def test(a) -> str:
            pass
        """
        )
        styled_code = textwrap.dedent(
            f"""
        def test(a):
            pass
        """
        )
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_var_typing_in_lambda(self):
        codestyle = CodestyleConfig(
            blank_lines_top=0,
            blank_lines_bottom=0,
            var_style_config=VarStyleConfig(
                name_style_config=NameStyleConfig(
                    name_style=NameStyle.LetterLow,
                ),
                typing_style_config=TypingStyleConfig(
                    whitespace_before_indicator="", whitespace_after_indicator=""
                ),
                whitespace_before_equal="",
                whitespace_after_equal="",
            ),
        )
        code = "lambda a: 1"
        styled_code = "lambda a: 1"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)


if __name__ == "__main__":
    unittest.main()
