import unittest

from learning_to_style_check.codestyle import (
    CodestyleConfig,
    apply_codestyle,
    VarStyleConfig,
    NumberStyleConfig,
    NumberStyle,
)
from learning_to_style_check.libcst import parse_module


class TestNumberStyle(unittest.TestCase):
    def test_exp_style_low(self):
        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                number_style_config=NumberStyleConfig(exp_style=NumberStyle.LOW),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "a = 1e-10"
        styled_code = "a = 1e-10"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                number_style_config=NumberStyleConfig(exp_style=NumberStyle.LOW),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "a = 1E-10"
        styled_code = "a = 1e-10"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_exp_style_up(self):
        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                number_style_config=NumberStyleConfig(exp_style=NumberStyle.UP),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "a = 1e-10"
        styled_code = "a = 1E-10"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                number_style_config=NumberStyleConfig(exp_style=NumberStyle.UP),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "a = 1E-10"
        styled_code = "a = 1E-10"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_hex_style_low(self):
        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                number_style_config=NumberStyleConfig(hex_style=NumberStyle.LOW),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "a = 0x1"
        styled_code = "a = 0x1"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                number_style_config=NumberStyleConfig(hex_style=NumberStyle.LOW),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "a = 0X1"
        styled_code = "a = 0x1"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_hex_style_up(self):
        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                number_style_config=NumberStyleConfig(hex_style=NumberStyle.UP),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "a = 0x1"
        styled_code = "a = 0X1"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                number_style_config=NumberStyleConfig(hex_style=NumberStyle.UP),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "a = 0X1"
        styled_code = "a = 0X1"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_hex_letter_case(self):
        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                number_style_config=NumberStyleConfig(
                    hex_style=NumberStyle.LOW, hex_letter_case=NumberStyle.UP
                ),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "a = 0xabef123"
        styled_code = "a = 0xABEF123"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                number_style_config=NumberStyleConfig(
                    hex_style=NumberStyle.LOW, hex_letter_case=NumberStyle.LOW
                ),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "a = 0xabef123"
        styled_code = "a = 0xabef123"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                number_style_config=NumberStyleConfig(
                    hex_style=NumberStyle.LOW, hex_letter_case=NumberStyle.LOW
                ),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "a = 0xABEF123"
        styled_code = "a = 0xabef123"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                number_style_config=NumberStyleConfig(
                    hex_style=NumberStyle.LOW, hex_letter_case=NumberStyle.UP
                ),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "a = 0xABEF123"
        styled_code = "a = 0xABEF123"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                number_style_config=NumberStyleConfig(
                    hex_style=NumberStyle.LOW, hex_letter_case=NumberStyle.UP
                ),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "a = 0xABef123"
        styled_code = "a = 0xABEF123"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                number_style_config=NumberStyleConfig(
                    hex_style=NumberStyle.LOW, hex_letter_case=NumberStyle.LOW
                ),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "a = 0xABef123"
        styled_code = "a = 0xabef123"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_oct_style_low(self):
        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                number_style_config=NumberStyleConfig(oct_style=NumberStyle.LOW),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "a = 0o1"
        styled_code = "a = 0o1"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                number_style_config=NumberStyleConfig(oct_style=NumberStyle.LOW),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "a = 0O1"
        styled_code = "a = 0o1"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_oct_style_up(self):
        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                number_style_config=NumberStyleConfig(oct_style=NumberStyle.UP),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "a = 0o1"
        styled_code = "a = 0O1"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                number_style_config=NumberStyleConfig(oct_style=NumberStyle.UP),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "a = 0O1"
        styled_code = "a = 0O1"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_bin_style_low(self):
        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                number_style_config=NumberStyleConfig(bin_style=NumberStyle.LOW),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "a = 0b1"
        styled_code = "a = 0b1"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                number_style_config=NumberStyleConfig(bin_style=NumberStyle.LOW),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "a = 0B1"
        styled_code = "a = 0b1"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_bin_style_up(self):
        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                number_style_config=NumberStyleConfig(bin_style=NumberStyle.UP),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "a = 0b1"
        styled_code = "a = 0B1"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                number_style_config=NumberStyleConfig(bin_style=NumberStyle.UP),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "a = 0B1"
        styled_code = "a = 0B1"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_imag_style_low(self):
        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                number_style_config=NumberStyleConfig(imag_style=NumberStyle.LOW),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "a = 1j"
        styled_code = "a = 1j"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                number_style_config=NumberStyleConfig(imag_style=NumberStyle.LOW),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "a = 1J"
        styled_code = "a = 1j"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_imag_style_up(self):
        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                number_style_config=NumberStyleConfig(imag_style=NumberStyle.UP),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "a = 1j"
        styled_code = "a = 1J"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                number_style_config=NumberStyleConfig(imag_style=NumberStyle.UP),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "a = 1J"
        styled_code = "a = 1J"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_imag_style_low_exp_low(self):
        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                number_style_config=NumberStyleConfig(
                    imag_style=NumberStyle.LOW, exp_style=NumberStyle.LOW
                ),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "a = 42e42j"
        styled_code = "a = 42e42j"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                number_style_config=NumberStyleConfig(
                    imag_style=NumberStyle.LOW, exp_style=NumberStyle.LOW
                ),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "a = 42e42J"
        styled_code = "a = 42e42j"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                number_style_config=NumberStyleConfig(
                    imag_style=NumberStyle.LOW, exp_style=NumberStyle.LOW
                ),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "a = 42E42j"
        styled_code = "a = 42e42j"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                number_style_config=NumberStyleConfig(
                    imag_style=NumberStyle.LOW, exp_style=NumberStyle.LOW
                ),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "a = 42E42J"
        styled_code = "a = 42e42j"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_imag_style_up_exp_low(self):
        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                number_style_config=NumberStyleConfig(
                    imag_style=NumberStyle.UP, exp_style=NumberStyle.LOW
                ),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "a = 42e42j"
        styled_code = "a = 42e42J"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                number_style_config=NumberStyleConfig(
                    imag_style=NumberStyle.UP, exp_style=NumberStyle.LOW
                ),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "a = 42e42J"
        styled_code = "a = 42e42J"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                number_style_config=NumberStyleConfig(
                    imag_style=NumberStyle.UP, exp_style=NumberStyle.LOW
                ),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "a = 42E42j"
        styled_code = "a = 42e42J"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                number_style_config=NumberStyleConfig(
                    imag_style=NumberStyle.UP, exp_style=NumberStyle.LOW
                ),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "a = 42E42J"
        styled_code = "a = 42e42J"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_imag_style_low_exp_up(self):
        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                number_style_config=NumberStyleConfig(
                    imag_style=NumberStyle.LOW, exp_style=NumberStyle.UP
                ),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "a = 42e42j"
        styled_code = "a = 42E42j"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                number_style_config=NumberStyleConfig(
                    imag_style=NumberStyle.LOW, exp_style=NumberStyle.UP
                ),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "a = 42e42J"
        styled_code = "a = 42E42j"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                number_style_config=NumberStyleConfig(
                    imag_style=NumberStyle.LOW, exp_style=NumberStyle.UP
                ),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "a = 42E42j"
        styled_code = "a = 42E42j"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                number_style_config=NumberStyleConfig(
                    imag_style=NumberStyle.LOW, exp_style=NumberStyle.UP
                ),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "a = 42E42J"
        styled_code = "a = 42E42j"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_imag_style_up_exp_up(self):
        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                number_style_config=NumberStyleConfig(
                    imag_style=NumberStyle.UP, exp_style=NumberStyle.UP
                ),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "a = 42e42j"
        styled_code = "a = 42E42J"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                number_style_config=NumberStyleConfig(
                    imag_style=NumberStyle.UP, exp_style=NumberStyle.UP
                ),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "a = 42e42J"
        styled_code = "a = 42E42J"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                number_style_config=NumberStyleConfig(
                    imag_style=NumberStyle.UP, exp_style=NumberStyle.UP
                ),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "a = 42E42j"
        styled_code = "a = 42E42J"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                number_style_config=NumberStyleConfig(
                    imag_style=NumberStyle.UP, exp_style=NumberStyle.UP
                ),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "a = 42E42J"
        styled_code = "a = 42E42J"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)


class TestNumberUnderscoreStyle(unittest.TestCase):
    def test_int_underscore(self):
        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                number_style_config=NumberStyleConfig(int_underscore_every=3),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "a = 1234567890123"
        styled_code = "a = 1_234_567_890_123"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                number_style_config=NumberStyleConfig(int_underscore_every=2),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "a = 1234567890123"
        styled_code = "a = 1_23_45_67_89_01_23"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                number_style_config=NumberStyleConfig(int_underscore_every=1),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "a = 1234567890123"
        styled_code = "a = 1_2_3_4_5_6_7_8_9_0_1_2_3"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                number_style_config=NumberStyleConfig(int_underscore_every=0),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "a = 1_2_3_4_5_6_7_8_9_0_1_2_3"
        styled_code = "a = 1234567890123"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                number_style_config=NumberStyleConfig(int_underscore_every=3),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "a = 123456789_0123"
        styled_code = "a = 1_234_567_890_123"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                number_style_config=NumberStyleConfig(int_underscore_every=3),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "a = 1000000000003"
        styled_code = "a = 1_000_000_000_003"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                number_style_config=NumberStyleConfig(int_underscore_every=1),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "a = 1"
        styled_code = "a = 1"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                number_style_config=NumberStyleConfig(int_underscore_every=1),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "a = 0"
        styled_code = "a = 0"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                number_style_config=NumberStyleConfig(int_underscore_every=1),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "a = -1"
        styled_code = "a = -1"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_float_underscore(self):
        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                number_style_config=NumberStyleConfig(float_underscore_every=3),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "a = 123456.7890123"
        styled_code = "a = 123_456.7_890_123"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                number_style_config=NumberStyleConfig(float_underscore_every=2),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "a = 123456.7890123"
        styled_code = "a = 12_34_56.7_89_01_23"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                number_style_config=NumberStyleConfig(float_underscore_every=1),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "a = 123456.7890123"
        styled_code = "a = 1_2_3_4_5_6.7_8_9_0_1_2_3"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                number_style_config=NumberStyleConfig(int_underscore_every=0),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "a = 1_2_3_4_5_6.7_8_9_0_1_2_3"
        styled_code = "a = 123456.7890123"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                number_style_config=NumberStyleConfig(float_underscore_every=3),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "a = 123456.7890_123"
        styled_code = "a = 123_456.7_890_123"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                number_style_config=NumberStyleConfig(float_underscore_every=3),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "a = 120000.0000123"
        styled_code = "a = 120_000.0_000_123"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                number_style_config=NumberStyleConfig(float_underscore_every=1),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "a = 1.1"
        styled_code = "a = 1.1"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                number_style_config=NumberStyleConfig(float_underscore_every=1),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "a = 0.0"
        styled_code = "a = 0.0"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                number_style_config=NumberStyleConfig(float_underscore_every=1),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "a = -1.0"
        styled_code = "a = -1.0"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                number_style_config=NumberStyleConfig(float_underscore_every=1),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "a = .0"
        styled_code = "a = .0"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                number_style_config=NumberStyleConfig(float_underscore_every=1),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "a = 0."
        styled_code = "a = 0."
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_exp_underscore(self):
        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                number_style_config=NumberStyleConfig(exp_underscore_every=3),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "a = 1234567890123e123"
        styled_code = "a = 1_234_567_890_123e123"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                number_style_config=NumberStyleConfig(exp_underscore_every=2),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "a = 1234567890123e123"
        styled_code = "a = 1_23_45_67_89_01_23e1_23"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                number_style_config=NumberStyleConfig(exp_underscore_every=1),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "a = 1234567890123e123"
        styled_code = "a = 1_2_3_4_5_6_7_8_9_0_1_2_3e1_2_3"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                number_style_config=NumberStyleConfig(exp_underscore_every=0),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "a = 1_2_3_4_5_6_7_8_9_0_1_2_3e1_2_3"
        styled_code = "a = 1234567890123e123"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                number_style_config=NumberStyleConfig(exp_underscore_every=3),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "a = 1234567890000e0000123"
        styled_code = "a = 1_234_567_890_000e0_000_123"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                number_style_config=NumberStyleConfig(exp_underscore_every=1),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "a = 1e0"
        styled_code = "a = 1e0"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                number_style_config=NumberStyleConfig(exp_underscore_every=1),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "a = 0e0"
        styled_code = "a = 0e0"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                number_style_config=NumberStyleConfig(exp_underscore_every=2),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "a = 0.000e-10"
        styled_code = "a = 0.0_00e-10"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                number_style_config=NumberStyleConfig(exp_underscore_every=1),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "a = 0e-0"
        styled_code = "a = 0e-0"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                number_style_config=NumberStyleConfig(exp_underscore_every=1),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "a = 0e-1"
        styled_code = "a = 0e-1"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_hex_underscore(self):
        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                number_style_config=NumberStyleConfig(hex_underscore_every=3),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "a = 0x123ffaffefff"
        styled_code = "a = 0x123_ffa_ffe_fff"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                number_style_config=NumberStyleConfig(hex_underscore_every=2),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "a = 0x123ffaffefff"
        styled_code = "a = 0x12_3f_fa_ff_ef_ff"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                number_style_config=NumberStyleConfig(hex_underscore_every=1),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "a = 0x123ffaffefff"
        styled_code = "a = 0x1_2_3_f_f_a_f_f_e_f_f_f"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                number_style_config=NumberStyleConfig(int_underscore_every=0),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "a = 0x1_2_3_f_f_a_f_f_e_f_f_f"
        styled_code = "a = 0x123ffaffefff"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                number_style_config=NumberStyleConfig(hex_underscore_every=3),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "a = 0x0000123ffaffefff"
        styled_code = "a = 0x0_000_123_ffa_ffe_fff"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                number_style_config=NumberStyleConfig(hex_underscore_every=3),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "a = 0x1"
        styled_code = "a = 0x1"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                number_style_config=NumberStyleConfig(hex_underscore_every=3),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "a = 0x0"
        styled_code = "a = 0x0"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_oct_underscore(self):
        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                number_style_config=NumberStyleConfig(oct_underscore_every=3),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "a = 0o12345671234"
        styled_code = "a = 0o12_345_671_234"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                number_style_config=NumberStyleConfig(oct_underscore_every=2),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "a = 0o12345671234"
        styled_code = "a = 0o1_23_45_67_12_34"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                number_style_config=NumberStyleConfig(oct_underscore_every=1),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "a = 0o12345671234"
        styled_code = "a = 0o1_2_3_4_5_6_7_1_2_3_4"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                number_style_config=NumberStyleConfig(oct_underscore_every=0),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "a = 0o1_2_3_4_5_6_7_1_2_3_4"
        styled_code = "a = 0o12345671234"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                number_style_config=NumberStyleConfig(oct_underscore_every=1),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "a = 0o1"
        styled_code = "a = 0o1"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                number_style_config=NumberStyleConfig(oct_underscore_every=1),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "a = 0o0"
        styled_code = "a = 0o0"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_bin_underscore(self):
        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                number_style_config=NumberStyleConfig(bin_underscore_every=3),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "a = 0b101010101010101"
        styled_code = "a = 0b101_010_101_010_101"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                number_style_config=NumberStyleConfig(bin_underscore_every=2),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "a = 0b101010101010101"
        styled_code = "a = 0b1_01_01_01_01_01_01_01"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                number_style_config=NumberStyleConfig(bin_underscore_every=1),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "a = 0b101010101010101"
        styled_code = "a = 0b1_0_1_0_1_0_1_0_1_0_1_0_1_0_1"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                number_style_config=NumberStyleConfig(int_underscore_every=0),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "a = 0b1_0_1_0_1_0_1_0_1_0_1_0_1_0_1"
        styled_code = "a = 0b101010101010101"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                number_style_config=NumberStyleConfig(bin_underscore_every=3),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "a = 0b0000101010101010101"
        styled_code = "a = 0b0_000_101_010_101_010_101"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                number_style_config=NumberStyleConfig(bin_underscore_every=3),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "a = 0b1"
        styled_code = "a = 0b1"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                number_style_config=NumberStyleConfig(bin_underscore_every=3),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "a = 0b0"
        styled_code = "a = 0b0"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

    def test_imag_underscore(self):
        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                number_style_config=NumberStyleConfig(imag_underscore_every=3),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "a = 123.456789012j"
        styled_code = "a = 123.456_789_012j"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                number_style_config=NumberStyleConfig(imag_underscore_every=2),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "a = 123.456789012j"
        styled_code = "a = 1_23.4_56_78_90_12j"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                number_style_config=NumberStyleConfig(imag_underscore_every=1),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "a = 123.456789012j"
        styled_code = "a = 1_2_3.4_5_6_7_8_9_0_1_2j"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                number_style_config=NumberStyleConfig(imag_underscore_every=0),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "a = 1_2_3.4_5_6_7_8_9_0_1_2j"
        styled_code = "a = 123.456789012j"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                number_style_config=NumberStyleConfig(imag_underscore_every=3),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "a = 1230000.0000456789012j"
        styled_code = "a = 1_230_000.0_000_456_789_012j"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                number_style_config=NumberStyleConfig(imag_underscore_every=1),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "a = 0.1j"
        styled_code = "a = 0.1j"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)

        codestyle = CodestyleConfig(
            var_style_config=VarStyleConfig(
                number_style_config=NumberStyleConfig(imag_underscore_every=3),
                whitespace_before_equal=" ",
                whitespace_after_equal=" ",
            )
        )
        code = "a = 0.0j"
        styled_code = "a = 0.0j"
        transformed_code = apply_codestyle(code, codestyle)
        self.assertEqual(styled_code, transformed_code)
        parse_module(transformed_code)


if __name__ == "__main__":
    unittest.main()
