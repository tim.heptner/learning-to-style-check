import textwrap
import unittest

from learning_to_style_check.features import tokenize


class TestTokenizer(unittest.TestCase):
    def test_int_number_tokenization(self):
        self.assertEqual(["<INT>"], tokenize("0"))
        self.assertEqual(["<INT>"], tokenize("10"))
        self.assertEqual(["<INT>"], tokenize("4242"))
        self.assertEqual(["<INT>"], tokenize("-4242"))
        self.assertEqual(["-", "<SPACE1>", "<INT>"], tokenize("- 4242"))
        self.assertEqual(
            ["<INT>", "<SPACE1>", "+", "<SPACE1>", "<INT>"], tokenize("-4242 + 5")
        )

    def test_float_number_tokenization(self):
        self.assertEqual(["<FLOAT>"], tokenize("42.42"))
        self.assertEqual(["<FLOAT>"], tokenize("-42.42"))
        self.assertEqual(["-", "<SPACE1>", "<FLOAT>"], tokenize("- 42.42"))
        self.assertEqual(["<FLOAT>"], tokenize("42."))
        self.assertEqual(["<FLOAT>"], tokenize(".42"))

    def test_scientific_notation_tokenization(self):
        self.assertEqual(["<INT>", "<EXP_LOW>", "<INT>"], tokenize("4242e2"))
        self.assertEqual(["<INT>", "<EXP_LOW>", "<INT>"], tokenize("-4e2"))
        self.assertEqual(
            ["-", "<SPACE1>", "<INT>", "<EXP_LOW>", "<INT>"], tokenize("- 4e2")
        )
        self.assertEqual(["<INT>", "<EXP_LOW>", "<INT>"], tokenize("4e2"))
        self.assertEqual(["<INT>", "<EXP_UP>", "<INT>"], tokenize("4242E2"))
        self.assertEqual(["<INT>", "<EXP_UP>", "<INT>"], tokenize("4E2"))

        self.assertEqual(["<INT>", "<EXP_LOW>", "<INT>"], tokenize("4242e+2"))
        self.assertEqual(["<INT>", "<EXP_LOW>", "<INT>"], tokenize("4242e-2"))
        self.assertEqual(["<INT>", "<EXP_UP>", "<INT>"], tokenize("4242E+2"))
        self.assertEqual(["<INT>", "<EXP_UP>", "<INT>"], tokenize("4242E-2"))

        self.assertEqual(["<FLOAT>", "<EXP_LOW>", "<INT>"], tokenize("4.2e2"))
        self.assertEqual(["<FLOAT>", "<EXP_LOW>", "<INT>"], tokenize("-4.2e2"))
        self.assertEqual(
            ["-", "<SPACE1>", "<FLOAT>", "<EXP_LOW>", "<INT>"], tokenize("- 4.2e2")
        )
        self.assertEqual(["<FLOAT>", "<EXP_LOW>", "<INT>"], tokenize("42.2e2"))
        self.assertEqual(["<FLOAT>", "<EXP_LOW>", "<INT>"], tokenize(".2e2"))
        self.assertEqual(["<FLOAT>", "<EXP_LOW>", "<INT>"], tokenize("4.e2"))
        self.assertEqual(["<FLOAT>", "<EXP_UP>", "<INT>"], tokenize("4.2E2"))
        self.assertEqual(["<FLOAT>", "<EXP_UP>", "<INT>"], tokenize("42.2E2"))
        self.assertEqual(["<FLOAT>", "<EXP_UP>", "<INT>"], tokenize(".2E2"))
        self.assertEqual(["<FLOAT>", "<EXP_UP>", "<INT>"], tokenize("4.E2"))

        self.assertEqual(["<FLOAT>", "<EXP_LOW>", "<INT>"], tokenize("4.2e+2"))
        self.assertEqual(["<FLOAT>", "<EXP_LOW>", "<INT>"], tokenize("4.2e-2"))

        self.assertEqual(["<INT>", "<EXP_LOW>", "<INT>"], tokenize("1e0"))
        self.assertEqual(["<INT>", "<EXP_LOW>", "<INT>"], tokenize("1e+0"))
        self.assertEqual(["<INT>", "<EXP_LOW>", "<INT>"], tokenize("1e-0"))
        self.assertEqual(["<INT>", "<EXP_LOW>", "<INT>"], tokenize("1e00"))
        self.assertEqual(["<INT>", "<EXP_LOW>", "<INT>"], tokenize("1e+00"))
        self.assertEqual(["<INT>", "<EXP_LOW>", "<INT>"], tokenize("1e-00"))

        self.assertEqual(["<INT>", "<EXP_LOW>", "<INT>"], tokenize("1e04"))
        self.assertEqual(["<INT>", "<EXP_LOW>", "<INT>"], tokenize("1e+04"))
        self.assertEqual(["<INT>", "<EXP_LOW>", "<INT>"], tokenize("1e-04"))
        self.assertEqual(["<INT>", "<EXP_LOW>", "<INT>"], tokenize("1e-004"))

        self.assertEqual(["<INT>", "<EXP_UP>", "<INT>"], tokenize("1E0"))
        self.assertEqual(["<INT>", "<EXP_UP>", "<INT>"], tokenize("1E+0"))
        self.assertEqual(["<INT>", "<EXP_UP>", "<INT>"], tokenize("1E-0"))
        self.assertEqual(["<INT>", "<EXP_UP>", "<INT>"], tokenize("1E00"))
        self.assertEqual(["<INT>", "<EXP_UP>", "<INT>"], tokenize("1E+00"))
        self.assertEqual(["<INT>", "<EXP_UP>", "<INT>"], tokenize("1E-00"))

        self.assertEqual(["<INT>", "<EXP_UP>", "<INT>"], tokenize("1E04"))
        self.assertEqual(["<INT>", "<EXP_UP>", "<INT>"], tokenize("1E+04"))
        self.assertEqual(["<INT>", "<EXP_UP>", "<INT>"], tokenize("1E-04"))
        self.assertEqual(["<INT>", "<EXP_UP>", "<INT>"], tokenize("1E-004"))

        self.assertEqual(["<FLOAT>", "<EXP_LOW>", "<INT>"], tokenize("1.0e0"))
        self.assertEqual(["<FLOAT>", "<EXP_LOW>", "<INT>"], tokenize("1.0e04"))
        self.assertEqual(["<FLOAT>", "<EXP_LOW>", "<INT>"], tokenize("1.0e+04"))
        self.assertEqual(["<FLOAT>", "<EXP_LOW>", "<INT>"], tokenize("1.0e-04"))
        self.assertEqual(["<FLOAT>", "<EXP_LOW>", "<INT>"], tokenize("1.0e-004"))
        self.assertEqual(["<FLOAT>", "<EXP_UP>", "<INT>"], tokenize("1.0E0"))
        self.assertEqual(["<FLOAT>", "<EXP_UP>", "<INT>"], tokenize("1.0E04"))
        self.assertEqual(["<FLOAT>", "<EXP_UP>", "<INT>"], tokenize("1.0E+04"))
        self.assertEqual(["<FLOAT>", "<EXP_UP>", "<INT>"], tokenize("1.0E-04"))
        self.assertEqual(["<FLOAT>", "<EXP_UP>", "<INT>"], tokenize("1.0E-004"))

        self.assertEqual(
            ["<INT>", "<EXP_LOW>", "<INT>", "<IMAGINARY_LOW>"], tokenize("4e0j")
        )
        self.assertEqual(
            ["<INT>", "<EXP_LOW>", "<INT>", "<IMAGINARY_LOW>"], tokenize("4e2j")
        )
        self.assertEqual(
            ["<INT>", "<EXP_LOW>", "<INT>", "<IMAGINARY_LOW>"], tokenize("4e02j")
        )
        self.assertEqual(
            ["<INT>", "<EXP_LOW>", "<INT>", "<IMAGINARY_LOW>"], tokenize("4e002j")
        )

        self.assertEqual(
            ["<INT>", "<EXP_UP>", "<INT>", "<IMAGINARY_LOW>"], tokenize("4E0j")
        )
        self.assertEqual(
            ["<INT>", "<EXP_UP>", "<INT>", "<IMAGINARY_LOW>"], tokenize("4E2j")
        )
        self.assertEqual(
            ["<INT>", "<EXP_UP>", "<INT>", "<IMAGINARY_LOW>"], tokenize("4E02j")
        )
        self.assertEqual(
            ["<INT>", "<EXP_UP>", "<INT>", "<IMAGINARY_LOW>"], tokenize("4E002j")
        )

        self.assertEqual(
            ["<INT>", "<EXP_LOW>", "<INT>", "<IMAGINARY_UP>"], tokenize("4e0J")
        )
        self.assertEqual(
            ["<INT>", "<EXP_LOW>", "<INT>", "<IMAGINARY_UP>"], tokenize("4e2J")
        )
        self.assertEqual(
            ["<INT>", "<EXP_LOW>", "<INT>", "<IMAGINARY_UP>"], tokenize("4e02J")
        )
        self.assertEqual(
            ["<INT>", "<EXP_LOW>", "<INT>", "<IMAGINARY_UP>"], tokenize("4e002J")
        )

        self.assertEqual(
            ["<INT>", "<EXP_UP>", "<INT>", "<IMAGINARY_UP>"], tokenize("4E0J")
        )
        self.assertEqual(
            ["<INT>", "<EXP_UP>", "<INT>", "<IMAGINARY_UP>"], tokenize("4E2J")
        )
        self.assertEqual(
            ["<INT>", "<EXP_UP>", "<INT>", "<IMAGINARY_UP>"], tokenize("4E02J")
        )
        self.assertEqual(
            ["<INT>", "<EXP_UP>", "<INT>", "<IMAGINARY_UP>"], tokenize("4E002J")
        )

        self.assertEqual(
            ["<INT>", "+", "<INT>", "<EXP_LOW>", "<INT>", "<IMAGINARY_LOW>"],
            tokenize("43+4e2j"),
        )
        self.assertEqual(
            ["<INT>", "+", "<INT>", "<EXP_LOW>", "<INT>", "<IMAGINARY_UP>"],
            tokenize("43+4e2J"),
        )
        self.assertEqual(
            ["<FLOAT>", "<EXP_LOW>", "<INT>", "<IMAGINARY_LOW>"], tokenize("4.4e2j")
        )

        self.assertEqual(
            ["<FLOAT>", "<EXP_LOW>", "<INT>", "<IMAGINARY_UP>"], tokenize("4.4e2J")
        )
        self.assertEqual(
            ["<FLOAT>", "<EXP_LOW>", "<INT>", "<IMAGINARY_UP>"], tokenize("-4.4e2J")
        )
        self.assertEqual(
            ["-", "<SPACE1>", "<FLOAT>", "<EXP_LOW>", "<INT>", "<IMAGINARY_UP>"],
            tokenize("- 4.4e2J"),
        )
        self.assertEqual(
            ["<FLOAT>", "<EXP_UP>", "<INT>", "<IMAGINARY_UP>"], tokenize("4.4E2J")
        )

    def test_hexadecimal_tokenization(self):
        self.assertEqual(["<HEX_LOW>", "<HEX_LETTER_LOW>"], tokenize("0x00"))
        self.assertEqual(["<HEX_LOW>", "<HEX_LETTER_LOW>"], tokenize("0x42"))
        self.assertEqual(["<HEX_LOW>", "<HEX_LETTER_LOW>"], tokenize("-0x42"))
        self.assertEqual(
            ["-", "<SPACE1>", "<HEX_LOW>", "<HEX_LETTER_LOW>"], tokenize("- 0x42")
        )
        self.assertEqual(["<HEX_LOW>", "<HEX_LETTER_LOW>"], tokenize("0xa2"))
        self.assertEqual(["<HEX_LOW>", "<HEX_LETTER_LOW>"], tokenize("0x7fff"))
        self.assertEqual(["<HEX_UP>", "<HEX_LETTER_LOW>"], tokenize("0X00"))
        self.assertEqual(["<HEX_UP>", "<HEX_LETTER_LOW>"], tokenize("0X42"))
        self.assertEqual(["<HEX_UP>", "<HEX_LETTER_LOW>"], tokenize("-0X42"))
        self.assertEqual(
            ["-", "<SPACE1>", "<HEX_UP>", "<HEX_LETTER_LOW>"], tokenize("- 0X42")
        )
        self.assertEqual(["<HEX_UP>", "<HEX_LETTER_UP>"], tokenize("0XA2"))
        self.assertEqual(["<HEX_UP>", "<HEX_LETTER_UP>"], tokenize("0X7FFF"))

        self.assertEqual(["<HEX_UP>", "<HEX_LETTER_MIXED>"], tokenize("0XA2a"))
        self.assertEqual(["<HEX_UP>", "<HEX_LETTER_MIXED>"], tokenize("0X7FfF"))

    def test_octal_tokenization(self):
        self.assertEqual(["<OCTAL_LOW>", "<INT>"], tokenize("0o00"))
        self.assertEqual(["<OCTAL_LOW>", "<INT>"], tokenize("0o42"))
        self.assertEqual(["<OCTAL_LOW>", "<INT>"], tokenize("-0o42"))
        self.assertEqual(["-", "<SPACE1>", "<OCTAL_LOW>", "<INT>"], tokenize("- 0o42"))
        self.assertEqual(["<OCTAL_LOW>", "<INT>"], tokenize("0o177"))
        self.assertEqual(["<OCTAL_LOW>", "<INT>"], tokenize("0o200"))

        self.assertEqual(["<OCTAL_UP>", "<INT>"], tokenize("0O00"))
        self.assertEqual(["<OCTAL_UP>", "<INT>"], tokenize("0O42"))
        self.assertEqual(["<OCTAL_UP>", "<INT>"], tokenize("-0O42"))
        self.assertEqual(["-", "<SPACE1>", "<OCTAL_UP>", "<INT>"], tokenize("- 0O42"))
        self.assertEqual(["<OCTAL_UP>", "<INT>"], tokenize("0O177"))
        self.assertEqual(["<OCTAL_UP>", "<INT>"], tokenize("0O200"))

    def test_binary_tokenization(self):
        self.assertEqual(["<BINARY_LOW>", "<BINARY>"], tokenize("0b00"))
        self.assertEqual(["<BINARY_LOW>", "<BINARY>"], tokenize("0b01"))
        self.assertEqual(["<BINARY_LOW>", "<BINARY>"], tokenize("-0b01"))
        self.assertEqual(
            ["-", "<SPACE1>", "<BINARY_LOW>", "<BINARY>"], tokenize("- 0b01")
        )
        self.assertEqual(["<BINARY_LOW>", "<BINARY>"], tokenize("0b1111"))
        self.assertEqual(["<BINARY_LOW>", "<BINARY>"], tokenize("0b1000"))

        self.assertEqual(["<BINARY_UP>", "<BINARY>"], tokenize("0B00"))
        self.assertEqual(["<BINARY_UP>", "<BINARY>"], tokenize("0B01"))
        self.assertEqual(["<BINARY_UP>", "<BINARY>"], tokenize("-0B01"))
        self.assertEqual(
            ["-", "<SPACE1>", "<BINARY_UP>", "<BINARY>"], tokenize("- 0B01")
        )
        self.assertEqual(["<BINARY_UP>", "<BINARY>"], tokenize("0B1111"))
        self.assertEqual(["<BINARY_UP>", "<BINARY>"], tokenize("0B1000"))

    def test_complex_number_tokenization(self):
        self.assertEqual(["<INT>", "<IMAGINARY_LOW>"], tokenize("0j"))
        self.assertEqual(["<INT>", "<IMAGINARY_LOW>"], tokenize("42j"))
        self.assertEqual(["<INT>", "<IMAGINARY_LOW>"], tokenize("-42j"))
        self.assertEqual(
            ["-", "<SPACE1>", "<INT>", "<IMAGINARY_LOW>"], tokenize("- 42j")
        )

        self.assertEqual(["<INT>", "<IMAGINARY_UP>"], tokenize("0J"))
        self.assertEqual(["<INT>", "<IMAGINARY_UP>"], tokenize("42J"))
        self.assertEqual(["<INT>", "<IMAGINARY_UP>"], tokenize("-42J"))
        self.assertEqual(
            ["-", "<SPACE1>", "<INT>", "<IMAGINARY_UP>"], tokenize("- 42J")
        )

        self.assertEqual(["<INT>", "+", "<INT>", "<IMAGINARY_LOW>"], tokenize("43+42j"))
        self.assertEqual(
            ["<INT>", "+", "<INT>", "<IMAGINARY_LOW>"], tokenize("-43+42j")
        )
        self.assertEqual(
            ["-", "<SPACE1>", "<INT>", "+", "<INT>", "<IMAGINARY_LOW>"],
            tokenize("- 43+42j"),
        )
        self.assertEqual(["<INT>", "-", "<INT>", "<IMAGINARY_LOW>"], tokenize("43-42j"))

        self.assertEqual(["<INT>", "+", "<INT>", "<IMAGINARY_UP>"], tokenize("43+42J"))
        self.assertEqual(["<INT>", "+", "<INT>", "<IMAGINARY_UP>"], tokenize("-43+42J"))
        self.assertEqual(
            ["-", "<SPACE1>", "<INT>", "+", "<INT>", "<IMAGINARY_UP>"],
            tokenize("- 43+42J"),
        )
        self.assertEqual(["<INT>", "-", "<INT>", "<IMAGINARY_UP>"], tokenize("43-42J"))

        self.assertEqual(["<FLOAT>", "<IMAGINARY_LOW>"], tokenize("4.42j"))
        self.assertEqual(
            ["<FLOAT>", "-", "<INT>", "<IMAGINARY_LOW>"], tokenize("4.42-42j")
        )
        self.assertEqual(
            ["<FLOAT>", "-", "<INT>", "<IMAGINARY_LOW>"], tokenize("-4.42-42j")
        )
        self.assertEqual(
            ["-", "<SPACE1>", "<FLOAT>", "-", "<INT>", "<IMAGINARY_LOW>"],
            tokenize("- 4.42-42j"),
        )
        self.assertEqual(
            ["<FLOAT>", "-", "<FLOAT>", "<IMAGINARY_LOW>"], tokenize("4.42-4.2j")
        )
        self.assertEqual(["<FLOAT>", "<IMAGINARY_LOW>"], tokenize(".4j"))
        self.assertEqual(["<FLOAT>", "<IMAGINARY_UP>"], tokenize("4.42J"))
        self.assertEqual(["<FLOAT>", "<IMAGINARY_UP>"], tokenize("-4.42J"))
        self.assertEqual(
            ["-", "<SPACE1>", "<FLOAT>", "<IMAGINARY_UP>"], tokenize("- 4.42J")
        )
        self.assertEqual(["<FLOAT>", "<IMAGINARY_UP>"], tokenize(".4J"))

        self.assertEqual(
            ["<INT>", "<IMAGINARY_UP>", ".", "<LOWERCASE>"], tokenize("123J.real")
        )

    def test_name_tokenization(self):
        self.assertEqual(["<LETTER_LOW>"], tokenize("a"))
        self.assertEqual(["<LETTER_UP>"], tokenize("U"))
        self.assertEqual(["<LOWERCASE>"], tokenize("lower"))
        self.assertEqual(["<UPPER_CAMEL_CASE>"], tokenize("UpperCamelCase"))
        self.assertEqual(["<UPPER_CAMEL_CASE>"], tokenize("Up"))
        self.assertEqual(["<UPPER_CAMEL_CASE2>"], tokenize("TestHTTP"))
        self.assertEqual(["<UPPER_CAMEL_CASE2>"], tokenize("TestHTTPserver"))
        self.assertEqual(["<LOWER_CAMEL_CASE>"], tokenize("lowerCamelCase"))
        self.assertEqual(["<LOWER_CAMEL_CASE2>"], tokenize("testHTTP"))
        self.assertEqual(["<LOWER_CAMEL_CASE2>"], tokenize("testHTTPserver"))
        self.assertEqual(["<SCREAMING_SNAKE_CASE>"], tokenize("SCREAMING_SNAKE_CASE"))
        self.assertEqual(["<SNAKE_CASE>"], tokenize("snake_case"))
        self.assertEqual(["<MAGIC_NAME>"], tokenize("__init__"))
        self.assertEqual(["<MAGIC_NAME>"], tokenize("__42__"))

        self.assertEqual(
            ["<SINGLE_LEADING_UNDERSCORE_NAME>", "<LETTER_LOW>"], tokenize("_s")
        )
        self.assertEqual(
            ["<SINGLE_LEADING_UNDERSCORE_NAME>", "<LETTER_UP>"], tokenize("_U")
        )
        self.assertEqual(
            ["<SINGLE_LEADING_UNDERSCORE_NAME>", "<LOWERCASE>"], tokenize("_lower")
        )
        self.assertEqual(
            ["<SINGLE_LEADING_UNDERSCORE_NAME>", "<UPPER_CAMEL_CASE>"],
            tokenize("_UpperCamelCase"),
        )
        self.assertEqual(
            ["<SINGLE_LEADING_UNDERSCORE_NAME>", "<UPPER_CAMEL_CASE>"], tokenize("_Up")
        )
        self.assertEqual(
            ["<SINGLE_LEADING_UNDERSCORE_NAME>", "<UPPER_CAMEL_CASE2>"],
            tokenize("_TestHTTP"),
        )
        self.assertEqual(
            ["<SINGLE_LEADING_UNDERSCORE_NAME>", "<UPPER_CAMEL_CASE2>"],
            tokenize("_TestHTTPserver"),
        )
        self.assertEqual(
            ["<SINGLE_LEADING_UNDERSCORE_NAME>", "<LOWER_CAMEL_CASE>"],
            tokenize("_lowerCamelCase"),
        )
        self.assertEqual(
            ["<SINGLE_LEADING_UNDERSCORE_NAME>", "<LOWER_CAMEL_CASE2>"],
            tokenize("_testHTTP"),
        )
        self.assertEqual(
            ["<SINGLE_LEADING_UNDERSCORE_NAME>", "<LOWER_CAMEL_CASE2>"],
            tokenize("_testHTTPserver"),
        )
        self.assertEqual(
            ["<SINGLE_LEADING_UNDERSCORE_NAME>", "<SCREAMING_SNAKE_CASE>"],
            tokenize("_SCREAMING_SNAKE_CASE"),
        )
        self.assertEqual(
            ["<SINGLE_LEADING_UNDERSCORE_NAME>", "<SNAKE_CASE>"],
            tokenize("_snake_case"),
        )

        self.assertEqual(
            ["<DOUBLE_LEADING_UNDERSCORE_NAME>", "<LETTER_LOW>"], tokenize("__s")
        )
        self.assertEqual(
            ["<DOUBLE_LEADING_UNDERSCORE_NAME>", "<LETTER_UP>"], tokenize("__U")
        )
        self.assertEqual(
            ["<DOUBLE_LEADING_UNDERSCORE_NAME>", "<LOWERCASE>"], tokenize("__lower")
        )
        self.assertEqual(
            ["<DOUBLE_LEADING_UNDERSCORE_NAME>", "<UPPER_CAMEL_CASE>"],
            tokenize("__UpperCamelCase"),
        )
        self.assertEqual(
            ["<DOUBLE_LEADING_UNDERSCORE_NAME>", "<UPPER_CAMEL_CASE>"], tokenize("__Up")
        )
        self.assertEqual(
            ["<DOUBLE_LEADING_UNDERSCORE_NAME>", "<UPPER_CAMEL_CASE2>"],
            tokenize("__TestHTTP"),
        )
        self.assertEqual(
            ["<DOUBLE_LEADING_UNDERSCORE_NAME>", "<UPPER_CAMEL_CASE2>"],
            tokenize("__TestHTTPserver"),
        )
        self.assertEqual(
            ["<DOUBLE_LEADING_UNDERSCORE_NAME>", "<LOWER_CAMEL_CASE>"],
            tokenize("__lowerCamelCase"),
        )
        self.assertEqual(
            ["<DOUBLE_LEADING_UNDERSCORE_NAME>", "<LOWER_CAMEL_CASE2>"],
            tokenize("__testHTTP"),
        )
        self.assertEqual(
            ["<DOUBLE_LEADING_UNDERSCORE_NAME>", "<LOWER_CAMEL_CASE2>"],
            tokenize("__testHTTPserver"),
        )
        self.assertEqual(
            ["<DOUBLE_LEADING_UNDERSCORE_NAME>", "<SCREAMING_SNAKE_CASE>"],
            tokenize("__SCREAMING_SNAKE_CASE"),
        )
        self.assertEqual(
            ["<DOUBLE_LEADING_UNDERSCORE_NAME>", "<SNAKE_CASE>"],
            tokenize("__snake_case"),
        )

        self.assertEqual(
            ["<DOUBLE_LEADING_UNDERSCORE_NAME>", "<SNAKE_CASE>"], tokenize("__9_p")
        )
        self.assertEqual(
            ["<DOUBLE_LEADING_UNDERSCORE_NAME>", "<LETTER_LOW>"], tokenize("__9")
        )
        self.assertEqual(
            ["<DOUBLE_LEADING_UNDERSCORE_NAME>", "<SNAKE_CASE>"], tokenize("__42_42")
        )

        self.assertEqual(
            ["<LETTER_LOW>", "<SINGLE_TRAILING_UNDERSCORE_NAME>"], tokenize("s_")
        )
        self.assertEqual(
            ["<LETTER_UP>", "<SINGLE_TRAILING_UNDERSCORE_NAME>"], tokenize("U_")
        )
        self.assertEqual(
            ["<LOWERCASE>", "<SINGLE_TRAILING_UNDERSCORE_NAME>"], tokenize("lower_")
        )
        self.assertEqual(
            ["<UPPER_CAMEL_CASE>", "<SINGLE_TRAILING_UNDERSCORE_NAME>"],
            tokenize("UpperCamelCase_"),
        )
        self.assertEqual(
            ["<UPPER_CAMEL_CASE>", "<SINGLE_TRAILING_UNDERSCORE_NAME>"], tokenize("Up_")
        )
        self.assertEqual(
            ["<UPPER_CAMEL_CASE2>", "<SINGLE_TRAILING_UNDERSCORE_NAME>"],
            tokenize("TestHTTP_"),
        )
        self.assertEqual(
            ["<UPPER_CAMEL_CASE2>", "<SINGLE_TRAILING_UNDERSCORE_NAME>"],
            tokenize("TestHTTPserver_"),
        )
        self.assertEqual(
            ["<LOWER_CAMEL_CASE>", "<SINGLE_TRAILING_UNDERSCORE_NAME>"],
            tokenize("lowerCamelCase_"),
        )
        self.assertEqual(
            ["<LOWER_CAMEL_CASE2>", "<SINGLE_TRAILING_UNDERSCORE_NAME>"],
            tokenize("testHTTP_"),
        )
        self.assertEqual(
            ["<LOWER_CAMEL_CASE2>", "<SINGLE_TRAILING_UNDERSCORE_NAME>"],
            tokenize("testHTTPserver_"),
        )
        self.assertEqual(
            ["<SCREAMING_SNAKE_CASE>", "<SINGLE_TRAILING_UNDERSCORE_NAME>"],
            tokenize("SCREAMING_SNAKE_CASE_"),
        )
        self.assertEqual(
            ["<SNAKE_CASE>", "<SINGLE_TRAILING_UNDERSCORE_NAME>"],
            tokenize("snake_case_"),
        )

        self.assertEqual(
            ["<LETTER_LOW>", "<DOUBLE_TRAILING_UNDERSCORE_NAME>"], tokenize("s__")
        )
        self.assertEqual(
            ["<LETTER_UP>", "<DOUBLE_TRAILING_UNDERSCORE_NAME>"], tokenize("U__")
        )
        self.assertEqual(
            ["<LOWERCASE>", "<DOUBLE_TRAILING_UNDERSCORE_NAME>"], tokenize("lower__")
        )
        self.assertEqual(
            ["<UPPER_CAMEL_CASE>", "<DOUBLE_TRAILING_UNDERSCORE_NAME>"],
            tokenize("UpperCamelCase__"),
        )
        self.assertEqual(
            ["<UPPER_CAMEL_CASE>", "<DOUBLE_TRAILING_UNDERSCORE_NAME>"],
            tokenize("Up__"),
        )
        self.assertEqual(
            ["<UPPER_CAMEL_CASE2>", "<DOUBLE_TRAILING_UNDERSCORE_NAME>"],
            tokenize("TestHTTP__"),
        )
        self.assertEqual(
            ["<UPPER_CAMEL_CASE2>", "<DOUBLE_TRAILING_UNDERSCORE_NAME>"],
            tokenize("TestHTTPserver__"),
        )
        self.assertEqual(
            ["<LOWER_CAMEL_CASE>", "<DOUBLE_TRAILING_UNDERSCORE_NAME>"],
            tokenize("lowerCamelCase__"),
        )
        self.assertEqual(
            ["<LOWER_CAMEL_CASE2>", "<DOUBLE_TRAILING_UNDERSCORE_NAME>"],
            tokenize("testHTTP__"),
        )
        self.assertEqual(
            ["<LOWER_CAMEL_CASE2>", "<DOUBLE_TRAILING_UNDERSCORE_NAME>"],
            tokenize("testHTTPserver__"),
        )
        self.assertEqual(
            ["<SCREAMING_SNAKE_CASE>", "<DOUBLE_TRAILING_UNDERSCORE_NAME>"],
            tokenize("SCREAMING_SNAKE_CASE__"),
        )
        self.assertEqual(
            ["<SNAKE_CASE>", "<DOUBLE_TRAILING_UNDERSCORE_NAME>"],
            tokenize("snake_case__"),
        )

        self.assertEqual(["<UNDEF_NAME_PATTERN>"], tokenize("loSCR_EAM_ING"))

    def test_normal_string_tokenization(self):
        self.assertEqual(['"""', '"""'], tokenize('""""""'))
        self.assertEqual(['"""', '<STRING>', '"""'], tokenize('"""<SPACE1>"""'))
        self.assertEqual(
            ['"""', '<STRING>', '"""'],
            tokenize('"""this is a simple text without multilines 42"""'),
        )
        self.assertEqual(
            ['"""', '<STRING>', '"""'], tokenize('""",.-_+*-;:!$%&/()=?`´~{}[]\t"""')
        )
        self.assertEqual(
            ['"""', '<STRING>', '"""'],
            tokenize('"""this is a simple text\nwith multilines\n42"""'),
        )

        self.assertEqual(['"', '"'], tokenize('""'))
        self.assertEqual(['"', '<STRING>', '"'], tokenize('"<SPACE1>"'))
        self.assertEqual(['"', '<STRING>', '"'], tokenize('"this is a simple text"'))
        self.assertEqual(['"', '<STRING>', '"'], tokenize('",.-_+*-;:!$%&/()=?`´~{}[]\t"'))

        self.assertEqual(["'''", "'''"], tokenize("''''''"))
        self.assertEqual(["'''", "<STRING>", "'''"], tokenize("''' '''"))
        self.assertEqual(
            ["'''", "<STRING>", "'''"],
            tokenize("'''this is a simple text without multilines 42'''"),
        )
        self.assertEqual(
            ["'''", "<STRING>", "'''"],
            tokenize("'''this is a simple text\nwith multilines\n42'''"),
        )

        self.assertEqual(["'", "'"], tokenize("''"))
        self.assertEqual(["'", "<STRING>", "'"], tokenize("' '"))
        self.assertEqual(["'", "<STRING>", "'"], tokenize("'this is a simple text'"))

    def test_f_string_tokenization(self):
        self.assertEqual(['f', '"""', '"""'], tokenize('f""""""'))
        self.assertEqual(['f', '"""', '<STRING>', '"""'], tokenize('f"""<SPACE1>"""'))
        self.assertEqual(['f', '"""', '<STRING>', '"""'], tokenize('f"""{i}"""'))
        self.assertEqual(
            ['f', '"""', '<STRING>', '"""'],
            tokenize('f"""this is a simple text without multilines 42"""'),
        )
        self.assertEqual(
            ['F', '"""', '<STRING>', '"""'],
            tokenize('F"""this is a simple text without multilines 42"""'),
        )
        self.assertEqual(
            ['f', '"""', '<STRING>', '"""'], tokenize('f""",.-_+*-;:!$%&/()=?`´~[]\t"""')
        )
        self.assertEqual(
            ['f', '"""', '<STRING>', '"""'],
            tokenize('f"""this is a simple text\nwith multilines\n42"""'),
        )

        self.assertEqual(['f', '"', '"'], tokenize('f""'))
        self.assertEqual(['f', '"', '<STRING>', '"'], tokenize('f"<SPACE1>"'))
        self.assertEqual(['f', '"', '<STRING>', '"'], tokenize('f"{i}"'))
        self.assertEqual(['f', '"', '<STRING>', '"'], tokenize('f"this is a simple text"'))
        self.assertEqual(['F', '"', '<STRING>', '"'], tokenize('F"this is a simple text"'))
        self.assertEqual(['f', '"', '<STRING>', '"'], tokenize('f",.-_+*-;:!$%&/()=?`´~[]\t"'))

        self.assertEqual(["f", "'''", "'''"], tokenize("f''''''"))
        self.assertEqual(["f", "'''", "<STRING>", "'''"], tokenize("f''' '''"))
        self.assertEqual(["f", "'''", "<STRING>", "'''"], tokenize("f'''{i}'''"))
        self.assertEqual(
            ["f", "'''", "<STRING>", "'''"],
            tokenize("f'''this is a simple text without multilines 42'''"),
        )
        self.assertEqual(
            ["F", "'''", "<STRING>", "'''"],
            tokenize("F'''this is a simple text without multilines 42'''"),
        )
        self.assertEqual(
            ["f", "'''", "<STRING>", "'''"], tokenize("f''',.-_+*-;:!$%&/()=?`´~[]\t'''")
        )
        self.assertEqual(
            ["f", "'''", "<STRING>", "'''"],
            tokenize("f'''this is a simple text\nwith multilines\n42'''"),
        )

        self.assertEqual(["f", "'", "'"], tokenize("f''"))
        self.assertEqual(["f", "'", "<STRING>", "'"], tokenize("f' '"))
        self.assertEqual(["f", "'", "<STRING>", "'"], tokenize("f'{i}'"))
        self.assertEqual(["f", "'", "<STRING>", "'"], tokenize("f'this is a simple text'"))
        self.assertEqual(["F", "'", "<STRING>", "'"], tokenize("F'this is a simple text'"))
        self.assertEqual(["f", "'", "<STRING>", "'"], tokenize("f',.-_+*-;:!$%&/()=?`´~[]\t'"))

    def test_b_string_tokenization(self):
        self.assertEqual(['b', '"""', '"""'], tokenize('b""""""'))
        self.assertEqual(['b', '"""', '<STRING>', '"""'], tokenize('b"""<SPACE1>"""'))
        self.assertEqual(
            ['b', '"""', '<STRING>', '"""'],
            tokenize('b"""this is a simple text without multilines 42"""'),
        )
        self.assertEqual(
            ['B', '"""', '<STRING>', '"""'],
            tokenize('B"""this is a simple text without multilines 42"""'),
        )
        self.assertEqual(
            ['b', '"""', '<STRING>', '"""'], tokenize('b""",.-_+*-;:!$%&/()=?`´~{}[]\t"""')
        )
        self.assertEqual(
            ['b', '"""', '<STRING>', '"""'],
            tokenize('b"""this is a simple text\nwith multilines\n42"""'),
        )

        self.assertEqual(['b', '"', '"'], tokenize('b""'))
        self.assertEqual(['b', '"', '<STRING>', '"'], tokenize('b"<SPACE1>"'))
        self.assertEqual(['b', '"', '<STRING>', '"'], tokenize('b"this is a simple text"'))
        self.assertEqual(['B', '"', '<STRING>', '"'], tokenize('B"this is a simple text"'))
        self.assertEqual(['b', '"', '<STRING>', '"'], tokenize('b",.-_+*-;:!$%&/()=?`´~{}[]\t"'))

        self.assertEqual(["b", "'''", "'''"], tokenize("b''''''"))
        self.assertEqual(["b", "'''", "<STRING>", "'''"], tokenize("b''' '''"))
        self.assertEqual(
            ["b", "'''", "<STRING>", "'''"],
            tokenize("b'''this is a simple text without multilines 42'''"),
        )
        self.assertEqual(
            ["B", "'''", "<STRING>", "'''"],
            tokenize("B'''this is a simple text without multilines 42'''"),
        )
        self.assertEqual(
            ["b", "'''", "<STRING>", "'''"], tokenize("b''',.-_+*-;:!$%&/()=?`´~{}[]\t'''")
        )
        self.assertEqual(
            ["b", "'''", "<STRING>", "'''"],
            tokenize("b'''this is a simple text\nwith multilines\n42'''"),
        )

        self.assertEqual(["b", "'", "'"], tokenize("b''"))
        self.assertEqual(["b", "'", "<STRING>", "'"], tokenize("b' '"))
        self.assertEqual(["b", "'", "<STRING>", "'"], tokenize("b'this is a simple text'"))
        self.assertEqual(["B", "'", "<STRING>", "'"], tokenize("B'this is a simple text'"))
        self.assertEqual(["b", "'", "<STRING>", "'"], tokenize("b',.-_+*-;:!$%&/()=?`´~{}[]\t'"))

    def test_r_string_tokenization(self):
        self.assertEqual(['r', '"""', '"""'], tokenize('r""""""'))
        self.assertEqual(['r', '"""', '<STRING>', '"""'], tokenize('r"""<SPACE1>"""'))
        self.assertEqual(
            ['r', '"""', '<STRING>', '"""'],
            tokenize('r"""this is a simple text without multilines 42"""'),
        )
        self.assertEqual(
            ['R', '"""', '<STRING>', '"""'],
            tokenize('R"""this is a simple text without multilines 42"""'),
        )
        self.assertEqual(
            ['r', '"""', '<STRING>', '"""'], tokenize('r""",.-_+*-;:!$%&/()=?`´~{}[]\t"""')
        )
        self.assertEqual(
            ['r', '"""', '<STRING>', '"""'],
            tokenize('r"""this is a simple text\nwith multilines\n42"""'),
        )

        self.assertEqual(['r', '"', '"'], tokenize('r""'))
        self.assertEqual(['r', '"', '<STRING>', '"'], tokenize('r"<SPACE1>"'))
        self.assertEqual(['r', '"', '<STRING>', '"'], tokenize('r"this is a simple text"'))
        self.assertEqual(['R', '"', '<STRING>', '"'], tokenize('R"this is a simple text"'))
        self.assertEqual(['r', '"', '<STRING>', '"'], tokenize('r",.-_+*-;:!$%&/()=?`´~{}[]\t"'))

        self.assertEqual(["r", "'''", "'''"], tokenize("r''''''"))
        self.assertEqual(["r", "'''", "<STRING>", "'''"], tokenize("r''' '''"))
        self.assertEqual(
            ["r", "'''", "<STRING>", "'''"],
            tokenize("r'''this is a simple text without multilines 42'''"),
        )
        self.assertEqual(
            ["R", "'''", "<STRING>", "'''"],
            tokenize("R'''this is a simple text without multilines 42'''"),
        )
        self.assertEqual(
            ["r", "'''", "<STRING>", "'''"], tokenize("r''',.-_+*-;:!$%&/()=?`´~{}[]\t'''")
        )
        self.assertEqual(
            ["r", "'''", "<STRING>", "'''"],
            tokenize("r'''this is a simple text\nwith multilines\n42'''"),
        )

        self.assertEqual(["r", "'", "'"], tokenize("r''"))
        self.assertEqual(["r", "'", "<STRING>", "'"], tokenize("r' '"))
        self.assertEqual(["r", "'", "<STRING>", "'"], tokenize("r'this is a simple text'"))
        self.assertEqual(["R", "'", "<STRING>", "'"], tokenize("R'this is a simple text'"))
        self.assertEqual(["r", "'", "<STRING>", "'"], tokenize("r',.-_+*-;:!$%&/()=?`´~{}[]\t'"))

    def test_u_string_tokenization(self):
        self.assertEqual(['u', '"""', '"""'], tokenize('u""""""'))
        self.assertEqual(['u', '"""', '<STRING>', '"""'], tokenize('u"""<SPACE1>"""'))
        self.assertEqual(
            ['u', '"""', '<STRING>', '"""'],
            tokenize('u"""this is a simple text without multilines 42"""'),
        )
        self.assertEqual(
            ['U', '"""', '<STRING>', '"""'],
            tokenize('U"""this is a simple text without multilines 42"""'),
        )
        self.assertEqual(
            ['u', '"""', '<STRING>', '"""'], tokenize('u""",.-_+*-;:!$%&/()=?`´~{}[]\t"""')
        )
        self.assertEqual(
            ['u', '"""', '<STRING>', '"""'],
            tokenize('u"""this is a simple text\nwith multilines\n42"""'),
        )

        self.assertEqual(['u', '"', '"'], tokenize('u""'))
        self.assertEqual(['u', '"', '<STRING>', '"'], tokenize('u"<SPACE1>"'))
        self.assertEqual(['u', '"', '<STRING>', '"'], tokenize('u"this is a simple text"'))
        self.assertEqual(['U', '"', '<STRING>', '"'], tokenize('U"this is a simple text"'))
        self.assertEqual(['u', '"', '<STRING>', '"'], tokenize('u",.-_+*-;:!$%&/()=?`´~{}[]\t"'))

        self.assertEqual(["u", "'''", "'''"], tokenize("u''''''"))
        self.assertEqual(["u", "'''", "<STRING>", "'''"], tokenize("u''' '''"))
        self.assertEqual(
            ["u", "'''", "<STRING>", "'''"],
            tokenize("u'''this is a simple text without multilines 42'''"),
        )
        self.assertEqual(
            ["U", "'''", "<STRING>", "'''"],
            tokenize("U'''this is a simple text without multilines 42'''"),
        )
        self.assertEqual(
            ["u", "'''", "<STRING>", "'''"], tokenize("u''',.-_+*-;:!$%&/()=?`´~{}[]\t'''")
        )
        self.assertEqual(
            ["u", "'''", "<STRING>", "'''"],
            tokenize("u'''this is a simple text\nwith multilines\n42'''"),
        )

        self.assertEqual(["u", "'", "'"], tokenize("u''"))
        self.assertEqual(["u", "'", "<STRING>", "'"], tokenize("u' '"))
        self.assertEqual(["u", "'", "<STRING>", "'"], tokenize("u'this is a simple text'"))
        self.assertEqual(["U", "'", "<STRING>", "'"], tokenize("U'this is a simple text'"))
        self.assertEqual(["u", "'", "<STRING>", "'"], tokenize("u',.-_+*-;:!$%&/()=?`´~{}[]\t'"))

    def test_fr_string_tokenization(self):
        self.assertEqual(['fr', '"""', '"""'], tokenize('fr""""""'))
        self.assertEqual(['fr', '"""', '<STRING>', '"""'], tokenize('fr"""<SPACE1>"""'))
        self.assertEqual(['fr', '"""', '<STRING>', '"""'], tokenize('fr"""{i}"""'))
        self.assertEqual(
            ['fr', '"""', '<STRING>', '"""'],
            tokenize('fr"""this is a simple text without multilines 42"""'),
        )
        self.assertEqual(
            ['FR', '"""', '<STRING>', '"""'],
            tokenize('FR"""this is a simple text without multilines 42"""'),
        )
        self.assertEqual(
            ['fR', '"""', '<STRING>', '"""'],
            tokenize('fR"""this is a simple text without multilines 42"""'),
        )
        self.assertEqual(
            ['Fr', '"""', '<STRING>', '"""'],
            tokenize('Fr"""this is a simple text without multilines 42"""'),
        )
        self.assertEqual(
            ['fr', '"""', '<STRING>', '"""'], tokenize('fr""",.-_+*-;:!$%&/()=?`´~[]\t"""')
        )
        self.assertEqual(
            ['fr', '"""', '<STRING>', '"""'],
            tokenize('fr"""this is a simple text\nwith multilines\n42"""'),
        )

        self.assertEqual(['fr', '"', '"'], tokenize('fr""'))
        self.assertEqual(['fr', '"', '<STRING>', '"'], tokenize('fr"<SPACE1>"'))
        self.assertEqual(['fr', '"', '<STRING>', '"'], tokenize('fr"{i}"'))
        self.assertEqual(['fr', '"', '<STRING>', '"'], tokenize('fr"this is a simple text"'))
        self.assertEqual(['FR', '"', '<STRING>', '"'], tokenize('FR"this is a simple text"'))
        self.assertEqual(['fR', '"', '<STRING>', '"'], tokenize('fR"this is a simple text"'))
        self.assertEqual(['Fr', '"', '<STRING>', '"'], tokenize('Fr"this is a simple text"'))
        self.assertEqual(['fr', '"', '<STRING>', '"'], tokenize('fr",.-_+*-;:!$%&/()=?`´~[]\t"'))

        self.assertEqual(["fr", "'''", "'''"], tokenize("fr''''''"))
        self.assertEqual(["fr", "'''", "<STRING>", "'''"], tokenize("fr''' '''"))
        self.assertEqual(["fr", "'''", "<STRING>", "'''"], tokenize("fr'''{i}'''"))
        self.assertEqual(
            ["fr", "'''", "<STRING>", "'''"],
            tokenize("fr'''this is a simple text without multilines 42'''"),
        )
        self.assertEqual(
            ["FR", "'''", "<STRING>", "'''"],
            tokenize("FR'''this is a simple text without multilines 42'''"),
        )
        self.assertEqual(
            ["fR", "'''", "<STRING>", "'''"],
            tokenize("fR'''this is a simple text without multilines 42'''"),
        )
        self.assertEqual(
            ["Fr", "'''", "<STRING>", "'''"],
            tokenize("Fr'''this is a simple text without multilines 42'''"),
        )
        self.assertEqual(
            ["fr", "'''", "<STRING>", "'''"], tokenize("fr''',.-_+*-;:!$%&/()=?`´~[]\t'''")
        )
        self.assertEqual(
            ["fr", "'''", "<STRING>", "'''"],
            tokenize("fr'''this is a simple text\nwith multilines\n42'''"),
        )

        self.assertEqual(["fr", "'", "'"], tokenize("fr''"))
        self.assertEqual(["fr", "'", "<STRING>", "'"], tokenize("fr' '"))
        self.assertEqual(["fr", "'", "<STRING>", "'"], tokenize("fr'{i}'"))
        self.assertEqual(["fr", "'", "<STRING>", "'"], tokenize("fr'this is a simple text'"))
        self.assertEqual(["FR", "'", "<STRING>", "'"], tokenize("FR'this is a simple text'"))
        self.assertEqual(["fR", "'", "<STRING>", "'"], tokenize("fR'this is a simple text'"))
        self.assertEqual(["Fr", "'", "<STRING>", "'"], tokenize("Fr'this is a simple text'"))
        self.assertEqual(["fr", "'", "<STRING>", "'"], tokenize("fr',.-_+*-;:!$%&/()=?`´~[]\t'"))

    def test_rf_string_tokenization(self):
        self.assertEqual(['rf', '"""', '"""'], tokenize('rf""""""'))
        self.assertEqual(['rf', '"""', '<STRING>', '"""'], tokenize('rf"""<SPACE1>"""'))
        self.assertEqual(['rf', '"""', '<STRING>', '"""'], tokenize('rf"""{i}"""'))
        self.assertEqual(
            ['rf', '"""', '<STRING>', '"""'],
            tokenize('rf"""this is a simple text without multilines 42"""'),
        )
        self.assertEqual(
            ['RF', '"""', '<STRING>', '"""'],
            tokenize('RF"""this is a simple text without multilines 42"""'),
        )
        self.assertEqual(
            ['rF', '"""', '<STRING>', '"""'],
            tokenize('rF"""this is a simple text without multilines 42"""'),
        )
        self.assertEqual(
            ['Rf', '"""', '<STRING>', '"""'],
            tokenize('Rf"""this is a simple text without multilines 42"""'),
        )
        self.assertEqual(
            ['rf', '"""', '<STRING>', '"""'], tokenize('rf""",.-_+*-;:!$%&/()=?`´~[]\t"""')
        )
        self.assertEqual(
            ['rf', '"""', '<STRING>', '"""'],
            tokenize('rf"""this is a simple text\nwith multilines\n42"""'),
        )

        self.assertEqual(['rf', '"', '"'], tokenize('rf""'))
        self.assertEqual(['rf', '"', '<STRING>', '"'], tokenize('rf"<SPACE1>"'))
        self.assertEqual(['rf', '"', '<STRING>', '"'], tokenize('rf"{i}"'))
        self.assertEqual(['rf', '"', '<STRING>', '"'], tokenize('rf"this is a simple text"'))
        self.assertEqual(['RF', '"', '<STRING>', '"'], tokenize('RF"this is a simple text"'))
        self.assertEqual(['rF', '"', '<STRING>', '"'], tokenize('rF"this is a simple text"'))
        self.assertEqual(['Rf', '"', '<STRING>', '"'], tokenize('Rf"this is a simple text"'))
        self.assertEqual(['rf', '"', '<STRING>', '"'], tokenize('rf",.-_+*-;:!$%&/()=?`´~[]\t"'))

        self.assertEqual(["rf", "'''", "'''"], tokenize("rf''''''"))
        self.assertEqual(["rf", "'''", "<STRING>", "'''"], tokenize("rf''' '''"))
        self.assertEqual(["rf", "'''", "<STRING>", "'''"], tokenize("rf'''{i}'''"))
        self.assertEqual(
            ["rf", "'''", "<STRING>", "'''"],
            tokenize("rf'''this is a simple text without multilines 42'''"),
        )
        self.assertEqual(
            ["RF", "'''", "<STRING>", "'''"],
            tokenize("RF'''this is a simple text without multilines 42'''"),
        )
        self.assertEqual(
            ["rF", "'''", "<STRING>", "'''"],
            tokenize("rF'''this is a simple text without multilines 42'''"),
        )
        self.assertEqual(
            ["Rf", "'''", "<STRING>", "'''"],
            tokenize("Rf'''this is a simple text without multilines 42'''"),
        )
        self.assertEqual(
            ["rf", "'''", "<STRING>", "'''"], tokenize("rf''',.-_+*-;:!$%&/()=?`´~[]\t'''")
        )
        self.assertEqual(
            ["rf", "'''", "<STRING>", "'''"],
            tokenize("rf'''this is a simple text\nwith multilines\n42'''"),
        )

        self.assertEqual(["rf", "'", "'"], tokenize("rf''"))
        self.assertEqual(["rf", "'", "<STRING>", "'"], tokenize("rf' '"))
        self.assertEqual(["rf", "'", "<STRING>", "'"], tokenize("rf'{i}'"))
        self.assertEqual(["rf", "'", "<STRING>", "'"], tokenize("rf'this is a simple text'"))
        self.assertEqual(["RF", "'", "<STRING>", "'"], tokenize("RF'this is a simple text'"))
        self.assertEqual(["rF", "'", "<STRING>", "'"], tokenize("rF'this is a simple text'"))
        self.assertEqual(["Rf", "'", "<STRING>", "'"], tokenize("Rf'this is a simple text'"))
        self.assertEqual(["rf", "'", "<STRING>", "'"], tokenize("rf',.-_+*-;:!$%&/()=?`´~[]\t'"))

    def test_rb_string_tokenization(self):
        self.assertEqual(['rb', '"""', '"""'], tokenize('rb""""""'))
        self.assertEqual(['rb', '"""', '<STRING>', '"""'], tokenize('rb"""<SPACE1>"""'))
        self.assertEqual(
            ['rb', '"""', '<STRING>', '"""'],
            tokenize('rb"""this is a simple text without multilines 42"""'),
        )
        self.assertEqual(
            ['RB', '"""', '<STRING>', '"""'],
            tokenize('RB"""this is a simple text without multilines 42"""'),
        )
        self.assertEqual(
            ['rB', '"""', '<STRING>', '"""'],
            tokenize('rB"""this is a simple text without multilines 42"""'),
        )
        self.assertEqual(
            ['Rb', '"""', '<STRING>', '"""'],
            tokenize('Rb"""this is a simple text without multilines 42"""'),
        )
        self.assertEqual(
            ['rb', '"""', '<STRING>', '"""'], tokenize('rb""",.-_+*-;:!$%&/()=?`´~{}[]\t"""')
        )
        self.assertEqual(
            ['rb', '"""', '<STRING>', '"""'],
            tokenize('rb"""this is a simple text\nwith multilines\n42"""'),
        )

        self.assertEqual(['rb', '"', '"'], tokenize('rb""'))
        self.assertEqual(['rb', '"', '<STRING>', '"'], tokenize('rb"<SPACE1>"'))
        self.assertEqual(['rb', '"', '<STRING>', '"'], tokenize('rb"this is a simple text"'))
        self.assertEqual(['RB', '"', '<STRING>', '"'], tokenize('RB"this is a simple text"'))
        self.assertEqual(['rB', '"', '<STRING>', '"'], tokenize('rB"this is a simple text"'))
        self.assertEqual(['Rb', '"', '<STRING>', '"'], tokenize('Rb"this is a simple text"'))
        self.assertEqual(['rb', '"', '<STRING>', '"'], tokenize('rb",.-_+*-;:!$%&/()=?`´~{}[]\t"'))

        self.assertEqual(["rb", "'''", "'''"], tokenize("rb''''''"))
        self.assertEqual(["rb", "'''", "<STRING>", "'''"], tokenize("rb''' '''"))
        self.assertEqual(
            ["rb", "'''", "<STRING>", "'''"],
            tokenize("rb'''this is a simple text without multilines 42'''"),
        )
        self.assertEqual(
            ["RB", "'''", "<STRING>", "'''"],
            tokenize("RB'''this is a simple text without multilines 42'''"),
        )
        self.assertEqual(
            ["rB", "'''", "<STRING>", "'''"],
            tokenize("rB'''this is a simple text without multilines 42'''"),
        )
        self.assertEqual(
            ["Rb", "'''", "<STRING>", "'''"],
            tokenize("Rb'''this is a simple text without multilines 42'''"),
        )
        self.assertEqual(
            ["rb", "'''", "<STRING>", "'''"], tokenize("rb''',.-_+*-;:!$%&/()=?`´~{}[]\t'''")
        )
        self.assertEqual(
            ["rb", "'''", "<STRING>", "'''"],
            tokenize("rb'''this is a simple text\nwith multilines\n42'''"),
        )

        self.assertEqual(["rb", "'", "'"], tokenize("rb''"))
        self.assertEqual(["rb", "'", "<STRING>", "'"], tokenize("rb' '"))
        self.assertEqual(["rb", "'", "<STRING>", "'"], tokenize("rb'this is a simple text'"))
        self.assertEqual(["RB", "'", "<STRING>", "'"], tokenize("RB'this is a simple text'"))
        self.assertEqual(["rB", "'", "<STRING>", "'"], tokenize("rB'this is a simple text'"))
        self.assertEqual(["Rb", "'", "<STRING>", "'"], tokenize("Rb'this is a simple text'"))
        self.assertEqual(["rb", "'", "<STRING>", "'"], tokenize("rb',.-_+*-;:!$%&/()=?`´~{}[]\t'"))

    def test_br_string_tokenization(self):
        self.assertEqual(['br', '"""', '"""'], tokenize('br""""""'))
        self.assertEqual(['br', '"""', '<STRING>', '"""'], tokenize('br"""<SPACE1>"""'))
        self.assertEqual(
            ['br', '"""', '<STRING>', '"""'],
            tokenize('br"""this is a simple text without multilines 42"""'),
        )
        self.assertEqual(
            ['BR', '"""', '<STRING>', '"""'],
            tokenize('BR"""this is a simple text without multilines 42"""'),
        )
        self.assertEqual(
            ['bR', '"""', '<STRING>', '"""'],
            tokenize('bR"""this is a simple text without multilines 42"""'),
        )
        self.assertEqual(
            ['Br', '"""', '<STRING>', '"""'],
            tokenize('Br"""this is a simple text without multilines 42"""'),
        )
        self.assertEqual(
            ['br', '"""', '<STRING>', '"""'], tokenize('br""",.-_+*-;:!$%&/()=?`´~{}[]\t"""')
        )
        self.assertEqual(
            ['br', '"""', '<STRING>', '"""'],
            tokenize('br"""this is a simple text\nwith multilines\n42"""'),
        )

        self.assertEqual(['br', '"', '"'], tokenize('br""'))
        self.assertEqual(['br', '"', '<STRING>', '"'], tokenize('br"<SPACE1>"'))
        self.assertEqual(['br', '"', '<STRING>', '"'], tokenize('br"this is a simple text"'))
        self.assertEqual(['BR', '"', '<STRING>', '"'], tokenize('BR"this is a simple text"'))
        self.assertEqual(['bR', '"', '<STRING>', '"'], tokenize('bR"this is a simple text"'))
        self.assertEqual(['Br', '"', '<STRING>', '"'], tokenize('Br"this is a simple text"'))
        self.assertEqual(['br', '"', '<STRING>', '"'], tokenize('br",.-_+*-;:!$%&/()=?`´~{}[]\t"'))

        self.assertEqual(["br", "'''", "'''"], tokenize("br''''''"))
        self.assertEqual(["br", "'''", "<STRING>", "'''"], tokenize("br''' '''"))
        self.assertEqual(
            ["br", "'''", "<STRING>", "'''"],
            tokenize("br'''this is a simple text without multilines 42'''"),
        )
        self.assertEqual(
            ["BR", "'''", "<STRING>", "'''"],
            tokenize("BR'''this is a simple text without multilines 42'''"),
        )
        self.assertEqual(
            ["bR", "'''", "<STRING>", "'''"],
            tokenize("bR'''this is a simple text without multilines 42'''"),
        )
        self.assertEqual(
            ["Br", "'''", "<STRING>", "'''"],
            tokenize("Br'''this is a simple text without multilines 42'''"),
        )
        self.assertEqual(
            ["br", "'''", "<STRING>", "'''"], tokenize("br''',.-_+*-;:!$%&/()=?`´~{}[]\t'''")
        )
        self.assertEqual(
            ["br", "'''", "<STRING>", "'''"],
            tokenize("br'''this is a simple text\nwith multilines\n42'''"),
        )

        self.assertEqual(["br", "'", "'"], tokenize("br''"))
        self.assertEqual(["br", "'", "<STRING>", "'"], tokenize("br' '"))
        self.assertEqual(["br", "'", "<STRING>", "'"], tokenize("br'this is a simple text'"))
        self.assertEqual(["BR", "'", "<STRING>", "'"], tokenize("BR'this is a simple text'"))
        self.assertEqual(["bR", "'", "<STRING>", "'"], tokenize("bR'this is a simple text'"))
        self.assertEqual(["Br", "'", "<STRING>", "'"], tokenize("Br'this is a simple text'"))
        self.assertEqual(["br", "'", "<STRING>", "'"], tokenize("br',.-_+*-;:!$%&/()=?`´~{}[]\t'"))

    def test_tokenize_spaces_between_tokens(self):
        self.assertEqual(
            ["<LETTER_LOW>", "<SPACE2>", "<SPACE1>", "=", "<SPACE2>", "<INT>"],
            tokenize("a   =  42"),
        )

        self.assertEqual(
            [
                "<LOWERCASE>",
                "<SPACE1>",
                "(",
                "<SPACE1>",
                "<LETTER_LOW>",
                "<SPACE2>",
                ",",
                "<SPACE1>",
                "<LETTER_LOW>",
                "<SPACE1>",
                ")",
            ],
            tokenize("abc ( a  , b )"),
        )

        self.assertEqual(
            [
                "def",
                "<SPACE1>",
                "<LOWERCASE>",
                "<SPACE1>",
                "(",
                "<SPACE1>",
                "<LETTER_LOW>",
                "<SPACE2>",
                ")",
                ":",
                "<SPACE1>",
                "\n",
                "<SPACE2>",
                "<LETTER_LOW>",
                "<SPACE2>",
                "=",
                "<SPACE4>",
                "<INT>",
            ],
            tokenize("def abc ( a  ): \n  t  =    45"),
        )

    def test_tokenize_tabs_between_tokens(self):
        self.assertEqual(
            ["<LETTER_LOW>", "<TAB1>", "=", "<TAB2>", "<INT>"], tokenize("a\t=\t\t42")
        )

        self.assertEqual(
            [
                "<LOWERCASE>",
                "<TAB1>",
                "(",
                "<TAB1>",
                "<LETTER_LOW>",
                "<TAB2>",
                ",",
                "<TAB2>",
                "<TAB1>",
                "<LOWERCASE>",
                "<TAB1>",
                ")",
                "<TAB1>",
            ],
            tokenize("abc\t(\ta\t\t,\t\t\tstr\t)\t"),
        )

        self.assertEqual(
            [
                "def",
                "<SPACE1>",
                "<LOWERCASE>",
                "<TAB1>",
                "(",
                "<TAB1>",
                "<LETTER_LOW>",
                "<TAB2>",
                ",",
                "<TAB2>",
                "<LETTER_LOW>",
                "<TAB1>",
                ")",
                ":",
                "<TAB1>",
                "\n",
                "<TAB2>",
                "<LETTER_LOW>",
                "<TAB2>",
                "=",
                "<TAB4>",
                "<INT>",
            ],
            tokenize("def abc\t(\ta\t\t,\t\tb\t):\t\n\t\tt\t\t=\t\t\t\t45"),
        )

    def test_tokenize_tabs_and_spaces_between_tokens(self):
        self.assertEqual(
            [
                "<LETTER_LOW>",
                "<TAB1>",
                "=",
                "<TAB2>",
                "<SPACE1>",
                "<TAB1>",
                "<SPACE2>",
                "<INT>",
            ],
            tokenize("a\t=\t\t \t  42"),
        )

    def test_tokenize_indent(self):
        self.assertEqual(
            [
                "def",
                "<SPACE1>",
                "<LETTER_LOW>",
                "(",
                ")",
                ":",
                "\n",
                "<SPACE1>",
                "<LETTER_LOW>",
                "<SPACE1>",
                "=",
                "<SPACE1>",
                "<INT>",
            ],
            tokenize("def t():\n a = 42"),
        )
        self.assertEqual(
            [
                "def",
                "<SPACE1>",
                "<LETTER_LOW>",
                "(",
                ")",
                ":",
                "\n",
                "<SPACE2>",
                "<LETTER_LOW>",
                "<SPACE1>",
                "=",
                "<SPACE1>",
                "<INT>",
            ],
            tokenize("def t():\n  a = 42"),
        )
        self.assertEqual(
            [
                "def",
                "<SPACE1>",
                "<LETTER_LOW>",
                "(",
                ")",
                ":",
                "\n",
                "<TAB1>",
                "<LETTER_LOW>",
                "<SPACE1>",
                "=",
                "<SPACE1>",
                "<INT>",
            ],
            tokenize("def t():\n\ta = 42"),
        )
        self.assertEqual(
            [
                "def",
                "<SPACE1>",
                "<LETTER_LOW>",
                "(",
                ")",
                ":",
                "\n",
                "<TAB2>",
                "<LETTER_LOW>",
                "<SPACE1>",
                "=",
                "<SPACE1>",
                "<INT>",
            ],
            tokenize("def t():\n\t\ta = 42"),
        )

        self.assertEqual(
            [
                "class",
                "<SPACE1>",
                "<LETTER_LOW>",
                ":",
                "\n",
                "<SPACE2>",
                "<LETTER_LOW>",
                "<SPACE1>",
                "=",
                "<SPACE1>",
                "<INT>",
                "\n",
                "<SPACE2>",
                "<LETTER_LOW>",
                "<SPACE1>",
                "=",
                "<SPACE1>",
                "<INT>",
            ],
            tokenize("class a:\n  b = 42\n  c = 4"),
        )

        code = textwrap.dedent(
            """
        class a:
        \t\t\tdef b():
        \t\t\t\tb = 42
        \t\t\t\tc = 4
        m = 5
        def n():
        \t\tl = 4"""
        )
        self.assertEqual(
            [
                "\n",
                "class",
                "<SPACE1>",
                "<LETTER_LOW>",
                ":",
                "\n",
                "<TAB2>",
                "<TAB1>",
                "def",
                "<SPACE1>",
                "<LETTER_LOW>",
                "(",
                ")",
                ":",
                "\n",
                "<TAB2>",
                "<TAB1>",
                "<TAB1>",
                "<LETTER_LOW>",
                "<SPACE1>",
                "=",
                "<SPACE1>",
                "<INT>",
                "\n",
                "<TAB2>",
                "<TAB1>",
                "<TAB1>",
                "<LETTER_LOW>",
                "<SPACE1>",
                "=",
                "<SPACE1>",
                "<INT>",
                "\n",
                "<LETTER_LOW>",
                "<SPACE1>",
                "=",
                "<SPACE1>",
                "<INT>",
                "\n",
                "def",
                "<SPACE1>",
                "<LETTER_LOW>",
                "(",
                ")",
                ":",
                "\n",
                "<TAB2>",
                "<LETTER_LOW>",
                "<SPACE1>",
                "=",
                "<SPACE1>",
                "<INT>",
            ],
            tokenize(code),
        )

        code = textwrap.dedent(
            """
        print('42')
        def abc(test:str='asd'):
            pass
        def m():
         pass"""
        )
        self.assertEqual(
            [
                "\n",
                "<LOWERCASE>",
                "(",
                "'",
                '<STRING>',
                "'",
                ")",
                "\n",
                "def",
                "<SPACE1>",
                "<LOWERCASE>",
                "(",
                "<LOWERCASE>",
                ":",
                "<LOWERCASE>",
                "=",
                "'",
                '<STRING>',
                "'",
                ")",
                ":",
                "\n",
                "<SPACE4>",
                "pass",
                "\n",
                "def",
                "<SPACE1>",
                "<LETTER_LOW>",
                "(",
                ")",
                ":",
                "\n",
                "<SPACE1>",
                "pass",
            ],
            tokenize(code),
        )

        code = textwrap.dedent(
            """
        print(a,
              b)"""
        )
        self.assertEqual(
            [
                "\n",
                "<LOWERCASE>",
                "(",
                "<LETTER_LOW>",
                ",",
                "\n",
                "<SPACE4>",
                "<SPACE2>",
                "<LETTER_LOW>",
                ")",
            ],
            tokenize(code),
        )

    def test_tokenize_trailing_indent(self):
        self.assertEqual(
            ["<LETTER_LOW>", "<SPACE1>", "=", "<SPACE1>", "<INT>", "<SPACE1>"],
            tokenize("a = 42 "),
        )
        self.assertEqual(
            ["<LETTER_LOW>", "<SPACE1>", "=", "<SPACE1>", "<INT>", "<SPACE2>"],
            tokenize("a = 42  "),
        )
        self.assertEqual(
            ["<LETTER_LOW>", "<SPACE1>", "=", "<SPACE1>", "<INT>", "<TAB1>"],
            tokenize("a = 42\t"),
        )
        self.assertEqual(
            ["<LETTER_LOW>", "<SPACE1>", "=", "<SPACE1>", "<INT>", "<TAB2>"],
            tokenize("a = 42\t\t"),
        )
        self.assertEqual(
            [
                "<LETTER_LOW>",
                "<SPACE1>",
                "=",
                "<SPACE1>",
                "<INT>",
                "<TAB2>",
                "<TAB1>",
                "<SPACE1>",
                "<TAB1>",
            ],
            tokenize("a = 42\t\t\t \t"),
        )

        self.assertEqual(
            [
                "class",
                "<SPACE1>",
                "<LETTER_LOW>",
                ":",
                "<SPACE2>",
                "\n",
                "<SPACE2>",
                "<LETTER_LOW>",
                "<SPACE1>",
                "=",
                "<SPACE1>",
                "<INT>",
                "<SPACE1>",
                "\n",
                "<SPACE2>",
                "<LETTER_LOW>",
                "<SPACE1>",
                "=",
                "<SPACE1>",
                "<INT>",
                "<SPACE4>",
            ],
            tokenize("class a:  \n  b = 42 \n  c = 4    "),
        )
        self.assertEqual(
            [
                "class",
                "<SPACE1>",
                "<LETTER_LOW>",
                ":",
                "<TAB1>",
                "\n",
                "<SPACE2>",
                "def",
                "<SPACE1>",
                "<LETTER_LOW>",
                "(",
                ")",
                ":",
                "<SPACE2>",
                "\n",
                "<SPACE2>",
                "<SPACE2>",
                "<LETTER_LOW>",
                "<SPACE1>",
                "=",
                "<SPACE1>",
                "<INT>",
                "<TAB2>",
                "\n",
                "<SPACE2>",
                "<SPACE2>",
                "<LETTER_LOW>",
                "<SPACE1>",
                "=",
                "<SPACE1>",
                "<INT>",
                "<SPACE1>",
            ],
            tokenize("class a:\t\n  def b():  \n    b = 42\t\t\n    c = 4 "),
        )

        code = textwrap.dedent(
            """
        print('42')\t\t\t\t\t
        def abc(test:str='asd'):  
            pass   """
        )
        self.assertEqual(
            [
                "\n",
                "<LOWERCASE>",
                "(",
                "'",
                '<STRING>',
                "'",
                ")",
                "<TAB4>",
                "<TAB1>",
                "\n",
                "def",
                "<SPACE1>",
                "<LOWERCASE>",
                "(",
                "<LOWERCASE>",
                ":",
                "<LOWERCASE>",
                "=",
                "'",
                '<STRING>',
                "'",
                ")",
                ":",
                "<SPACE2>",
                "\n",
                "<SPACE4>",
                "pass",
                "<SPACE2>",
                "<SPACE1>",
            ],
            tokenize(code),
        )

    def test_tokenize_simple_example(self):
        self.assertEqual(
            [
                "'''",
                "<STRING>",
                "'''",
                "\n",
                "<LETTER_LOW>",
                "<SPACE1>",
                "=",
                "<SPACE1>",
                "<INT>",
            ],
            tokenize("'''asdas'''\na = 42"),
        )

        self.assertEqual(
            [
                "def",
                "<SPACE1>",
                "<LOWERCASE>",
                "(",
                ")",
                ":",
                "\n",
                "<SPACE2>",
                "<LETTER_LOW>",
                "<SPACE1>",
                "=",
                "<SPACE1>",
                "<INT>",
            ],
            tokenize("def abc():\n  a = 42"),
        )

        code = textwrap.dedent(
            """
        for r in f.read():
            yield r
            print('42')"""
        )
        self.assertEqual(
            [
                "\n",
                "for",
                "<SPACE1>",
                "<LETTER_LOW>",
                "<SPACE1>",
                "in",
                "<SPACE1>",
                "<LETTER_LOW>",
                ".",
                "<LOWERCASE>",
                "(",
                ")",
                ":",
                "\n",
                "<SPACE4>",
                "yield",
                "<SPACE1>",
                "<LETTER_LOW>",
                "\n",
                "<SPACE4>",
                "<LOWERCASE>",
                "(",
                "'",
                "<STRING>",
                "'",
                ")",
            ],
            tokenize(code),
        )

        code = textwrap.dedent(
            """
        def m():
            try:
                with open(a) as f:
                    for r in f.read():
                        yield r
            except:
                break"""
        )
        self.assertEqual(
            [
                "\n",
                "def",
                "<SPACE1>",
                "<LETTER_LOW>",
                "(",
                ")",
                ":",
                "\n",
                "<SPACE4>",
                "try",
                ":",
                "\n",
                "<SPACE4>",
                "<SPACE4>",
                "with",
                "<SPACE1>",
                "<LOWERCASE>",
                "(",
                "<LETTER_LOW>",
                ")",
                "<SPACE1>",
                "as",
                "<SPACE1>",
                "<LETTER_LOW>",
                ":",
                "\n",
                "<SPACE4>",
                "<SPACE4>",
                "<SPACE4>",
                "for",
                "<SPACE1>",
                "<LETTER_LOW>",
                "<SPACE1>",
                "in",
                "<SPACE1>",
                "<LETTER_LOW>",
                ".",
                "<LOWERCASE>",
                "(",
                ")",
                ":",
                "\n",
                "<SPACE4>",
                "<SPACE4>",
                "<SPACE4>",
                "<SPACE4>",
                "yield",
                "<SPACE1>",
                "<LETTER_LOW>",
                "\n",
                "<SPACE4>",
                "except",
                ":",
                "\n",
                "<SPACE4>",
                "<SPACE4>",
                "break",
            ],
            tokenize(code),
        )

    def test_tokenize_unix_line_endings(self):
        code = "\n"
        self.assertEqual(["\n"], tokenize(code))

        code = """print('42')\ndef abc(test:str='asd'):\n    pass\n"""
        self.assertEqual(
            [
                "<LOWERCASE>",
                "(",
                "'",
                '<STRING>',
                "'",
                ")",
                "\n",
                "def",
                "<SPACE1>",
                "<LOWERCASE>",
                "(",
                "<LOWERCASE>",
                ":",
                "<LOWERCASE>",
                "=",
                "'",
                '<STRING>',
                "'",
                ")",
                ":",
                "\n",
                "<SPACE4>",
                "pass",
                "\n",
            ],
            tokenize(code),
        )

    def test_tokenize_windows_line_endings(self):
        code = "\r\n"
        self.assertEqual(["\r", "\n"], tokenize(code))

        code = """print('42')\r\ndef abc(test:str='asd'):\r\n    pass\r\n"""
        self.assertEqual(
            [
                "<LOWERCASE>",
                "(",
                "'",
                "<STRING>",
                "'",
                ")",
                "\r",
                "\n",
                "def",
                "<SPACE1>",
                "<LOWERCASE>",
                "(",
                "<LOWERCASE>",
                ":",
                "<LOWERCASE>",
                "=",
                "'",
                "<STRING>",
                "'",
                ")",
                ":",
                "\r",
                "\n",
                "<SPACE4>",
                "pass",
                "\r",
                "\n",
            ],
            tokenize(code),
        )

    def test_tokenize_mac_line_endings(self):
        code = "\r"
        self.assertEqual(["\r"], tokenize(code))

        code = """print('42')\rdef abc(test:str='asd'):\r    pass\r"""
        self.assertEqual(
            [
                "<LOWERCASE>",
                "(",
                "'",
                '<STRING>',
                "'",
                ")",
                "\r",
                "def",
                "<SPACE1>",
                "<LOWERCASE>",
                "(",
                "<LOWERCASE>",
                ":",
                "<LOWERCASE>",
                "=",
                "'",
                '<STRING>',
                "'",
                ")",
                ":",
                "\r",
                "<SPACE4>",
                "pass",
                "\r",
            ],
            tokenize(code),
        )

    def test_tokenize_empty_code(self):
        self.assertEqual(0, len(tokenize("")))

    def test_tokenize_comment_lines(self):
        self.assertEqual(["<COMMENT>"], tokenize("# test comment"))
        self.assertEqual(
            ["<COMMENT>", "\n", "<COMMENT>"],
            tokenize("# test comment\n# test comment 2"),
        )

        code = textwrap.dedent(
            """
        # comment before code
        a = 42 # comment next to code
        # comment after code"""
        )
        self.assertEqual(
            [
                "\n",
                "<COMMENT>",
                "\n",
                "<LETTER_LOW>",
                "<SPACE1>",
                "=",
                "<SPACE1>",
                "<INT>",
                "<SPACE1>",
                "<COMMENT>",
                "\n",
                "<COMMENT>",
            ],
            tokenize(code),
        )

        code = textwrap.dedent(
            """
        a = [
            # comment text
        ]"""
        )
        self.assertEqual(
            [
                "\n",
                "<LETTER_LOW>",
                "<SPACE1>",
                "=",
                "<SPACE1>",
                "[",
                "\n",
                "<SPACE4>",
                "<COMMENT>",
                "\n",
                "]",
            ],
            tokenize(code),
        )

        code = textwrap.dedent(
            """
        a = [
            # comment text
            42
        ]"""
        )
        self.assertEqual(
            [
                "\n",
                "<LETTER_LOW>",
                "<SPACE1>",
                "=",
                "<SPACE1>",
                "[",
                "\n",
                "<SPACE4>",
                "<COMMENT>",
                "\n",
                "<SPACE4>",
                "<INT>",
                "\n",
                "]",
            ],
            tokenize(code),
        )

        code = textwrap.dedent(
            """
        a = (
            # comment text
        )"""
        )
        self.assertEqual(
            [
                "\n",
                "<LETTER_LOW>",
                "<SPACE1>",
                "=",
                "<SPACE1>",
                "(",
                "\n",
                "<SPACE4>",
                "<COMMENT>",
                "\n",
                ")",
            ],
            tokenize(code),
        )

        code = textwrap.dedent(
            """
        a = (
            # comment text
            42
        )"""
        )
        self.assertEqual(
            [
                "\n",
                "<LETTER_LOW>",
                "<SPACE1>",
                "=",
                "<SPACE1>",
                "(",
                "\n",
                "<SPACE4>",
                "<COMMENT>",
                "\n",
                "<SPACE4>",
                "<INT>",
                "\n",
                ")",
            ],
            tokenize(code),
        )

        code = textwrap.dedent(
            """
        a = {
            # comment text
        }"""
        )
        self.assertEqual(
            [
                "\n",
                "<LETTER_LOW>",
                "<SPACE1>",
                "=",
                "<SPACE1>",
                "{",
                "\n",
                "<SPACE4>",
                "<COMMENT>",
                "\n",
                "}",
            ],
            tokenize(code),
        )

        code = textwrap.dedent(
            """
        a = {
            # comment text
            42
        }"""
        )
        self.assertEqual(
            [
                "\n",
                "<LETTER_LOW>",
                "<SPACE1>",
                "=",
                "<SPACE1>",
                "{",
                "\n",
                "<SPACE4>",
                "<COMMENT>",
                "\n",
                "<SPACE4>",
                "<INT>",
                "\n",
                "}",
            ],
            tokenize(code),
        )

    def test_tokenize_shebang(self):
        self.assertEqual(
            [
                "<SHEBANG_ENV>",
                "\n",
                "<LETTER_LOW>",
                "<SPACE1>",
                "=",
                "<SPACE1>",
                "<INT>",
            ],
            tokenize("#!/usr/bin/env python\na = 42"),
        )

        self.assertEqual(
            [
                "<SHEBANG_PYTHON>",
                "\n",
                "<LETTER_LOW>",
                "<SPACE1>",
                "=",
                "<SPACE1>",
                "<INT>",
            ],
            tokenize("#!/usr/bin/python\na = 42"),
        )

        self.assertEqual(
            [
                "<SHEBANG_PYTHON3>",
                "\n",
                "<LETTER_LOW>",
                "<SPACE1>",
                "=",
                "<SPACE1>",
                "<INT>",
            ],
            tokenize("#!/usr/bin/python3\na = 42"),
        )

        self.assertEqual(
            ["<SHEBANG>", "\n", "<LETTER_LOW>", "<SPACE1>", "=", "<SPACE1>", "<INT>"],
            tokenize("#!/usr/bin/test\na = 42"),
        )
        self.assertEqual(
            ["<SHEBANG>", "\n", "<LETTER_LOW>", "<SPACE1>", "=", "<SPACE1>", "<INT>"],
            tokenize("#!/usr/bin/abc\na = 42"),
        )

        self.assertEqual(
            ["<LETTER_LOW>", "<SPACE1>", "=", "<SPACE1>", "<INT>", "\n", "<COMMENT>"],
            tokenize("a = 42\n#!/usr/bin/python"),
        )

    def test_tokenize_encoding_comment(self):
        self.assertEqual(
            [
                "<ENCODING_COMMENT>",
                "\n",
                "<LETTER_LOW>",
                "<SPACE1>",
                "=",
                "<SPACE1>",
                "<INT>",
            ],
            tokenize("# -*- coding: utf-8 -*-\na = 42"),
        )

        self.assertEqual(
            [
                "<LETTER_LOW>",
                "<SPACE1>",
                "=",
                "<SPACE1>",
                "<INT>",
                "\n",
                "<ENCODING_COMMENT>",
            ],
            tokenize("a = 42\n# -*- coding: utf-8 -*-"),
        )

        self.assertEqual(
            ["<SHEBANG_ENV>", "\n", "<ENCODING_COMMENT>"],
            tokenize("#!/usr/bin/env python\n# -*- coding: utf-8 -*-"),
        )

        self.assertEqual(
            [
                "<LETTER_LOW>",
                "<SPACE1>",
                "=",
                "<SPACE1>",
                "<INT>",
                "\n",
                "<LETTER_LOW>",
                "<SPACE1>",
                "=",
                "<SPACE1>",
                "<INT>",
                "\n",
                "<COMMENT>",
            ],
            tokenize("a = 42\nb = 4\n# -*- coding: utf-8 -*-"),
        )

    def test_tokenize_line_continuation(self):
        code = textwrap.dedent(
            """
        a = 42 \\
            + 42"""
        )
        self.assertEqual(
            [
                "\n",
                "<LETTER_LOW>",
                "<SPACE1>",
                "=",
                "<SPACE1>",
                "<INT>",
                "<SPACE1>",
                "<LINE_CONTINUATION>",
                "<SPACE4>",
                "+",
                "<SPACE1>",
                "<INT>",
            ],
            tokenize(code),
        )

        code = textwrap.dedent(
            """
        a = 42 \\
        \\
            + 42"""
        )
        self.assertEqual(
            [
                "\n",
                "<LETTER_LOW>",
                "<SPACE1>",
                "=",
                "<SPACE1>",
                "<INT>",
                "<SPACE1>",
                "<LINE_CONTINUATION>",
                "<LINE_CONTINUATION>",
                "<SPACE4>",
                "+",
                "<SPACE1>",
                "<INT>",
            ],
            tokenize(code),
        )

    def test_tokenize_full_example(self):
        code = textwrap.dedent(
            """
        import os
        from pathlib import Path

        a = ''\t
        a = 'test'\t\t\t
        a = r'test' \t
        a = f'test'  \t
        a = rf'test'
        a = '42'
        print('')
        print('test')
        print('42')
        def abc(test:str='asd'):
            pass

        a = ''''''
        a = '''test'''
        a = r'''test'''
        a = f'''test'''
        a = rf'''test'''
        a = '''42'''
        print('''''')
        print('''test''')
        print('''42''')
        def abc(test:str='''asd'''):
            pass

        a = ""
        a = "test"
        a = r"test"
        a = f"test"
        a = rf"test"
        a = "42"
        print("")
        print("test")
        print("42")
        def abc(test:str="asd"):
            pass

        a = \"\"\"\"\"\"
        a = \"\"\"test\"\"\"
        a = r\"\"\"test\"\"\"
        a = f\"\"\"test\"\"\"
        a = rf\"\"\"test\"\"\"
        a = \"\"\"42\"\"\"
        print(\"\"\"\"\"\")
        print(\"\"\"test\"\"\")
        print(\"\"\"42\"\"\")
        def abc(test:str=\"\"\"asd\"\"\"):
            pass

        if True == False:
            return None
        elif True and False or False and False:
            del a
        else:
            assert a == 42

        while True: # infinite loop
            try:
                with open(a) as f:
                    for r in f.read():
                        yield r
            except:
                break
            finally:
                raise Error()

        for i in range(42):
            continue

        test = lambda X: print(X)

        class A:
            \"\"\"
            multiline comment
            \"\"\"
            def test():
                # simple comment test
                global a
                def inner():
                    nonlocal a

        AbCD = 4_2 in [4e2,4,2]
        n = .42 not in [42.,4,2]

        k = [0,1,2]
        l = [3,4,5]
        o = k is l"""
        )

        tokenized_code = textwrap.dedent(
            """
        import<SPACE1><LOWERCASE>
        from<SPACE1><LOWERCASE><SPACE1>import<SPACE1><UPPER_CAMEL_CASE>
        
        <LETTER_LOW><SPACE1>=<SPACE1>''<TAB1>
        <LETTER_LOW><SPACE1>=<SPACE1>'<STRING>'<TAB2><TAB1>
        <LETTER_LOW><SPACE1>=<SPACE1>r'<STRING>'<SPACE1><TAB1>
        <LETTER_LOW><SPACE1>=<SPACE1>f'<STRING>'<SPACE2><TAB1>
        <LETTER_LOW><SPACE1>=<SPACE1>rf'<STRING>'
        <LETTER_LOW><SPACE1>=<SPACE1>'<STRING>'
        <LOWERCASE>('')
        <LOWERCASE>('<STRING>')
        <LOWERCASE>('<STRING>')
        def<SPACE1><LOWERCASE>(<LOWERCASE>:<LOWERCASE>='<STRING>'):
        <SPACE4>pass
        
        <LETTER_LOW><SPACE1>=<SPACE1>''''''
        <LETTER_LOW><SPACE1>=<SPACE1>'''<STRING>'''
        <LETTER_LOW><SPACE1>=<SPACE1>r'''<STRING>'''
        <LETTER_LOW><SPACE1>=<SPACE1>f'''<STRING>'''
        <LETTER_LOW><SPACE1>=<SPACE1>rf'''<STRING>'''
        <LETTER_LOW><SPACE1>=<SPACE1>'''<STRING>'''
        <LOWERCASE>('''''')
        <LOWERCASE>('''<STRING>''')
        <LOWERCASE>('''<STRING>''')
        def<SPACE1><LOWERCASE>(<LOWERCASE>:<LOWERCASE>='''<STRING>'''):
        <SPACE4>pass
        
        <LETTER_LOW><SPACE1>=<SPACE1>""
        <LETTER_LOW><SPACE1>=<SPACE1>"<STRING>"
        <LETTER_LOW><SPACE1>=<SPACE1>r"<STRING>"
        <LETTER_LOW><SPACE1>=<SPACE1>f"<STRING>"
        <LETTER_LOW><SPACE1>=<SPACE1>rf"<STRING>"
        <LETTER_LOW><SPACE1>=<SPACE1>"<STRING>"
        <LOWERCASE>("")
        <LOWERCASE>("<STRING>")
        <LOWERCASE>("<STRING>")
        def<SPACE1><LOWERCASE>(<LOWERCASE>:<LOWERCASE>="<STRING>"):
        <SPACE4>pass
        
        <LETTER_LOW><SPACE1>=<SPACE1>\"\"\"\"\"\"
        <LETTER_LOW><SPACE1>=<SPACE1>\"\"\"<STRING>\"\"\"
        <LETTER_LOW><SPACE1>=<SPACE1>r\"\"\"<STRING>\"\"\"
        <LETTER_LOW><SPACE1>=<SPACE1>f\"\"\"<STRING>\"\"\"
        <LETTER_LOW><SPACE1>=<SPACE1>rf\"\"\"<STRING>\"\"\"
        <LETTER_LOW><SPACE1>=<SPACE1>\"\"\"<STRING>\"\"\"
        <LOWERCASE>(\"\"\"\"\"\")
        <LOWERCASE>(\"\"\"<STRING>\"\"\")
        <LOWERCASE>(\"\"\"<STRING>\"\"\")
        def<SPACE1><LOWERCASE>(<LOWERCASE>:<LOWERCASE>=\"\"\"<STRING>\"\"\"):
        <SPACE4>pass
        
        if<SPACE1>True<SPACE1>==<SPACE1>False:
        <SPACE4>return<SPACE1>None
        elif<SPACE1>True<SPACE1>and<SPACE1>False<SPACE1>or<SPACE1>False<SPACE1>and<SPACE1>False:
        <SPACE4>del<SPACE1><LETTER_LOW>
        else:
        <SPACE4>assert<SPACE1><LETTER_LOW><SPACE1>==<SPACE1><INT>
        
        while<SPACE1>True:<SPACE1><COMMENT>
        <SPACE4>try:
        <SPACE4><SPACE4>with<SPACE1><LOWERCASE>(<LETTER_LOW>)<SPACE1>as<SPACE1><LETTER_LOW>:
        <SPACE4><SPACE4><SPACE4>for<SPACE1><LETTER_LOW><SPACE1>in<SPACE1><LETTER_LOW>.<LOWERCASE>():
        <SPACE4><SPACE4><SPACE4><SPACE4>yield<SPACE1><LETTER_LOW>
        <SPACE4>except:
        <SPACE4><SPACE4>break
        <SPACE4>finally:
        <SPACE4><SPACE4>raise<SPACE1><UPPER_CAMEL_CASE>()
        
        for<SPACE1><LETTER_LOW><SPACE1>in<SPACE1><LOWERCASE>(<INT>):
        <SPACE4>continue
        
        <LOWERCASE><SPACE1>=<SPACE1>lambda<SPACE1><LETTER_UP>:<SPACE1><LOWERCASE>(<LETTER_UP>)
        
        class<SPACE1><LETTER_UP>:
        <SPACE4>\"\"\"<STRING>\"\"\"
        <SPACE4>def<SPACE1><LOWERCASE>():
        <SPACE4><SPACE4><COMMENT>
        <SPACE4><SPACE4>global<SPACE1><LETTER_LOW>
        <SPACE4><SPACE4>def<SPACE1><LOWERCASE>():
        <SPACE4><SPACE4><SPACE4>nonlocal<SPACE1><LETTER_LOW>
        
        <UPPER_CAMEL_CASE2><SPACE1>=<SPACE1><INT><NUMBER_UNDERSCORE><INT><SPACE1>in<SPACE1>[<INT><EXP_LOW><INT>,<INT>,<INT>]
        <LETTER_LOW><SPACE1>=<SPACE1><FLOAT><SPACE1>not<SPACE1>in<SPACE1>[<FLOAT>,<INT>,<INT>]
        
        <LETTER_LOW><SPACE1>=<SPACE1>[<INT>,<INT>,<INT>]
        <LETTER_LOW><SPACE1>=<SPACE1>[<INT>,<INT>,<INT>]
        <LETTER_LOW><SPACE1>=<SPACE1><LETTER_LOW><SPACE1>is<SPACE1><LETTER_LOW>"""
        )

        self.assertEqual(tokenized_code, "".join(tokenize(code)))


if __name__ == "__main__":
    unittest.main()
