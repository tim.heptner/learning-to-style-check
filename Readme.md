# Learn to style check

## About

This repository contains the code for the Codestyle Validator machine learning models.
The Codestyle Validator models are trained to predict if a python code sample follows a given codestyle without
retraining the model for each codestyle. During the prediction the model is given a code sample which should be
validated and code sample which already follows the codestyle. As a basis for the classification style-tree features are
used, which are based on a CST from the [libcst](https://github.com/Instagram/LibCST) package. Checkt out
the [Readme](learning_to_style_check/libcst/Readme.md) of the libcst submodule for more information about the
libcst usage. The style tree features represent all the required components of the code style rules in Python.

Moreover, this project allows you to build datasets with random code styles. Code that does not satisfy a code style can
be converted so that it is in compliance with the code style.

### Model and feature types

There are the following model types:

- Codestyle Validator GCN: a GCN model which uses the style tree features as input
- Codestyle Validator TreeLSTM: a TreeLSTM model which uses the style tree features as input
- Codestyle Validator TreeLSTM v2: a TreeLSTM model which uses the style tree features as input with some changes to the
  TreeLSTM model
- Baseline: a baseline model with cosine similarity which uses n-grams and BoW features based on the style tree features
  as input
- Decision Tree: a decision tree model uses n-grams and BoW features based on the style tree features as input

For more information about the model types check out the implementation in the [model](learning_to_style_check/model)
directory.
You can find more information about the dt model and baseline ine the [decision_tree](notebooks/decision_tree.ipynb)
and [baseline](notebooks/baseline.ipynb) notebooks.

### Codestyle sample types

There are two types of codestyle samples:

1. random codestyle samples: the codestyle of negative samples are different in at least one codestyle rule
2. single codestyle samples: the codestyle of negative samples are different in exactly one codestyle rule

## Installation

Before you start, make sure you have the latest python 3 and pip version installed.

First clone the repository:

```bash
git clone https://gitlab.ub.uni-bielefeld.de/tim.heptner/learning-to-style-check.git
```

Now install the python requirements:

```bash
pip install -r requirements.txt
```

If you want to train on GPU, uninstall the CPU version and install the dgl library with CUDA support, for example for
CUDA 10.18:

```bash
pip uninstall dgl
pip install dgl -f https://data.dgl.ai/wheels/cu118/repo.html
```

You can find more information about installing dgl with CUDA support for different
platforms [here](https://www.dgl.ai/pages/start.html).

After the installation add the module to the python path:

```bash
export PYTHONPATH="${PYTHONPATH}:$(pwd)" 
```

## Usage

### Prepare source code

You can use any source code you want to train the model. But the source code should follow a general similar codestyle.
Before you can build a dataset you have to prepare the source code with the following command:

```bash
python learning_to_style_check/dataset/prepare_code.py \
  <path-to-the-source-root> \
  --remove-setup-files \
  --remove-non-py-files \
  --remove-header "#"
```

This removes non python files, setup files and header comments from the source code.
You can find more information about the command line arguments with the `--help` flag:

```bash
python learning_to_style_check/dataset/prepare_code.py --help

usage: Prepare source code dataset [-h] [--remove-header REMOVE_HEADER] [--remove-non-py-files] [--remove-setup-files] path

positional arguments:
  path                  Path source code root directory

options:
  -h, --help            show this help message and exit
  --remove-header REMOVE_HEADER
                        Remove a header from source code which begins with this string
  --remove-non-py-files
                        Remove non python files
  --remove-setup-files  Remove setup.py files
```

### Build a code dataset

After you prepared the source code you can build a code dataset. The dataset is build from a directory with source code.
During the dataset creation the specified number of random codestyles are generated and applied to the source code.  
You can build the dataset with the following command:

```bash
python learning_to_style_check/dataset/build_code.py \
  <path-to-the-source> \
  <dataset-name> \
  <number-of-codestyles>
```

You can find more information about the command line arguments with the `--help` flag:

```bash
python learning_to_style_check/dataset/build_code.py --help

usage: Create a codestyle code dataset from a directory with source codes [-h] [--cpu-count CPU_COUNT] [--seed SEED] [--n-mutated-rules N_MUTATED_RULES] [--min-code-len MIN_CODE_LEN] [--max-code-len MAX_CODE_LEN]
                                                                          [--allowed-suffixes ALLOWED_SUFFIXES [ALLOWED_SUFFIXES ...]] [--train-test-split TRAIN_TEST_SPLIT] [--files-per-codestyle FILES_PER_CODESTYLE]
                                                                          [--unique-style-contexts UNIQUE_STYLE_CONTEXTS] [--push] [--save-to-disk] [--public]
                                                                          path dataset_name n_codestyles {random,single}

positional arguments:
  path                  Path source code root directory
  dataset_name          Name of the dataset on huggingface
  n_codestyles          Number of codestyles to generate
  {random,single}       Type of negative sample to generate. If random, the negative sample have a fully distinct codestyle. If single, the negative sample only has a single codestyle rule different from the positive sample.

options:
  -h, --help            show this help message and exit
  --cpu-count CPU_COUNT
                        Number of cpus to use
  --seed SEED           Random seed
  --n-mutated-rules N_MUTATED_RULES
                        Number of rules to mute for the 'single' negative sample type
  --min-code-len MIN_CODE_LEN
                        Minimum length of a code sample
  --max-code-len MAX_CODE_LEN
                        Maximum length of a code sample
  --allowed-suffixes ALLOWED_SUFFIXES [ALLOWED_SUFFIXES ...]
                        allowed file suffixes
  --train-test-split TRAIN_TEST_SPLIT
                        ratio of train/test split of codestyles
  --files-per-codestyle FILES_PER_CODESTYLE
                        number of files per codestyle
  --unique-style-contexts UNIQUE_STYLE_CONTEXTS
                        Number of unique codestyle per codestyle
  --push                push the the dataset to hugginface
  --save-to-disk        save the dataset to disk
  --public              push the the dataset with public access
```

For example create a random dataset with 1k unique code styles and push it to huggingface:

```bash
python learning_to_style_check/dataset/build_code.py \
  data/base_repos \
  python_codestyles-random-1k \
  1000 \
  random \
  --push
```

### Build a graph dataset

```bash
python learning_to_style_check/dataset/build_graph.py --help

usage: Convert a code codestyle dataset to a graph dataset [-h] [--cpu-count CPU_COUNT] [--from-disk] [--save-to-disk] [--push] [--public] [--dtype {int32,int64}] code_dataset_name graph_dataset_name

positional arguments:
  code_dataset_name     Name of the code dataset
  graph_dataset_name    Name of the graph dataset

options:
  -h, --help            show this help message and exit
  --cpu-count CPU_COUNT
                        Number of cpus to use
  --from-disk           load the dataset from disk
  --save-to-disk        save the dataset to disk
  --push                push the the dataset to huggingface
  --public              push the the dataset with public access
  --dtype {int32,int64}
                        data type of the dataset
```

### Training

You can train the Codestyle Validator GCN and Codestyle Validator TreeLSTM models with the following command:

```bash
python train.py --help

usage: Train a model on the python codestyles graph dataset [-h] [--epochs EPOCHS] [--train-batch-size TRAIN_BATCH_SIZE] [--eval-batch-size EVAL_BATCH_SIZE] [--cpu] [--push-to-hub] [--hub-model-id HUB_MODEL_ID]
                                                            [--from-pretrained FROM_PRETRAINED] [--optimizer {adamw,adam,adafactor}] [--eval-strategy {epoch,steps}] [--eval-steps EVAL_STEPS] [--save-strategy {epoch,steps}]
                                                            [--save-steps SAVE_STEPS] [--wandb-tags WANDB_TAGS [WANDB_TAGS ...]]
                                                            dataset_name {tree-lstm,tree-lstm-v2,gcn} ...

positional arguments:
  dataset_name          Name of the dataset to use
  {tree-lstm,tree-lstm-v2,gcn}
                        Model type to train

options:
  -h, --help            show this help message and exit
  --epochs EPOCHS       Number of epochs to train for
  --train-batch-size TRAIN_BATCH_SIZE
                        Batch size for training
  --eval-batch-size EVAL_BATCH_SIZE
                        Batch size for evaluation
  --cpu                 Use CPU instead of GPU
  --push-to-hub         Push the model to the hub
  --hub-model-id HUB_MODEL_ID
                        Model ID to push to
  --from-pretrained FROM_PRETRAINED
                        Continue from a pretrained model
  --optimizer {adamw,adam,adafactor}
                        Optimizer to use
  --eval-strategy {epoch,steps}
  --eval-steps EVAL_STEPS
  --save-strategy {epoch,steps}
  --save-steps SAVE_STEPS
  --wandb-tags WANDB_TAGS [WANDB_TAGS ...]
                        Tags to add to the wandb run
```

For example train the gcn model on the `random-1k-graphs` dataset for 15 epochs:

```bash
python3 train.py \
  --train-batch-size 128
  --eval-batch-size 128 
  --epochs 15 
  random-1k-graphs gcn
```

For the other architectures please check out the [decision_tree](notebooks/decision_tree.ipynb)
and [baseline](notebooks/baseline.ipynb) notebooks.

## Prebuild datasets

The following datasets are available on huggingface:

| dateset type | number of code styles |                 DOI                  |
|:------------:|:---------------------:|:------------------------------------:|
|    random    |          500          | [10.57967/hf/1229](https://doi.org/10.57967/hf/1229) |
|    single    |          500          | [10.57967/hf/1230](https://doi.org/10.57967/hf/1230) |
|    mixed     |          500          | [10.57967/hf/1231](https://doi.org/10.57967/hf/1231) |
|    random    |          1k           | [10.57967/hf/1232](https://doi.org/10.57967/hf/1232) |
|    single    |          1k           | [10.57967/hf/1233](https://doi.org/10.57967/hf/1233) |
|    mixed     |          1k           | [10.57967/hf/1234](https://doi.org/10.57967/hf/1234) |

Each dataset is build from the following repositories:

|                               repository                                |              tag or commit               |
|:-----------------------------------------------------------------------:|:----------------------------------------:|
|     [TheAlgorithms/Python](https://github.com/TheAlgorithms/Python)     | f614ed72170011d2d439f7901e1c8daa7deac8c4 |
| [huggingface/transformers](https://github.com/huggingface/transformers) |                 v4.31.0                  |
|     [huggingface/datasets](https://github.com/huggingface/datasets)     |                  2.13.1                  |
|    [huggingface/diffusers](https://github.com/huggingface/diffusers)    |                 v0.18.2                  |
|   [huggingface/accelerate](https://github.com/huggingface/accelerate)   |                 v0.21.0                  |

## Pretrained models

There are pretrained models available on huggingface:

| model |                                                                            link                                                                             |
|:-----:|:-----------------------------------------------------------------------------------------------------------------------------------------------------------:|
|  gcn  | [infinityofspace/codestyle_validator-gcn-random_1k-v1-dropout_0.3](https://huggingface.co/infinityofspace/codestyle_validator-gcn-random_1k-v1-dropout_0.3) |
|  dt   |         [infinityofspace/python_codestyle_validator-dt-random_500](https://huggingface.co/infinityofspace/python_codestyle_validator-dt-random_500)         |

## Acknowledgements

This project uses the following open source projects:

- [LibCST](https://github.com/Instagram/LibCST)
