from argparse import ArgumentParser

from datasets import load_dataset
from transformers import PreTrainedModel

from learning_to_style_check.libcst import NODE_NAMES
from learning_to_style_check.model.gcn import CodestyleValidatorGCN, CodestyleValidatorGCNConfig
from learning_to_style_check.model.tree_lstm import CodestyleValidatorTreeLSTM, CodestyleValidatorTreeLSTMConfig
from learning_to_style_check.model.tree_lstm_v2.config import CodestyleValidatorTreeLSTMv2Config
from learning_to_style_check.model.tree_lstm_v2.model import CodestyleValidatorTreeLSTMv2
from learning_to_style_check.train import TrainingArgsConfig
from learning_to_style_check.train import training_loop, simple_batcher, batcher_self_loops


def train_tree_lstm_v2(embedding_size: int = 300,
                       dropout: float = 0,
                       h_size: int = 150,
                       from_pretrained: str = None,
                       **kwargs):
    if from_pretrained:
        model = CodestyleValidatorTreeLSTMv2.from_pretrained(from_pretrained)
    else:
        model_config = CodestyleValidatorTreeLSTMv2Config(num_vocabs=len(NODE_NAMES),
                                                          embedding_size=embedding_size,
                                                          dropout=dropout,
                                                          lstm_h_size=h_size)

        model = CodestyleValidatorTreeLSTMv2(model_config)

    train(model, simple_batcher, **kwargs)


def train_tree_lstm(embedding_size: int = 300,
                    dropout: float = 0,
                    h_size: int = 150,
                    from_pretrained: str = None,
                    **kwargs):
    if from_pretrained:
        model = CodestyleValidatorTreeLSTM.from_pretrained(from_pretrained)
    else:
        model_config = CodestyleValidatorTreeLSTMConfig(num_vocabs=len(NODE_NAMES),
                                                        embedding_size=embedding_size,
                                                        dropout=dropout,
                                                        lstm_h_size=h_size)

        model = CodestyleValidatorTreeLSTM(model_config)

    train(model, simple_batcher, **kwargs)


def train_gcn(embedding_size: int = 300,
              dropout: float = 0,
              conv_out_size: int = 200,
              from_pretrained: str = None,
              **kwargs):
    if from_pretrained:
        model = CodestyleValidatorGCN.from_pretrained(from_pretrained)
    else:
        model_config = CodestyleValidatorGCNConfig(num_vocabs=len(NODE_NAMES),
                                                   embedding_size=embedding_size,
                                                   dropout=dropout,
                                                   conv_out_size=conv_out_size)

        model = CodestyleValidatorGCN(model_config)

    train(model, batcher_self_loops, **kwargs)


def train(model: PreTrainedModel,
          collate_fn: callable,
          dataset_name: str,
          epochs: int,
          train_batch_size: int,
          eval_batch_size: int,
          optimizer: str,
          eval_strategy: str,
          eval_steps: int,
          save_strategy: str,
          save_steps: int,
          cpu: bool = False,
          push_to_hub: bool = False,
          hub_model_id: str = None,
          wandb_tags: list = None,
          **kwargs):
    dataset = load_dataset(dataset_name)

    dataset = dataset.with_format("pt")

    train_config = TrainingArgsConfig()
    train_config.epochs = epochs
    train_config.train_batch_size = train_batch_size
    train_config.eval_batch_size = eval_batch_size
    train_config.eval_strategy = eval_strategy
    train_config.eval_steps = eval_steps
    train_config.save_strategy = save_strategy
    train_config.save_steps = save_steps
    train_config.cpu = cpu
    train_config.optimizer = optimizer
    train_config.push_to_hub = push_to_hub
    train_config.hub_model_id = hub_model_id

    tracker_kwargs = {}
    if wandb_tags:
        tracker_kwargs["wandb"] = {"tags": wandb_tags}

    training_loop(train_config=train_config,
                  model=model,
                  dataset=dataset,
                  collate_fn=collate_fn,
                  tracker_kwargs=tracker_kwargs)


if __name__ == "__main__":
    argparse = ArgumentParser("Train a model on the python codestyles graph dataset")

    argparse.add_argument("dataset_name", type=str, help="Name of the dataset to use")

    model_subparsers = argparse.add_subparsers(dest="model_type", required=True, help="Model type to train")

    # Training arguments
    argparse.add_argument("--epochs", type=int, default=10, help="Number of epochs to train for")
    argparse.add_argument("--train-batch-size", type=int, default=32, help="Batch size for training")
    argparse.add_argument("--eval-batch-size", type=int, default=32, help="Batch size for evaluation")
    argparse.add_argument("--cpu", action="store_true", help="Use CPU instead of GPU")
    argparse.add_argument("--push-to-hub", action="store_true", help="Push the model to the hub")
    argparse.add_argument("--hub-model-id", type=str, default=None, help="Model ID to push to")
    argparse.add_argument("--from-pretrained", type=str, default=None, help="Continue from a pretrained model")
    argparse.add_argument("--optimizer", type=str, default="adamw", choices=["adamw", "adam", "adafactor"],
                          help="Optimizer to use")
    argparse.add_argument("--eval-strategy", type=str, default="epoch", choices=["epoch", "steps"])
    argparse.add_argument("--eval-steps", type=int, default=500)
    argparse.add_argument("--save-strategy", type=str, default="epoch", choices=["epoch", "steps"])
    argparse.add_argument("--save-steps", type=int, default=500)

    argparse.add_argument("--wandb-tags", type=str, nargs="+", default=[], help="Tags to add to the wandb run")

    # Tree-LSTM specific
    tree_lstm_subparser = model_subparsers.add_parser("tree-lstm")
    tree_lstm_subparser.set_defaults(func=train_tree_lstm)
    tree_lstm_subparser.add_argument("--embedding-size", type=int, default=300, help="Size of the input embedding")
    tree_lstm_subparser.add_argument("--dropout", type=float, default=0, help="Dropout rate")
    tree_lstm_subparser.add_argument("--h-size", type=int, default=150, help="Size of the hidden LSTM layer")

    # Tree-LSTM v2 specific
    tree_lstm_subparser = model_subparsers.add_parser("tree-lstm-v2")
    tree_lstm_subparser.set_defaults(func=train_tree_lstm_v2)
    tree_lstm_subparser.add_argument("--embedding-size", type=int, default=300, help="Size of the input embedding")
    tree_lstm_subparser.add_argument("--dropout", type=float, default=0, help="Dropout rate")
    tree_lstm_subparser.add_argument("--set2set-n-iters", type=int, default=6,
                                     help="Number of iterations for the set2set layer")
    tree_lstm_subparser.add_argument("--set2set-n-layers", type=int, default=3,
                                     help="Number of layers for the set2set layer")
    tree_lstm_subparser.add_argument("--h-size", type=int, default=150, help="Size of the hidden LSTM layer")

    # GCN specific arguments
    gcn_subparser = model_subparsers.add_parser("gcn")
    gcn_subparser.set_defaults(func=train_gcn)
    gcn_subparser.add_argument("--embedding-size", type=int, default=300, help="Size of the input embedding")
    gcn_subparser.add_argument("--dropout", type=float, default=0, help="Dropout rate")
    gcn_subparser.add_argument("--conv-out-size", type=int, default=200, help="Size of the output convolution layer")

    args = argparse.parse_args()

    args.func(**vars(args))
