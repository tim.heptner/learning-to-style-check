# Naming rules

This folder contains samples of naming rules which can not be detected by `pycodestyle`. Each sample have a valid sample with a suffix `_pos`.

- `sample_1.py`: class name should be `CamelCase`
- `sample_2.py`: function name should be `snake_case`
- `sample_3.py`: class method name should be `snake_case`
- `sample_4.py`: first param in class instance methods should be `self` or `cls` for class methods
- `sample_5.py`: exception names should be `CamelCase` with error suffix
- `sample_6.py`: function and method params should be `snake_case`
- `sample_7.py`: variable names should be `snake_case`
- `sample_8.py`: constant names should be `UPPER_CASE_WITH_UNDERSCORES`
- `sample_9.py`: file ending must be `.py`
