# Naming rules

This folder contains samples of rules which can not be detected by `pycodestyle` (or `pydocstyle`). Each sample have a valid sample with a suffix `_pos`.

- `sample_1.py`: no wildcard imports
- `sample_2.py`: missing main execution check
- `sample_3.py`: same string indicator in all files
- `sample_4.py`: module docstring with licence text
- `sample_5.py`: import order
- `sample_6.py`: two statements on the same line (google styleguide)
- `sample_7.py`: TODO with parenthesized context

