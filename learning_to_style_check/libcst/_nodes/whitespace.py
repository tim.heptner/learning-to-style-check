# Copyright (c) Meta Platforms, Inc. and affiliates.
#
# This source code is licensed under the MIT license found in the
# LICENSE file in the root directory of this source tree.


import re
from abc import ABC, abstractmethod
from dataclasses import dataclass
from typing import Optional, Pattern, Sequence

from learning_to_style_check.libcst._add_slots import add_slots
from learning_to_style_check.libcst._nodes.base import BaseLeaf, BaseValueToken, CSTNode, CSTValidationError
from learning_to_style_check.libcst._nodes.internal import (
    CodegenState,
    visit_optional,
    visit_required,
    visit_sequence, TokengenState, TreegenState,
)
from learning_to_style_check.libcst._visitors import CSTVisitorT

# SimpleWhitespace includes continuation characters, which must be followed immediately
# by a newline. SimpleWhitespace does not include other kinds of newlines, because those
# may have semantic significance.
SIMPLE_WHITESPACE_RE: Pattern[str] = re.compile(r"([ \f\t]|\\(\r\n?|\n))*", re.UNICODE)
NEWLINE_RE: Pattern[str] = re.compile(r"\r\n?|\n", re.UNICODE)
COMMENT_RE: Pattern[str] = re.compile(r"#[^\r\n]*", re.UNICODE)

COMMENT_TOKEN = "<COMMENT>"

SHEBANG_TOKEN = "<SHEBANG>"
SHEBANG_ENV_TOKEN = "<SHEBANG_ENV>"
SHEBANG_PYTHON_TOKEN = "<SHEBANG_PYTHON>"
SHEBANG_PYTHON3_TOKEN = "<SHEBANG_PYTHON3>"

ENCODING_COMMENT_TOKEN = "<ENCODING_COMMENT>"

SPACING_LINE_CONTINUATION_TOKEN = "<LINE_CONTINUATION>"

SPACING_1_SPACE_TOKEN = "<SPACE1>"
SPACING_2_SPACE_TOKEN = "<SPACE2>"
SPACING_4_SPACE_TOKEN = "<SPACE4>"

SPACING_1_TAB_TOKEN = "<TAB1>"
SPACING_2_TAB_TOKEN = "<TAB2>"
SPACING_4_TAB_TOKEN = "<TAB4>"


class BaseParenthesizableWhitespace(CSTNode, ABC):
    """
    This is the kind of whitespace you might see inside the body of a statement or
    expression between two tokens. This is the most common type of whitespace.

    The list of allowed characters in a whitespace depends on whether it is found
    inside a parenthesized expression or not. This class allows nodes which can be
    found inside or outside a ``()``, ``[]`` or ``{}`` section to accept either
    whitespace form.

    https://docs.python.org/3/reference/lexical_analysis.html#implicit-line-joining

    Parenthesizable whitespace may contain a backslash character (``\\``), when used as
    a line-continuation character. While the continuation character isn't technically
    "whitespace", it serves the same purpose.

    Parenthesizable whitespace is often non-semantic (optional), but in cases where
    whitespace solves a grammar ambiguity between tokens (e.g. ``if test``, versus
    ``iftest``), it has some semantic value.
    """

    __slots__ = ()

    # TODO: Should we somehow differentiate places where we require non-zero whitespace
    # with a separate type?

    @property
    @abstractmethod
    def empty(self) -> bool:
        """
        Indicates that this node is empty (zero whitespace characters).
        """
        ...


@add_slots
@dataclass(frozen=True)
class SimpleWhitespace(BaseParenthesizableWhitespace, BaseValueToken):
    """
    This is the kind of whitespace you might see inside the body of a statement or
    expression between two tokens. This is the most common type of whitespace.

    A simple whitespace cannot contain a newline character unless it is directly
    preceeded by a line continuation character (``\\``). It can contain zero or
    more spaces or tabs. If you need a newline character without a line continuation
    character, use :class:`ParenthesizedWhitespace` instead.

    Simple whitespace is often non-semantic (optional), but in cases where whitespace
    solves a grammar ambiguity between tokens (e.g. ``if test``, versus ``iftest``),
    it has some semantic value.

    An example :class:`SimpleWhitespace` containing a space, a line continuation,
    a newline and another space is as follows::

        SimpleWhitespace(r" \\\\n ")
    """

    #: Actual string value of the simple whitespace. A legal value contains only
    #: space, ``\f`` and ``\t`` characters, and optionally a continuation
    #: (``\``) followed by a newline (``\n`` or ``\r\n``).
    value: str

    def _validate(self) -> None:
        if SIMPLE_WHITESPACE_RE.fullmatch(self.value) is None:
            raise CSTValidationError(
                f"Got non-whitespace value for whitespace node: {repr(self.value)}"
            )

    @property
    def empty(self) -> bool:
        """
        Indicates that this node is empty (zero whitespace characters).
        """

        return len(self.value) == 0

    @property
    def spacing(self) -> Sequence["Spacing"]:
        spacing_nodes = []
        for s in re.split(r"(\\\n| {4}| {2}| |\t{4}|\t{2}|\t)", self.value):
            if s:
                if s == SpacingLineContinuation.value:
                    spacing_nodes.append(SpacingLineContinuation())
                if s == Spacing1Space.value:
                    spacing_nodes.append(Spacing1Space())
                elif s == Spacing2Space.value:
                    spacing_nodes.append(Spacing2Space())
                elif s == Spacing4Space.value:
                    spacing_nodes.append(Spacing4Space())
                elif s == Spacing1Tab.value:
                    spacing_nodes.append(Spacing1Tab())
                elif s == Spacing2Tab.value:
                    spacing_nodes.append(Spacing2Tab())
                elif s == Spacing4Tab.value:
                    spacing_nodes.append(Spacing4Tab())

        return spacing_nodes

    def _tokengen_impl(self, state: CodegenState) -> None:
        for s in self.spacing:
            state.add_token(s.token)

    def _treegen_impl(self, state: TreegenState, parent: "Tree" = None) -> None:
        if self.value:
            tree = state.add_tree(self.__class__.__name__, parent)
            for n in self.spacing:
                state.add_tree(n.__class__.__name__, tree)


class Spacing(CSTNode, ABC):

    value: str

    def __init__(self):
        pass

    def _codegen_impl(self, state: CodegenState) -> None:
        state.add_token(self.value)

    def _tokengen_impl(self, state: CodegenState) -> None:
        state.add_token(self.value)

    def _treegen_impl(self, state: TreegenState, parent: "Tree" = None) -> None:
        tree = state.add_tree(self.__class__.__name__, parent)


@dataclass(frozen=True)
class SpacingLineContinuation(Spacing):

    value = "\\\n"
    token = SPACING_LINE_CONTINUATION_TOKEN

    def __init__(self):
        super().__init__()

    def _visit_and_replace_children(self, visitor: CSTVisitorT) -> "SpacingLineContinuation":
        return self


@dataclass(frozen=True)
class Spacing1Space(Spacing):

    value = " "
    token = SPACING_1_SPACE_TOKEN

    def __init__(self):
        super().__init__()

    def _visit_and_replace_children(self, visitor: CSTVisitorT) -> "Spacing1Space":
        return self


@dataclass(frozen=True)
class Spacing2Space(Spacing):

    value = "  "
    token = SPACING_2_SPACE_TOKEN

    def __init__(self):
        super().__init__()

    def _visit_and_replace_children(self, visitor: CSTVisitorT) -> "Spacing2Space":
        return self


@dataclass(frozen=True)
class Spacing4Space(Spacing):

    value = "    "
    token = SPACING_4_SPACE_TOKEN

    def __init__(self):
        super().__init__()

    def _visit_and_replace_children(self, visitor: CSTVisitorT) -> "Spacing4Space":
        return self


@dataclass(frozen=True)
class Spacing1Tab(Spacing):

    value = "\t"
    token = SPACING_1_TAB_TOKEN

    def __init__(self):
        super().__init__()

    def _visit_and_replace_children(self, visitor: CSTVisitorT) -> "Spacing1Tab":
        return self


@dataclass(frozen=True)
class Spacing2Tab(Spacing):

    value = "\t\t"
    token = SPACING_2_TAB_TOKEN

    def __init__(self):
        super().__init__()

    def _visit_and_replace_children(self, visitor: CSTVisitorT) -> "Spacing2Tab":
        return self


@dataclass(frozen=True)
class Spacing4Tab(Spacing):

    value = "\t\t\t\t"
    token = SPACING_4_TAB_TOKEN

    def __init__(self):
        super().__init__()

    def _visit_and_replace_children(self, visitor: CSTVisitorT) -> "Spacing4Tab":
        return self


class Newline(BaseLeaf):
    """
    Represents the newline that ends an :class:`EmptyLine` or a statement (as part of
    :class:`TrailingWhitespace`).

    Other newlines may occur in the document after continuation characters (the
    backslash, ``\\``), but those newlines are treated as part of the
    :class:`SimpleWhitespace`.

    Optionally, a value can be specified in order to overwrite the module's default
    newline. In general, this should be left as the default, which is ``None``. This
    is allowed because python modules are permitted to mix multiple unambiguous
    newline markers.
    """

    #: A value of ``None`` indicates that the module's default newline sequence should
    #: be used. A value of ``\n`` or ``\r\n`` indicates that the exact value specified
    #: will be used for this newline.
    value: Optional[str] = None

    def _validate(self) -> None:
        value = self.value
        if value and NEWLINE_RE.fullmatch(value) is None:
            raise CSTValidationError(
                f"Got an invalid value for newline node: {repr(value)}"
            )

    def _codegen_impl(self, state: CodegenState) -> None:
        value = self.value
        state.add_token(state.default_newline if value is None else value)

    def _tokengen_impl(self, state: TokengenState) -> None:
        value = self.value
        if value is None:
            state.default_newline._tokengen(state)
        else:
            for v in value:
                state.add_token(v)

    def _treegen_impl(self, state: TreegenState, parent: "Tree" = None) -> None:
        if self.value:
            tree = state.add_tree(self.__class__.__name__, parent)


@add_slots
@dataclass(frozen=True)
class UnixNewline(Newline):
    value: Optional[str] = "\n"


@add_slots
@dataclass(frozen=True)
class WindowsNewline(Newline):
    value: Optional[str] = "\r\n"


@add_slots
@dataclass(frozen=True)
class MacNewline(Newline):
    value: Optional[str] = "\r"


@add_slots
@dataclass(frozen=True)
class Comment(BaseValueToken):
    """
    A comment including the leading pound (``#``) character.

    The leading pound character is included in the 'value' property (instead of being
    stripped) to help re-enforce the idea that whitespace immediately after the pound
    character may be significant. E.g::

        # comment with whitespace at the start (usually preferred)
        #comment without whitespace at the start (usually not desirable)

    Usually wrapped in a :class:`TrailingWhitespace` or :class:`EmptyLine` node.
    """

    #: The comment itself. Valid values start with the pound (``#``) character followed
    #: by zero or more non-newline characters. Comments cannot include newlines.
    value: str
    token = COMMENT_TOKEN

    def _validate(self) -> None:
        if COMMENT_RE.fullmatch(self.value) is None:
            raise CSTValidationError(
                f"Got non-comment value for comment node: {repr(self.value)}"
            )

    def _tokengen_impl(self, state: TokengenState) -> None:
        state.add_token(self.token)


@add_slots
@dataclass(frozen=True)
class ShebangComment(Comment):
    token = SHEBANG_TOKEN


@add_slots
@dataclass(frozen=True)
class ShebangEnvComment(Comment):
    token = SHEBANG_ENV_TOKEN


@add_slots
@dataclass(frozen=True)
class ShebangPythonComment(Comment):
    token = SHEBANG_PYTHON_TOKEN


@add_slots
@dataclass(frozen=True)
class ShebangPython3Comment(Comment):
    token = SHEBANG_PYTHON3_TOKEN


@add_slots
@dataclass(frozen=True)
class EncodingComment(Comment):
    token = ENCODING_COMMENT_TOKEN


@add_slots
@dataclass(frozen=True)
class TrailingWhitespace(CSTNode):
    """
    The whitespace at the end of a line after a statement. If a line contains only
    whitespace, :class:`EmptyLine` should be used instead.
    """

    #: Any simple whitespace before any comment or newline.
    whitespace: SimpleWhitespace = SimpleWhitespace.field("")

    #: An optional comment appearing after any simple whitespace.
    comment: Optional[Comment] = None

    #: The newline character that terminates this trailing whitespace.
    newline: Newline = Newline.field()

    def _visit_and_replace_children(self, visitor: CSTVisitorT) -> "TrailingWhitespace":
        return TrailingWhitespace(
            whitespace=visit_required(self, "whitespace", self.whitespace, visitor),
            comment=visit_optional(self, "comment", self.comment, visitor),
            newline=visit_required(self, "newline", self.newline, visitor),
        )

    def _codegen_impl(self, state: CodegenState) -> None:
        self.whitespace._codegen(state)
        comment = self.comment
        if comment is not None:
            comment._codegen(state)
        self.newline._codegen(state)

    def _tokengen_impl(self, state: TokengenState) -> None:
        self.whitespace._tokengen(state)
        comment = self.comment
        if comment is not None:
            comment._tokengen(state)
        self.newline._tokengen(state)

    def _treegen_impl(self, state: TreegenState, parent: "Tree" = None) -> None:
        if self.comment is None and self.whitespace.empty and self.newline.value is None:
            return
        tree = state.add_tree(self.__class__.__name__, parent)
        self.whitespace._treegen(state, tree)
        comment = self.comment
        if comment is not None:
            comment._treegen(state, tree)
        self.newline._treegen(state, tree)


@add_slots
@dataclass(frozen=True)
class EmptyLine(CSTNode):
    """
    Represents a line with only whitespace/comments. Usually statements will own any
    :class:`EmptyLine` nodes above themselves, and a :class:`Module` will own the
    document's header/footer :class:`EmptyLine` nodes.
    """

    #: An empty line doesn't have to correspond to the current indentation level. For
    #: example, this happens when all trailing whitespace is stripped and there is
    #: an empty line between two statements.
    indent: bool = True

    #: Extra whitespace after the indent, but before the comment.
    whitespace: SimpleWhitespace = SimpleWhitespace.field("")

    #: An optional comment appearing after the indent and extra whitespace.
    comment: Optional[Comment] = None

    #: The newline character that terminates this empty line.
    newline: Newline = Newline.field()

    def _visit_and_replace_children(self, visitor: CSTVisitorT) -> "EmptyLine":
        return EmptyLine(
            indent=self.indent,
            whitespace=visit_required(self, "whitespace", self.whitespace, visitor),
            comment=visit_optional(self, "comment", self.comment, visitor),
            newline=visit_required(self, "newline", self.newline, visitor),
        )

    def _codegen_impl(self, state: CodegenState) -> None:
        if self.indent:
            state.add_indent_tokens()
        self.whitespace._codegen(state)
        comment = self.comment
        if comment is not None:
            comment._codegen(state)
        self.newline._codegen(state)

    def _tokengen_impl(self, state: TokengenState) -> None:
        if self.indent:
            state.add_indent_tokens()
        self.whitespace._tokengen(state)
        comment = self.comment
        if comment is not None:
            comment._tokengen(state)
        self.newline._tokengen(state)


    def _treegen_impl(self, state: TreegenState, parent: "Tree" = None) -> None:
        if self.comment is None and self.whitespace.empty and self.newline.value is None:
            return
        tree = state.add_tree(self.__class__.__name__, parent)
        self.whitespace._treegen(state, tree)
        comment = self.comment
        if comment is not None:
            comment._treegen(state, tree)
        self.newline._treegen(state, tree)


@add_slots
@dataclass(frozen=True)
class ParenthesizedWhitespace(BaseParenthesizableWhitespace):
    """
    This is the kind of whitespace you might see inside a parenthesized expression
    or statement between two tokens when there is a newline without a line
    continuation (``\\``) character.

    https://docs.python.org/3/reference/lexical_analysis.html#implicit-line-joining

    A parenthesized whitespace cannot be empty since it requires at least one
    :class:`TrailingWhitespace`. If you have whitespace that does not contain
    comments or newlines, use :class:`SimpleWhitespace` instead.
    """

    #: The whitespace that comes after the previous node, up to and including
    #: the end-of-line comment and newline.
    first_line: TrailingWhitespace = TrailingWhitespace.field()

    #: Any lines after the first that contain only indentation and/or comments.
    empty_lines: Sequence[EmptyLine] = ()

    #: Whether or not the final simple whitespace is indented regularly.
    indent: bool = False

    #: Extra whitespace after the indent, but before the next node.
    last_line: SimpleWhitespace = SimpleWhitespace.field("")

    def _visit_and_replace_children(
        self, visitor: CSTVisitorT
    ) -> "ParenthesizedWhitespace":
        return ParenthesizedWhitespace(
            first_line=visit_required(self, "first_line", self.first_line, visitor),
            empty_lines=visit_sequence(self, "empty_lines", self.empty_lines, visitor),
            indent=self.indent,
            last_line=visit_required(self, "last_line", self.last_line, visitor),
        )

    def _codegen_impl(self, state: CodegenState) -> None:
        self.first_line._codegen(state)
        for line in self.empty_lines:
            line._codegen(state)
        if self.indent:
            state.add_indent_tokens()
        self.last_line._codegen(state)

    def _tokengen_impl(self, state: TokengenState) -> None:
        self.first_line._tokengen(state)
        for line in self.empty_lines:
            line._tokengen(state)
        if self.indent:
            state.add_indent_tokens()
        self.last_line._tokengen(state)

    def _treegen_impl(self, state: TreegenState, parent: "Tree" = None) -> None:
        tree = state.add_tree(self.__class__.__name__, parent)
        self.first_line._treegen(state, tree)
        for line in self.empty_lines:
            line._treegen(state, tree)
        self.last_line._treegen(state, tree)

    @property
    def empty(self) -> bool:
        """
        Indicates that this node is empty (zero whitespace characters). For
        :class:`ParenthesizedWhitespace` this will always be ``False``.
        """

        # Its not possible to have a ParenthesizedWhitespace with zero characers.
        # If we did, the TrailingWhitespace would not have parsed.
        return False
