from typing import Iterator
from typing import List

from learning_to_style_check import libcst


def iter_tree(tree: libcst.CSTNode) -> Iterator[libcst.CSTNode]:
    """
    Iterate over all nodes in a tree, in depth-first order.
    """
    yield tree
    for child in tree.children:
        yield from iter_tree(child)


def get_node_names(file_path: str) -> List[str]:
    class_names = []

    with open(file_path, "r") as f:
        lines = f.read().split("\n")
        for i, l in enumerate(lines):
            if l.startswith("class") and "@dataclass" in lines[i - 1]:
                name = l.split(" ")[1].split("(")[0]
                class_names.append(name)

    return class_names
