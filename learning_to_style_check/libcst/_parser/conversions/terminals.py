# Copyright (c) Meta Platforms, Inc. and affiliates.
#
# This source code is licensed under the MIT license found in the
# LICENSE file in the root directory of this source tree.
# pyre-unsafe
import re
from typing import Any

from learning_to_style_check.libcst._nodes.expression import TripleDoubleQuotesString, DoubleQuotesString, \
    TripleSingleQuotesString, SingleQuotesString, RawLowStringPrefix, RawUpStringPrefix, \
    UnicodeLowStringPrefix, UnicodeUpStringPrefix, ByteLowStringPrefix, ByteUpStringPrefix, SimpleString
from learning_to_style_check.libcst._parser.types.config import ParserConfig
from learning_to_style_check.libcst._parser.types.partials import WithLeadingWhitespace
from learning_to_style_check.libcst._parser.types.token import Token
from learning_to_style_check.libcst._parser.whitespace_parser import (
    parse_empty_lines,
    parse_trailing_whitespace,
)


def convert_NAME(config: ParserConfig, token: Token) -> Any:
    return token


def convert_NUMBER(config: ParserConfig, token: Token) -> Any:
    return token


def convert_STRING(config: ParserConfig, token: Token) -> Any:
    prefixes = []
    for c in token.string:
        if c == "r":
            prefixes.append(RawLowStringPrefix())
        elif c == "R":
            prefixes.append(RawUpStringPrefix())
        elif c == "u":
            prefixes.append(UnicodeLowStringPrefix())
        elif c == "U":
            prefixes.append(UnicodeUpStringPrefix())
        elif c == "b":
            prefixes.append(ByteLowStringPrefix())
        elif c == "B":
            prefixes.append(ByteUpStringPrefix())
        else:
            break

    value_str = token.string[len(prefixes):]
    if re.fullmatch(r'^""".*"""$', value_str, flags=re.MULTILINE | re.DOTALL):
        value_node = TripleDoubleQuotesString(token.string)
    elif re.fullmatch(r'^".*"$', value_str, flags=re.MULTILINE | re.DOTALL):
        value_node = DoubleQuotesString(token.string)
    elif re.fullmatch(r"^'''.*'''$", value_str, flags=re.MULTILINE | re.DOTALL):
        value_node = TripleSingleQuotesString(token.string)
    elif re.fullmatch(r"^'.*'$", value_str, flags=re.MULTILINE | re.DOTALL):
        value_node = SingleQuotesString(token.string)

    else:
        raise ValueError(f"Unexpected string: {token.string!r}")

    return WithLeadingWhitespace(SimpleString(value_node=value_node, prefixes=prefixes),
                                 token.whitespace_before)


def convert_OP(config: ParserConfig, token: Token) -> Any:
    return token


def convert_NEWLINE(config: ParserConfig, token: Token) -> Any:
    # A NEWLINE token is only emitted for semantic newlines, which means that this
    # corresponds to a TrailingWhitespace, since that's the only semantic
    # newline-containing node.

    # N.B. Because this token is whitespace, and because the whitespace parser doesn't
    # try to prevent overflows, `token.whitespace_before` will end up overflowing into
    # the value of this newline token, so `parse_trailing_whitespace` will include
    # token.string's value. This is expected and desired behavior.
    return parse_trailing_whitespace(config, token.whitespace_before)


def convert_INDENT(config: ParserConfig, token: Token) -> Any:
    return token


def convert_DEDENT(config: ParserConfig, token: Token) -> Any:
    return token


def convert_ENDMARKER(config: ParserConfig, token: Token) -> Any:
    # Parse any and all empty lines with an indent similar to the header. That is,
    # indent of nothing and including all indents. In some cases, like when the
    # footer parser follows an indented suite, the state's indent can be wrong
    # due to the fact that it is shared with the _DEDENT node. We know that if
    # we're parsing the end of a file, we will have no indent.
    return parse_empty_lines(
        config, token.whitespace_before, override_absolute_indent=""
    )


def convert_FSTRING_START(config: ParserConfig, token: Token) -> Any:
    return token


def convert_FSTRING_END(config: ParserConfig, token: Token) -> Any:
    return token


def convert_FSTRING_STRING(config: ParserConfig, token: Token) -> Any:
    return token


def convert_ASYNC(config: ParserConfig, token: Token) -> Any:
    return token


def convert_AWAIT(config: ParserConfig, token: Token) -> Any:
    return token
