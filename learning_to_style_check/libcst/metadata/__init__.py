# Copyright (c) Meta Platforms, Inc. and affiliates.
#
# This source code is licensed under the MIT license found in the
# LICENSE file in the root directory of this source tree.


from learning_to_style_check.libcst._position import CodePosition, CodeRange
from learning_to_style_check.libcst.metadata.accessor_provider import AccessorProvider
from learning_to_style_check.libcst.metadata.base_provider import (
    BaseMetadataProvider,
    BatchableMetadataProvider,
    ProviderT,
    VisitorMetadataProvider,
)
from learning_to_style_check.libcst.metadata.expression_context_provider import (
    ExpressionContext,
    ExpressionContextProvider,
)
from learning_to_style_check.libcst.metadata.file_path_provider import FilePathProvider
from learning_to_style_check.libcst.metadata.full_repo_manager import FullRepoManager
from learning_to_style_check.libcst.metadata.name_provider import (
    FullyQualifiedNameProvider,
    QualifiedNameProvider,
)
from learning_to_style_check.libcst.metadata.parent_node_provider import ParentNodeProvider
from learning_to_style_check.libcst.metadata.position_provider import (
    PositionProvider,
    WhitespaceInclusivePositionProvider,
)
from learning_to_style_check.libcst.metadata.reentrant_codegen import (
    CodegenPartial,
    ExperimentalReentrantCodegenProvider,
)
from learning_to_style_check.libcst.metadata.scope_provider import (
    Access,
    Accesses,
    Assignment,
    Assignments,
    BaseAssignment,
    BuiltinAssignment,
    BuiltinScope,
    ClassScope,
    ComprehensionScope,
    FunctionScope,
    GlobalScope,
    ImportAssignment,
    QualifiedName,
    QualifiedNameSource,
    Scope,
    ScopeProvider,
)
from learning_to_style_check.libcst.metadata.span_provider import ByteSpanPositionProvider, CodeSpan
from learning_to_style_check.libcst.metadata.type_inference_provider import TypeInferenceProvider
from learning_to_style_check.libcst.metadata.wrapper import MetadataWrapper

__all__ = [
    "CodePosition",
    "CodeRange",
    "CodeSpan",
    "WhitespaceInclusivePositionProvider",
    "PositionProvider",
    "ByteSpanPositionProvider",
    "BaseMetadataProvider",
    "ExpressionContext",
    "ExpressionContextProvider",
    "BaseAssignment",
    "Assignment",
    "BuiltinAssignment",
    "ImportAssignment",
    "BuiltinScope",
    "Access",
    "Scope",
    "GlobalScope",
    "FunctionScope",
    "ClassScope",
    "ComprehensionScope",
    "ScopeProvider",
    "ParentNodeProvider",
    "QualifiedName",
    "QualifiedNameSource",
    "MetadataWrapper",
    "BatchableMetadataProvider",
    "VisitorMetadataProvider",
    "QualifiedNameProvider",
    "FullyQualifiedNameProvider",
    "ProviderT",
    "Assignments",
    "Accesses",
    "TypeInferenceProvider",
    "FullRepoManager",
    "AccessorProvider",
    "FilePathProvider",
    # Experimental APIs:
    "ExperimentalReentrantCodegenProvider",
    "CodegenPartial",
]
