from multiprocessing import Pool

import numpy as np
from scipy.spatial.distance import cosine


class BaselineCosineSimClassifier:
    # proportional vectors cos sim = 1
    # orthogonal vectors: cos sim = 0
    # opposite vectors: cos sim = -1

    def __init__(self, threshold=None, n_jobs=None):
        self.threshold = threshold
        self.inverted = False
        self.n_jobs = n_jobs

    def _calc_cosine_sim(self, X1, X2):
        with Pool(self.n_jobs) as p:
            return np.array(list(p.starmap(cosine, zip(X1, X2))))

    def fit(self, codes, style_contexts, y):
        # find the best threshold where althe most values are correctly classified
        similarities = self._calc_cosine_sim(codes, style_contexts)

        negative_average = np.average(similarities[np.where(y == 0)])
        positive_average = np.average(similarities[np.where(y == 1)])
        if negative_average > positive_average:
            self.inverted = True
        self.threshold = (negative_average + positive_average) / 2

    def predict(self, codes, style_contexts):
        if self.threshold is None:
            raise Exception(
                "cosine similarity threshold not defined, please fit or specify a threshold first the classifier first")

        similarities = self._calc_cosine_sim(codes, style_contexts)

        lower_indexs = np.where(similarities <= self.threshold)
        higher_indexs = np.where(similarities > self.threshold)

        scaled_lower_similarities = similarities[lower_indexs] / 2 / self.threshold
        scaled_higher_similarities = (similarities[higher_indexs] - self.threshold) / 2 / (1 - self.threshold)

        if self.inverted:
            similarities[higher_indexs] = scaled_higher_similarities
            similarities[lower_indexs] = scaled_lower_similarities + 0.5
        else:
            similarities[lower_indexs] = scaled_lower_similarities
            similarities[higher_indexs] = scaled_higher_similarities + 0.5

        return similarities
