import logging

import dgl
import dgl.nn.pytorch as dglnn
import torch
import torch.nn as nn
from dgl import DGLGraph
from transformers import PreTrainedModel

from learning_to_style_check.model.gcn.config import CodestyleValidatorGCNConfig
from learning_to_style_check.model.output import CodestyleValidatorOutput

logger = logging.getLogger(__name__)


class CodestyleValidatorGCN(PreTrainedModel):
    config_class = CodestyleValidatorGCNConfig

    def __init__(self, config: CodestyleValidatorGCNConfig):
        super(CodestyleValidatorGCN, self).__init__(config)

        self.config = config

        self.loss_fct = nn.BCELoss()

        self.embedding = nn.Embedding(config.num_vocabs, config.embedding_size)
        self.dropout = nn.Dropout(config.dropout)

        self.layers_code = dglnn.Sequential(
            dglnn.GraphConv(config.embedding_size, config.conv_out_size, activation=nn.ReLU()),
        )
        self.layers_style = dglnn.Sequential(
            dglnn.GraphConv(config.embedding_size, config.conv_out_size, activation=nn.ReLU()),
        )

        self.out_layer = nn.Sequential(
            nn.Linear(2 * config.conv_out_size, 300),
            nn.ReLU(),
            nn.Linear(300, 1),
            nn.Sigmoid()
        )

    def forward(self,
                graph_code: DGLGraph,
                graph_style: DGLGraph,
                label: torch.Tensor = None,
                **kwargs):
        h_code = self.dropout(self.embedding(graph_code.ndata["class"]))
        graph_code.ndata["h"] = self.layers_code(graph_code, h_code)

        h_style = self.dropout(self.embedding(graph_style.ndata["class"]))
        graph_style.ndata["h"] = self.layers_style(graph_style, h_style)

        h_code = dgl.mean_nodes(graph_code, "h")
        h_style = dgl.mean_nodes(graph_style, "h")

        h = torch.cat([h_code, h_style], dim=-1)
        pred = self.out_layer(h).view(-1)

        loss = None
        if label is not None:
            loss = self.loss_fct(pred, label)

        return CodestyleValidatorOutput(pred=pred, loss=loss)
