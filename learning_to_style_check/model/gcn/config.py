from transformers import PretrainedConfig


class CodestyleValidatorGCNConfig(PretrainedConfig):
    model_type = "gcn"

    def __init__(self,
                 num_vocabs: int = 221,
                 embedding_size: int = 300,
                 dropout: float = 0,
                 conv_out_size: int = 200,
                 **kwargs):
        self.num_vocabs = num_vocabs
        self.embedding_size = embedding_size
        self.dropout = dropout
        self.conv_out_size = conv_out_size

        super(CodestyleValidatorGCNConfig, self).__init__(**kwargs)
