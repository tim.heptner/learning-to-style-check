from transformers import PretrainedConfig


class CodestyleValidatorTreeLSTMConfig(PretrainedConfig):
    model_type = "tree_lstm"

    def __init__(self,
                 num_vocabs: int = 221,
                 embedding_size: int = 300,
                 dropout: float = 0,
                 lstm_h_size: int = 150,
                 **kwargs: object) -> object:
        self.num_vocabs = num_vocabs
        self.embedding_size = embedding_size
        self.dropout = dropout
        self.lstm_h_size = lstm_h_size

        super(CodestyleValidatorTreeLSTMConfig, self).__init__(**kwargs)
