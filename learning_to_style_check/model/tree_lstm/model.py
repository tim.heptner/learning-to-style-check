"""
Tree LSTM model based on the following paper:
Kai Sheng Tai, Richard Socher, and Christopher D. Manning. Improved Semantic Representations From Tree-Structured Long Short-Term Memory Networks
https://arxiv.org/abs/1503.00075

The implementation is based on the following example code fom the dgl library:
https://github.com/dmlc/dgl/blob/0c3b2b780f7ea6318ead8fcbe23e973467e4ca01/examples/pytorch/tree_lstm/tree_lstm.py

The following changes were made:

1. The model is now huggingface compatible model
2. The model gets two input trees (one for the code to check and one for the code which is used as codestyle reference)
3. There are two tree LSTM cells, one for each input tree
4. The last hidden state of the two tree LSTM cells (e.g. the root note) are concatenated and feed into a linear layer
"""

import logging

import dgl
import torch
import torch.nn as nn
from dgl import DGLGraph
from transformers import PreTrainedModel

from learning_to_style_check.model.output import CodestyleValidatorOutput
from learning_to_style_check.model.tree_lstm.config import CodestyleValidatorTreeLSTMConfig

logger = logging.getLogger(__name__)


class TreeLSTMCell(nn.Module):

    def __init__(self, x_size, h_size):
        super(TreeLSTMCell, self).__init__()
        self.W_iou = nn.Linear(x_size, 3 * h_size, bias=False)
        self.U_iou = nn.Linear(h_size, 3 * h_size, bias=False)
        self.b_iou = nn.Parameter(torch.zeros(1, 3 * h_size))
        self.U_f = nn.Linear(h_size, h_size)

    def message_func(self, edges):
        return {"h": edges.src["h"], "c": edges.src["c"]}

    def reduce_func(self, nodes):
        h_tild = torch.sum(nodes.mailbox["h"], 1)
        f = torch.sigmoid(self.U_f(nodes.mailbox["h"]))
        c = torch.sum(f * nodes.mailbox["c"], 1)
        return {"iou": self.U_iou(h_tild), "c": c}

    def apply_node_func(self, nodes):
        iou = nodes.data["iou"] + self.b_iou
        i, o, u = torch.chunk(iou, 3, 1)
        i, o, u = torch.sigmoid(i), torch.sigmoid(o), torch.tanh(u)
        c = i * u + nodes.data["c"]
        h = o * torch.tanh(c)
        return {"h": h, "c": c}


class CodestyleValidatorTreeLSTM(PreTrainedModel):
    config_class = CodestyleValidatorTreeLSTMConfig

    def __init__(self, config: CodestyleValidatorTreeLSTMConfig):
        super(CodestyleValidatorTreeLSTM, self).__init__(config)

        self.config = config

        self.loss_fct = nn.BCELoss()

        self.embedding = nn.Embedding(config.num_vocabs, config.embedding_size)
        self.dropout = nn.Dropout(config.dropout)

        self.tree_lstm_cell_code = TreeLSTMCell(config.embedding_size, config.lstm_h_size)

        self.tree_lstm_cell_Style = TreeLSTMCell(config.embedding_size, config.lstm_h_size)

        self.out_layer = nn.Sequential(
            nn.Linear(2 * config.lstm_h_size, 300),
            nn.ReLU(),
            nn.Linear(300, 1),
            nn.Sigmoid()
        )

    def _propagate(self, graph: DGLGraph, cell: TreeLSTMCell):
        device = graph.device

        graph.ndata["c"] = torch.zeros((graph.number_of_nodes(), self.config.lstm_h_size)).to(device)
        graph.ndata["h"] = torch.zeros((graph.number_of_nodes(), self.config.lstm_h_size)).to(device)

        embeddings = self.embedding(graph.ndata["class"])
        graph.ndata["iou"] = cell.W_iou(
            self.dropout(embeddings)
        )

        dgl.prop_nodes_topo(
            graph,
            cell.message_func,
            cell.reduce_func,
            apply_node_func=cell.apply_node_func,
        )

    def forward(self,
                graph_code: DGLGraph,
                graph_style: DGLGraph,
                label=None,
                **kwargs):
        self._propagate(graph_code, self.tree_lstm_cell_code)
        self._propagate(graph_style, self.tree_lstm_cell_Style)

        # get root node hidden state
        h_root_code = torch.stack([g.ndata["h"][0] for g in dgl.unbatch(graph_code)])
        h_root_style = torch.stack([g.ndata["h"][0] for g in dgl.unbatch(graph_style)])

        h = torch.cat([h_root_code, h_root_style], dim=1)
        pred = self.out_layer(h).view(-1)

        loss = None
        if label is not None:
            loss = self.loss_fct(pred, label)

        return CodestyleValidatorOutput(pred=pred, loss=loss)
