from dataclasses import dataclass
from typing import Optional

import torch
from transformers.utils import ModelOutput


@dataclass
class CodestyleValidatorOutput(ModelOutput):
    """
    Base class for outputs of codestyle classification models.

    Args:
        loss (`torch.FloatTensor` of shape `(1,)`, *optional*, returned when `labels` is provided):
            Classification loss.
        pred (`torch.FloatTensor` of shape `(batch_size, config.num_labels)`):
            Classification probabilities (after sigmoid).
    """

    loss: Optional[torch.FloatTensor] = None
    pred: torch.FloatTensor = None
