from transformers import PretrainedConfig


class CodestyleValidatorTreeLSTMv2Config(PretrainedConfig):
    model_type = "tree_lstm"

    def __init__(self,
                 num_vocabs: int = 221,
                 embedding_size: int = 300,
                 dropout: float = 0,
                 lstm_h_size: int = 150,
                 set2set_n_iters: int = 6,
                 set2set_n_layers: int = 3,
                 **kwargs):
        self.num_vocabs = num_vocabs
        self.embedding_size = embedding_size
        self.dropout = dropout
        self.lstm_h_size = lstm_h_size
        self.set2set_n_iters = set2set_n_iters
        self.set2set_n_layers = set2set_n_layers

        super(CodestyleValidatorTreeLSTMv2Config, self).__init__(**kwargs)
