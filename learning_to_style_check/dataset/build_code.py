import argparse
import copy
import io
import json
import logging
import os
import random
import time
from itertools import repeat, cycle
from math import ceil
from multiprocessing import Pool
from pathlib import Path
from typing import Union

import numpy as np
from datasets import DatasetDict, Dataset
from huggingface_hub import HfApi
from tqdm import tqdm

from learning_to_style_check.codestyle import apply_codestyle, CodestyleConfig, get_random_codestyle
from learning_to_style_check.codestyle.codestyle import mutate_codestyle, CodestyleConfigJSONEncoder

PUSH_RETRIES = 60


def _apply_codestyle_f(file_path: Union[str, Path], codestyle: CodestyleConfig) -> str:
    with open(file_path, "r") as f:
        code = f.read()

    try:
        mew_code = apply_codestyle(code, codestyle)
    except Exception as e:
        logging.error(f"Failed to apply codestyle to {file_path}")
        raise e
    return mew_code


def create_dataset(files,
                   codestyles,
                   files_per_codestyle,
                   unique_style_contexts,
                   negative_sample_type,
                   n_mutated_rules,
                   cpu_count):
    dataset_codes = []
    dataset_code_codestyles = []

    dataset_style_context = []
    dataset_style_context_codestyles = []

    dataset_labels = []

    additional_data = {"codestyles": copy.deepcopy(codestyles)}

    all_styled_codes = []

    with Pool(cpu_count) as p:
        for i, codestyle in tqdm(enumerate(codestyles), total=len(codestyles), desc="Applying codestyle",
                                 unit="codestyle"):
            # apply codestyle
            selected_files = random.sample(files, int(len(files) * files_per_codestyle))
            codes = p.starmap(_apply_codestyle_f, zip(selected_files, repeat(codestyle)))
            all_styled_codes.append([text for text in codes if text])

        for i, styled_codes in tqdm(enumerate(all_styled_codes), total=len(all_styled_codes),
                                    desc="Building positive and negative samples", unit="codestyle"):
            # pick randomly M items from this codestyle as style contexts
            style_contexts = random.sample(styled_codes, k=int(len(styled_codes) * unique_style_contexts))

            # positive samples:
            for code, style_context in zip(styled_codes, cycle(style_contexts)):
                dataset_codes.append(code)
                dataset_code_codestyles.append(i)

                dataset_style_context.append(style_context)
                dataset_style_context_codestyles.append(i)

                dataset_labels.append(1)

            # negative samples:
            if negative_sample_type == "random":
                negative_codes = []
                code_codestyle_idxs = []
                # randomly picked from other codestyles which are already styled with different style
                for codestyle_idx, other_styled_codes in zip(list(range(i)) + list(range(i + 1, len(codestyles))),
                                                             all_styled_codes[:i] + all_styled_codes[i + 1:]):
                    picked_codes = random.sample(other_styled_codes,
                                                 k=max(1, ceil(len(styled_codes) / 2 / (len(codestyles) - 1))))
                    negative_codes.extend(picked_codes)
                    code_codestyle_idxs.extend([codestyle_idx] * len(picked_codes))

                    if len(negative_codes) > len(styled_codes) // 2:
                        negative_codes = negative_codes[:len(styled_codes) // 2]
                        code_codestyle_idxs = code_codestyle_idxs[:len(styled_codes) // 2]
                        break

                # fill up with randomly picked code from this codestyle
                picked_codes = random.sample(styled_codes, k=len(styled_codes) - len(negative_codes))
                negative_codestyles_idxs = random.sample(list(range(i)) + list(range(i + 1, len(codestyles))),
                                                         k=min(len(codestyles) - 1, len(picked_codes) // 10))
                code_codestyle_idxs.extend([idx for _, idx in zip(picked_codes, cycle(negative_codestyles_idxs))])
                negative_codestyles = [codestyles[idx] for idx in negative_codestyles_idxs]
                negative_codes.extend(p.starmap(apply_codestyle, zip(picked_codes, cycle(negative_codestyles))))

                samples = set()
                for code, code_codestyle_idx, style_context in zip(negative_codes, code_codestyle_idxs,
                                                                   cycle(style_contexts)):
                    # check if we already have this sample
                    if (code, style_context) in samples or code == "" or style_context == "":
                        continue
                    else:
                        samples.add((code, style_context))

                    dataset_codes.append(code)
                    dataset_code_codestyles.append(code_codestyle_idx)

                    dataset_style_context.append(style_context)
                    dataset_style_context_codestyles.append(i)

                    dataset_labels.append(0)
            elif negative_sample_type == "single":
                mutated_codestyles = [mutate_codestyle(copy.deepcopy(codestyles[i]), n_mutated_rules) for _ in
                                      range(min(len(codestyles), len(styled_codes) // 10))]
                additional_data["codestyles"].extend(mutated_codestyles)

                negative_codes = []

                # randomly pick from all codestyles except the current one
                for other_styled_codes in all_styled_codes[:i] + all_styled_codes[i + 1:]:
                    negative_codes.extend(random.sample(other_styled_codes,
                                                        k=max(1, ceil(len(styled_codes) / 2 / (len(codestyles) - 1)))))
                    if len(negative_codes) > len(styled_codes) // 2:
                        negative_codes = negative_codes[:len(styled_codes) // 2]
                        break

                # fill up with randomly picked code from this codestyle
                negative_codes.extend(random.sample(styled_codes, k=len(styled_codes) - len(negative_codes)))

                negative_codes = p.starmap(apply_codestyle, zip(negative_codes, cycle(mutated_codestyles)))

                samples = set()
                for code, code_mutated_codestyle_idx, style_context in zip(negative_codes,
                                                                           cycle(range(len(mutated_codestyles))),
                                                                           cycle(style_contexts)):
                    # check if we already have this sample
                    if (code, style_context) in samples or code == "" or style_context == "":
                        continue
                    else:
                        samples.add((code, style_context))

                    dataset_codes.append(code)
                    dataset_code_codestyles.append(len(codestyles) + code_mutated_codestyle_idx)

                    dataset_style_context.append(style_context)
                    dataset_style_context_codestyles.append(i)

                    dataset_labels.append(0)

    return Dataset.from_dict({
        "code": dataset_codes,
        "code_codestyle": dataset_code_codestyles,
        "style_context": dataset_style_context,
        "style_context_codestyle": dataset_style_context_codestyles,
        "label": dataset_labels
    }), additional_data


def build_code_dataset(path: str,
                       dataset_name: str,
                       n_codestyles: int,
                       negative_sample_type: str,
                       n_mutated_rules: int,
                       allowed_suffixes: list,
                       train_test_split: float,
                       files_per_codestyle: float,
                       unique_style_contexts: float,
                       min_code_len: int,
                       max_code_len: int,
                       save_to_disk: bool,
                       push: bool,
                       public: bool,
                       cpu_count: int = os.cpu_count(),
                       seed: int = 42) -> None:
    random.seed(seed)

    source_root_path = Path(path)

    files = []
    for suffix in allowed_suffixes:
        files.extend(list(source_root_path.rglob(f"*{suffix}")))
    # filter out files that are too short or too long
    filtered_files = []
    for file in files:
        with open(file, "r") as f:
            code = f.read()
        if min_code_len <= len(code) <= max_code_len:
            filtered_files.append(file)
    files = filtered_files

    print(f"Found {len(files)} files")

    # shuffle files
    random.shuffle(files)

    # generate n unique random full codestyles
    random_codestyles = set()
    while len(random_codestyles) < n_codestyles:
        random_codestyles.add(get_random_codestyle())
    random_codestyles = list(random_codestyles)

    print("Creating train dataset")
    train_codestyles = random_codestyles[:int(len(random_codestyles) * train_test_split)]
    train_files = files[:int(len(files) * train_test_split)]
    train_dataset, additional_data_train = create_dataset(train_files,
                                                          train_codestyles,
                                                          files_per_codestyle,
                                                          unique_style_contexts,
                                                          negative_sample_type,
                                                          n_mutated_rules,
                                                          cpu_count)

    print("Creating test dataset")
    test_codestyles = random_codestyles[int(len(random_codestyles) * train_test_split):]
    test_files = files[int(len(files) * train_test_split):]
    test_dataset, additional_data_test = create_dataset(test_files,
                                                        test_codestyles,
                                                        files_per_codestyle,
                                                        unique_style_contexts,
                                                        negative_sample_type,
                                                        n_mutated_rules,
                                                        cpu_count)

    dataset = DatasetDict({
        "train": train_dataset,
        "test": test_dataset
    })

    print(dataset)
    print("train label distribution", np.unique(dataset["train"]["label"], return_counts=True))
    print("test label distribution", np.unique(dataset["test"]["label"], return_counts=True))

    if save_to_disk:
        dataset.save_to_disk(dataset_name)
        with open(Path(dataset_name) / "additional_data.json", "w") as f:
            json.dump({"train": additional_data_train, "test": additional_data_test},
                      f, cls=CodestyleConfigJSONEncoder)
    if push:
        retries = 0
        while retries < PUSH_RETRIES:
            try:
                dataset.push_to_hub(dataset_name, private=not public)
                break
            except Exception as e:
                logging.error(f"Failed to push to hub, reason: {e}")
                retries += 1
                time.sleep(10 * retries)

        temp_io = io.BytesIO()
        text_io = io.TextIOWrapper(temp_io)
        json.dump({"train": additional_data_train, "test": additional_data_test},
                  text_io, cls=CodestyleConfigJSONEncoder)

        text_io.seek(0)

        api = HfApi()
        retries = 0
        while retries < PUSH_RETRIES:
            try:
                api.upload_file(
                    repo_id=dataset_name,
                    repo_type="dataset",
                    path_or_fileobj=temp_io,
                    path_in_repo="additional_data.json",
                    commit_message="Add additional data"
                )
                break
            except Exception as e:
                logging.error(f"Failed to push additional data to hub, reason: {e}")
                retries += 1
                time.sleep(10 * retries)

        temp_io.close()


if __name__ == "__main__":
    parser = argparse.ArgumentParser("Create a codestyle code dataset from a directory with source codes")
    parser.add_argument("path", type=str, help="Path source code root directory")
    parser.add_argument("dataset_name", type=str, help="Name of the dataset on huggingface")
    parser.add_argument("n_codestyles", type=int, help="Number of codestyles to generate")
    parser.add_argument("negative_sample_type", choices=["random", "single"],
                        help="Type of negative sample to generate. If random, the negative sample have a fully distinct"
                             " codestyle. If single, the negative sample only has a single codestyle rule different"
                             " from the positive sample.")

    parser.add_argument("--cpu-count", type=int, default=os.cpu_count(), help="Number of cpus to use")
    parser.add_argument("--seed", type=int, default=42, help="Random seed")

    parser.add_argument("--n-mutated-rules", type=int, default=1,
                        help="Number of rules to mute for the 'single' negative sample type")

    parser.add_argument("--min-code-len", type=int, default=150, help="Minimum length of a code sample")
    parser.add_argument("--max-code-len", type=int, default=20000, help="Maximum length of a code sample")

    parser.add_argument("--allowed-suffixes", nargs="+", help="allowed file suffixes", default=[".py"])
    parser.add_argument("--train-test-split", type=float, default=0.7,
                        help="ratio of train/test split of codestyles")

    parser.add_argument("--files-per-codestyle", type=float, default=0.2,
                        help="number of files per codestyle")
    parser.add_argument("--unique-style-contexts", type=float, default=0.5,
                        help="Number of unique codestyle per codestyle")

    parser.add_argument("--push", action="store_true", help="push the the dataset to hugginface", default=False)
    parser.add_argument("--save-to-disk", action="store_true", help="save the dataset to disk", default=False)
    parser.add_argument("--public", action="store_true", help="push the the dataset with public access",
                        default=False)

    args = parser.parse_args()

    build_code_dataset(**vars(args))
