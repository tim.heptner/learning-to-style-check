import argparse
import logging
import os
import time

from datasets import load_dataset, load_from_disk

from learning_to_style_check.features import get_style_tree, convert_style_tree_to_dgl_graph

PUSH_RETRIES = 60


def build_graph_dataset(code_dataset_name: str,
                        graph_dataset_name: str,
                        cpu_count: int,
                        from_disk: bool,
                        save_to_disk: bool,
                        push: bool,
                        public: bool,
                        dtype: str) -> None:
    """
    Build a dataset with graphs from a dataset with code.

    :param code_dataset_name: name of the code dataset
    :param graph_dataset_name: name of the graph dataset
    :param cpu_count: number of cpus to use
    :param save_to_disk: save the dataset to disk
    :param push: push the dataset to huggingface
    :param public: push the dataset with public access
    :param branch: branch to push to
    :dtype: data type of the dataset (e.g. int32, int64)
    """

    if from_disk:
        code_dataset = load_from_disk(code_dataset_name)
    else:
        code_dataset = load_dataset(code_dataset_name)

    def build_graphs(samples):
        code_edges = []
        code_node_classes = []

        style_context_edges = []
        style_context_node_classes = []

        for code, style_context in zip(samples["code"], samples["style_context"]):
            code_tree = get_style_tree(code)
            code_graph = convert_style_tree_to_dgl_graph(code_tree)
            if dtype == "int32":
                code_graph = code_graph.int()
            code_edges.append(code_graph.edges(form="uv"))
            code_node_classes.append(code_graph.ndata["class"])

            style_context_tree = get_style_tree(style_context)
            style_context_graph = convert_style_tree_to_dgl_graph(style_context_tree)
            if dtype == "int32":
                style_context_graph = style_context_graph.int()
            style_context_edges.append(style_context_graph.edges(form="uv"))
            style_context_node_classes.append(style_context_graph.ndata["class"])

        return {
            "code_edges": code_edges,
            "code_node_classes": code_node_classes,

            "style_context_edges": style_context_edges,
            "style_context_node_classes": style_context_node_classes,
        }

    graph_dataset = code_dataset.map(build_graphs,
                                     batched=True,
                                     batch_size=200,
                                     num_proc=cpu_count,
                                     remove_columns=["code", "style_context"])

    if save_to_disk:
        graph_dataset.save_to_disk(graph_dataset_name)
    if push:
        retries = 0
        while retries < PUSH_RETRIES:
            try:
                graph_dataset.push_to_hub(graph_dataset_name, private=not public)
                break
            except Exception as e:
                logging.error(f"Failed to push to hub, reason: {e}")
                retries += 1
                time.sleep(10 * retries)


if __name__ == "__main__":
    parser = argparse.ArgumentParser("Convert a code codestyle dataset to a graph dataset")
    parser.add_argument("code_dataset_name", type=str, help="Name of the code dataset")
    parser.add_argument("graph_dataset_name", type=str, help="Name of the graph dataset")

    parser.add_argument("--cpu-count", type=int, help="Number of cpus to use", default=os.cpu_count())
    parser.add_argument("--from-disk", action="store_true", help="load the dataset from disk", default=False)

    parser.add_argument("--save-to-disk", action="store_true", help="save the dataset to disk", default=False)
    parser.add_argument("--push", action="store_true", help="push the the dataset to huggingface", default=False)
    parser.add_argument("--public", action="store_true", help="push the the dataset with public access",
                        default=False)

    parser.add_argument("--dtype", choices=["int32", "int64"], help="data type of the dataset", default="int32")

    args = parser.parse_args()

    build_graph_dataset(**vars(args))
