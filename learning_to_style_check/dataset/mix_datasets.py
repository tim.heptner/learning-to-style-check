import sys

from datasets import load_dataset, interleave_datasets, DatasetDict

if len(sys.argv) != 3:
    raise Exception("missing dataset size or username argument")

dataset_size = sys.argv[1]
username = sys.argv[2]

dataset_random = load_dataset(f"{username}/python_codestyles-random-" + dataset_size).shuffle(42)
dataset_single = load_dataset(f"{username}/python_codestyles-single-" + dataset_size).shuffle(42)

dataset_random_1_train = dataset_random["train"].shard(num_shards=2, index=0)
dataset_single_1_train = dataset_single["train"].shard(num_shards=2, index=0)

train_dataset_1 = interleave_datasets([
    dataset_random_1_train,
    dataset_single_1_train
], seed=42)

dataset_random_1_test = dataset_random["test"].shard(num_shards=2, index=0)
dataset_single_1_test = dataset_single["test"].shard(num_shards=2, index=0)

test_dataset_1 = interleave_datasets([
    dataset_random_1_test,
    dataset_single_1_test
], seed=42)

dataset_1 = DatasetDict({
    "train": train_dataset_1,
    "test": test_dataset_1
})

dataset_1.push_to_hub(f"{username}/python_codestyles-mixed1-" + dataset_size, private=True)

######################

dataset_random_2_train = dataset_random["train"].shard(num_shards=2, index=1)
dataset_single_2_train = dataset_single["train"].shard(num_shards=2, index=1)

train_dataset_2 = interleave_datasets([
    dataset_random_2_train,
    dataset_single_2_train
], seed=42)

dataset_random_2_test = dataset_random["test"].shard(num_shards=2, index=1)
dataset_single_2_test = dataset_single["test"].shard(num_shards=2, index=1)

test_dataset_2 = interleave_datasets([
    dataset_random_2_test,
    dataset_single_2_test
], seed=42)

dataset_2 = DatasetDict({
    "train": train_dataset_2,
    "test": test_dataset_2
})

dataset_2.push_to_hub(f"{username}/python_codestyles-mixed2-" + dataset_size, private=True)
