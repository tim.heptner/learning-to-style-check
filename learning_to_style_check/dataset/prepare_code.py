import argparse
import re
from pathlib import Path
from typing import Optional


def get_code_length(lines: list[str]) -> int:
    return sum(len(re.sub(r"\r\n|\r|\n| |\t", "", line)) for line in lines)


def prepare(path: str,
            remove_header: Optional[str] = None,
            remove_non_py_files: bool = False,
            remove_setup_files: bool = False):
    total_files = 0
    removed_files = 0
    removed_headers = 0
    for p in Path(path).rglob("*"):
        if p.is_file():
            if p.suffix != ".py" or (remove_setup_files and p.name == "setup.py"):
                if remove_non_py_files or remove_setup_files:
                    removed_files += 1
                    p.unlink()
            else:
                total_files += 1
                with open(p, "r") as f:
                    code = f.readlines()

                new_code = code

                if remove_header:
                    header_idx_start = None
                    header_idx_end = None
                    for i, line in enumerate(code):
                        if line.startswith(remove_header) and header_idx_start is None:
                            header_idx_start = i

                        if line.startswith("#") and header_idx_start is not None:
                            header_idx_end = i
                        else:
                            break

                    if header_idx_start is not None and header_idx_end is not None:
                        removed_headers += 1
                        new_code = code[header_idx_end + 1:]

                        # remove also remaining leading empty lines
                        empty_lines = 0
                        for line in new_code:
                            if get_code_length([line]) == 0:
                                empty_lines += 1
                            else:
                                break
                        new_code = new_code[empty_lines:]
                    elif header_idx_start is not None:
                        removed_headers += 1
                        new_code = ""

                if new_code != code:
                    if get_code_length(new_code) == 0:
                        removed_files += 1
                        p.unlink()
                    else:
                        pass
                        with open(p, "w") as f:
                            f.writelines(new_code)

    print(f"Total files: {total_files}")
    print(f"Removed files: {removed_files}")
    print(f"Removed headers: {removed_headers}")


if __name__ == "__main__":
    parser = argparse.ArgumentParser("Prepare source code dataset")
    parser.add_argument("path", type=str, help="Path source code root directory")
    parser.add_argument("--remove-header", type=str,
                        help="Remove a header from source code which begins with this string")
    parser.add_argument("--remove-non-py-files", action="store_true", help="Remove non python files")
    parser.add_argument("--remove-setup-files", action="store_true", help="Remove setup.py files")

    args = parser.parse_args()

    prepare(**vars(args))
