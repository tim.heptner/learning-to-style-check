import networkx as nx
from dgl import DGLGraph
from matplotlib import pyplot as plt

from learning_to_style_check.libcst import NODE_NAMES


def plot_graph(graph: DGLGraph, figsize=(7, 5), dpi=200) -> None:
    networkx_graph = graph.to_networkx()
    id_to_name = {}
    for name in NODE_NAMES:
        id_to_name[len(id_to_name)] = name

    labels = {int(n): id_to_name[int(graph.ndata["class"][i])] for i, n in enumerate(graph.nodes())}

    plt.figure(figsize=figsize, dpi=dpi)
    pos = nx.nx_agraph.graphviz_layout(networkx_graph, prog="dot")
    nx.draw(
        networkx_graph,
        pos,
        with_labels=False,
        arrows=False,
    )
    nx.draw_networkx_labels(networkx_graph, pos, labels, font_size=4)
    plt.show()
