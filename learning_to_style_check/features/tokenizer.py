from typing import List

from learning_to_style_check.libcst import parse_module
from learning_to_style_check.libcst._nodes.internal import TokengenState
from learning_to_style_check.libcst._nodes.statement import IndentSpacing
from learning_to_style_check.libcst._parser.py_whitespace_parser import parse_newline


def tokenize(code: str) -> List[str]:
    tree = parse_module(code)
    state = TokengenState(
        default_indent=IndentSpacing(tree.default_indent),
        default_newline=parse_newline(tree.default_newline)
    )
    tree._tokengen(state)

    return list(filter(None, state.tokens))
