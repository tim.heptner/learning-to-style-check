from typing import List

import dgl
from dgl import DGLGraph
from torch import tensor

from learning_to_style_check.libcst import parse_module, NODE_NAMES
from learning_to_style_check.libcst._nodes.internal import TreegenState, Tree
from learning_to_style_check.libcst._nodes.statement import IndentSpacing
from learning_to_style_check.libcst._parser.py_whitespace_parser import parse_newline


def get_style_tree(code: str) -> Tree:
    cst_tree = parse_module(code)

    state = TreegenState(
        default_indent=IndentSpacing(cst_tree.default_indent),
        default_newline=parse_newline(cst_tree.default_newline)
    )
    cst_tree._treegen(state)

    return state.root_tree


def get_flatted_style_tree(code: str) -> List[str]:
    tree = get_style_tree(code)

    def recursive_flatten(tree: Tree) -> List[str]:
        result = [tree.name]
        for child in tree.children:
            result += recursive_flatten(child)
        return result

    return recursive_flatten(tree)


def convert_style_tree_to_dgl_graph(tree: Tree) -> DGLGraph:
    nodes_to_id = {}
    for node in NODE_NAMES:
        nodes_to_id[node] = len(nodes_to_id)

    child_id = 1
    node_class_ids = []
    src_ids = []
    dst_ids = []

    def recursive_add_edges(tree: Tree, parent_id: int = 0):
        nonlocal child_id
        node_class_ids.append(nodes_to_id[tree.name])

        for child in tree.children:
            src_ids.append(parent_id)
            dst_ids.append(child_id)
            child_id += 1
            recursive_add_edges(child, child_id - 1)

    recursive_add_edges(tree)

    if src_ids:
        g = dgl.graph((tensor(src_ids), tensor(dst_ids)))
        g.ndata["class"] = tensor(node_class_ids)
    else:
        g = dgl.graph([])
        g.add_nodes(1, {"class": tensor(node_class_ids)})

    return g
