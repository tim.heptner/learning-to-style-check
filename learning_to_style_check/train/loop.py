import logging
import sys
import time
from dataclasses import asdict
from datetime import datetime
from functools import partial
from os import environ
from pathlib import Path

import dgl
import evaluate
import torch
from accelerate import Accelerator
from accelerate.utils import set_seed
from datasets import Dataset
from huggingface_hub.utils import HfHubHTTPError
from torch.optim import Adam, AdamW
from torch.utils.data import DataLoader
from tqdm.autonotebook import tqdm
from transformers import Adafactor, PreTrainedModel, get_linear_schedule_with_warmup, \
    get_cosine_schedule_with_warmup

from learning_to_style_check.train import TrainingArgsConfig
from learning_to_style_check.train.metrics import calc_roc_auc, calc_roc_curve, calc_confusion_matrix, \
    calc_brier_score_loss, calc_balanced_accuracy

project_name = environ.get("PROJECT_NAME", default="learning_to_style_check")


def training_loop(train_config: TrainingArgsConfig,
                  model: PreTrainedModel,
                  dataset: Dataset,
                  collate_fn: callable = None,
                  tracker_kwargs: dict = None):
    """
    Training loop for a model.

    :param train_config: A dictionary containing the configuration for the training loop.
    :param model: A model to train.
    :param dataset: A dataset to train on.
    :param collate_fn: A function to collate the data.
    :param tracker_kwargs: A dictionary containing additional keyword arguments for the tracker.
    """

    if train_config.push_to_hub and train_config.hub_model_id is None:
        raise ValueError("hub_model_id must be set if push_to_hub is True")

    set_seed(train_config.seed)
    dgl.seed(train_config.seed)

    output_dir = Path(train_config.output_dir) / datetime.now().strftime("%Y-%m-%d_%H-%M-%S-%f")

    accelerator = Accelerator(cpu=train_config.cpu,
                              device_placement=True,
                              mixed_precision=train_config.mixed_precision,
                              log_with="all",
                              project_dir=output_dir)

    accelerator.init_trackers(project_name, config=asdict(train_config),
                              init_kwargs=tracker_kwargs if tracker_kwargs else {})

    train_dataloader = DataLoader(
        dataset["train"],
        shuffle=True,
        collate_fn=partial(collate_fn,
                           device=accelerator.device),
        batch_size=train_config.train_batch_size
    )
    eval_dataloader = DataLoader(
        dataset["test"],
        shuffle=False,
        collate_fn=partial(collate_fn,
                           device=accelerator.device),
        batch_size=train_config.eval_batch_size
    )

    if train_config.optimizer == "adam":
        optimizer = Adam(model.parameters(), lr=train_config.lr)
    elif train_config.optimizer == "adamw":
        optimizer = AdamW(model.parameters(), lr=train_config.lr)
    elif train_config.optimizer == "adafactor":
        optimizer = Adafactor(model.parameters(), lr=train_config.lr, relative_step=False, warmup_init=False)
    else:
        raise ValueError(f"Unsupported optimizer {train_config['optimizer']}")

    gradient_accumulation_steps = train_config.gradient_accumulation_steps

    if train_config.learning_rate_scheduler_type == "linear":
        lr_scheduler = get_linear_schedule_with_warmup(
            optimizer=optimizer,
            num_warmup_steps=100,
            num_training_steps=(len(train_dataloader) * train_config.epochs) // gradient_accumulation_steps,
        )
    elif train_config.learning_rate_scheduler_type == "cosine":
        lr_scheduler = get_cosine_schedule_with_warmup(
            optimizer=optimizer,
            num_warmup_steps=100,
            num_training_steps=(len(train_dataloader) * train_config.epochs) // gradient_accumulation_steps,
        )
    else:
        raise ValueError(f"Unsupported learning rate scheduler {train_config['learning_rate_scheduler_type']}")

    metrics = evaluate.combine(["accuracy", "f1", "precision", "recall"])
    custom_metrics = [calc_roc_auc, calc_roc_curve, calc_confusion_matrix, calc_brier_score_loss,
                      calc_balanced_accuracy]

    eval_strategy = train_config.eval_strategy
    eval_steps = train_config.eval_steps
    save_strategy = train_config.save_strategy
    save_steps = train_config.save_steps

    model, optimizer, train_dataloader, eval_dataloader, lr_scheduler = accelerator.prepare(model,
                                                                                            optimizer,
                                                                                            train_dataloader,
                                                                                            eval_dataloader,
                                                                                            lr_scheduler)

    train_progress_bar = tqdm(iterable=range(train_config.epochs * (len(train_dataloader))),
                              disable=not accelerator.is_local_main_process,
                              file=sys.stdout)

    train_step = 0
    for epoch in range(0, train_config.epochs):
        train_progress_bar.set_description(f"Epoch {epoch + 1}/{train_config.epochs}")

        model.train()

        total_loss = torch.tensor(0.0, device=accelerator.device)

        for step, batch in enumerate(train_dataloader):
            outputs = model(**batch)

            loss = outputs.loss
            loss = loss / gradient_accumulation_steps
            total_loss += loss.detach().float()

            accelerator.backward(loss)
            if step % gradient_accumulation_steps == 0:
                optimizer.step()
                lr_scheduler.step()
                optimizer.zero_grad()

                train_progress_bar.update(1)

            train_step += 1

            if save_strategy == "steps" and train_step % save_steps == 0:
                total_train_step = epoch * len(train_dataloader) + step

                step_save_dir = output_dir / f"step_{total_train_step}"
                accelerator.save_state(str(step_save_dir))

                accelerator.wait_for_everyone()
                if accelerator.is_main_process:
                    model.config.save_pretrained(step_save_dir)

                    if train_config.push_to_hub:
                        m = accelerator.unwrap_model(model)
                        _push_to_hub(m, train_config.hub_model_id, train_config.hub_private_repo,
                                     f"checkpoint step {total_train_step}")

            if eval_strategy == "steps" and train_step % eval_steps == 0:
                eval_res = eval(accelerator,
                                eval_dataloader,
                                metrics,
                                custom_metrics,
                                model)

                eval_res["step"] = train_step
                eval_res["epoch"] = epoch + 1
                eval_res["train_loss"] = total_loss.item() / (step + 1)

                accelerator.log(eval_res, step=epoch + 1)

                if accelerator.is_local_main_process:
                    tqdm.write(f"epoch {epoch + 1} step {train_step} eval: " + str(eval_res))

        if save_strategy == "epoch":
            epoch_save_dir = output_dir / f"epoch_{epoch + 1}"
            accelerator.save_state(str(epoch_save_dir))

            accelerator.wait_for_everyone()
            if accelerator.is_main_process:
                model.config.save_pretrained(epoch_save_dir)

                if train_config.push_to_hub:
                    m = accelerator.unwrap_model(model)
                    _push_to_hub(m, train_config.hub_model_id, train_config.hub_private_repo,
                                 f"checkpoint epoch {epoch + 1}")

        if eval_strategy == "epoch":
            eval_res = eval(accelerator,
                            eval_dataloader,
                            metrics,
                            custom_metrics,
                            model)

            eval_res["step"] = train_step
            eval_res["epoch"] = epoch + 1
            eval_res["train_loss"] = total_loss.item() / len(train_dataloader)

            accelerator.log(eval_res, step=epoch + 1)

            if accelerator.is_local_main_process:
                tqdm.write(f"epoch {epoch + 1} step {train_step} eval: " + str(eval_res))

    accelerator.end_training()


def _push_to_hub(model, hub_model_id, hub_private_repo, commit_message):
    retries = 0
    failed = True
    while retries < 10 and failed:
        try:
            model.push_to_hub(hub_model_id,
                              private=hub_private_repo,
                              commit_message=commit_message)
            failed = False
        except HfHubHTTPError as e:
            logging.error(f"Failed to push to hub, reason: {e}")
            failed = True
            if 500 <= e.response.status_code < 600 and "exceeded our daily quotas" not in e.server_message:
                retries += 1
                time.sleep(10)
            else:
                break

    if failed:
        logging.warning("Failed to push to hub. Continuing training.")


def eval(accelerator,
         eval_dataloader,
         metrics,
         custom_metrics,
         model):
    model.eval()

    references = []
    prediction_labels = []
    prediction_scores = []

    eval_loss = torch.tensor(0.0, device=accelerator.device)
    for step, batch in tqdm(enumerate(eval_dataloader),
                            total=len(eval_dataloader),
                            desc="Eval",
                            disable=not accelerator.is_local_main_process,
                            file=sys.stdout,
                            leave=False):
        with torch.no_grad():
            outputs = model(**batch)

        loss = outputs.loss
        eval_loss += loss.detach()

        pred_scores, refs = accelerator.gather_for_metrics((outputs.pred, batch["label"]))
        pred_labels = torch.round(pred_scores)
        metrics.add_batch(
            predictions=pred_labels,
            references=refs,
        )

        prediction_scores.extend(outputs.pred.cpu().numpy())
        references.extend(refs.cpu().numpy())
        prediction_labels.extend(pred_labels.cpu().numpy())

    eval_dict = {
        "eval_loss": eval_loss.item() / len(eval_dataloader)
    }
    eval_dict.update(metrics.compute())
    for metric in custom_metrics:
        eval_dict.update(metric(references=references,
                                predictions=prediction_labels,
                                predictions_scores=prediction_scores))

    model.train()

    return eval_dict
