import dgl
import torch

from learning_to_style_check.utils import get_graphs


def simple_batcher(samples, device):
    labels = torch.tensor([sample["label"] for sample in samples], dtype=torch.float32).to(device)

    code_graphs = []
    style_context_graphs = []
    for sample in samples:
        g_code, g_style = get_graphs(sample)

        code_graphs.append(g_code)
        style_context_graphs.append(g_style)

    return {
        "graph_code": dgl.batch(code_graphs).to(device),
        "graph_style": dgl.batch(style_context_graphs).to(device),
        "label": labels
    }


def batcher_self_loops(samples, device):
    labels = torch.tensor([sample["label"] for sample in samples], dtype=torch.float32).to(device)

    code_graphs = []
    style_context_graphs = []
    for sample in samples:
        g_code, g_style = get_graphs(sample)

        g_code = dgl.add_self_loop(g_code)
        code_graphs.append(g_code)

        g_style = dgl.add_self_loop(g_style)
        style_context_graphs.append(g_style)

    return {
        "graph_code": dgl.batch(code_graphs).to(device),
        "graph_style": dgl.batch(style_context_graphs).to(device),
        "label": labels
    }
