from sklearn import metrics


def calc_balanced_accuracy(references, predictions, **kwargs):
    return {"balanced_accuracy": metrics.balanced_accuracy_score(references, predictions)}


def calc_roc_auc(references, predictions_scores, **kwargs):
    try:
        return {"roc_auc": metrics.roc_auc_score(references, predictions_scores)}
    except ValueError:
        return {"roc_auc": None}


def calc_roc_curve(references, predictions_scores, pos_label=None, **kwargs):
    return {"roc_curve": [v.tolist() for v in metrics.roc_curve(references, predictions_scores, pos_label=pos_label)]}


def calc_confusion_matrix(references, predictions, **kwargs):
    return {"confusion_matrix": metrics.confusion_matrix(references, predictions).tolist()}


def calc_brier_score_loss(references, predictions_scores, **kwargs):
    return {"brier_score_loss": metrics.brier_score_loss(references, predictions_scores)}
