from dataclasses import dataclass
from typing import Optional


@dataclass
class TrainingArgsConfig:
    seed: int = 42

    cpu: bool = False
    fp16: bool = True
    mixed_precision: Optional[str] = None

    eval_strategy: str = "epoch"
    eval_steps: int = 500
    save_strategy: str = "epoch"
    save_steps: int = 500

    train_batch_size: int = 6
    eval_batch_size: int = 6
    gradient_accumulation_steps: int = 1

    optimizer: str = "adamw"
    lr: float = 1e-3
    epochs: int = 8
    learning_rate_scheduler_type: str = "linear"

    push_to_hub: bool = False
    hub_private_repo: bool = True
    hub_model_id: Optional[str] = None
    output_dir: str = "trained_model"
