import dgl


def get_graphs(sample) -> (dgl.DGLGraph, dgl.DGLGraph):
    code_edges = sample["code_edges"]
    g_code = dgl.graph((code_edges[0], code_edges[1]))
    if g_code.num_nodes() == 0:
        g_code.add_nodes(1)
    g_code.ndata["class"] = sample["code_node_classes"]

    style_context_edges = sample["style_context_edges"]
    g_style = dgl.graph((style_context_edges[0], style_context_edges[1]))
    if g_style.num_nodes() == 0:
        g_style.add_nodes(1)
    g_style.ndata["class"] = sample["style_context_node_classes"]

    return g_code, g_style
