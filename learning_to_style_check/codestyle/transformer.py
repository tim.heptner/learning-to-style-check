import random
import re
from typing import Optional, Dict, List, Union, Type

from learning_to_style_check import codestyle
from learning_to_style_check import libcst
from learning_to_style_check.codestyle import CodestyleConfig, NameStyleConfig, NumberStyleConfig, NumberStyle, \
    DocStringStyleConfig
from learning_to_style_check.codestyle.codestyle import StringPrefixStyle
from learning_to_style_check.libcst import RemovalSentinel, parse_statement
from learning_to_style_check.libcst._nodes.expression import StringPrefix
from learning_to_style_check.libcst._nodes.internal import CodegenState
from learning_to_style_check.libcst._nodes.statement import IndentSpacing
from learning_to_style_check.libcst._parser.conversions.expression import parse_number
from learning_to_style_check.libcst.codemod import ContextAwareTransformer, CodemodContext
from learning_to_style_check.libcst.metadata import WhitespaceInclusivePositionProvider

UNIX_LINE_ENDING = b"\n"
WINDOWS_LINE_ENDING = b"\r\n"
MAC_LINE_ENDING = b"\r"


def _remove_first_leading_newline(root_tree: libcst.CSTNode, current_node: libcst.CSTNode) -> Optional[libcst.CSTNode]:
    """
    Recursively remove the first newline found in the tree.
    libcst does not support removing nodes in a sequential order, so we have to do this recursively.

    :param root_tree: The root tree to remove the first newline from.
    :param current_node: The current node to check for newlines.
    :return: The new root tree without the first newline.
    """
    for child in current_node.children:
        if isinstance(child, libcst.EmptyLine) and child.newline and not child.comment:
            return root_tree.deep_remove(child)

        new_root_tree = _remove_first_leading_newline(root_tree, child)
        if new_root_tree is not None:
            return new_root_tree

    return None


def _get_leading_newlines(node: libcst.CSTNode) -> List[libcst.CSTNode]:
    """
    Recursively get all leading newlines in the tree.

    :param node: The node to get the leading newlines from.
    :return: A list of all leading newlines.
    """
    leading_newlines = []

    for child in node.children:
        if isinstance(child, libcst.EmptyLine) and child.newline and not child.comment:
            leading_newlines.append(child)
        else:
            newlines = _get_leading_newlines(child)
            if newlines:
                leading_newlines.extend(newlines)
            break

    return leading_newlines


def _remove_first_trailing_newline(root_tree: libcst.CSTNode, current_node: libcst.CSTNode) -> Optional[libcst.CSTNode]:
    """
    Recursively remove the first trailing newline found in the tree.
    libcst does not support removing nodes in a sequential order, so we have to do this recursively.

    :param root_tree: The root tree to remove the first trailing newline from.
    :param current_node: The current node to check for trailing newlines.
    :return: The new root tree without the first trailing newline.
    """
    for child in reversed(current_node.children):
        if isinstance(child, libcst.EmptyLine) and child.newline:
            return root_tree.deep_remove(child)

        new_root_tree = _remove_first_trailing_newline(root_tree, child)
        if new_root_tree is not None:
            return new_root_tree

    return None


def _get_trailing_newlines(node: libcst.CSTNode) -> List[libcst.CSTNode]:
    """
    Recursively get all trailing newlines in the tree.

    :param node: The node to get the trailing newlines from.
    :return: A list of all trailing newlines.
    """
    trailing_newlines = []

    for child in reversed(node.children):
        if isinstance(child, libcst.EmptyLine) and child.newline and not child.comment:
            trailing_newlines.append(child)
        else:
            newlines = _get_trailing_newlines(child)
            if len(newlines) > 0:
                trailing_newlines.extend(newlines)
            else:
                break

    return trailing_newlines


def _find_parent_node(current_node: libcst.CSTNode, node: libcst.CSTNode) -> Optional[libcst.CSTNode]:
    for child in current_node.children:
        if child is node:
            return current_node

        parent = _find_parent_node(child, node)
        if parent is not None:
            return parent

    return None


def _find_docstrings(parent_node: libcst.CSTNode) -> List[libcst.CSTNode]:
    """
    Find all docstrings in the parent node.

    :param parent_node: The parent node to search for docstrings.
    :return: A list of all docstring nodes.
    """

    nodes = []
    for c in parent_node.children:
        if isinstance(c, libcst.SimpleStatementLine) and c.body and isinstance(c.body[0], libcst.Expr) and isinstance(
                c.body[0].value, libcst.SimpleString):
            nodes.append(c.body[0])

    return nodes


def _add_docstring(node: Union[libcst.Module, libcst.BaseSuite], style: DocStringStyleConfig,
                   newline: Type[libcst.Newline]) -> libcst.CSTNode:
    if style.string_style_config.quotes == '"""':
        docstring_value = libcst.TripleDoubleQuotesString('"""simple docstring"""')
    elif style.string_style_config.quotes == "'''":
        docstring_value = libcst.TripleSingleQuotesString("'''simple docstring'''")
    elif style.string_style_config.quotes == '"':
        docstring_value = libcst.DoubleQuotesString('"simple docstring"')
    elif style.string_style_config.quotes == "'":
        docstring_value = libcst.SingleQuotesString("'simple docstring'")
    else:
        raise ValueError(f"Unknown quotes: {style.string_style_config.quotes}")

    simple_statement_line = libcst.SimpleStatementLine(body=[libcst.Expr(value=libcst.SimpleString(docstring_value))])

    leading_lines_lines = [libcst.EmptyLine(newline=newline(), indent=False) for _ in range(style.blank_lines_before)]
    trailing_lines = [libcst.EmptyLine(newline=newline(), indent=False) for _ in range(style.blank_lines_after)]


    next_nodes = list(node.body)
    if len(next_nodes) > 0:
        currently_leading_lines = _get_leading_newlines(node)

        if len(currently_leading_lines) < style.blank_lines_after:
            if hasattr(next_nodes[0], "leading_lines"):
                next_nodes[0] = next_nodes[0].with_changes(leading_lines=trailing_lines)
            elif isinstance(next_nodes[0], libcst.EmptyLine) and not next_nodes[0].comment:
                # only add remaining blank lines
                next_nodes = trailing_lines[-(style.blank_lines_after - len(currently_leading_lines)):] + next_nodes
            else:
                next_nodes = trailing_lines + next_nodes
        else:
            # remove too many blank lines
            for _ in range(len(currently_leading_lines) - style.blank_lines_after):
                node = _remove_first_leading_newline(node, node)

            next_nodes = list(node.body)

        new_body = leading_lines_lines + [simple_statement_line] + next_nodes
    else:
        new_body = leading_lines_lines + [simple_statement_line] + trailing_lines

    return node.with_changes(body=new_body)


class CodestyleTransformer(ContextAwareTransformer):
    METADATA_DEPENDENCIES = (WhitespaceInclusivePositionProvider,)

    def __init__(self, context: CodemodContext, codestyle: CodestyleConfig):
        super().__init__(context)

        self.stack = []
        self.codestyle = codestyle
        self.leading_blank_lines_nodes: Dict[libcst.CSTNode, List[libcst.CSTNode]] = {}

    def visit_Module(self, node: libcst.Module) -> Optional[bool]:
        self.stack.append(node)
        return True

    def leave_Module(self, original_node: libcst.Module, updated_node: libcst.Module) -> libcst.Module:
        newline_class = self._get_newline_class()

        # remove header and footer and add them as body
        updated_node = updated_node.with_changes(
            body=list(updated_node.header) + list(updated_node.body) + list(updated_node.footer), header=[], footer=[])

        if self.codestyle.docstring_style_config:
            updated_node = _add_docstring(updated_node, self.codestyle.docstring_style_config, newline_class)

        updated_node = updated_node.with_changes(has_trailing_newline=False)

        leading_blank_lines = _get_leading_newlines(updated_node)

        if len(leading_blank_lines) > self.codestyle.blank_lines_top:
            for _ in range(len(leading_blank_lines) - self.codestyle.blank_lines_top):
                updated_node = _remove_first_leading_newline(updated_node, updated_node)
        elif len(leading_blank_lines) < self.codestyle.blank_lines_top:
            updated_node = updated_node.with_changes(body=[libcst.EmptyLine(newline=newline_class()) for _ in range(
                self.codestyle.blank_lines_top - len(leading_blank_lines))] + list(updated_node.body))

        trailing_blank_lines = _get_trailing_newlines(updated_node)

        if len(trailing_blank_lines) > self.codestyle.blank_lines_bottom:
            for _ in range(len(trailing_blank_lines) - self.codestyle.blank_lines_bottom):
                updated_node = _remove_first_trailing_newline(updated_node, updated_node)
        elif len(trailing_blank_lines) < self.codestyle.blank_lines_bottom:
            updated_node = updated_node.with_changes(
                body=list(updated_node.body) + [libcst.EmptyLine(newline=newline_class()) for _ in
                                                range(self.codestyle.blank_lines_bottom - len(trailing_blank_lines))])

        return updated_node

    def visit_ClassDef(self, node: libcst.ClassDef) -> Optional[bool]:
        self.stack.append(node)
        return True

    def leave_ClassDef(self, original_node: libcst.ClassDef, updated_node: libcst.ClassDef) -> libcst.CSTNode:
        if self.codestyle.class_style_config:
            class_style = self.codestyle.class_style_config
            if class_style.name_style_config:
                updated_node = self._apply_name_style(updated_node, class_style.name_style_config)

            if class_style.args_style_config:
                if updated_node.lpar != libcst.MaybeSentinel.DEFAULT:
                    whitespace_before_args = libcst.SimpleWhitespace(
                        class_style.args_style_config.whitespace_before_args)
                    updated_node = updated_node.with_changes(
                        lpar=updated_node.lpar.with_changes(whitespace_after=whitespace_before_args))
                if updated_node.rpar != libcst.MaybeSentinel.DEFAULT:
                    updated_node = updated_node.with_changes(
                        rpar=updated_node.rpar.with_changes(whitespace_before=libcst.SimpleWhitespace("")))

            newline_class = self._get_newline_class()

            leading_lines = self.leading_blank_lines_nodes.get(original_node, [])
            if len(leading_lines) < class_style.blank_lines_before:
                leading_lines = [libcst.EmptyLine(newline=newline_class(), indent=False) for _ in
                                 range(class_style.blank_lines_before)]

            parent = _find_parent_node(self.context.module, original_node)
            if parent is None:
                raise ValueError("Parent node of function not found")

            # add leading blank lines for each context case
            updated_node = updated_node.with_changes(leading_lines=leading_lines)

            # add trailing blank lines
            idx = parent.body.index(original_node)
            if idx < len(parent.body) - 1:
                self.leading_blank_lines_nodes[parent.body[idx + 1]] = [
                    libcst.EmptyLine(newline=newline_class(), indent=False) for _ in
                    range(class_style.blank_lines_after)]

            # handle docstring
            if class_style.docstring_style_config:
                updated_node = updated_node.with_changes(
                    body=_add_docstring(updated_node.body, class_style.docstring_style_config, newline_class))

        self.stack.pop()
        return updated_node

    def visit_FunctionDef(self, node: libcst.FunctionDef) -> Optional[bool]:
        self.stack.append(node)

        return True

    def leave_FunctionDef(self, original_node: libcst.FunctionDef, updated_node: libcst.FunctionDef) -> libcst.CSTNode:
        self.stack.pop()
        style_context_node = self._get_style_context_node()

        if style_context_node is not None and isinstance(style_context_node,
                                                         libcst.ClassDef) and self.codestyle.class_style_config and self.codestyle.class_style_config.method_style_config:
            function_style = self.codestyle.class_style_config.method_style_config
        elif style_context_node is not None and isinstance(style_context_node,
                                                           libcst.Module) and self.codestyle.function_style_config:
            function_style = self.codestyle.function_style_config
        else:
            function_style = None

        if function_style:
            if function_style.name_style_config:
                updated_node = self._apply_name_style(updated_node, function_style.name_style_config)

            if function_style.args_style_config:
                whitespace_before_args = libcst.SimpleWhitespace(
                    function_style.args_style_config.whitespace_before_args)
                updated_node = updated_node.with_changes(whitespace_before_params=whitespace_before_args)

            newline_class = self._get_newline_class()

            leading_lines = self.leading_blank_lines_nodes.get(original_node, [])
            if len(leading_lines) < function_style.blank_lines_before:
                leading_lines = [libcst.EmptyLine(newline=newline_class(), indent=False) for _ in
                                 range(function_style.blank_lines_before)]

            updated_node = updated_node.with_changes(leading_lines=leading_lines)

            parent = _find_parent_node(self.context.module, original_node)
            if parent is None:
                raise ValueError("Parent node of function not found")

            # add trailing blank lines
            self.leading_blank_lines_nodes[parent.body[-1]] = [libcst.EmptyLine(newline=newline_class(), indent=False)
                                                               for _ in range(function_style.blank_lines_after)]

            # handle docstring
            if function_style.docstring_style_config:
                updated_node = updated_node.with_changes(
                    body=_add_docstring(updated_node.body, function_style.docstring_style_config, newline_class))

            if function_style.typing_style_config:
                if updated_node.returns is None:
                    annotation_value = random.choice(codestyle.TYPINGS)
                    annotation_node = parse_statement(f"a:{annotation_value}").body[0].annotation.annotation

                    annotation = libcst.Annotation(annotation=annotation_node,
                                                   whitespace_before_indicator=libcst.SimpleWhitespace(
                                                       function_style.typing_style_config.whitespace_before_indicator),
                                                   whitespace_after_indicator=libcst.SimpleWhitespace(
                                                       function_style.typing_style_config.whitespace_after_indicator))
                else:
                    annotation = updated_node.returns.with_changes(whitespace_before_indicator=libcst.SimpleWhitespace(
                        function_style.typing_style_config.whitespace_before_indicator),
                        whitespace_after_indicator=libcst.SimpleWhitespace(
                            function_style.typing_style_config.whitespace_after_indicator))

                updated_node = updated_node.with_changes(returns=annotation)
            else:
                updated_node = updated_node.with_changes(returns=None)

        return updated_node

    def visit_Assign(self, node: libcst.Assign) -> Optional[bool]:
        self.stack.append(node)
        return True

    def leave_Assign(self, original_node: libcst.Assign, updated_node: libcst.Assign) -> libcst.CSTNode:
        style_context_node = self._get_style_context_node()
        self.stack.pop()

        if self.codestyle.function_style_config and self.codestyle.function_style_config.var_style_config and isinstance(
                style_context_node, libcst.FunctionDef):
            var_style = self.codestyle.function_style_config.var_style_config
        elif self.codestyle.class_style_config and self.codestyle.class_style_config.method_style_config and self.codestyle.class_style_config.method_style_config.var_style_config and isinstance(
                style_context_node, libcst.FunctionDef) and self._is_context_of_type(libcst.ClassDef): \
                var_style = self.codestyle.class_style_config.method_style_config.var_style_config
        elif self.codestyle.class_style_config and self.codestyle.class_style_config.var_style_config and isinstance(
                style_context_node, libcst.ClassDef):
            var_style = self.codestyle.class_style_config.var_style_config
        elif self.codestyle.var_style_config:
            var_style = self.codestyle.var_style_config
        else:
            return updated_node

        targets = []
        for target in updated_node.targets:
            if var_style.name_style_config:
                whitespace_before_equal = var_style.whitespace_before_equal + var_style.name_style_config.whitespace_after
                if isinstance(target.target, libcst.Tuple):
                    elements = []
                    for elem in target.target.elements:
                        if type(var_style.name_style_config.name_style) is tuple:
                            if isinstance(var_style.name_style_config.name_style[0], libcst.NameStyle):
                                sub_name_idx = 0
                                name_idx = 1
                            else:
                                sub_name_idx = 1
                                name_idx = 0

                            sub_name_class = var_style.name_style_config.name_style[sub_name_idx].get_node_class()
                            sub_name = sub_name_class(value=sub_name_class.default_value)
                            name_class = var_style.name_style_config.name_style[name_idx].get_node_class()
                            name = name_class(name=sub_name, lpar=target.target.lpar, rpar=target.target.rpar, )
                        else:
                            name_class = var_style.name_style_config.name_style.get_node_class()
                            name = name_class(value=name_class.default_value, lpar=target.target.lpar,
                                              rpar=target.target.rpar, )
                        comma = libcst.MaybeSentinel.DEFAULT
                        if elem.comma != libcst.MaybeSentinel.DEFAULT:
                            comma = libcst.Comma(
                                whitespace_after=libcst.SimpleWhitespace(var_style.name_style_config.whitespace_before),
                                whitespace_before=libcst.SimpleWhitespace(var_style.name_style_config.whitespace_after))
                        elem = elem.with_changes(value=name, comma=comma)
                        elements.append(elem)
                    target = target.with_changes(target=target.target.with_changes(elements=elements))
                else:
                    if type(var_style.name_style_config.name_style) is tuple:
                        if isinstance(var_style.name_style_config.name_style[0], libcst.NameStyle):
                            sub_name_idx = 0
                            name_idx = 1
                        else:
                            sub_name_idx = 1
                            name_idx = 0

                        sub_name_class = var_style.name_style_config.name_style[sub_name_idx].get_node_class()
                        sub_name = sub_name_class(value=sub_name_class.default_value)
                        name_class = var_style.name_style_config.name_style[name_idx].get_node_class()
                        name = name_class(name=sub_name, lpar=target.target.lpar, rpar=target.target.rpar, )
                    else:
                        name_class = var_style.name_style_config.name_style.get_node_class()
                        name = name_class(value=name_class.default_value, lpar=target.target.lpar,
                                          rpar=target.target.rpar, )
                    target = target.with_changes(target=name)
            else:
                whitespace_before_equal = var_style.whitespace_before_equal

            target = target.with_changes(whitespace_before_equal=libcst.SimpleWhitespace(whitespace_before_equal),
                                         whitespace_after_equal=libcst.SimpleWhitespace(
                                             var_style.whitespace_after_equal), )
            targets.append(target)

        updated_node = updated_node.with_changes(targets=targets)

        if var_style.typing_style_config:
            # convert the assignment to an assignment with an annotation
            annotation_value = random.choice(codestyle.TYPINGS)
            annotation_node = parse_statement(f"a:{annotation_value}").body[0].annotation.annotation

            annotation = libcst.Annotation(annotation=annotation_node,
                                           whitespace_before_indicator=libcst.SimpleWhitespace(
                                               var_style.typing_style_config.whitespace_before_indicator),
                                           whitespace_after_indicator=libcst.SimpleWhitespace(
                                               var_style.typing_style_config.whitespace_after_indicator))

            updated_node = libcst.AnnAssign(target=updated_node.targets[0].target, annotation=annotation,
                                            semicolon=updated_node.semicolon, value=updated_node.value,
                                            equal=libcst.AssignEqual(
                                                whitespace_before=updated_node.targets[0].whitespace_before_equal,
                                                whitespace_after=updated_node.targets[0].whitespace_after_equal))

        return updated_node

    def visit_AnnAssign(self, node: libcst.AnnAssign) -> Optional[bool]:
        self.stack.append(node)
        return True

    def leave_AnnAssign(self, original_node: libcst.AnnAssign, updated_node: libcst.AnnAssign) -> libcst.CSTNode:
        style_context_node = self._get_style_context_node()
        self.stack.pop()

        if self.codestyle.function_style_config and self.codestyle.function_style_config.var_style_config and isinstance(
                style_context_node, libcst.FunctionDef):
            var_style = self.codestyle.function_style_config.var_style_config
        elif self.codestyle.class_style_config and self.codestyle.class_style_config.method_style_config and self.codestyle.class_style_config.method_style_config.var_style_config and isinstance(
                style_context_node, libcst.FunctionDef) and self._is_context_of_type(libcst.ClassDef): \
                var_style = self.codestyle.class_style_config.method_style_config.var_style_config
        elif self.codestyle.class_style_config and self.codestyle.class_style_config.var_style_config and isinstance(
                style_context_node, libcst.ClassDef):
            var_style = self.codestyle.class_style_config.var_style_config
        elif self.codestyle.var_style_config:
            var_style = self.codestyle.var_style_config
        else:
            return updated_node

        if var_style.name_style_config:
            whitespace_before_equal = var_style.whitespace_before_equal + var_style.name_style_config.whitespace_after

            if type(var_style.name_style_config.name_style) is tuple:
                if isinstance(var_style.name_style_config.name_style[0], libcst.NameStyle):
                    sub_name_idx = 0
                    name_idx = 1
                else:
                    sub_name_idx = 1
                    name_idx = 0

                sub_name_class = var_style.name_style_config.name_style[sub_name_idx].get_node_class()
                sub_name = sub_name_class(value=sub_name_class.default_value)
                name_class = var_style.name_style_config.name_style[name_idx].get_node_class()
                name = name_class(name=sub_name, lpar=updated_node.target.lpar, rpar=updated_node.target.rpar, )
            else:
                name_class = var_style.name_style_config.name_style.get_node_class()
                name = name_class(value=name_class.default_value, lpar=updated_node.target.lpar,
                                  rpar=updated_node.target.rpar, )
            updated_node = updated_node.with_changes(target=name)
        else:
            whitespace_before_equal = var_style.whitespace_before_equal

        if var_style.typing_style_config:
            if updated_node.equal != libcst.MaybeSentinel.DEFAULT:
                updated_node = updated_node.with_changes(equal=updated_node.equal.with_changes(
                    whitespace_before=libcst.SimpleWhitespace(whitespace_before_equal),
                    whitespace_after=libcst.SimpleWhitespace(var_style.whitespace_after_equal)))
            updated_node = updated_node.with_changes(annotation=updated_node.annotation.with_changes(
                whitespace_before_indicator=libcst.SimpleWhitespace(
                    var_style.typing_style_config.whitespace_before_indicator),
                whitespace_after_indicator=libcst.SimpleWhitespace(
                    var_style.typing_style_config.whitespace_after_indicator)))
        else:
            # convert an assignment with an annotation to a normal assignment without an annotation
            if updated_node.value is None:
                value = libcst.Integer(value="42")
            else:
                value = updated_node.value
            updated_node = libcst.Assign(targets=[libcst.AssignTarget(target=updated_node.target,
                                                                      whitespace_before_equal=libcst.SimpleWhitespace(
                                                                          whitespace_before_equal),
                                                                      whitespace_after_equal=libcst.SimpleWhitespace(
                                                                          var_style.whitespace_after_equal))],
                                         value=value, semicolon=updated_node.semicolon)

        return updated_node

    def leave_SimpleString(self, original_node: libcst.SimpleString,
                           updated_node: libcst.SimpleString) -> libcst.SimpleString:
        context_node = self._get_context_node()

        # check if we are in value assignment
        if isinstance(context_node, (libcst.Assign, libcst.FormattedString, libcst.Arg)):
            style_context_node = self._get_style_context_node()
            # we are a variable or argument
            if self.codestyle.function_style_config and self.codestyle.function_style_config.var_style_config and isinstance(
                    style_context_node, libcst.FunctionDef):
                string_style = self.codestyle.function_style_config.var_style_config.string_style_config
            elif self.codestyle.class_style_config and self.codestyle.class_style_config.method_style_config and self.codestyle.class_style_config.method_style_config.var_style_config and isinstance(
                    style_context_node, libcst.FunctionDef) and self._is_context_of_type(libcst.ClassDef):
                string_style = self.codestyle.class_style_config.method_style_config.var_style_config.string_style_config
            elif self.codestyle.class_style_config and self.codestyle.class_style_config.var_style_config and isinstance(
                    style_context_node, libcst.ClassDef):
                string_style = self.codestyle.class_style_config.var_style_config.string_style_config
            elif self.codestyle.var_style_config:
                string_style = self.codestyle.var_style_config.string_style_config
            else:
                return updated_node

            if self._is_context_of_type(libcst.FormattedString):
                quotes = string_style.f_string_quotes[1]
            else:
                quotes = string_style.quotes
        else:
            # we are a something else, this case is handled somewhere else
            return updated_node

        if isinstance(updated_node.value_node,
                      (libcst.TripleDoubleQuotesString, libcst.TripleSingleQuotesString)) and quotes in ['"', "'"]:
            # we have to remove newlines, because they are not allowed in single or double quotes strings
            raw_value = re.sub(r"\n", r"\\n", updated_node.raw_value)
        else:
            raw_value = updated_node.raw_value

        if quotes == '"""':
            raw_value = re.sub(r'(?<!\\)"', r'\"', raw_value)
            value_node = libcst.TripleDoubleQuotesString(value=updated_node.prefix + quotes + raw_value + quotes)
        elif quotes == "'''":
            raw_value = re.sub(r"(?<!\\)'", r"\'", raw_value)
            value_node = libcst.TripleSingleQuotesString(value=updated_node.prefix + quotes + raw_value + quotes)
        elif quotes == '"':
            raw_value = re.sub(r'(?<!\\)"', r'\"', raw_value)
            value_node = libcst.DoubleQuotesString(value=updated_node.prefix + quotes + raw_value + quotes)
        elif quotes == "'":
            raw_value = re.sub(r"(?<!\\)'", r"\'", raw_value)
            value_node = libcst.SingleQuotesString(value=updated_node.prefix + quotes + raw_value + quotes)
        else:
            raise ValueError(f"Unknown quotes: {quotes}")

        return updated_node.with_changes(value_node=value_node)

    def visit_FormattedString(self, node: libcst.FormattedString) -> Optional[bool]:
        self.stack.append(node)
        return True

    def leave_FormattedString(self, original_node: libcst.FormattedString, updated_node: libcst.FormattedString) -> \
            Union[libcst.FormattedString, libcst.String]:
        self.stack.pop()

        # WORKAROUND for duplicate prefix value location
        start = "".join([p.value for p in updated_node.prefixes]) + updated_node.start[len(updated_node.prefix):]
        updated_node = updated_node.with_changes(start=start)

        context_node = self._get_context_node()

        # check if we are in value assignment
        if not isinstance(context_node, libcst.Module):
            style_context_node = self._get_style_context_node()

            # we are a variable
            if self.codestyle.function_style_config and self.codestyle.function_style_config.var_style_config and isinstance(
                    style_context_node, libcst.FunctionDef):
                string_style = self.codestyle.function_style_config.var_style_config.string_style_config
            elif self.codestyle.class_style_config and self.codestyle.class_style_config.method_style_config and self.codestyle.class_style_config.method_style_config.var_style_config and isinstance(
                    style_context_node, libcst.FunctionDef) and self._is_context_of_type(libcst.ClassDef):
                string_style = self.codestyle.class_style_config.method_style_config.var_style_config.string_style_config
            elif self.codestyle.class_style_config and self.codestyle.class_style_config.var_style_config and isinstance(
                    style_context_node, libcst.ClassDef):
                string_style = self.codestyle.class_style_config.var_style_config.string_style_config
            elif self.codestyle.var_style_config:
                string_style = self.codestyle.var_style_config.string_style_config
            else:
                return updated_node
        else:
            # we are a docstring, this case is handled somewhere else
            return updated_node

        if self._is_context_of_type(libcst.FormattedString):
            quotes = string_style.f_string_quotes[1]
            if quotes == '"""':
                string_node = libcst.TripleDoubleQuotesString(value='"""nested fstring"""')
            elif quotes == "'''":
                string_node = libcst.TripleSingleQuotesString(value="'''nested fstring'''")
            elif quotes == '"':
                string_node = libcst.DoubleQuotesString(value='"nested fstring"')
            elif quotes == "'":
                string_node = libcst.SingleQuotesString(value="'nested fstring'")
            else:
                raise ValueError(f"Unknown quotes: {quotes}")
            return string_node

        quotes = string_style.f_string_quotes[0]

        if quotes == '"""':
            # escape quotes in f-string
            new_parts = []
            for p in updated_node.parts:
                if isinstance(p, libcst.FormattedStringText):
                    new_parts.append(p.with_changes(value=re.sub(r'(?<!\\)"', r'\"', p.value)))
                else:
                    new_parts.append(p)

            string_quotes = libcst.TripleDoubleQuotesString(value=quotes)
        elif quotes == "'''":
            # escape quotes in f-string
            new_parts = []
            for p in updated_node.parts:
                if isinstance(p, libcst.FormattedStringText):
                    new_parts.append(p.with_changes(value=re.sub(r"(?<!\\)'", r"\'", p.value)))
                else:
                    new_parts.append(p)

            string_quotes = libcst.TripleSingleQuotesString(value=quotes)
        elif quotes == '"':
            # escape quotes and newlines in f-string
            new_parts = []
            for p in updated_node.parts:
                if isinstance(p, libcst.FormattedStringText):
                    if isinstance(updated_node.string_quotes,
                                  (libcst.TripleDoubleQuotesString, libcst.TripleSingleQuotesString)):
                        new_string = re.sub(r"\n", r"\\n", p.value)
                    else:
                        new_string = p.value
                    new_parts.append(p.with_changes(value=re.sub(r'(?<!\\)"', r'\"', new_string)))
                else:
                    new_parts.append(p)

            string_quotes = libcst.DoubleQuotesString(value=quotes)
        elif quotes == "'":
            # escape quotes and remove newlines in f-string
            new_parts = []
            for p in updated_node.parts:
                if isinstance(p, libcst.FormattedStringText):
                    if isinstance(updated_node.string_quotes,
                                  (libcst.TripleDoubleQuotesString, libcst.TripleSingleQuotesString)):
                        new_string = re.sub(r"\n", r"\\n", p.value)
                    else:
                        new_string = p.value

                    new_parts.append(p.with_changes(value=re.sub(r"(?<!\\)'", r"\'", new_string)))
                else:
                    new_parts.append(p)

            string_quotes = libcst.SingleQuotesString(value=quotes)
        else:
            raise ValueError(f"Unknown quotes: {quotes}")

        return updated_node.with_changes(parts=new_parts, string_quotes=string_quotes,
                                         start=updated_node.prefix + quotes, end=quotes)

    def leave_IndentedBlock(self, original_node: libcst.IndentedBlock,
                            updated_node: libcst.IndentedBlock) -> libcst.IndentedBlock:
        # find the first node in the stack which is a function or class
        node = None
        for n in reversed(self.stack):
            if isinstance(n, (libcst.FunctionDef, libcst.ClassDef)):
                node = n
                break

        if node is not None and isinstance(node, libcst.FunctionDef) and self.codestyle.function_style_config:
            indent = IndentSpacing(self.codestyle.indent_char * self.codestyle.function_style_config.indent_size)
        elif node is not None and isinstance(node, libcst.ClassDef) and self.codestyle.class_style_config:
            indent = IndentSpacing(self.codestyle.indent_char * self.codestyle.class_style_config.indent_size)
        else:
            indent = IndentSpacing(self.codestyle.indent_char * self.codestyle.indent_size)

        return updated_node.with_changes(indent=indent)

    def leave_SimpleStatementLine(self, original_node: libcst.SimpleStatementLine,
                                  updated_node: libcst.SimpleStatementLine) -> Union[
        libcst.SimpleStatementLine, RemovalSentinel]:
        expr_node = updated_node.body[0]
        if isinstance(expr_node, libcst.Expr) and isinstance(expr_node.value, libcst.SimpleString):
            # we are a docstring, each docstring is handled by its parent node
            # remove any leading blank lines for the next node
            parent = _find_parent_node(self.context.module, original_node)
            if parent is None:
                raise ValueError("No parent node found for SimpleStatementLine")
            idx = parent.children.index(original_node)
            if idx < len(parent.children) - 1:
                next_node = parent.children[idx + 1]
                self.leading_blank_lines_nodes[next_node] = []

            return RemovalSentinel.REMOVE
        elif original_node in self.leading_blank_lines_nodes:
            leading_lines = self.leading_blank_lines_nodes[original_node]
            updated_node = updated_node.with_changes(leading_lines=leading_lines)
            del self.leading_blank_lines_nodes[original_node]

        return updated_node

    def visit_Arg(self, node: libcst.Arg) -> Optional[bool]:
        self.stack.append(node)
        return True

    def leave_Arg(self, original_node: libcst.Arg, updated_node: libcst.Arg) -> libcst.Arg:
        self.stack.pop()

        style_context_node = self._get_style_context_node()

        args_style = None
        if isinstance(style_context_node, libcst.ClassDef) and self.codestyle.class_style_config:
            args_style = self.codestyle.class_style_config.args_style_config
        elif isinstance(style_context_node, libcst.FunctionDef):
            if self._is_context_of_type(libcst.ClassDef):
                if self.codestyle.class_style_config and self.codestyle.class_style_config.method_style_config:
                    args_style = self.codestyle.class_style_config.method_style_config.args_style_config
            elif self.codestyle.function_style_config:
                args_style = self.codestyle.function_style_config.args_style_config

        if args_style:
            if issubclass(updated_node.value.__class__,
                          libcst.Name) and updated_node.value.value not in args_style.name_style_config.keep_names:
                name_style = args_style.name_style_config.name_style
                if type(name_style) is tuple:
                    if isinstance(name_style[0], libcst.NameStyle):
                        sub_name_idx = 0
                        name_idx = 1
                    else:
                        sub_name_idx = 1
                        name_idx = 0

                    sub_name_class = name_style[sub_name_idx].get_node_class()
                    sub_name = sub_name_class(value=sub_name_class.default_value)
                    name_class = name_style[name_idx].get_node_class()
                    name = name_class(name=sub_name, lpar=updated_node.value.lpar, rpar=updated_node.value.rpar, )
                else:
                    name_class = name_style.get_node_class()
                    name = name_class(value=name_class.default_value, lpar=updated_node.value.lpar,
                                      rpar=updated_node.value.rpar, )

                updated_node = updated_node.with_changes(value=name)

            if updated_node.comma == libcst.MaybeSentinel.DEFAULT:
                comma = libcst.MaybeSentinel.DEFAULT
                whitespace_after = libcst.SimpleWhitespace(args_style.whitespace_after_args)
                updated_node = updated_node.with_changes(whitespace_after_arg=whitespace_after)
            else:
                whitespace_before = libcst.SimpleWhitespace(args_style.name_style_config.whitespace_before)
                whitespace_after = libcst.SimpleWhitespace(args_style.name_style_config.whitespace_after)
                updated_node = updated_node.with_changes(whitespace_after_arg=libcst.SimpleWhitespace(""))
                comma = libcst.Comma(whitespace_after=whitespace_before, whitespace_before=whitespace_after)

            updated_node = updated_node.with_changes(comma=comma)

        return updated_node

    def visit_Lambda(self, node: "Lambda") -> Optional[bool]:
        self.stack.append(node)
        return True

    def leave_Lambda(self, original_node: "Lambda", updated_node: "Lambda") -> "Lambda":
        self.stack.pop()
        return updated_node

    def visit_Param(self, node: libcst.Param) -> Optional[bool]:
        self.stack.append(node)
        return True

    def leave_Param(self, original_node: libcst.Param, updated_node: libcst.Param) -> libcst.Param:
        self.stack.pop()

        style_context_node = self._get_style_context_node()

        args_style = None
        if isinstance(style_context_node, libcst.ClassDef) and self.codestyle.class_style_config:
            args_style = self.codestyle.class_style_config.args_style_config
        elif isinstance(style_context_node, libcst.FunctionDef):
            if self._is_context_of_type(libcst.ClassDef):
                if self.codestyle.class_style_config and self.codestyle.class_style_config.method_style_config:
                    args_style = self.codestyle.class_style_config.method_style_config.args_style_config
            elif self.codestyle.function_style_config:
                args_style = self.codestyle.function_style_config.args_style_config

        if args_style:
            name_style = args_style.name_style_config.name_style
            if updated_node.name.value not in args_style.name_style_config.keep_names:
                if type(name_style) is tuple:
                    if isinstance(name_style[0], libcst.NameStyle):
                        sub_name_idx = 0
                        name_idx = 1
                    else:
                        sub_name_idx = 1
                        name_idx = 0

                    sub_name_class = name_style[sub_name_idx].get_node_class()
                    sub_name = sub_name_class(value=sub_name_class.default_value)
                    name_class = name_style[name_idx].get_node_class()
                    name = name_class(name=sub_name, lpar=updated_node.name.lpar, rpar=updated_node.name.rpar, )
                else:
                    name_class = name_style.get_node_class()
                    name = name_class(value=name_class.default_value, lpar=updated_node.name.lpar,
                                      rpar=updated_node.name.rpar, )

                updated_node = updated_node.with_changes(name=name)

            if updated_node.comma == libcst.MaybeSentinel.DEFAULT:
                comma = libcst.MaybeSentinel.DEFAULT
                whitespace_after = libcst.SimpleWhitespace(args_style.whitespace_after_args)
                updated_node = updated_node.with_changes(whitespace_after_param=whitespace_after)
            else:
                whitespace_before = libcst.SimpleWhitespace(args_style.name_style_config.whitespace_before)
                whitespace_after = libcst.SimpleWhitespace(args_style.name_style_config.whitespace_after)
                updated_node = updated_node.with_changes(whitespace_after_param=libcst.SimpleWhitespace(""))
                comma = libcst.Comma(whitespace_after=whitespace_before, whitespace_before=whitespace_after)

            updated_node = updated_node.with_changes(comma=comma)

            if args_style.typing_style_config and not self._is_context_of_type(libcst.Lambda):
                if updated_node.annotation is None:
                    annotation_value = random.choice(codestyle.TYPINGS)
                    annotation_node = parse_statement(f"a:{annotation_value}").body[0].annotation.annotation

                    annotation = libcst.Annotation(annotation=annotation_node,
                                                   whitespace_before_indicator=libcst.SimpleWhitespace(
                                                       args_style.typing_style_config.whitespace_before_indicator),
                                                   whitespace_after_indicator=libcst.SimpleWhitespace(
                                                       args_style.typing_style_config.whitespace_after_indicator))
                else:
                    annotation = updated_node.annotation.with_changes(
                        whitespace_before_indicator=libcst.SimpleWhitespace(
                            args_style.typing_style_config.whitespace_before_indicator),
                        whitespace_after_indicator=libcst.SimpleWhitespace(
                            args_style.typing_style_config.whitespace_after_indicator))
                updated_node = updated_node.with_changes(annotation=annotation)
            else:
                updated_node = updated_node.with_changes(annotation=None)

        return updated_node

    def leave_Number(self, original_node: libcst.Number, updated_node: libcst.Number) -> libcst.Number:
        number_style = self._get_number_style()
        if number_style is None:
            return updated_node

        underscore_every = None
        value_str_without_underscores = ""
        for p in updated_node.parts:
            if p.value != "_":
                value_str_without_underscores += p.value

            if type(p) is libcst.ExpLow or type(p) is libcst.ExpUp:
                underscore_every = number_style.exp_underscore_every
            elif type(p) is libcst.HexLow or type(p) is libcst.HexUp:
                underscore_every = number_style.hex_underscore_every
            elif type(p) is libcst.OctLow or type(p) is libcst.OctUp:
                underscore_every = number_style.oct_underscore_every
            elif type(p) is libcst.BinLow or type(p) is libcst.BinUp:
                underscore_every = number_style.bin_underscore_every
            elif type(p) is libcst.ImagLow or type(p) is libcst.ImagUp:
                underscore_every = number_style.imag_underscore_every
            elif type(p) is libcst.Integer and underscore_every is None:
                underscore_every = number_style.int_underscore_every
            elif type(p) is libcst.Float and underscore_every is None:
                underscore_every = number_style.float_underscore_every

        if underscore_every == 0 or underscore_every is None:
            return parse_number(value_str_without_underscores)

        def add_underscores(number_str) -> str:
            new_parts = []
            char_count = 0
            for p in parse_number(number_str).parts[::-1]:
                if type(p) in [libcst.ExpLow, libcst.ExpUp, libcst.HexLow, libcst.HexUp, libcst.OctLow, libcst.OctUp,
                               libcst.BinLow, libcst.BinUp, libcst.ImagLow, libcst.ImagUp]:
                    new_parts.append(p.value)
                else:
                    for c in p.value[::-1]:
                        if c in ["+", "-"]:
                            new_parts.append(c)
                        elif c == ".":
                            new_parts.append(c)
                            char_count = 0
                        else:
                            if 0 < char_count < len(number_str) and char_count % underscore_every == 0:
                                new_parts.append("_")
                            new_parts.append(c)
                            char_count += 1

                if type(p) is libcst.ExpLow or type(p) is libcst.ExpUp:
                    char_count = 0

            return "".join(new_parts[::-1])

        new_value_str = add_underscores(value_str_without_underscores)

        return parse_number(new_value_str)

    def leave_HexadecimalLow(self, original_node: libcst.HexadecimalLow,
                             updated_node: libcst.HexadecimalLow) -> libcst.CSTNode:
        number_style = self._get_number_style()

        if number_style is not None and number_style.hex_letter_case:
            if number_style.hex_letter_case == NumberStyle.UP:
                updated_node = updated_node.with_changes(value=updated_node.value.upper())
            else:
                updated_node = updated_node.with_changes(value=updated_node.value.lower())

        return updated_node

    def leave_HexadecimalUp(self, original_node: libcst.HexadecimalUp,
                            updated_node: libcst.HexadecimalUp) -> libcst.CSTNode:
        number_style = self._get_number_style()

        if number_style is not None and number_style.hex_letter_case:
            if number_style.hex_letter_case == NumberStyle.UP:
                updated_node = updated_node.with_changes(value=updated_node.value.upper())
            else:
                updated_node = updated_node.with_changes(value=updated_node.value.lower())

        return updated_node

    def leave_HexadecimalMixed(self, original_node: libcst.HexadecimalMixed,
                               updated_node: libcst.HexadecimalMixed) -> libcst.CSTNode:
        number_style = self._get_number_style()

        if number_style is not None and number_style.hex_letter_case:
            if number_style.hex_letter_case == NumberStyle.UP:
                updated_node = updated_node.with_changes(value=updated_node.value.upper())
            else:
                updated_node = updated_node.with_changes(value=updated_node.value.lower())

        return updated_node

    def leave_ExpLow(self, original_node: libcst.ExpLow, updated_node: libcst.ExpLow) -> libcst.NumberNotation:
        number_style = self._get_number_style()

        if number_style is not None and number_style.exp_style and number_style.exp_style != libcst.ExpLow:
            if number_style.exp_style == NumberStyle.LOW:
                updated_node = libcst.ExpLow()
            elif number_style.exp_style == NumberStyle.UP:
                updated_node = libcst.ExpUp()
            else:
                raise ValueError(f"Unknown number style: {number_style}")

        return updated_node

    def leave_ExpUp(self, original_node: libcst.ExpUp, updated_node: libcst.ExpUp) -> libcst.NumberNotation:
        number_style = self._get_number_style()

        if number_style is not None and number_style.exp_style and number_style.exp_style != libcst.ExpUp:
            if number_style.exp_style == NumberStyle.LOW:
                updated_node = libcst.ExpLow()
            elif number_style.exp_style == NumberStyle.UP:
                updated_node = libcst.ExpUp()
            else:
                raise ValueError(f"Unknown number style: {number_style}")

        return updated_node

    def leave_HexLow(self, original_node: libcst.HexLow, updated_node: libcst.HexLow) -> libcst.NumberNotation:
        number_style = self._get_number_style()

        if number_style is not None and number_style.hex_style and number_style.hex_style != libcst.HexLow:
            if number_style.hex_style == NumberStyle.LOW:
                updated_node = libcst.HexLow()
            elif number_style.hex_style == NumberStyle.UP:
                updated_node = libcst.HexUp()
            else:
                raise ValueError(f"Unknown number style: {number_style}")

        return updated_node

    def leave_HexUp(self, original_node: libcst.HexUp, updated_node: libcst.HexUp) -> libcst.NumberNotation:
        number_style = self._get_number_style()

        if number_style is not None and number_style.hex_style and number_style.hex_style != libcst.HexUp:
            if number_style.hex_style == NumberStyle.LOW:
                updated_node = libcst.HexLow()
            elif number_style.hex_style == NumberStyle.UP:
                updated_node = libcst.HexUp()
            else:
                raise ValueError(f"Unknown number style: {number_style}")

        return updated_node

    def leave_OctLow(self, original_node: libcst.OctLow, updated_node: libcst.OctLow) -> libcst.NumberNotation:
        number_style = self._get_number_style()

        if number_style is not None and number_style.oct_style and number_style.oct_style != libcst.OctLow:
            if number_style.oct_style == NumberStyle.LOW:
                updated_node = libcst.OctLow()
            elif number_style.oct_style == NumberStyle.UP:
                updated_node = libcst.OctUp()
            else:
                raise ValueError(f"Unknown number style: {number_style}")

        return updated_node

    def leave_OctUp(self, original_node: libcst.OctUp, updated_node: libcst.OctUp) -> libcst.NumberNotation:
        number_style = self._get_number_style()

        if number_style is not None and number_style.oct_style and number_style.oct_style != libcst.OctUp:
            if number_style.oct_style == NumberStyle.LOW:
                updated_node = libcst.OctLow()
            elif number_style.oct_style == NumberStyle.UP:
                updated_node = libcst.OctUp()
            else:
                raise ValueError(f"Unknown number style: {number_style}")

        return updated_node

    def leave_BinLow(self, original_node: libcst.BinLow, updated_node: libcst.BinLow) -> libcst.NumberNotation:
        number_style = self._get_number_style()

        if number_style is not None and number_style.bin_style and number_style.bin_style != libcst.BinLow:
            if number_style.bin_style == NumberStyle.LOW:
                updated_node = libcst.BinLow()
            elif number_style.bin_style == NumberStyle.UP:
                updated_node = libcst.BinUp()
            else:
                raise ValueError(f"Unknown number style: {number_style}")

        return updated_node

    def leave_BinUp(self, original_node: libcst.BinUp, updated_node: libcst.BinUp) -> libcst.NumberNotation:
        number_style = self._get_number_style()

        if number_style is not None and number_style.bin_style and number_style.bin_style != libcst.BinUp:
            if number_style.bin_style == NumberStyle.LOW:
                updated_node = libcst.BinLow()
            elif number_style.bin_style == NumberStyle.UP:
                updated_node = libcst.BinUp()
            else:
                raise ValueError(f"Unknown number style: {number_style}")

        return updated_node

    def leave_ImagLow(self, original_node: libcst.ImagLow, updated_node: libcst.ImagLow) -> libcst.NumberNotation:
        number_style = self._get_number_style()

        if number_style is not None and number_style.imag_style and number_style.imag_style != libcst.ImagLow:
            if number_style.imag_style == NumberStyle.LOW:
                updated_node = libcst.ImagLow()
            elif number_style.imag_style == NumberStyle.UP:
                updated_node = libcst.ImagUp()
            else:
                raise ValueError(f"Unknown number style: {number_style}")

        return updated_node

    def leave_ImagUp(self, original_node: libcst.ImagUp, updated_node: libcst.ImagUp) -> libcst.NumberNotation:
        number_style = self._get_number_style()

        if number_style is not None and number_style.imag_style and number_style.imag_style != libcst.ImagUp:
            if number_style.imag_style == NumberStyle.LOW:
                updated_node = libcst.ImagLow()
            elif number_style.imag_style == NumberStyle.UP:
                updated_node = libcst.ImagUp()
            else:
                raise ValueError(f"Unknown number style: {number_style}")

        return updated_node

    def leave_UnixNewline(self, original_node: libcst.UnixNewline, updated_node: libcst.UnixNewline) -> libcst.Newline:
        if self.codestyle.line_ending != "\n":
            return self._get_newline_class()()
        else:
            return updated_node

    def leave_WindowsNewline(self, original_node: libcst.WindowsNewline,
                             updated_node: libcst.WindowsNewline) -> libcst.Newline:
        if self.codestyle.line_ending != "\r\n":
            return self._get_newline_class()()
        else:
            return updated_node

    def leave_MacNewline(self, original_node: libcst.MacNewline, updated_node: libcst.MacNewline) -> libcst.Newline:
        if self.codestyle.line_ending != "\r":
            return self._get_newline_class()()
        else:
            return updated_node

    def leave_FormattedLowStringPrefix(self, original_node: libcst.FormattedLowStringPrefix,
                                       updated_node: libcst.FormattedLowStringPrefix) -> StringPrefix:
        string_style = self._get_string_style()
        if string_style and string_style.f_prefix == StringPrefixStyle.UP:
            return libcst.FormattedUpStringPrefix()

        return updated_node

    def leave_FormattedUpStringPrefix(self, original_node: libcst.FormattedUpStringPrefix,
                                      updated_node: libcst.FormattedUpStringPrefix) -> StringPrefix:
        string_style = self._get_string_style()
        if string_style and string_style.f_prefix == StringPrefixStyle.LOW:
            return libcst.FormattedLowStringPrefix()

        return updated_node

    def leave_RawLowStringPrefix(self, original_node: libcst.RawLowStringPrefix,
                                 updated_node: libcst.RawLowStringPrefix) -> StringPrefix:
        string_style = self._get_string_style()
        if string_style and string_style.r_prefix == StringPrefixStyle.UP:
            return libcst.RawUpStringPrefix()

        return updated_node

    def leave_RawUpStringPrefix(self, original_node: libcst.RawUpStringPrefix,
                                updated_node: libcst.RawUpStringPrefix) -> StringPrefix:
        string_style = self._get_string_style()
        if string_style and string_style.r_prefix == StringPrefixStyle.LOW:
            return libcst.RawLowStringPrefix()

        return updated_node

    def leave_ByteLowStringPrefix(self, original_node: libcst.ByteLowStringPrefix,
                                  updated_node: libcst.ByteLowStringPrefix) -> StringPrefix:
        string_style = self._get_string_style()
        if string_style and string_style.b_prefix == StringPrefixStyle.UP:
            return libcst.ByteUpStringPrefix()

        return updated_node

    def leave_ByteUpStringPrefix(self, original_node: libcst.ByteUpStringPrefix,
                                 updated_node: libcst.ByteUpStringPrefix) -> StringPrefix:
        string_style = self._get_string_style()
        if string_style and string_style.b_prefix == StringPrefixStyle.LOW:
            return libcst.ByteLowStringPrefix()

        return updated_node

    def leave_UnicodeLowStringPrefix(self, original_node: libcst.UnicodeLowStringPrefix,
                                     updated_node: libcst.UnicodeLowStringPrefix) -> StringPrefix:
        string_style = self._get_string_style()
        if string_style and string_style.u_prefix == StringPrefixStyle.UP:
            return libcst.UnicodeUpStringPrefix()

        return updated_node

    def leave_UnicodeUpStringPrefix(self, original_node: libcst.UnicodeUpStringPrefix,
                                    updated_node: libcst.UnicodeUpStringPrefix) -> StringPrefix:
        string_style = self._get_string_style()
        if string_style and string_style.u_prefix == StringPrefixStyle.LOW:
            return libcst.UnicodeLowStringPrefix()

        return updated_node

    def _get_string_style(self):
        style_context_node = self._get_style_context_node()

        string_style = None
        if isinstance(style_context_node,
                      libcst.ClassDef) and self.codestyle.class_style_config and self.codestyle.class_style_config.var_style_config and self.codestyle.class_style_config.var_style_config.string_style_config:
            string_style = self.codestyle.class_style_config.var_style_config.string_style_config
        elif isinstance(style_context_node,
                        libcst.FunctionDef) and self.codestyle.function_style_config and self.codestyle.function_style_config.var_style_config and self.codestyle.function_style_config.var_style_config.string_style_config:
            # check if we are a class method or a module function
            if self._is_context_of_type(libcst.ClassDef):
                string_style = self.codestyle.class_style_config.method_style_config.var_style_config.string_style_config
            else:
                string_style = self.codestyle.function_style_config.var_style_config.string_style_config
        elif self.codestyle.var_style_config and self.codestyle.var_style_config.string_style_config:
            string_style = self.codestyle.var_style_config.string_style_config

        return string_style

    def _get_number_style(self) -> Union[NumberStyleConfig, None]:
        style_context_node = self._get_style_context_node()

        number_style = None
        if isinstance(style_context_node,
                      libcst.ClassDef) and self.codestyle.class_style_config and self.codestyle.class_style_config.var_style_config and self.codestyle.class_style_config.var_style_config.number_style_config:
            number_style = self.codestyle.class_style_config.var_style_config.number_style_config
        elif isinstance(style_context_node,
                        libcst.FunctionDef) and self.codestyle.function_style_config and self.codestyle.function_style_config.var_style_config and self.codestyle.function_style_config.var_style_config.number_style_config:
            # check if we are a class method or a module function
            if self._is_context_of_type(libcst.ClassDef):
                number_style = self.codestyle.class_style_config.method_style_config.var_style_config.number_style_config
            else:
                number_style = self.codestyle.function_style_config.var_style_config.number_style_config
        elif self.codestyle.var_style_config and self.codestyle.var_style_config.number_style_config:
            number_style = self.codestyle.var_style_config.number_style_config

        return number_style

    def _get_style_context_node(self) -> Union[libcst.CSTNode, None]:
        for node in reversed(self.stack):
            if isinstance(node, (libcst.FunctionDef, libcst.ClassDef, libcst.Module)):
                return node
        return None

    def _get_context_node(self, skip=0) -> Union[libcst.CSTNode, None]:
        """
        Get the current context node, which is the last function or class in the stack.

        :param skip: Skip the last n nodes in the stack.
        """

        if len(self.stack) > skip:
            return self.stack[-1 - skip]
        else:
            return None

    def _is_context_of_type(self, node_type: Union[Type[libcst.CSTNode]]) -> bool:
        """
        Check if the current context is of the given type.

        :param node_type: The type to check for.
        :return: True if the current context is of the given type, False otherwise.
        """
        for node in self.stack:
            if isinstance(node, node_type):
                return True

    def _apply_name_style(self, node: Union[libcst.FunctionDef, libcst.ClassDef], name_style_config: NameStyleConfig) -> \
            Union[libcst.FunctionDef, libcst.ClassDef]:
        if node.name.value in name_style_config.keep_names:
            return node

        if type(name_style_config.name_style) is tuple:
            if isinstance(name_style_config.name_style[0], libcst.NameStyle):
                sub_name_idx = 0
                name_idx = 1
            else:
                sub_name_idx = 1
                name_idx = 0

            sub_name_class = name_style_config.name_style[sub_name_idx].get_node_class()
            sub_name = sub_name_class(value=sub_name_class.default_value)
            name_class = name_style_config.name_style[name_idx].get_node_class()
            name = name_class(name=sub_name, lpar=node.name.lpar, rpar=node.name.rpar, )
        else:
            name_class = name_style_config.name_style.get_node_class()
            name = name_class(value=name_class.default_value, lpar=node.name.lpar, rpar=node.name.rpar, )

        if isinstance(node, libcst.FunctionDef):
            node = node.with_changes(
                whitespace_after_def=libcst.SimpleWhitespace(" " + name_style_config.whitespace_before))
        else:
            node = node.with_changes(
                whitespace_after_class=libcst.SimpleWhitespace(" " + name_style_config.whitespace_before))

        return node.with_changes(name=name,
                                 whitespace_after_name=libcst.SimpleWhitespace(name_style_config.whitespace_after))

    def _get_newline_class(self):
        if self.codestyle.line_ending == "\n":
            return libcst.UnixNewline
        elif self.codestyle.line_ending == "\r\n":
            return libcst.WindowsNewline
        elif self.codestyle.line_ending == "\r":
            return libcst.MacNewline
        else:
            raise ValueError(f"Unknown line ending: {self.codestyle.line_ending}")


class CodestyleTransformError(Exception):
    """
    Exception raised when a codestyle transform fails.
    """


def apply_codestyle(code: str, codestyle: CodestyleConfig) -> str:
    """
    Apply the given codestyle to the given code.

    :param code: The code to apply the codestyle to.
    :param codestyle: The codestyle to apply.
    :return: The code with the codestyle applied.
    """

    tree = libcst.parse_module(code)

    try:
        context = CodemodContext()
        codestyle_transformer = CodestyleTransformer(context, codestyle)
        codestyle_tree = codestyle_transformer.transform_module(tree)

        state = CodegenState(default_indent=codestyle.indent_char * codestyle.indent_size,
                             default_newline=codestyle.line_ending, trailing_newline=codestyle.blank_lines_bottom > 0, )
        codestyle_tree._codegen(state)
    except Exception as e:
        raise CodestyleTransformError(e)

    return "".join(state.tokens)
