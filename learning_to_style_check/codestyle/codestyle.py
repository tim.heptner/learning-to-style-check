import dataclasses
import itertools
import json
import random
import textwrap
from dataclasses import dataclass
from enum import Enum
from typing import Optional, Union, Tuple, Any

from learning_to_style_check import libcst

F_STRING_QUOTES = [
    ("'", '"'),
    ('"', "'"),
    ("'''", '"'),
    ("'''", "'"),
    ('"""', '"'),
    ('"""', "'"),
]

SAMPLE_CODE = textwrap.dedent('''
"""
This is a module docstring
"""

import os
import sys

TEST_VAR = 42
var2 = 'test string'
TEST_VAR2 = f"test {TEST_VAR} {' ' * 2}"
TEST_VAR2 = rb"test ' test"

test("""test string""")
call(f"test {TEST_VAR} {' ' * 2}")


class Foo:
    def __init__(self, a, b):
        self.a = a
        self.b = b

    def bar(self, c, d):
        """
        This is a method docstring
        """
        return self.a + self.b + c + d


def test(e, f):
    return e + f


def test2(g):
    """
    This is a function docstring
    """
    m = 42
    n = "test string"
    return g + "test string"
''')

DEFAULT_KEEP_NAMES = (
    "self",
    "cls",

    "__new__",
    "__init__",
    "__del__",
    "__repr__",
    "__str__",
    "__bytes__",
    "__format__",
    "__lt__",
    "__le__",
    "__eq__",
    "__ne__",
    "__gt__",
    "__ge__",
    "__hash__",
    "__bool__",

    "__getattr__",
    "__getattribute__",
    "__setattr__",
    "__delattr__",
    "__dir__",

    "__get__",
    "__set__",
    "__delete__",

    "__init_subclass__",
    "__set_name__",

    "__instancecheck__",
    "__subclasscheck__",

    "__call__",

    "__len__",
    "__length_hint__",
    "__getitem__",
    "__setitem__",
    "__delitem__",
    "__missing__",
    "__iter__",
    "__reversed__",
    "__contains__",

    "__add__",
    "__sub__",
    "__mul__",
    "__matmul__",
    "__truediv__",
    "__floordiv__",
    "__mod__",
    "__divmod__",
    "__pow__",
    "__lshift__",
    "__rshift__",
    "__and__",
    "__xor__",
    "__or__",
    "__radd__",
    "__rsub__",
    "__rmul__",
    "__rmatmul__",
    "__rtruediv__",
    "__rfloordiv__",
    "__rmod__",
    "__rdivmod__",
    "__rpow__",
    "__rlshift__",
    "__rrshift__",
    "__rand__",
    "__rxor__",
    "__ror__",
    "__iadd__",
    "__isub__",
    "__imul__",
    "__imatmul__",
    "__itruediv__",
    "__ifloordiv__",
    "__imod__",
    "__ipow__",
    "__ilshift__",
    "__irshift__",
    "__iand__",
    "__ixor__",
    "__ior__",
    "__neg__",
    "__pos__",
    "__abs__",
    "__invert__",
    "__complex__",
    "__int__",
    "__float__",
    "__index__",
    "__round__",
    "__trunc__",
    "__floor__",
    "__ceil__",

    "__enter__",
    "__exit__",
    "__await__",
    "__aiter__",
    "__anext__",
    "__aenter__",
    "__aexit__",
    "__copy__",
    "__deepcopy__",
    "__getstate__",
    "__setstate__",
    "__reduce__",

    "__match_args__",

    "__class__",

    "__doc__",
    "__name__",
    "__qualname__",
    "__module__",
    "__defaults__",
    "__code__",
    "__globals__",
    "__dict__",
    "__closure__",
    "__annotations__",
    "__kwdefaults__",
)

TYPINGS = ["int", "str", "Any", "Optional[int]", "Optional[Any]", "Union[str, Any]", "List[Any]",
           "List[str]", "Tuple", "Dict"]


class StringPrefixStyle(str, Enum):
    LOW = "LOW"
    UP = "UP"


@dataclass(frozen=True, eq=True)
class StringStyleConfig:
    quotes: str = '"'
    f_string_quotes: Tuple[str, str] = ('"', "'")  # (outer quotes, inner quotes)
    f_prefix: StringPrefixStyle = StringPrefixStyle.LOW
    b_prefix: StringPrefixStyle = StringPrefixStyle.LOW
    r_prefix: StringPrefixStyle = StringPrefixStyle.LOW
    u_prefix: StringPrefixStyle = StringPrefixStyle.LOW


@dataclass(frozen=True, eq=True)
class DocStringStyleConfig:
    string_style_config: StringStyleConfig
    blank_lines_before: int = 0
    blank_lines_after: int = 0


@dataclass(frozen=True, eq=True)
class NameStyleConfig:
    name_style: Union[
        "NameStyle",
        Tuple["NameStylePrefix", "NameStyle"],
        Tuple["NameStyle", "NameStyleSuffix"]
    ]

    whitespace_before: str = ""
    whitespace_after: str = ""

    # default names are general var names (like self or cls) and magic names (like __init__ or __new__):
    # https://docs.python.org/3/reference/datamodel.html
    keep_names: Tuple[str, ...] = DEFAULT_KEEP_NAMES


class NumberStyle(str, Enum):
    LOW = "LOW"
    UP = "UP"


@dataclass(frozen=True, eq=True)
class NumberStyleConfig:
    int_underscore_every: int = 0

    float_underscore_every: int = 0

    exp_style: Optional[NumberStyle] = None
    exp_underscore_every: int = 0

    hex_style: Optional[NumberStyle] = None
    hex_underscore_every: int = 0
    hex_letter_case: Optional[NumberStyle] = None

    oct_style: Optional[NumberStyle] = None
    oct_underscore_every: int = 0

    bin_style: Optional[NumberStyle] = None
    bin_underscore_every: int = 0

    imag_style: Optional[NumberStyle] = None
    imag_underscore_every: int = 0


@dataclass(frozen=True, eq=True)
class TypingStyleConfig:
    whitespace_before_indicator: str = ""
    whitespace_after_indicator: str = ""


@dataclass(frozen=True, eq=True)
class ArgsStyleConfig:
    name_style_config: NameStyleConfig = None
    typing_style_config: Optional[TypingStyleConfig] = None
    whitespace_before_args: str = ""
    whitespace_after_args: str = ""


@dataclass(frozen=True, eq=True)
class VarStyleConfig:
    name_style_config: Optional[NameStyleConfig] = None
    typing_style_config: Optional[TypingStyleConfig] = None
    string_style_config: Optional[StringStyleConfig] = None
    number_style_config: Optional[NumberStyleConfig] = None

    whitespace_before_equal: str = ""
    whitespace_after_equal: str = ""


@dataclass(frozen=True, eq=True)
class FunctionStyleConfig:
    name_style_config: Optional[NameStyleConfig] = None
    args_style_config: Optional[ArgsStyleConfig] = None

    var_style_config: Optional[VarStyleConfig] = None

    typing_style_config: Optional[TypingStyleConfig] = None

    docstring_style_config: Optional[DocStringStyleConfig] = None

    indent_size: int = 4

    blank_lines_before: int = 1
    blank_lines_after: int = 1


@dataclass(frozen=True, eq=True)
class ClassStyleConfig:
    name_style_config: Optional[NameStyleConfig] = None
    args_style_config: Optional[ArgsStyleConfig] = None

    method_style_config: Optional[FunctionStyleConfig] = None
    var_style_config: Optional[VarStyleConfig] = None

    docstring_style_config: Optional[DocStringStyleConfig] = None

    indent_size: int = 4

    blank_lines_before: int = 2
    blank_lines_after: int = 2


@dataclass(frozen=True, eq=True)
class CodestyleConfig:
    function_style_config: Optional[FunctionStyleConfig] = None
    class_style_config: Optional[ClassStyleConfig] = None
    var_style_config: Optional[VarStyleConfig] = None

    docstring_style_config: Optional[DocStringStyleConfig] = None

    # global indent char, can not be overriden
    indent_char: str = " "  # space
    # default indent size, can be overriden by class_style and function_style
    indent_size: int = 4

    # module level blank lines take precedence over class and function level blank lines
    blank_lines_top: int = 0
    blank_lines_bottom: int = 0

    line_ending: str = "\n"


UNDERSCORE_NUMBER_EVERY_RANGE = list(range(0, 5))
WHITESPACE_RANGE = range(0, 8)
BLANKLINES_RANGE = list(range(0, 8))
INDENT_RANGE = list(range(1, 8))

ALLOWED_VALUES = {
    "line_ending": ["\n", "\r\n", "\r"],
    "blank_lines_top": BLANKLINES_RANGE,
    "blank_lines_bottom": BLANKLINES_RANGE,
    "indent_char": [" ", "\t"],
    "indent_size": INDENT_RANGE,
    "blank_lines_before": BLANKLINES_RANGE,
    "blank_lines_after": BLANKLINES_RANGE,
    "whitespace_before": list(set([" " * i for i in WHITESPACE_RANGE] + ["\t" * i for i in WHITESPACE_RANGE])),
    "whitespace_after": list(set([" " * i for i in WHITESPACE_RANGE] + ["\t" * i for i in WHITESPACE_RANGE])),
    "whitespace_before_equal": list(set([" " * i for i in WHITESPACE_RANGE] + ["\t" * i for i in WHITESPACE_RANGE])),
    "whitespace_after_equal": list(set([" " * i for i in WHITESPACE_RANGE] + ["\t" * i for i in WHITESPACE_RANGE])),
    "whitespace_before_args": list(set([" " * i for i in WHITESPACE_RANGE] + ["\t" * i for i in WHITESPACE_RANGE])),
    "whitespace_after_args": list(set([" " * i for i in WHITESPACE_RANGE] + ["\t" * i for i in WHITESPACE_RANGE])),
    "whitespace_before_indicator": list(set([" " * i for i in WHITESPACE_RANGE]
                                            + ["\t" * i for i in WHITESPACE_RANGE])),
    "whitespace_after_indicator": list(set([" " * i for i in WHITESPACE_RANGE] + ["\t" * i for i in WHITESPACE_RANGE])),
    "int_underscore_every": UNDERSCORE_NUMBER_EVERY_RANGE,
    "float_underscore_every": UNDERSCORE_NUMBER_EVERY_RANGE,
    "exp_style": list(NumberStyle),
    "exp_underscore_every": UNDERSCORE_NUMBER_EVERY_RANGE,
    "hex_style": list(NumberStyle),
    "hex_underscore_every": UNDERSCORE_NUMBER_EVERY_RANGE,
    "hex_letter_case": list(NumberStyle),
    "oct_style": list(NumberStyle),
    "oct_underscore_every": UNDERSCORE_NUMBER_EVERY_RANGE,
    "bin_style": list(NumberStyle),
    "bin_underscore_every": UNDERSCORE_NUMBER_EVERY_RANGE,
    "imag_style": list(NumberStyle),
    "imag_underscore_every": UNDERSCORE_NUMBER_EVERY_RANGE,
    "quotes": ['"', "'", '"""', "'''"],
    "docstring_quotes": ['"""', "'''"],
    "f_string_quotes": F_STRING_QUOTES,
    "f_prefix": list(StringPrefixStyle),
    "b_prefix": list(StringPrefixStyle),
    "r_prefix": list(StringPrefixStyle),
    "u_prefix": list(StringPrefixStyle),
    "name_style": [
        *list(libcst.NameStyle),
        *list(itertools.product(list(libcst.NameStylePrefix),
                                filter(lambda x: x != libcst.NameStyle.MagicName, list(libcst.NameStyle)))),
        *list(itertools.product(filter(lambda x: x != libcst.NameStyle.MagicName, list(libcst.NameStyle)),
                                list(libcst.NameStyleSuffix)))
    ]
}


def get_random_name_style(seed: Optional[int] = None):
    """
    Generate a random name style config
    """
    if seed is not None:
        random.seed(seed)

    return NameStyleConfig(
        whitespace_before=random.choice(ALLOWED_VALUES["whitespace_before"]),
        whitespace_after=random.choice(ALLOWED_VALUES["whitespace_after"]),

        name_style=random.choice(ALLOWED_VALUES["name_style"])
    )


def get_random_string_style(seed: Optional[int] = None):
    """
    Generate a random string style config
    """
    if seed is not None:
        random.seed(seed)

    return StringStyleConfig(
        quotes=random.choice(ALLOWED_VALUES["quotes"]),
        f_string_quotes=random.choice(ALLOWED_VALUES["f_string_quotes"]),
        f_prefix=random.choice(ALLOWED_VALUES["f_prefix"]),
        b_prefix=random.choice(ALLOWED_VALUES["b_prefix"]),
        r_prefix=random.choice(ALLOWED_VALUES["r_prefix"]),
        u_prefix=random.choice(ALLOWED_VALUES["u_prefix"])
    )


def get_random_number_style(seed: Optional[int] = None):
    """
    Generate a random number style config
    """
    if seed is not None:
        random.seed(seed)

    return NumberStyleConfig(
        int_underscore_every=random.choice(ALLOWED_VALUES["int_underscore_every"]),
        float_underscore_every=random.choice(ALLOWED_VALUES["float_underscore_every"]),
        exp_style=random.choice(ALLOWED_VALUES["exp_style"]),
        exp_underscore_every=random.choice(ALLOWED_VALUES["exp_underscore_every"]),
        hex_style=random.choice(ALLOWED_VALUES["hex_style"]),
        hex_underscore_every=random.choice(ALLOWED_VALUES["hex_underscore_every"]),
        hex_letter_case=random.choice(ALLOWED_VALUES["hex_letter_case"]),
        oct_style=random.choice(ALLOWED_VALUES["oct_style"]),
        oct_underscore_every=random.choice(ALLOWED_VALUES["oct_underscore_every"]),
        bin_style=random.choice(ALLOWED_VALUES["bin_style"]),
        bin_underscore_every=random.choice(ALLOWED_VALUES["bin_underscore_every"]),
        imag_style=random.choice(ALLOWED_VALUES["imag_style"]),
        imag_underscore_every=random.choice(ALLOWED_VALUES["imag_underscore_every"]),
    )


def get_random_docstring_style(seed: Optional[int] = None):
    """
    Generate a random docstring style config
    """
    if seed is not None:
        random.seed(seed)

    return DocStringStyleConfig(
        blank_lines_before=random.choice(ALLOWED_VALUES["blank_lines_before"]),
        blank_lines_after=random.choice(ALLOWED_VALUES["blank_lines_after"]),
        string_style_config=StringStyleConfig(
            quotes=random.choice(ALLOWED_VALUES["docstring_quotes"]),
        )
    )


def get_random_var_style(seed: Optional[int] = None):
    """
    Generate a random var style config
    """
    if seed is not None:
        random.seed(seed)

    return VarStyleConfig(
        name_style_config=get_random_name_style(seed),

        string_style_config=get_random_string_style(seed),

        number_style_config=get_random_number_style(seed),

        typing_style_config=random.choice([None, get_typing_style_config(seed)]),

        whitespace_before_equal=random.choice(ALLOWED_VALUES["whitespace_before_equal"]),
        whitespace_after_equal=random.choice(ALLOWED_VALUES["whitespace_after_equal"]),
    )


def get_typing_style_config(seed: Optional[int] = None):
    """
    Generate a random typing style config
    """
    if seed is not None:
        random.seed(seed)

    return TypingStyleConfig(
        whitespace_before_indicator=random.choice(ALLOWED_VALUES["whitespace_before_indicator"]),
        whitespace_after_indicator=random.choice(ALLOWED_VALUES["whitespace_after_indicator"])
    )


def get_random_arg_style(seed: Optional[int] = None):
    """
    Generate a random arg style config
    """
    if seed is not None:
        random.seed(seed)

    return ArgsStyleConfig(
        whitespace_before_args=random.choice(ALLOWED_VALUES["whitespace_before_args"]),
        whitespace_after_args=random.choice(ALLOWED_VALUES["whitespace_after_args"]),

        name_style_config=get_random_name_style(seed),

        typing_style_config=random.choice([None, get_typing_style_config()])
    )


def get_random_function_style(seed: Optional[int] = None):
    """
    Generate a random function style config
    """
    if seed is not None:
        random.seed(seed)

    return FunctionStyleConfig(
        blank_lines_before=random.choice(ALLOWED_VALUES["blank_lines_before"]),
        blank_lines_after=random.choice(ALLOWED_VALUES["blank_lines_after"]),
        indent_size=random.choice(ALLOWED_VALUES["indent_size"]),

        name_style_config=get_random_name_style(seed),

        args_style_config=get_random_arg_style(seed),

        typing_style_config=random.choice([None, get_typing_style_config()]),

        docstring_style_config=random.choice([None, get_random_docstring_style(seed)]),

        var_style_config=get_random_var_style(seed)
    )


def get_random_class_style(seed: Optional[int] = None):
    """
    Generate a random class style config
    """
    if seed is not None:
        random.seed(seed)

    return ClassStyleConfig(
        blank_lines_before=random.choice(ALLOWED_VALUES["blank_lines_before"]),
        blank_lines_after=random.choice(ALLOWED_VALUES["blank_lines_after"]),
        indent_size=random.choice(ALLOWED_VALUES["indent_size"]),

        name_style_config=get_random_name_style(seed),

        args_style_config=get_random_arg_style(seed),

        docstring_style_config=random.choice([None, get_random_docstring_style(seed)]),

        var_style_config=get_random_var_style(seed),

        method_style_config=get_random_function_style(seed)
    )


def get_random_codestyle(seed: Optional[int] = None):
    """
    Generate a random codestyle config
    """
    if seed is not None:
        random.seed(seed)

    return CodestyleConfig(
        line_ending=random.choice(ALLOWED_VALUES["line_ending"]),

        blank_lines_top=random.choice(ALLOWED_VALUES["blank_lines_top"]),
        blank_lines_bottom=random.choice(ALLOWED_VALUES["blank_lines_bottom"]),

        indent_size=random.choice(ALLOWED_VALUES["indent_size"]),
        indent_char=random.choice(ALLOWED_VALUES["indent_char"]),

        docstring_style_config=random.choice([None, get_random_docstring_style(seed)]),

        var_style_config=get_random_var_style(seed),

        function_style_config=get_random_function_style(seed),

        class_style_config=get_random_class_style(seed),
    )


ALLOWED_CONFIG_VALUES = {
    "function_style_config": [get_random_function_style()],
    "class_style_config": [get_random_class_style()],
    "var_style_config": [get_random_var_style()],
    "docstring_style_config": [None, get_random_docstring_style()],
    "name_style_config": [get_random_name_style()],
    "number_style_config": [get_random_number_style()],
    "args_style_config": [get_random_arg_style()],
    "method_style_config": [get_random_function_style()],
    "string_style_config": [get_random_string_style()],
    "typing_style_config": [None, get_typing_style_config()],
}


def mutate_codestyle(codestyle: CodestyleConfig, n_rules: int = 1) -> CodestyleConfig:
    """
    Mutate n rules of the codestyle config.

    :param codestyle: codestyle config to mutate
    :param n_rules: number of rules to mutate
    :return: mutated codestyle config
    """

    if n_rules == 0:
        return codestyle

    new_codestyle = codestyle
    for i in range(n_rules):
        attribute_names = list(new_codestyle.__dataclass_fields__.keys())
        if "keep_names" in attribute_names:
            attribute_names.remove("keep_names")

        rule = random.choice(attribute_names)
        current_value = getattr(new_codestyle, rule)

        if dataclasses.is_dataclass(current_value) and rule in ALLOWED_CONFIG_VALUES:
            if None in ALLOWED_CONFIG_VALUES[rule]:
                new_codestyle = dataclasses.replace(new_codestyle, **{rule: None})
            else:
                # mutate the nested dataclass
                new_codestyle = dataclasses.replace(new_codestyle, **{rule: mutate_codestyle(current_value, n_rules=1)})
        elif rule in ALLOWED_CONFIG_VALUES and current_value is None:
            allowed_values = ALLOWED_CONFIG_VALUES[rule]

            current_value_index = allowed_values.index(current_value)
            new_value = random.choice(allowed_values[:current_value_index] + allowed_values[current_value_index + 1:])

            new_codestyle = dataclasses.replace(new_codestyle, **{rule: new_value})
        else:
            allowed_values = ALLOWED_VALUES[rule]

            current_value_index = allowed_values.index(current_value)
            new_value = random.choice(allowed_values[:current_value_index] + allowed_values[current_value_index + 1:])

            new_codestyle = dataclasses.replace(new_codestyle, **{rule: new_value})
    return new_codestyle


class CodestyleConfigJSONEncoder(json.JSONEncoder):
    def default(self, o: Any) -> Any:
        if dataclasses.is_dataclass(o):
            return dataclasses.asdict(o)
        else:
            return super().default(o)


class CodestyleConfigJSONDecoder(json.JSONDecoder):
    def __init__(self, *args, **kwargs):
        super().__init__(object_hook=self.object_hook, *args, **kwargs)

    def object_hook(self, o: Any) -> Any:
        if "function_style_config" in o:
            o["function_style_config"] = FunctionStyleConfig(**o["function_style_config"])
        if "class_style_config" in o:
            o["class_style_config"] = ClassStyleConfig(**o["class_style_config"])
        if "var_style_config" in o:
            o["var_style_config"] = VarStyleConfig(**o["var_style_config"])
        if "docstring_style_config" in o and o["docstring_style_config"] is not None:
            o["docstring_style_config"] = DocStringStyleConfig(**o["docstring_style_config"])
        if "name_style_config" in o:
            o["name_style_config"] = NameStyleConfig(**o["name_style_config"])
        if "number_style_config" in o:
            o["number_style_config"] = NumberStyleConfig(**o["number_style_config"])
        if "args_style_config" in o:
            o["args_style_config"] = ArgsStyleConfig(**o["args_style_config"])
        if "method_style_config" in o:
            o["method_style_config"] = FunctionStyleConfig(**o["method_style_config"])
        if "string_style_config" in o:
            o["string_style_config"] = StringStyleConfig(**o["string_style_config"])
        if "typing_style_config" in o and o["typing_style_config"] is not None:
            o["typing_style_config"] = TypingStyleConfig(**o["typing_style_config"])

        if "name_style" in o:
            if type(o["name_style"]) == str:
                o["name_style"] = libcst.NameStyle(o["name_style"])
            elif "Underscore" in o["name_style"][0]:
                o["name_style"] = (libcst.NameStylePrefix(o["name_style"][0]), libcst.NameStyle(o["name_style"][1]))
            else:
                o["name_style"] = (libcst.NameStyle(o["name_style"][0]), libcst.NameStyleSuffix(o["name_style"][1]))

        if "keep_names" in o:
            o["keep_names"] = tuple(o["keep_names"])

        if "f_string_quotes" in o:
            o["f_string_quotes"] = tuple(o["f_string_quotes"])
        if "f_prefix" in o:
            o["f_prefix"] = StringPrefixStyle(o["f_prefix"])
        if "b_prefix" in o:
            o["b_prefix"] = StringPrefixStyle(o["b_prefix"])
        if "r_prefix" in o:
            o["r_prefix"] = StringPrefixStyle(o["r_prefix"])
        if "u_prefix" in o:
            o["u_prefix"] = StringPrefixStyle(o["u_prefix"])

        if "exp_style" in o:
            o["exp_style"] = NumberStyle(o["exp_style"])
        if "hex_style" in o:
            o["hex_style"] = NumberStyle(o["hex_style"])
        if "hex_letter_case" in o:
            o["hex_letter_case"] = NumberStyle(o["hex_letter_case"])
        if "oct_style" in o:
            o["oct_style"] = NumberStyle(o["oct_style"])
        if "bin_style" in o:
            o["bin_style"] = NumberStyle(o["bin_style"])
        if "imag_style" in o:
            o["imag_style"] = NumberStyle(o["imag_style"])

        if "line_ending" in o:
            return CodestyleConfig(**o)
        return o
